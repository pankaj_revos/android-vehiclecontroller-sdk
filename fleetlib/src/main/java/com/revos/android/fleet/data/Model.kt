package com.revos.android.fleet.data

import android.os.Parcelable
import com.revos.scripts.type.ModelProtocol
import com.revos.scripts.type.ModelAccessType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Model (
    var id: String? = null,
    var name: String? = null,
    var protocol: ModelProtocol? = null,
    var modelAccessType: ModelAccessType? = null,
    var speedDivisor : Int? = 0,
    var odoDivisor : Int? = 0,
    var maxSpeed : Int? = 0,
    var speedLimit : Int? = 0,
    var pickupLimit : Int? = 0,
    var brakeRegenLimit : Int? = 0,
    var zeroThrottleRegenLimit : Int? = 0,
    var currentLimit : Int? = 0,
    var overVoltageLimit : Int? = 0,
    var underVoltageLimit : Int? = 0,
    var wheelDiameter : Int? = 0,
    var batteryMaxVoltageLimit : Double = 0.0,
    var batteryMinVoltageLimit : Double = 0.0,
    var isHillAssist : Boolean? = false,
    var isParking : Boolean? = false,
    var isRegenBraking : Boolean? = false,
    var eAbs : Boolean? = false,
) : Parcelable