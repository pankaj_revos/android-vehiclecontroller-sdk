package com.revos.android.fleet.data

data class DataFeedVehicleSnapshot (
    var vin: String? = null,
    var ignition: DataFeedIgnition? = null,
    var battery: DataFeedBattery? = null,
    var uart: DataFeedUart? = null,
    var location: DataFeedSnapshotLocation? = null,
    var alarm: DataFeedAlarm? = null,
)