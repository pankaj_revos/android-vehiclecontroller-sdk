package com.revos.android.fleet.utils.vehicleDataTransfer;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.revos.android.fleet.ble.BleUtils;

import org.apache.commons.lang3.StringUtils;

public class VehicleControlHelper {

    private final String KEY = "key";
    private final String INPUT_FORMAT = "inputFormat";

    //json keys
    private final String VALUE_KEY = "value";
    private final String PARAMETERS_KEY = "parameters";
    private final String LENGTH_KEY = "length";
    private final String ID_KEY = "id";

    private final String VEHICLE_LOCK_ON_KEY = "vehicleLock";
    private final String VEHICLE_LOCK_OFF_KEY = "vehicleUnlock";

    private final String HEADLAMP_ON_KEY = "headLampOn";
    private final String HEADLAMP_OFF_KEY = "headLampOff";

    private final String RIGHT_INDICATOR_ON_KEY = "rightIndicatorOn";
    private final String RIGHT_INDICATOR_OFF_KEY = "rightIndicatorOff";

    private final String LEFT_INDICATOR_ON_KEY = "leftIndicatorOn";
    private final String LEFT_INDICATOR_OFF_KEY = "leftIndicatorOff";

    private final String HORN_ON_KEY = "hornOn";
    private final String HORN_OFF_KEY = "hornOff";

    private final String MOTOR_POWER_ON_KEY = "motorPowerOn";
    private final String MOTOR_POWER_OFF_KEY = "motorPowerOff";

    private final String HANDLE_LOCK_ON_KEY = "handleLockOn";
    private final String HANDLE_LOCK_OFF_KEY = "handleLockOff";

    private final String COMBINE_LOCK_ON_KEY = "combineLockOn";
    private final String COMBINE_LOCK_OFF_KEY = "combineLockOff";

    private final String SEAT_UNLOCK_KEY = "seatUnlock";

    private final String START_LOG_SYNC_KEY = "startLogSync";

    private final String ANTI_THEFT_OFF_KEY = "antiTheftOff";
    private final String ANTI_THEFT_SILENT_KEY = "antiTheftSilent";
    private final String ANTI_THEFT_NORMAL_KEY = "antiTheftNormal";
    private final String ANTI_THEFT_SENSITIVITY_KEY = "antiTheftSensitivityControl";

    private final String FIND_MY_VEHICLE_KEY = "findMyVehicle";

    private final String FOLLOW_ME_HEADLAMP_CONTROL_KEY = "followMeHeadlampControl";

    private final String GENERIC_WHEEL_LOCK_KEY = "genericWheelLock";

    private final String GEO_FENCING_WHEEL_LOCK_KEY = "geofencingWheelLock";

    private final String VEHICLE_RANGE_UPDATE_KEY = "vehicleRangeUpdate";

    private SpeedometerDataExchanger mSpeedometerDataExchanger;


    public VehicleControlHelper (Context context) {
        mSpeedometerDataExchanger = SpeedometerDataExchanger.getInstance(context);
    }


    public byte getVehicleLockControlByte(boolean on) {
        String paramId = VEHICLE_LOCK_ON_KEY;
        if(!on) {
            paramId = VEHICLE_LOCK_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getHeadLampControlByte(boolean on) {
        String paramId = HEADLAMP_ON_KEY;
        if(!on) {
            paramId = HEADLAMP_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getRightIndicatorControlByte(boolean on) {
        String paramId = RIGHT_INDICATOR_ON_KEY;
        if(!on) {
            paramId = RIGHT_INDICATOR_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getLeftIndicatorControlByte(boolean on) {
        String paramId = LEFT_INDICATOR_ON_KEY;
        if(!on) {
            paramId = LEFT_INDICATOR_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getHornControlByte(boolean on) {
        String paramId = HORN_ON_KEY;
        if(!on) {
            paramId = HORN_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getMotorPowerControlByte(boolean on) {
        String paramId = MOTOR_POWER_ON_KEY;
        if(!on) {
            paramId = MOTOR_POWER_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getHandleControlByte(boolean on) {
        String paramId = HANDLE_LOCK_ON_KEY;
        if(!on) {
            paramId = HANDLE_LOCK_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getCombineLockControlByte(boolean on) {
        String paramId = COMBINE_LOCK_ON_KEY;
        if(!on) {
            paramId = COMBINE_LOCK_OFF_KEY;
        }
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getSeatUnlockByte() {
        String paramId = SEAT_UNLOCK_KEY;
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getStartLogSyncByte() {
        String paramId = START_LOG_SYNC_KEY;
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getAntiTheftOffByte() {
        String paramId = ANTI_THEFT_OFF_KEY;
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getAntiTheftSilentByte() {
        String paramId = ANTI_THEFT_SILENT_KEY;
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getAntiTheftNormalByte() {
        String paramId = ANTI_THEFT_NORMAL_KEY;
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte getFindMyVehicleByte() {
        String paramId = FIND_MY_VEHICLE_KEY;
        if(mSpeedometerDataExchanger == null){
            return (byte)0xFF;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return (byte)0xFF;
        }
        return BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
    }

    public byte[] getFollowMeHeadlampControlByteArray(int numberOfSeconds) {
        byte[] commandByteArray = new byte[1];
        commandByteArray[0] = (byte)0xFF;
        String paramId = FOLLOW_ME_HEADLAMP_CONTROL_KEY;

        if(mSpeedometerDataExchanger == null){
            return commandByteArray;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return commandByteArray;
        }

        if(metadataJsonObject.has(LENGTH_KEY) && metadataJsonObject.has(PARAMETERS_KEY)) {
            commandByteArray = constructMultiByteInputArray(metadataJsonObject, numberOfSeconds);
        }

        return commandByteArray;
    }

    public byte[] getAntiTheftSensitivityByteArray(int sensitivityLevel) {
        byte[] commandByteArray = new byte[1];
        commandByteArray[0] = (byte)0xFF;
        String paramId = ANTI_THEFT_SENSITIVITY_KEY;
        if(mSpeedometerDataExchanger == null){
            return commandByteArray;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return commandByteArray;
        }

        if(metadataJsonObject.has(LENGTH_KEY) && metadataJsonObject.has(PARAMETERS_KEY)) {
            commandByteArray = constructMultiByteInputArray(metadataJsonObject, sensitivityLevel);
        }

        return commandByteArray;
    }

    public byte[] getGenericWheelLockControlByteArray(boolean lockWheel) {
        byte[] commandByteArray = new byte[1];
        commandByteArray[0] = (byte)0xFF;
        String paramId = GENERIC_WHEEL_LOCK_KEY;
        if(mSpeedometerDataExchanger == null){
            return commandByteArray;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return commandByteArray;
        }

        if(metadataJsonObject.has(LENGTH_KEY) && metadataJsonObject.has(PARAMETERS_KEY)) {
            commandByteArray = constructMultiByteInputArray(metadataJsonObject, lockWheel ? 1 : 0);
        }

        return commandByteArray;
    }

    public byte[] getGeoFencingWheelLockControlByteArray(boolean setLock) {
        byte[] commandByteArray = new byte[1];
        commandByteArray[0] = (byte)0xFF;
        String paramId = GEO_FENCING_WHEEL_LOCK_KEY;
        if(mSpeedometerDataExchanger == null){
            return commandByteArray;
        }
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return commandByteArray;
        }

        if(metadataJsonObject.has(LENGTH_KEY) && metadataJsonObject.has(PARAMETERS_KEY)) {
            commandByteArray = constructMultiByteInputArray(metadataJsonObject, setLock ? 1 : 0);
        }

        return commandByteArray;
    }


    public byte[] getUpdateVehicleRangeUpdateByteArray(int range) {
        byte[] commandByteArray = new byte[1];
        commandByteArray[0] = (byte)0xFF;
        String paramId = VEHICLE_RANGE_UPDATE_KEY;
        JsonObject metadataJsonObject = mSpeedometerDataExchanger.getVehicleControlMetadata(paramId);
        if(metadataJsonObject == null) {
            return commandByteArray;
        }

        if(metadataJsonObject.has(LENGTH_KEY) && metadataJsonObject.has(PARAMETERS_KEY)) {
            commandByteArray = constructMultiByteInputArray(metadataJsonObject, range);
        }

        return commandByteArray;
    }

    private byte[] constructMultiByteInputArray(JsonObject metadataJsonObject, int arg) {
        byte[] commandByteArray = new byte[metadataJsonObject.get(LENGTH_KEY).getAsInt()];

        JsonArray parametersJsonArray = metadataJsonObject.getAsJsonArray(PARAMETERS_KEY);

        for(JsonElement jsonElement : parametersJsonArray) {
            JsonObject parameterJsonObject = jsonElement.getAsJsonObject();

            //insert the key value as first byte
            if(parameterJsonObject.get(ID_KEY).getAsString().equals(KEY)) {
                if(parameterJsonObject.has(VALUE_KEY)) {
                    commandByteArray[0] = BleUtils.hexStringWithZeroXToByte(parameterJsonObject.get(VALUE_KEY).getAsString());
                }
            }

            //insert parameter value as byte in array
            if(parameterJsonObject.get(ID_KEY).getAsString().equals(INPUT_FORMAT)) {
                String hexStr = Integer.toHexString(arg);
                int paddedHexStrSize = parameterJsonObject.get(LENGTH_KEY).getAsInt() * 2;
                String paddedHexStr = StringUtils.leftPad(hexStr, paddedHexStrSize, '0');

                byte[] inputByteArray = BleUtils.hexStringToByteArray(paddedHexStr);

                System.arraycopy(inputByteArray, 0, commandByteArray, 1, inputByteArray.length);
            }
        }
        return commandByteArray;
    }

}
