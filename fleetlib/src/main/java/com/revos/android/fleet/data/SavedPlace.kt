package com.revos.android.fleet.data

data class SavedPlace (
    var latitude : Double = 0.0,
    var longitude : Double = 0.0,
    var name: String? = null,
    var address: String? = null,
)