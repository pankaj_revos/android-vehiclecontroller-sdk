package com.revos.android.fleet.data

import com.revos.scripts.FetchAllKYCAssociationsQuery
import com.revos.scripts.type.KYCStatus

data class KycAssociation (
    var rentalProviderName: String? = null,
    var rentalProviderId: String? = null,
    var kycAssociationStatus: KYCStatus? = null,
    var kycAssociationId: String? = null,
    var documentList: List<FetchAllKYCAssociationsQuery.DocumentStatus>? = null,
)