package com.revos.android.fleet.data

import java.util.ArrayList

data class Contact (
    var name: String? = null,
    var isImageSynced : Boolean = false,
    var imagePath : String = "",
    var id : Int = 0,
    var phoneNumbers: ArrayList<String>? = null
)