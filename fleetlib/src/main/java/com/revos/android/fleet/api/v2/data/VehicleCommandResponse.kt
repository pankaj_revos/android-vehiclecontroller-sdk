package com.revos.android.fleet.api.v2.data


import com.google.gson.annotations.SerializedName

internal data class VehicleCommandResponse(
    @SerializedName("data")
    val data: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)