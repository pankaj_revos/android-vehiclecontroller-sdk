package com.revos.android.fleet.services;

import static com.revos.android.fleet.utils.ConstantsKt.BT_EVENT_REQUEST_CLEAR_IMAGE;
import static com.revos.android.fleet.utils.ConstantsKt.BT_EVENT_REQUEST_SEND_CALL_BITMAP;
import static com.revos.android.fleet.utils.ConstantsKt.CALL_LOG_HASHMAP_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.GENERAL_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.GEN_DELIMITER;
import static com.revos.android.fleet.utils.ConstantsKt.GOOGLE_MAPS_PACKAGE_NAME;
import static com.revos.android.fleet.utils.ConstantsKt.IS_GOOGLE_NAV_ACTIVE_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.LAST_GOOGLE_NOTIFICATION_TIMESTAMP;
import static com.revos.android.fleet.utils.ConstantsKt.NAV_EVENT_TRIP_ADVICE_DATA_UPDATED;
import static com.revos.android.fleet.utils.ConstantsKt.NAV_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.NOTIFICATION_LISTENER_CONNECTED_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.NOTIFICATION_SERVICE_BINDED_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.PHONE_EVENT_CREATE_CALL_LOG;
import static com.revos.android.fleet.utils.ConstantsKt.PHONE_EVENT_REQUEST_ACCEPT_CALL_VIA_NOTIFICATION;
import static com.revos.android.fleet.utils.ConstantsKt.PHONE_NUMBER_REGEX;
import static com.revos.android.fleet.utils.ConstantsKt.PHONE_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.SPOTIFY_PACKAGE_NAME;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_DATA_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_DESTINATION_NAME_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_DISTANCE_FROM_TURN_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_ETA_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_IS_REROUTING_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_NEXT_ROAD_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_REMAINING_DISTANCE_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_REMAINING_TIME_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_SUBTEXT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_TEXT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_ADVICE_TITLE_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.YOUTUBE_MUSIC_PACKAGE_NAME;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.telephony.TelephonyManager;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.revos.android.fleet.R;
import com.revos.android.fleet.eventbus.EventBusMessage;
import com.revos.android.fleet.data.GenericPhoneEntry;
import com.revos.android.fleet.receivers.PhoneStateReceiver;
import com.revos.android.fleet.utils.general.ImageUtils;
import com.revos.android.fleet.utils.general.PhoneUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import timber.log.Timber;

/**
 * Created by mohit on 16/5/17.
 */

public class NotificationReaderService extends NotificationListenerService {

    private static HashMap<String, Object> mTripAdviceDataHashMap;

    public static Bitmap mCachedNavigationBitmap = null;
    public static Bitmap mCachedCroppedAndScaledNavigationBitmap = null;
    public static boolean mAlignIconLeft  = false;

    private static Context mContext;

    private Set<String> mCallUiPackageNameSet;

    private Timer mOneSecondTimer;

    public static String mActiveMusicPlayerPackageName = "";
    public static Notification mYoutubeMusicNotification;
    public static Notification mSpotifyNotification;

    //auto suggest variables
    private static final int SEND_CALL_ICON = 100;
    private static final long NOTIFICATION_READ_COOL_OFF = 1000;

    @Override
    public IBinder onBind(Intent intent) {
        Timber.d("BINDED");
        getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(NOTIFICATION_SERVICE_BINDED_KEY, true).apply();
        mContext = getApplicationContext();
        return super.onBind(intent);
    }


    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if(msg.what == SEND_CALL_ICON) {
                if(PhoneStateReceiver.currentState == TelephonyManager.CALL_STATE_RINGING) {
                    String contactString = PhoneUtils.getInstance(getApplicationContext()).getContactInAction();
                    String contactNumber = PhoneUtils.getInstance(getApplicationContext()).getNumberInAction();

                    String stringToBeDisplayed = contactString == null ? contactNumber : contactString;

                    if(stringToBeDisplayed == null || stringToBeDisplayed.isEmpty() || stringToBeDisplayed.equals("null")) {
                        stringToBeDisplayed = " ";
                    }

                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_SEND_CALL_BITMAP + GEN_DELIMITER + stringToBeDisplayed));
                }
            }
            return false;
        }
    });


    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        //set the notification listener connected flag
        getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, true).apply();

        mContext = getApplicationContext();

        mCachedNavigationBitmap = null;
        mActiveMusicPlayerPackageName = "";
        initializeHashMaps();

        //register with the EventBus, if not registered already
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        Timber.d("listener_connected");
        //start the timer
        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }

        mOneSecondTimer = new Timer();
        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    updatePhoneCallState();
                    processNotifications();
                } catch (Exception e) {
                    FirebaseCrashlytics.getInstance().recordException(e);
                }
            }
        }, 100, 1000);
        //clear the display string in action
        PhoneUtils.getInstance(getApplicationContext()).setNumberInAction(null);
    }

    private void updatePhoneCallState() {
        TelephonyManager telephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyMgr != null) {
            Timber.d("call_state: %s", telephonyMgr.getCallState());
            PhoneStateReceiver.currentState = telephonyMgr.getCallState();
        }
    }

    @Override
    public void onListenerDisconnected() {
        try {
            if (mOneSecondTimer != null) {
                mOneSecondTimer.cancel();
                mOneSecondTimer.purge();
            }
            Timber.d("listener_disconnected");
            super.onListenerDisconnected();
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //handler.post(runnableCode);
        Timber.d("service_started");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        //unregister with the EventBus, if registered
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        Timber.d("service_destroyed");
        try {
            if (mOneSecondTimer != null) {
                mOneSecondTimer.cancel();
                mOneSecondTimer.purge();
            }
            mContext = null;
            super.onDestroy();
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);

        Timber.d("notification posted : " + sbn.getPackageName());

        if(sbn.getPackageName().equals(YOUTUBE_MUSIC_PACKAGE_NAME)) {
            mActiveMusicPlayerPackageName = YOUTUBE_MUSIC_PACKAGE_NAME;
        }

        if(sbn.getPackageName().equals(SPOTIFY_PACKAGE_NAME)) {
            mActiveMusicPlayerPackageName = SPOTIFY_PACKAGE_NAME;
        }

        try {
            //set the notification listener connected flag
            getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, true).apply();
            if (sbn.getPackageName().equals(GOOGLE_MAPS_PACKAGE_NAME)) {
                //keep this as commit, do not use apply here
                getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit()
                        .putLong(LAST_GOOGLE_NOTIFICATION_TIMESTAMP, System.currentTimeMillis()).apply();
            } else if (mCallUiPackageNameSet.contains(sbn.getPackageName())) {
                Timber.d("posted: " + sbn.getPackageName() + ", " + sbn.getNotification().extras);
                //Setup Telephony Services
                TelephonyManager telephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (telephonyMgr != null) {
                    Timber.d("call_state: %s", telephonyMgr.getCallState());

                    if (telephonyMgr.getCallState() != TelephonyManager.CALL_STATE_IDLE) {
                        try {
                            processCallerNotification();
                        } catch (Exception e) {
                            FirebaseCrashlytics.getInstance().recordException(e);
                        }
                    }
                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        if(sbn.getPackageName().equals(GOOGLE_MAPS_PACKAGE_NAME)) {
            //keep this as commit, do not use apply here
            /*getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit()
                    .putBoolean(LAST_GOOGLE_NOTIFICATION_TIMESTAMP, false).apply();*/
        } else if(mCallUiPackageNameSet.contains(sbn.getPackageName())) {
            Timber.d("removed: " + sbn.getPackageName() + ", " + sbn.getNotification().extras);
        }
    }

    public void processNotifications() {
        StatusBarNotification[] sbns = getActiveNotifications();

        if(sbns.length > 0) {
            getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, true).apply();
        }

        getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit().putBoolean(IS_GOOGLE_NAV_ACTIVE_KEY, false).apply();
        for(int notificationIndex = 0; notificationIndex < sbns.length; ++notificationIndex) {
            StatusBarNotification sbn = sbns[notificationIndex];
            if(sbn == null) {
                continue;
            }

            if(sbn.getPackageName().equals(GOOGLE_MAPS_PACKAGE_NAME)) {
                getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit().putBoolean(IS_GOOGLE_NAV_ACTIVE_KEY, true).apply();
                new ProcessGoogleMapNotificationTask().execute(sbn);
            }

            AudioManager audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);

            if(sbn.getPackageName().equals(YOUTUBE_MUSIC_PACKAGE_NAME)) {
                mYoutubeMusicNotification = sbn.getNotification();
                if(audioManager.isMusicActive() && (mActiveMusicPlayerPackageName == null || mActiveMusicPlayerPackageName.isEmpty())) {
                    mActiveMusicPlayerPackageName = YOUTUBE_MUSIC_PACKAGE_NAME;
                }
            }

            if(sbn.getPackageName().equals(SPOTIFY_PACKAGE_NAME)) {
                mSpotifyNotification = sbn.getNotification();
                if(audioManager.isMusicActive() && (mActiveMusicPlayerPackageName == null || mActiveMusicPlayerPackageName.isEmpty())) {
                    mActiveMusicPlayerPackageName = SPOTIFY_PACKAGE_NAME;
                }
            }
        }
    }


    private void initializeHashMaps() {
        createCallUiPackageNameHashSet();
    }

    private void createCallUiPackageNameHashSet() {
        mCallUiPackageNameSet = new HashSet<>();
        mCallUiPackageNameSet.add("com.android.phone");
        mCallUiPackageNameSet.add("com.android.incallui");
        mCallUiPackageNameSet.add("com.google.android.dialer");
        mCallUiPackageNameSet.add("com.android.dialer");
        mCallUiPackageNameSet.add("com.asus.asusincallui");
        mCallUiPackageNameSet.add("com.samsung.android.incallui");
    }
    /*private int getIconIndex(int iconID) {
        for(int iconIndex = 0; iconIndex < Constants.turnIconIds.length; ++iconIndex) {
            if(turnIconIds[iconIndex] == iconID) {
                return iconIndex;
            }
        }
        return -1;
    }*/


    private static HashMap<String, Object> processGoogleMapsNotification(StatusBarNotification sbn) {
        try {
            HashMap<String, Object> tripAdviceDataHashMap = new HashMap<>();
            Notification notification = sbn.getNotification();
            Bundle extrasBundle = notification.extras;

            CharSequence androidTitle = extrasBundle.getCharSequence("android.title");
            CharSequence androidSubText = extrasBundle.getCharSequence("android.subText");
            CharSequence androidText = extrasBundle.getCharSequence("android.text");

            String titleStr, subTextStr, textStr;
            titleStr = subTextStr = textStr = "";
            if (androidTitle != null) {
                titleStr = androidTitle.toString();
            }
            if (androidSubText != null) {
                subTextStr = androidSubText.toString();
            }
            if (androidText != null) {
                textStr = androidText.toString();
            }

            //todo::handle localization
            if (titleStr.toLowerCase().contains(mContext.getResources().getString(R.string.rerouting).toLowerCase())
                    || subTextStr.toLowerCase().contains(mContext.getResources().getString(R.string.rerouting).toLowerCase())
                    || textStr.toLowerCase().contains(mContext.getResources().getString(R.string.rerouting).toLowerCase())) {
                tripAdviceDataHashMap.put(TRIP_ADVICE_IS_REROUTING_KEY, "yes");
                return tripAdviceDataHashMap;
            }

            //check if there's a direction bitmap
            if (notification.largeIcon == null && extrasBundle.get("android.largeIcon") == null) {
                return tripAdviceDataHashMap;
            }

            Bitmap bitmap = null;
            if (notification.largeIcon != null) {
                bitmap = notification.largeIcon;
            } else if (extrasBundle.get("android.largeIcon") != null) {
                Object obj = extrasBundle.get("android.largeIcon");

                if (obj == null) {
                    return tripAdviceDataHashMap;
                }

                Method[] method = obj.getClass().getDeclaredMethods();
                for (int index = 0; index < method.length; ++index) {
                    Method currentMethod = method[index];
                    currentMethod.setAccessible(true);
                    try {
                        Object currentObject = currentMethod.invoke(obj, (Object[]) null);

                        if (currentObject != null && String.valueOf(currentObject.getClass()).contains("android.graphics.Bitmap")) {
                            bitmap = (Bitmap) currentObject;
                        }
                    } catch (Exception e) {
                        //do nothing
                    }
                }
            }

            if (bitmap == null) {
                return tripAdviceDataHashMap;
            }

            tripAdviceDataHashMap = populateTripAdviceHashMap(tripAdviceDataHashMap, titleStr, subTextStr, textStr, bitmap);

            return tripAdviceDataHashMap;
        } catch (Exception e) {
            return null;
        }
    }

    private static HashMap<String, Object> populateTripAdviceHashMap(HashMap<String, Object> tripAdviceDataHashMap, String title, String subText, String text, Bitmap directionBitmap) {
        tripAdviceDataHashMap = processBitmapData(tripAdviceDataHashMap, directionBitmap);
        tripAdviceDataHashMap = processTextBasedData(tripAdviceDataHashMap, title, subText, text);

        return tripAdviceDataHashMap;
    }

    private static HashMap<String,Object> processTextBasedData(HashMap<String, Object> tripAdviceDataHashMap, String title, String subText, String text) {
        //store title, subText and text in hashmap first
        tripAdviceDataHashMap.put(TRIP_ADVICE_TITLE_KEY, title);
        tripAdviceDataHashMap.put(TRIP_ADVICE_SUBTEXT_KEY, subText);
        tripAdviceDataHashMap.put(TRIP_ADVICE_TEXT_KEY, text);

        Locale identifiedLocaleFromTimeUnits = null;
        Locale identifiedLocale = Locale.getDefault();
        //use subtext to process the trip summary
        //check if we have a trip summary text, check for middle dot character
        String[] tokens = subText.split("\u00B7");
        if(tokens.length == 3) {
            //definitely a trip summary text
            String timeStr = tokens[0].trim();
            String distanceFromDestinationStr = tokens[1].trim();
            String etaStr = tokens[2].trim();

            //calculate time in minutes
            tokens = timeStr.split(" ");
            int timeValue = 0;
            //same for eta
            for(int tokenIndex = 0; tokenIndex < tokens.length; ++tokenIndex) {
                String stringCompared = tokens[tokenIndex];
                if(stringCompared.equalsIgnoreCase(mContext.getString(R.string.hours)) && tokenIndex > 0) {
                    timeValue += Integer.parseInt(tokens[tokenIndex - 1]) * 60;
                    //the string equals hr string
                    //find out the locale based on the actual String value
                    identifiedLocaleFromTimeUnits = determineLocaleFromUnits(stringCompared, R.string.hours);
                }
                if(stringCompared.equalsIgnoreCase(mContext.getString(R.string.minutes)) && tokenIndex > 0) {
                    timeValue += Integer.parseInt(tokens[tokenIndex - 1]);
                    //the string equals min string
                    //find out the locale based on the actual String value
                    identifiedLocaleFromTimeUnits = determineLocaleFromUnits(stringCompared, R.string.minutes);
                }
            }
            tripAdviceDataHashMap.put(TRIP_ADVICE_REMAINING_TIME_KEY,timeValue);

            //calculate distance from destination, split on non breaking space
            String splitRegexForDistanceFromDestination = "\u00A0";
            if(identifiedLocaleFromTimeUnits.equals(new Locale("en"))) {
                splitRegexForDistanceFromDestination = "\u00A0";
            } else if(identifiedLocaleFromTimeUnits.equals(new Locale("hi")) || identifiedLocaleFromTimeUnits.equals(new Locale("hi_IN"))) {
                splitRegexForDistanceFromDestination = "\u00A0";
            } else if(identifiedLocaleFromTimeUnits.equals(new Locale("vi_VN"))) {
                splitRegexForDistanceFromDestination ="\u00A0";
            } else if(identifiedLocaleFromTimeUnits.equals(new Locale("ar_EG"))) {
                splitRegexForDistanceFromDestination = "\u00A0";
            } else if(identifiedLocaleFromTimeUnits.equals(new Locale("it")) || identifiedLocaleFromTimeUnits.equals(new Locale("it_IT"))
                            || identifiedLocaleFromTimeUnits.equals(new Locale("it_CH"))) {
                splitRegexForDistanceFromDestination = "\u00A0";
            }
            tokens = distanceFromDestinationStr.split(splitRegexForDistanceFromDestination);
            if(tokens.length == 2) {
                if(tokens[1].equalsIgnoreCase(mContext.getString(R.string.meters)) || tokens[1].equalsIgnoreCase(mContext.getString(R.string.kilometers))) {
                    //it is definitely remaining distance
                    //clean any unnecessary commas
                    if(identifiedLocale.getLanguage().equals("it") || identifiedLocale.getLanguage().equals("it_IT") || identifiedLocale.getLanguage().equals("it_CH")){
                        tokens[0] = tokens[0].replace(",", ".");
                    } else {
                        tokens[0] = tokens[0].replace(",", "");
                    }

                    float remainingDistance = tokens[1].equalsIgnoreCase(mContext.getString(R.string.meters)) ? Integer.parseInt(tokens[0]) : Float.parseFloat(tokens[0]) * 1000;
                    tripAdviceDataHashMap.put(TRIP_ADVICE_REMAINING_DISTANCE_KEY, remainingDistance);
                }
            }

            //get ETA string
            tokens = etaStr.split(" ");
            String etaPlaceHolderString = mContext.getString(R.string.eta);
            etaPlaceHolderString = etaPlaceHolderString.toLowerCase();

            int etaStringPos = etaStr.toLowerCase().indexOf(etaPlaceHolderString);
            String etaStrVal;
            if(etaPlaceHolderString.equals("arrivo")) {
                etaStrVal = etaStr.substring(etaPlaceHolderString.length());
            } else {
                etaStrVal = etaStr.substring(0, etaStringPos);
            }
            tripAdviceDataHashMap.put(TRIP_ADVICE_ETA_KEY, etaStrVal);

            /*if(tokens.length == 2 && tokens[1].equalsIgnoreCase(mContext.getString(R.string.eta))) {
                String etaStrVal = tokens[0];
                tripAdviceDataHashMap.put(TRIP_ADVICE_ETA_KEY, etaStrVal);
            }*/
        }

        //use title to process distance from turn and next road string
        if(!title.toLowerCase().contains(mContext.getString(R.string.exit_navigation).toLowerCase())) {
            tokens = title.split("-");
            //try to get distance from turn information
            String[] distanceFromTurnTokens = tokens[0].split("\u00A0");
            if(distanceFromTurnTokens.length == 2) {
                //possibly a distance from turn advice
                if(distanceFromTurnTokens[1].trim().equalsIgnoreCase(mContext.getString(R.string.meters)) || distanceFromTurnTokens[1].trim().equalsIgnoreCase(mContext.getString(R.string.kilometers))) {
                    //it is definitely a distance from turn advice
                    float distanceFromTurnInMeters = distanceFromTurnTokens[1].trim().equalsIgnoreCase(mContext.getString(R.string.meters)) ? Integer.parseInt(distanceFromTurnTokens[0].trim()) : Float.parseFloat(distanceFromTurnTokens[0].trim()) * 1000;
                    tripAdviceDataHashMap.put(TRIP_ADVICE_DISTANCE_FROM_TURN_KEY, distanceFromTurnInMeters);
                }
            }

            //sometimes the distance from turn is missing, in that case just store the next road text
            if(tokens.length == 2) {
                tripAdviceDataHashMap.put(TRIP_ADVICE_NEXT_ROAD_KEY, tokens[1].trim());
            } else if(tokens.length == 1) {
                tripAdviceDataHashMap.put(TRIP_ADVICE_NEXT_ROAD_KEY, tokens[0].trim());
            }
        }

        //use text to find destination name
        //check if we have a trip summary text, check for hyphen
        tokens = text.split("\u002D");
        int etaTokenIndex = 0;
        boolean etaFound = false;
        for(; etaTokenIndex < tokens.length; ++etaTokenIndex) {
            if(tokens[etaTokenIndex].toLowerCase().trim().contains(mContext.getString(R.string.eta).toLowerCase())) {
                etaFound = true;
                break;
            }
        }

        if(tokens.length >= 2 && etaFound) {
            //definitely a trip summary text

            String destinationNameStr = tokens[0].trim();
            for(int tokenIndex = 1; tokenIndex < etaTokenIndex; ++tokenIndex) {
                destinationNameStr += " \u002D " + tokens[tokenIndex].trim();
            }

            tripAdviceDataHashMap.put(TRIP_ADVICE_DESTINATION_NAME_KEY, destinationNameStr);
        }


        return tripAdviceDataHashMap;
    }

    private static Locale determineLocaleFromUnits(String stringToBeCompared, int stringResourceId) {
        Locale determinedLocale = Locale.getDefault();

        if(stringToBeCompared.equals(getLocaleString("en", stringResourceId))) {
            determinedLocale = new Locale("en");
        } else if(stringToBeCompared.equals(getLocaleString("hi", stringResourceId))
                || stringToBeCompared.equals(getLocaleString("hi_IN", stringResourceId))) {
            determinedLocale = new Locale("hi");
        } else if(stringToBeCompared.equals(getLocaleString("vi", stringResourceId))) {
            determinedLocale = new Locale("vi_VN");
        } else if(stringToBeCompared.equals(getLocaleString("ar", stringResourceId))
                    || stringToBeCompared.equals(getLocaleString("ar_EG", stringResourceId))) {
            determinedLocale = new Locale("ar_EG");
        } else if(stringToBeCompared.equals(getLocaleString("it", stringResourceId))
                    || stringToBeCompared.equals(getLocaleString("it_IT", stringResourceId))
                    || stringToBeCompared.equals(getLocaleString("it_CH", stringResourceId))) {
            determinedLocale = new Locale("it_IT");
        }

        return determinedLocale;
    }

    private static String getLocaleString(String localeCodeString, int stringResourceId) {
        Configuration configuration = getLocaleConfiguration(localeCodeString);

        return mContext.createConfigurationContext(configuration).getResources().getString(stringResourceId);
    }

    private static Configuration getLocaleConfiguration(String localeCodeString) {
        Configuration configuration = new Configuration(mContext.getResources().getConfiguration());
        configuration.setLocale(new Locale(localeCodeString));
        return configuration;
    }

    private static HashMap<String,Object> processBitmapData(HashMap<String, Object> tripAdviceDataHashMap, Bitmap directionBitmap) {

        if(mCachedNavigationBitmap == null || ImageUtils.findBitmapPercentageDiff(mCachedNavigationBitmap, directionBitmap) > 5) {
            mCachedNavigationBitmap = directionBitmap;
            createCroppedAndScaledBitmap(directionBitmap);
        }

        return tripAdviceDataHashMap;
    }

    private static void createCroppedAndScaledBitmap(Bitmap originalBitmap) {
        Bitmap scaledAndBinaryAlphaBitmap = Bitmap.createScaledBitmap(originalBitmap, 32, 32, false);
        scaledAndBinaryAlphaBitmap.setHasAlpha(true);

        int xMin, xMax, yMin, yMax;
        xMax = yMax = 0;
        xMin = scaledAndBinaryAlphaBitmap.getWidth();
        yMin = scaledAndBinaryAlphaBitmap.getHeight();

        int xAtYMax = 0;

        //process the bitmap first
        for(int x = 0; x < scaledAndBinaryAlphaBitmap.getWidth(); ++x) {
            for(int y = 0; y < scaledAndBinaryAlphaBitmap.getHeight(); ++y) {
                int pixel = scaledAndBinaryAlphaBitmap.getPixel(x,y);

                //if the alpha is not 255 then set the alpha to zero
                if(Color.alpha(pixel) != 0xFF) {
                    scaledAndBinaryAlphaBitmap.setPixel(x,y, 0x00000000);
                } else {
                    scaledAndBinaryAlphaBitmap.setPixel(x,y, 0xFF000000);
                    if(x <= xMin) {
                        xMin = x;
                    }
                    if(y <= yMin) {
                        yMin = y;
                    }
                    if(x >= xMax) {
                        xMax = x;
                    }
                    if(y >= yMax) {
                        yMax = y;
                        xAtYMax = x;
                    }
                }
            }
        }

        mAlignIconLeft = xAtYMax < scaledAndBinaryAlphaBitmap.getWidth() / 2;


        Timber.d("xMin: " + xMin + " xMax: " + xMax + " yMin: " + yMin + " yMax: " + yMax);
        if(xMin > xMax || yMin > yMax) {
            return;
        }
        mCachedCroppedAndScaledNavigationBitmap = Bitmap.createBitmap(scaledAndBinaryAlphaBitmap, xMin, yMin, xMax - xMin + 1, yMax - yMin + 1);

        scaledAndBinaryAlphaBitmap.recycle();

        if(ImageUtils.isBitmapSymmetricAtBaseline(mCachedCroppedAndScaledNavigationBitmap)) {
            mAlignIconLeft = true;
        }

    }

    private static class ProcessGoogleMapNotificationTask extends AsyncTask<StatusBarNotification, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(StatusBarNotification... params) {
            mTripAdviceDataHashMap = processGoogleMapsNotification(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(mTripAdviceDataHashMap != null && mTripAdviceDataHashMap.size() == 1) {
                if(mTripAdviceDataHashMap.containsKey(TRIP_ADVICE_IS_REROUTING_KEY)) {
                    mContext.getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit().putString(TRIP_ADVICE_DATA_KEY, new Gson().toJson(mTripAdviceDataHashMap)).apply();
                    EventBusMessage message = new EventBusMessage(NAV_EVENT_TRIP_ADVICE_DATA_UPDATED);
                    message.setData(mTripAdviceDataHashMap);
                    EventBus.getDefault().post(message);
                }
            } else if(mTripAdviceDataHashMap != null && !mTripAdviceDataHashMap.isEmpty() && mTripAdviceDataHashMap.size() > 1) {
                //write this to shared preferences
                mContext.getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit().putString(TRIP_ADVICE_DATA_KEY, new Gson().toJson(mTripAdviceDataHashMap)).apply();
                EventBusMessage message = new EventBusMessage(NAV_EVENT_TRIP_ADVICE_DATA_UPDATED);
                message.setData(mTripAdviceDataHashMap);
                EventBus.getDefault().post(message);
            } else {
                if(PhoneStateReceiver.currentState == TelephonyManager.CALL_STATE_RINGING) {
                    return;
                }
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CLEAR_IMAGE));
            }
        }

    }


    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.getMessage().equals(PHONE_EVENT_REQUEST_ACCEPT_CALL_VIA_NOTIFICATION)) {
            //try answering call using pending intent inside notification
            answerCallWithNotification();
        } else if(event.getMessage().contains(PHONE_EVENT_CREATE_CALL_LOG)) {
            createCallLog(event.getMessage().split(GEN_DELIMITER)[1]);
        }
    }

    private void createCallLog(String callEntryType) {
        PhoneUtils phoneUtils = PhoneUtils.getInstance(getApplicationContext());
        String numberInAction = phoneUtils.getNumberInAction();
        String contactInAction = phoneUtils.getContactInAction();

        if(numberInAction == null && contactInAction == null) {
            return;
        }

        GenericPhoneEntry phoneEntry = new GenericPhoneEntry();

        phoneEntry.setEntryType(callEntryType);
        phoneEntry.setDate(PhoneStateReceiver.callStartTime);

        if(contactInAction != null) {
            phoneEntry.setName(contactInAction);
        } else {
            phoneEntry.setName(numberInAction);
        }

        if(numberInAction != null) {
            phoneEntry.setPhoneNumber(numberInAction);
        } else {
            phoneEntry.setPhoneNumber(contactInAction);
        }

        //write these phoneEntry to shared pref hashmap
        SharedPreferences sharedPreferences = getSharedPreferences(PHONE_PREFS, MODE_PRIVATE);

        HashMap<String, GenericPhoneEntry> callLogHashMap = new HashMap<>();

        String callLogHashMapStr = sharedPreferences.getString(CALL_LOG_HASHMAP_KEY, null);
        if(callLogHashMapStr != null) {
            callLogHashMap = new Gson().fromJson(callLogHashMapStr, new TypeToken<HashMap<String, GenericPhoneEntry>>(){}.getType());
        }

        callLogHashMap.put(phoneEntry.getPhoneNumber(), phoneEntry);

        //write this hashmap to shared pref again
        sharedPreferences.edit().putString(CALL_LOG_HASHMAP_KEY, new Gson().toJson(callLogHashMap)).apply();

        //the call entry has been created, clear these two values from prefs
        phoneUtils.setNumberInAction(null);
        phoneUtils.setContactInAction(null);

    }

    private void processCallerNotification() {
        findAndSetDisplayStringInAction();
        Timber.d("numberInAction: %s", PhoneUtils.getInstance(getApplicationContext()).getNumberInAction());
        showCallIconIfRequired();
    }

    private void showCallIconIfRequired() {
        handler.removeMessages(SEND_CALL_ICON);
        handler.sendEmptyMessageDelayed(SEND_CALL_ICON, NOTIFICATION_READ_COOL_OFF);
    }

    private void findAndSetDisplayStringInAction() {
        //iterate over the active notifications and check if anyone matches the internal dialer package name
        StatusBarNotification[] activeNotifications = getActiveNotifications();

        //search all the notifications, do a regex match to see if it's a number or contact name
        for(StatusBarNotification sbn : activeNotifications) {
            if(mCallUiPackageNameSet.contains(sbn.getPackageName())) {

                //extract information from the notification
                String notificationTitle = sbn.getNotification().extras.getString("android.title", null);
                if(notificationTitle != null) {

                    notificationTitle = notificationTitle.trim();

                    //check if this string matches a phone number pattern
                    //the matches method does a match on the entire length of the string
                    //hence there is no need for characters ^ and $
                    if(notificationTitle.matches(PHONE_NUMBER_REGEX)) {
                        //the title is most likely a number
                        //sanitize this number first
                        String cleanNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(notificationTitle);
                        PhoneUtils.getInstance(getApplicationContext()).setNumberInAction(cleanNumber);
                        return;
                    } else {
                        //the title is most likely a text
                        //check if this text matches any of the contacts
                        if(PhoneUtils.getInstance(getApplicationContext()).findContactNumbers(notificationTitle) != null) {
                            //this is one of the contacts
                            PhoneUtils.getInstance(getApplicationContext()).setContactInAction(notificationTitle);
                            return;
                        }
                    }
                } else {
                    PhoneUtils.getInstance(getApplicationContext()).setNumberInAction(" ");
                    PhoneUtils.getInstance(getApplicationContext()).setContactInAction(" ");
                }
            }
        }
    }

    private void answerCallWithNotification() {
        StatusBarNotification[] sbnList = getActiveNotifications();
        for(int sbnIndex = 0; sbnIndex < sbnList.length; ++sbnIndex) {
            StatusBarNotification sbn = sbnList[sbnIndex];
            Notification notification = sbn.getNotification();
            if(mCallUiPackageNameSet.contains(sbn.getPackageName())) {
                Notification.Action[] actions = notification.actions;

                Notification.Action answerAction = null;
                for(int actionIndex = 0; actionIndex < actions.length; ++actionIndex) {
                    if(actions[actionIndex].title.toString().equalsIgnoreCase("answer")) {
                        answerAction = actions[actionIndex];
                    }
                }
                if(answerAction != null) {
                    PendingIntent answerPendingIntent = answerAction.actionIntent;
                    try {
                        answerPendingIntent.send();
                    } catch (PendingIntent.CanceledException e) {
                        //todo::handle exception
                    }
                }
            }
        }
    }
}
