package com.revos.android.fleet.api.v2.payment

import com.google.gson.GsonBuilder
import com.revos.android.fleet.BuildConfig
import com.revos.android.fleet.api.v2.data.GetAllVehiclesModel
import com.revos.android.fleet.api.v2.data.GetOrderStatusResponseModel
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

internal interface RevosPaymentService {

    companion object {
        private val BASE_URL = if(BuildConfig.DEBUG) "https://payments.dev.revos.in/v1/" else "https://payments.revos.in/v1/"

        fun create(): RevosPaymentService {
            val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val clientBuilder = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)

            val client = if(BuildConfig.DEBUG){
                clientBuilder.addInterceptor(logger).build()
            }else{
                clientBuilder.build()
            }

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
                .create(RevosPaymentService::class.java)
        }
    }

    @POST("payments/orders/cashfree/orderstatus")
    suspend fun getOrderStatus(@Header("token") appToken: String,
                               @Header("Authorization") bearerToken: String,
                               @Body requestBody: RequestBody) : GetOrderStatusResponseModel
}