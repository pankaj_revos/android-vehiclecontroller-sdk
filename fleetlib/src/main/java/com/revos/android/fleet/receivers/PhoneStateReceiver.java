package com.revos.android.fleet.receivers;

import static com.revos.android.fleet.utils.ConstantsKt.BT_EVENT_REQUEST_CLEAR_IMAGE;
import static com.revos.android.fleet.utils.ConstantsKt.ENTRY_TYPE_CALL_INCOMING;
import static com.revos.android.fleet.utils.ConstantsKt.ENTRY_TYPE_CALL_MISSED;
import static com.revos.android.fleet.utils.ConstantsKt.ENTRY_TYPE_CALL_OUTGOING;
import static com.revos.android.fleet.utils.ConstantsKt.GEN_DELIMITER;
import static com.revos.android.fleet.utils.ConstantsKt.PHONE_EVENT_CREATE_CALL_LOG;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.revos.android.fleet.eventbus.EventBusMessage;
import org.greenrobot.eventbus.EventBus;
import java.util.Date;
import timber.log.Timber;

public class PhoneStateReceiver extends BroadcastReceiver {

    //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations
    public static int currentState = TelephonyManager.CALL_STATE_IDLE;
    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    public static Date callStartTime;
    private static boolean isIncoming;

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getExtras() != null && intent.getExtras().getString(TelephonyManager.EXTRA_STATE ) != null) {

            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            if(stateStr != null) {
                int state = 0;
                if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    state = TelephonyManager.CALL_STATE_IDLE;
                } else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    state = TelephonyManager.CALL_STATE_OFFHOOK;
                } else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    state = TelephonyManager.CALL_STATE_RINGING;
                }

                onCallStateChanged(context, state);
            }
        }
    }

    protected void onIncomingCallStarted(Context ctx, Date start){
        Timber.d("incoming call started");
    }
    protected void onOutgoingCallStarted(Context ctx, Date start){
        Timber.d("outgoing call started");
    }
    protected void onIncomingCallEnded(Context ctx, Date start, Date end){
        Timber.d("incoming call ended");
        EventBus.getDefault().post(new EventBusMessage(PHONE_EVENT_CREATE_CALL_LOG + GEN_DELIMITER + ENTRY_TYPE_CALL_INCOMING));
    }
    protected void onOutgoingCallEnded(Context ctx, Date start, Date end){
        Timber.d("outgoing call ended");
        EventBus.getDefault().post(new EventBusMessage(PHONE_EVENT_CREATE_CALL_LOG + GEN_DELIMITER + ENTRY_TYPE_CALL_OUTGOING));
    }
    protected void onMissedCall(Context ctx, Date start){
        Timber.d("missed call");
        EventBus.getDefault().post(new EventBusMessage(PHONE_EVENT_CREATE_CALL_LOG + GEN_DELIMITER + ENTRY_TYPE_CALL_MISSED));
    }

    //Deals with actual events

    //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    public void onCallStateChanged(Context context, int state) {
        currentState = state;
        Timber.d("phone state%s", state);
        if(lastState == state){
            //No change, debounce extras
            return;
        }
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = new Date();
                onIncomingCallStarted(context, callStartTime);
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                if(lastState != TelephonyManager.CALL_STATE_RINGING){
                    isIncoming = false;
                    callStartTime = new Date();
                    onOutgoingCallStarted(context, callStartTime);
                }
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                if(lastState == TelephonyManager.CALL_STATE_RINGING){
                    //Ring but no pickup-  a miss
                    onMissedCall(context, callStartTime);
                }
                else if(isIncoming){
                    onIncomingCallEnded(context, callStartTime, new Date());
                }
                else{
                    onOutgoingCallEnded(context, callStartTime, new Date());
                }
                break;
        }
        lastState = state;

        if(state != TelephonyManager.CALL_STATE_RINGING) {
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CLEAR_IMAGE));
        }
    }
}
