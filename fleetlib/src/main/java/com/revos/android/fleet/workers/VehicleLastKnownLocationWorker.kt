package com.revos.android.fleet.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.revos.android.fleet.repository.MainRepository
import com.revos.android.fleet.repository.VehicleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class VehicleLastKnownLocationWorker(context: Context, workers: WorkerParameters)
    : CoroutineWorker(context, workers) {

    private val vehicleRepository = VehicleRepository()

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            vehicleRepository.refreshVehicleLastKnownLocation(applicationContext)
            Log.d(TAG, "Vehicle location updated")
            Result.success()
        } catch (ex: Exception) {
            Log.e(TAG, "Error adding geofence to database", ex)
            Result.failure()
        }
    }

    companion object {
        private const val TAG = "VehicleLocationWorker"
    }
}