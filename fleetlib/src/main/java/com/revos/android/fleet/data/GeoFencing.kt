package com.revos.android.fleet.data

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import com.revos.scripts.type.FenceType
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "geofence")
@Parcelize
data class GeoFencing (
    @PrimaryKey @ColumnInfo(name = "id") var geoFenceId: String,
    var geoFenceType: FenceType? = null,
    var circleRadius : Double? = 0.0,
    var polygonVerticesList: ArrayList<LatLng>? = null,
    var circleGeoFenceCentre: LatLng? = null,
    var geoFenceName : String? = null,
) : Parcelable