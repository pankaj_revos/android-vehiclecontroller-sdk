package com.revos.android.fleet.data.v2

data class RentalVehicle(
    val createdAt: String? = null,
    val id: String? = null,
    val isDeliverable: Boolean? = null,
    val isHealthy: Boolean? = null,
    val lastMarkedLatitude: Double? = null,
    val lastMarkedLongitude: Double? = null,
    val modelTag: String? = null,
    val pricingInfo: List<PricingInfo>?,
    val rentalStatus: String? = null,
    val status: String? = null,
    val updatedAt: String? = null,
    val vin: String? = null) {

    data class PricingInfo(
        val amountPayable: Float,
        val baseAmount: Int,
        val costPerUnit: Int,
        val createdAt: String,
        val id: String,
        val minimumPayableAmount: Float,
        val name: String,
        val status: String,
        val type: String,
        val unit: String,
        val updatedAt: String )
}

