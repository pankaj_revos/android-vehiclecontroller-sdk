package com.revos.android.fleet.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TripDataFeed (
    var tripId: String? = null,
    var vin: String? = null,
    var location: List<DataFeedAllTripsLocation>? = null,
    var distance : Double = 0.0,
    var energy : Double = 0.0,
    var maxWheelRPM : Float = 0f,
    var minWheelRPM : Float = 0f,
    var minGpsSpeed : Double = 0.0,
    var maxGpsSpeed : Double = 0.0,
    var avgGpsSpeed : Double = 0.0,
    var batteryVoltageAdc: List<DataFeedBatteryVoltageAdc>? = null,
    var mode: TripModeDataFeed? = null,
    var startTime: String? = null,
    var endTime: String? = null,
) : Parcelable