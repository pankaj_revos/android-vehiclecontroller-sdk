package com.revos.android.fleet.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TripModeDataFeed (
    var ECONOMY : Double = 0.0,
    var RIDE : Double = 0.0,
    var SPORT : Double = 0.0,
) : Parcelable