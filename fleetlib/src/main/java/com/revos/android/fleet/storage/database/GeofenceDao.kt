package com.revos.android.fleet.storage.database

import androidx.room.*
import com.revos.android.fleet.data.GeoFencing
import kotlinx.coroutines.flow.Flow

@Dao
interface GeofenceDao {
    @Query("SELECT * FROM geofence ORDER BY id")
    fun getAllGeofence(): Flow<List<GeoFencing>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(geofence: GeoFencing)

    @Query("SELECT * FROM geofence WHERE id = :geofenceId")
    fun getGeofenceById(geofenceId: String): Flow<GeoFencing?>

    @Query("DELETE FROM geofence WHERE id = :geofenceId")
    suspend fun deleteGeofenceById(geofenceId : String)

    @Query("DELETE FROM geofence")
    suspend fun deleteAllGeofence()

    @Update
    fun updateGeofence(geofence: GeoFencing)
}