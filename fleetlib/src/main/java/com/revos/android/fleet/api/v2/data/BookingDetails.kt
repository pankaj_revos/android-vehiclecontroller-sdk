package com.revos.android.fleet.api.v2.data

import com.google.gson.annotations.SerializedName

data class BookingDetails(
    @SerializedName("asset")
    val asset: String,
    @SerializedName("bookingData")
    val bookingData: BookingData,
    @SerializedName("bookingTime")
    val bookingTime: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("endTime")
    val endTime: String,
    @SerializedName("_id")
    val id: String,
    @SerializedName("leasee")
    val leasee: String,
    @SerializedName("leasor")
    val leasor: String,
    @SerializedName("priceInfo")
    val priceInfo: String,
    @SerializedName("startTime")
    val startTime: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("trips")
    val trips: List<String>,
    @SerializedName("type")
    val type: String,
    @SerializedName("updatedAt")
    val updatedAt: String,
    @SerializedName("__v")
    val v: Int,
    @SerializedName("vin")
    val vin: String,
    @SerializedName("orderId")
    val orderId: String?,
    @SerializedName("orderDetails")
    val orderDetails : OrderDetails?) {

    data class BookingData(
        @SerializedName("endOdo")
        val endOdo: Int,
        @SerializedName("startOdo")
        val startOdo: Int
    )

    data class OrderDetails(
        @SerializedName("status")
        val status: String,
        @SerializedName("paymentLink")
        val paymentLink: String,
    )
}
