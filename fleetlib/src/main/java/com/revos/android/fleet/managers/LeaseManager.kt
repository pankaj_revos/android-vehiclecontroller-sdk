package com.revos.android.fleet.managers

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.UiThread
import com.google.android.gms.maps.model.LatLngBounds
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.revos.android.fleet.FleetUser
import com.revos.android.fleet.R
import com.revos.android.fleet.api.v2.data.*
import com.revos.android.fleet.api.v2.data.GetAllVehiclesModel
import com.revos.android.fleet.data.Booking
import com.revos.android.fleet.api.v2.data.RentalVehicleModel as RentalVehicleV2
import com.revos.android.fleet.data.v2.RentalVehicle
import com.revos.android.fleet.data.legacy.RentalVehicle as RentalVehicleLegacy
import com.revos.android.fleet.eventbus.EventBusMessage
import com.revos.android.fleet.payment.PaymentManager
import com.revos.android.fleet.repository.LeaseRepository
import com.revos.android.fleet.repository.MainRepository
import com.revos.android.fleet.utils.*
import com.revos.android.fleet.utils.EVENT_MESSAGE_TRANSACTION_FAILED
import com.revos.android.fleet.utils.EVENT_MESSAGE_TRANSACTION_SUCCESSFUL
import com.revos.android.fleet.utils.general.PrefUtils
import com.revos.scripts.type.Stage
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

object LeaseManager : PaymentManager() {

    interface Callback {
        fun onVehiclesAvailable(rentalVehicles: List<RentalVehicle>){}
        fun onVehicleAvailable(rentalVehicle : RentalVehicle){}
        fun onBookingsAvailable(bookings : List<Booking>){}
        fun onBookingAvailable(booking : Booking){}
        fun onBookingCreated(booking : Booking){}
        fun onBookingSuccessful(booking : Booking){}
        fun onPaymentFailed(booking : Booking){}
        fun onBookingStarted(bookingId : String){}
        fun onBookingEnded(bookingId : String){}
        fun onBookingCancelled(bookingId : String){}
        fun onVehicleLocked(bookingId : String){}
        fun onVehicleUnLocked(bookingId : String){}
        fun onPaymentPending(){}
        fun onBookingUpdated(booking: Booking){}
        fun onPaymentInitialised(orderId : String, paymentLink : String){}
        fun onPaymentSuccess(){}
        fun onError(message: String?){}
    }

    private val leaseRepository: LeaseRepository = LeaseRepository()
    private val mainRepository: MainRepository = MainRepository()
    private val callbacks = HashSet<Callback>()
    private var fleetManagerJob: Job? = null
    private var fleetUser : FleetUser? = null
    var currentBooking : Booking? = null
    var bookingsMap = HashMap<String, Booking>()

    fun addCallback(callback : Callback){
        callbacks.add(callback)
    }

    fun removeCallback(callback: Callback){
        callbacks.remove(callback)
    }

    init {
        //register with the EventBus
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    /**
     * @suppress
     * */
    @Subscribe
    fun onEventBusMessage(event: EventBusMessage) {
        if (event.status == EVENT_MESSAGE_TRANSACTION_FAILED) {
            updateBookingPaymentFailed(fleetUser!!, event.message)
        } else if (event.status == EVENT_MESSAGE_TRANSACTION_SUCCESSFUL) {
            updateBookingPaymentConfirm(fleetUser!!, event.message)
        }
    }

    private fun updateBookingPaymentFailed(fleetUser: FleetUser, bookingId : String) {
        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        val bearerToken = "Bearer $userToken"
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val paymentFailedResponse =
                    leaseRepository.paymentFailed(bookingId = bookingId, fleetUser.appToken, bearerToken)
                if (isPaymentFailed(paymentFailedResponse.status)) {
                    for (callback in callbacks) {
                        callback.onPaymentFailed(bookingNetworkMapper(paymentFailedResponse))
                    }
                }
            } catch (e: Exception) {
                Timber.e(e)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(e.message)
                    }
                }
            }
        }
    }

    private fun isVehicleBooked(status : String) : Boolean {
        return status == Booking.Status.BOOKED.name
    }

    private fun isBookingTerminated(status : String) : Boolean {
        return status == Booking.Status.TERMINATED.name
    }

    private fun isBookingEnded(status : String) : Boolean {
        return status == Booking.Status.ENDED.name
    }

    private fun isBookingActive(status : String) : Boolean {
        return status == Booking.Status.ACTIVE.name
    }
    private fun isPaymentFailed(status : String) : Boolean {
        return status == Booking.Status.TERMINATED_FAILED_PAYMENT.name
    }


    private fun updateBookingPaymentConfirm(fleetUser: FleetUser, bookingId: String) {

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return

        val bearerToken = "Bearer $userToken"

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val paymentConfirmResponse =
                    leaseRepository.paymentConfirm(bookingId = bookingId, fleetUser.appToken, bearerToken, null)
                if (isVehicleBooked(paymentConfirmResponse.status)) {
                    currentBooking = bookingNetworkMapper(paymentConfirmResponse)
                    bookingsMap[currentBooking!!.id!!] = currentBooking!!
                    withContext(Dispatchers.Main){
                        for (callback in callbacks) {
                            callback.onBookingSuccessful(bookingNetworkMapper(paymentConfirmResponse))
                        }
                    }
                }
            } catch (e: Exception) {
                Timber.e(e)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(e.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun getAllRentalVehicle(context: Context, latLngBounds: LatLngBounds ){

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                //todo : update to use lease api once ready
                val listOfVehicle = mainRepository.fetchAllRentalVehicles(context, latLngBounds)
                var vehicles = ArrayList<RentalVehicle>()
                for(vehicle in listOfVehicle){
                    vehicles.add(rentalVehicleNetworkMapper(vehicle))
                }
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onVehiclesAvailable(vehicles)
                    }
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun getRentalVehicle(fleetUser: FleetUser, vin : String ){

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val vehicle = leaseRepository.getRentalVehicle(vin, fleetUser.appToken, "Bearer $userToken")
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onVehicleAvailable(rentalVehicleNetworkMapper(vehicle.data))
                    }
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun getAllBookings(fleetUser: FleetUser, first: Int, skip: Int) {

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return

        val bearerToken = "Bearer $userToken"

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val booking = leaseRepository.getAllBookings(first, skip, fleetUser.appToken, bearerToken)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onBookingsAvailable(getBookings(booking))
                    }
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun getBooking(fleetUser: FleetUser, bookingId: String) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return
        val bearerToken = "Bearer $userToken"

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val booking = leaseRepository.getBooking(bookingId, fleetUser.appToken, bearerToken)
                bookingsMap[booking.data.bookingDetails.id] = bookingNetworkMapper(booking.data.bookingDetails)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onBookingAvailable(bookingNetworkMapper(booking.data.bookingDetails))
                    }
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun createBooking(fleetUser: FleetUser, booking: Booking, activity : Activity) {

        this.fleetUser = fleetUser

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return
        val bearerToken = "Bearer $userToken"

        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("amount", booking.amount)
        bodyJsonObject.addProperty("vin", booking.vin)
        bodyJsonObject.addProperty("startTime", getDateInUTC(booking.startTime))
        bodyJsonObject.addProperty("endTime", getDateInUTC(booking.endTime))
        bodyJsonObject.addProperty("type", booking.type.name)
        bodyJsonObject.addProperty("remarks", booking.remarks)
        bodyJsonObject.addProperty("amountPaid", booking.amount)

        var stage = Stage.TEST
        if (fleetUser.context.getString(R.string.cf_stage) == "PROD") {
            stage = Stage.PROD
        }
        bodyJsonObject.addProperty("stage", stage.name)
        bodyJsonObject.addProperty("priceInfo", booking.priceInfo)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val bookingResponse = leaseRepository.createBooking(fleetUser.appToken, bearerToken, requestBody)

                currentBooking = bookingNetworkMapper(bookingResponse)

                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onBookingCreated(currentBooking!!)
                    }
                }
                if(bookingResponse.status == Booking.Status.PENDING_PAYMENT.name){

                    val bodyJsonPaymentInit = JsonObject()
                    bodyJsonPaymentInit.addProperty("amount", booking.amount)
                    bodyJsonPaymentInit.addProperty("amountPaid", 0)
                    bodyJsonPaymentInit.addProperty("remarks", booking.remarks)
                    var stage = Stage.TEST
                    if (fleetUser.context.getString(R.string.cf_stage) == "PROD") {
                        stage = Stage.PROD
                    }
                    bodyJsonPaymentInit.addProperty("stage", stage.name)

                    val user = PrefUtils.getInstance(fleetUser.context).userFromPrefs
                    val requestBodyPaymentInit = bodyJsonPaymentInit.toString().toRequestBody("application/json".toMediaTypeOrNull())
                    val response = leaseRepository.paymentInit(bookingId = bookingResponse.id,
                        fleetUser.appToken, bearerToken, requestBodyPaymentInit )

                    if(response.orderDetails != null && response.orderId != null){
                        withContext(Dispatchers.Main){
                            for(callback in callbacks){
                                callback.onPaymentInitialised(response.orderId, response.orderDetails.paymentLink)
                            }
                        }
                    }else{
                        //todo : to check from updated api response
                        /*choosePaymentMethodAndPay(booking, activity,
                            response.cftoken, response.vendorSplit, user )*/
                    }
                }else{
                    throw Exception("Failed to book a vehicle. status: ${bookingResponse.status}")
                }
            }catch (exc: Exception) {
                Timber.e(exc)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    private fun getDateInUTC(date: Date): String {
        val outputFmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        outputFmt.timeZone = TimeZone.getTimeZone("UTC")
        return outputFmt.format(date)
    }

    @UiThread
    fun getPaymentStatus(fleetUser: FleetUser, orderId : String, booking: Booking ){

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val bodyJsonObject = JsonObject()
                bodyJsonObject.addProperty("orderId", orderId)
                val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())

                val orderResponse = leaseRepository.getOrderStatus(fleetUser.appToken, userToken, requestBody)
                withContext(Dispatchers.Main){
                    if(isPaymentCompleted(orderResponse.orderStatus)){

                        val bodyJsonPaymentConfirm = JsonObject()
                        bodyJsonPaymentConfirm.addProperty("paymentOrderId", orderId)
                        val requestBodyPaymentConfirm = bodyJsonPaymentConfirm.toString().toRequestBody("application/json".toMediaTypeOrNull())

                        val paymentConfirmResponse = leaseRepository.paymentConfirm(booking.id!!, fleetUser.appToken, userToken, requestBodyPaymentConfirm)
                        if (isVehicleBooked(paymentConfirmResponse.status)) {
                            currentBooking = bookingNetworkMapper(paymentConfirmResponse)
                            withContext(Dispatchers.Main){
                                for (callback in callbacks) {
                                    callback.onBookingSuccessful(bookingNetworkMapper(paymentConfirmResponse))
                                }
                            }
                        }
                    }else{
                        for(callback in callbacks){
                            callback.onPaymentPending()
                        }
                    }
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    private fun isPaymentCompleted(orderStatus : String) : Boolean {
        return (orderStatus == GetOrderStatusResponseModel.Details.OrderDetails.ORDER_STATUS.PAID.name)
    }

    private fun isPaymentPending(orderStatus : String) : Boolean {
        return (orderStatus == GetOrderStatusResponseModel.Details.OrderDetails.ORDER_STATUS.ACTIVE.name)
    }

    @UiThread
    fun startBooking(fleetUser: FleetUser, bookingId : String) {

        this.fleetUser = fleetUser

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return
        val bearerToken = "Bearer $userToken"

        if(!bookingsMap.containsKey(bookingId)){
            for(callback in callbacks){
                callback.onError("No booking found with booking Id : $bookingId")
            }
            return
        }

        val bookingToStart = bookingsMap[bookingId]
        if(bookingToStart == null || bookingToStart.status != Booking.Status.BOOKED){
            for(callback in callbacks){
                callback.onError("Please pay to book vehicle")
            }
            return
        }

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                var bookingResponse : BookingDetails? = null
                try{
                    bookingResponse = leaseRepository.startBooking(bookingId, fleetUser.appToken, bearerToken)
                }catch (ex : Exception){
                    Timber.e(ex.message)
                    cancel()
                }

                if(bookingResponse != null && isBookingActive(bookingResponse.status)){

                    val call = leaseRepository.unlockVehicle(bookingResponse.vin, fleetUser.appToken, bearerToken)
                    call.enqueue(object :  retrofit2.Callback<ResponseBody> {

                        override fun onResponse( call: Call<ResponseBody>,
                                                 response: Response<ResponseBody> ) {
                            val body = response.body()

                            if(response.code() != 200){
                                val bodyJsonObject = JsonParser.parseString(body?.string()).asJsonObject
                                for(callback in callbacks){
                                    callback.onError(bodyJsonObject.get("message").asString)
                                }
                                cancel()
                            }else{
                                for(callback in callbacks){
                                    callback.onBookingStarted(bookingResponse.id)
                                }
                            }
                        }

                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            for(callback in callbacks){
                                callback.onError("Failed to unlock vehicle")
                            }
                            cancel()
                        }
                    })

                }else{
                    throw Exception("Failed to start booking}")
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun endBooking(fleetUser: FleetUser, vin: String, bookingId : String ) {

        this.fleetUser = fleetUser

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return
        val bearerToken = "Bearer $userToken"

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{

                val call = leaseRepository.lockVehicle(vin, fleetUser.appToken, bearerToken)
                call.enqueue(object :  retrofit2.Callback<ResponseBody> {

                    override fun onResponse( call: Call<ResponseBody>,
                                             response: Response<ResponseBody> ) {
                        val body = response.body()

                        if(response.code() != 200){
                            val bodyJsonObject = JsonParser.parseString(body?.string()).asJsonObject
                            for(callback in callbacks){
                                callback.onError(bodyJsonObject.get("message").asString)
                            }
                            cancel()
                        }else{
                            CoroutineScope(Dispatchers.IO).launch{
                                val bookingResponse = async {
                                    leaseRepository.endBooking(bookingId, fleetUser.appToken, bearerToken)
                                }.await()

                                if(isBookingEnded(bookingResponse.status)){
                                    withContext(Dispatchers.Main){
                                        for(callback in callbacks){
                                            callback.onBookingEnded(bookingResponse.id)
                                        }
                                    }
                                }else{
                                    throw Exception("Failed to end booking. status: ${bookingResponse.status}")
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        for(callback in callbacks){
                            callback.onError("Failed to lock vehicle")
                        }
                        cancel()
                    }
                })
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun cancelBooking(fleetUser: FleetUser, bookingId : String,
                      paymentOrderId : String, refundAmount : Int) {

        this.fleetUser = fleetUser

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return
        val bearerToken = "Bearer $userToken"

        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("paymentOrderId", paymentOrderId )
        bodyJsonObject.addProperty("refundAmount", refundAmount)
        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val cancelBookingResponse = async {
                    leaseRepository.cancelBooking(bookingId, fleetUser.appToken, bearerToken, requestBody)
                }.await()

                if(isBookingTerminated(cancelBookingResponse.status)){
                    withContext(Dispatchers.Main){
                        for(callback in callbacks){
                            callback.onBookingCancelled(cancelBookingResponse.id)
                        }
                    }
                }else{
                    throw Exception("Failed to start booking. status: ${cancelBookingResponse.status}")
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun modifyBooking(fleetUser: FleetUser, booking: Booking) {

        this.fleetUser = fleetUser

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return
        val bearerToken = "Bearer $userToken"

        val bodyJsonObject = JsonObject()

        bodyJsonObject.addProperty("amount", booking.amount)
        bodyJsonObject.addProperty("vin", booking.vin)
        bodyJsonObject.addProperty("startTime", getDateInUTC(booking.startTime))
        bodyJsonObject.addProperty("endTime", getDateInUTC(booking.endTime))
        bodyJsonObject.addProperty("type", booking.type.name)
        bodyJsonObject.addProperty("remarks", booking.remarks)
        bodyJsonObject.addProperty("amountPaid", booking.amount)

        var stage = Stage.TEST
        if (fleetUser.context.getString(R.string.cf_stage) == "PROD") {
            stage = Stage.PROD
        }

        bodyJsonObject.addProperty("stage", stage.name)
        bodyJsonObject.addProperty("priceInfo", booking.priceInfo)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val modifiedBookingResponse = async {
                    leaseRepository.modifyBooking(booking.id!!, fleetUser.appToken, bearerToken, requestBody)
                }.await()

                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onBookingUpdated(bookingNetworkMapper(modifiedBookingResponse))
                    }
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    internal fun lockVehicle(fleetUser: FleetUser, vin : String, appToken: String) {

        this.fleetUser = fleetUser

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }

        val userToken = PrefUtils.getInstance(fleetUser.context).authTokenFromPrefs
        if(userToken.isNull()) return
        val bearerToken = "Bearer $userToken"

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val call = leaseRepository.lockVehicle(vin, fleetUser.appToken, bearerToken)
                call.enqueue(object :  retrofit2.Callback<ResponseBody> {

                    override fun onResponse( call: Call<ResponseBody>,
                                             response: Response<ResponseBody> ) {
                        val body = response.body()

                        if(response.code() != 200){
                            val bodyJsonObject = JsonParser.parseString(body?.string()).asJsonObject
                            for(callback in callbacks){
                                callback.onError(bodyJsonObject.get("message").asString)
                            }
                            cancel()
                        }else{
                            for(callback in callbacks){
                                callback.onVehicleLocked("Vehicle Locked")
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        for(callback in callbacks){
                            callback.onError("Failed to lock vehicle")
                        }
                        cancel()
                    }
                })
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    internal fun unlockVehicle(fleetUser: FleetUser, vin : String, appToken: String) {

        this.fleetUser = fleetUser

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        val sharedPreferences: SharedPreferences =
            fleetUser.context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)

        val userToken = sharedPreferences.getString(USER_TOKEN_KEY, null) ?: return
        val bearerToken = "Bearer $userToken"

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val call = leaseRepository.unlockVehicle(vin, fleetUser.appToken, bearerToken)
                call.enqueue(object :  retrofit2.Callback<ResponseBody> {

                    override fun onResponse( call: Call<ResponseBody>,
                                             response: Response<ResponseBody> ) {
                        val body = response.body()

                        if(response.code() != 200){
                            val bodyJsonObject = JsonParser.parseString(body?.string()).asJsonObject
                            for(callback in callbacks){
                                callback.onError(bodyJsonObject.get("message").asString)
                            }
                            cancel()
                        }else{
                            for(callback in callbacks){
                                callback.onVehicleUnLocked("Vehicle unlocked")
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        for(callback in callbacks){
                            callback.onError("Failed to unlock vehicle")
                        }
                        cancel()
                    }
                })
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    for(callback in callbacks){
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }


    private fun getBookings(bookingsFromServer : List<BookingDetails>) : List<Booking> {
        val bookings = ArrayList<Booking>()
        for(booking in bookingsFromServer){
            val bookingObj = bookingNetworkMapper(booking)
            bookings.add(bookingObj)
           //bookingsMap[bookingObj.id!!] = bookingObj (Not all objects has booking id right now )
        }
        return bookings
    }

    private fun getBookingType(type : String) : Booking.TYPE {
        if(type == Booking.TYPE.LONG_TERM.name){
            return Booking.TYPE.LONG_TERM
        }
        return Booking.TYPE.SHORT_TERM
    }

    private fun getBookingStatus(status : String) : Booking.Status {
        when (status) {
            Booking.Status.PAYMENT_INITIALISED.name -> {
                return Booking.Status.PAYMENT_INITIALISED
            }
            Booking.Status.ACTIVE.name -> {
                return Booking.Status.ACTIVE
            }
            Booking.Status.ENDED.name -> {
                return Booking.Status.ENDED
            }
            Booking.Status.BOOKED.name -> {
                return Booking.Status.BOOKED
            }
            Booking.Status.PENDING_PAYMENT.name -> {
                return Booking.Status.PENDING_PAYMENT
            }
            else -> return Booking.Status.TERMINATED
        }
    }

    private fun bookingNetworkMapper(bookingFromServer : BookingDetails) : Booking {
        val booking = Booking(
            bookingTime = DateTime(bookingFromServer.bookingTime).toDate(),
            createdAt = DateTime(bookingFromServer.createdAt).toDate(),
            endTime = DateTime(bookingFromServer.endTime).toDate(),
            startTime = DateTime(bookingFromServer.startTime).toDate(),
            id = bookingFromServer.id,
            type = getBookingType(bookingFromServer.type),
            vin = bookingFromServer.vin,
            amount = 0.0f,
            remarks = "NA"
        )
        booking.asset = bookingFromServer.asset
        booking.leasee = bookingFromServer.leasee
        booking.leasor = bookingFromServer.leasor
        booking.priceInfo = bookingFromServer.priceInfo
        booking.status = getBookingStatus(bookingFromServer.status)
        booking.trips = bookingFromServer.trips
        booking.v = bookingFromServer.v
        booking.updatedAt = bookingFromServer.updatedAt

        return booking
    }

    private fun rentalVehicleNetworkMapper(vehicleFromServer : GetAllVehiclesModel.Data.Vehicle) : RentalVehicle {
        return RentalVehicle(
            isDeliverable = vehicleFromServer.isDeliverable,
            vin = vehicleFromServer.vin,
            modelTag = vehicleFromServer.modelTag,
            rentalStatus = vehicleFromServer.rentalStatus,
            status = vehicleFromServer.status,
            lastMarkedLatitude = vehicleFromServer.lastMarkedLatitude,
            lastMarkedLongitude = vehicleFromServer.lastMarkedLongitude,
            createdAt = vehicleFromServer.createdAt,
            isHealthy = vehicleFromServer.isHealthy,
            id = vehicleFromServer.id,
            pricingInfo = null,
            updatedAt = vehicleFromServer.updatedAt
        )
    }

    private fun rentalVehicleNetworkMapper(vehicleFromServer : RentalVehicleLegacy) : RentalVehicle {
        return RentalVehicle(
            isDeliverable = vehicleFromServer.isDeliverable,
            vin = vehicleFromServer.vin,
            modelTag = vehicleFromServer.model?.name,
            rentalStatus = vehicleFromServer.rentalStatus,
            status = vehicleFromServer.status,
            lastMarkedLatitude = vehicleFromServer.latitude,
            lastMarkedLongitude = vehicleFromServer.longitude,
            createdAt = null,
            isHealthy = true,
            id = vehicleFromServer.vin,
            pricingInfo = null,
            updatedAt = null,
        )
    }

    private fun rentalVehicleNetworkMapper(vehicleFromServer : RentalVehicleV2.Data) : RentalVehicle {
        return RentalVehicle(
            isDeliverable = vehicleFromServer.isDeliverable,
            vin = vehicleFromServer.vin,
            modelTag = vehicleFromServer.modelTag,
            rentalStatus = vehicleFromServer.rentalStatus,
            status = vehicleFromServer.status,
            lastMarkedLongitude = vehicleFromServer.lastMarkedLatitude,
            lastMarkedLatitude = vehicleFromServer.lastMarkedLongitude,
            createdAt = vehicleFromServer.createdAt,
            isHealthy = vehicleFromServer.isHealthy,
            id = vehicleFromServer.id,
            pricingInfo = getPricingInfo(vehicleFromServer.pricingInfo),
            updatedAt = vehicleFromServer.updatedAt
        )
    }

    private fun getPricingInfo(pricing : List<RentalVehicleV2.Data.PricingInfo>) : ArrayList<RentalVehicle.PricingInfo> {
        val pricingInfo = ArrayList<RentalVehicle.PricingInfo>()
        for(pInfo in pricing){
            val obj = RentalVehicle.PricingInfo(amountPayable = pInfo.amountPayable,
                                                baseAmount = pInfo.baseAmount,
                                                costPerUnit = pInfo.costPerUnit,
                createdAt = pInfo.createdAt,
                id = pInfo.id,
                minimumPayableAmount = pInfo.minimumPayableAmount,
                name = pInfo.name,
                status = pInfo.status,
                type = pInfo.type,
                unit = pInfo.unit,
                updatedAt = pInfo.updatedAt
            )
            pricingInfo.add(obj)
        }
        return pricingInfo
    }

}