package com.revos.android.fleet.api.v2.data


import com.google.gson.annotations.SerializedName

data class GetOrderStatusResponseModel(
    @SerializedName("details")
    val details: Details,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("traceId")
    val traceId: String) {

    data class Details(
        @SerializedName("orderDetails")
        val orderDetails: OrderDetails,
        @SerializedName("orderId")
        val orderId: String) {

        data class OrderDetails(
            @SerializedName("orderAmount")
            val orderAmount: String,
            @SerializedName("orderExpiryTime")
            val orderExpiryTime: String,
            @SerializedName("orderStatus")
            val orderStatus: String,
            @SerializedName("status")
            val status: String,
            @SerializedName("txStatus")
            val txStatus: String,
            @SerializedName("txTime")
            val txTime: String,
            @SerializedName("txMsg")
            val txMsg: String,
            @SerializedName("paymentMode")
            val paymentMode: String,
            @SerializedName("orderCurrency")
            val orderCurrency: String,
            @SerializedName("paymentDetails")
            val paymentDetails : PaymentDetails) {

            data class PaymentDetails(
                @SerializedName("payersVPA")
                val payersVPA: String,
                @SerializedName("utr")
                val utr: String,
            )
            enum class ORDER_STATUS { ACTIVE , PAID, PENDING  }
        }
    }
}