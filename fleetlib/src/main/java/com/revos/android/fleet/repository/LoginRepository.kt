package com.revos.android.fleet.repository

import com.revos.android.fleet.api.v2.auth.LoginResponseModel
import com.revos.android.fleet.api.v2.auth.RevosAuthService
import okhttp3.RequestBody

internal class LoginRepository {
    private var revosAuthService : RevosAuthService = RevosAuthService.create()

    suspend fun openRegister(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.openRegisterUser(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun openLogin(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.openLoginUser(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun registerUsingFirebase(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.registerUserWithFirebase(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun loginUsingFirebase(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.loginUserWithFirebase(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }
}