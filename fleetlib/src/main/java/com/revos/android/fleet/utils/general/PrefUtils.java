package com.revos.android.fleet.utils.general;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.fleet.utils.ConstantsKt.ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.APP_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.DEVICE_IS_ODO_RESET_REQUIRED_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.DEVICE_JSON_DATA_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.DEVICE_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.GENERAL_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.LIVE_PACKET_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.LOG_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.MODEL_IMAGE_HASHMAP_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.NAV_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.NETWORKING_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.PHONE_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.TRACKING_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_DATA_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.TRIP_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.USER_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.USER_SETTINGS_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.USER_TOKEN_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.VEHICLE_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.VEHICLE_LAST_KNOWN_LATITUDE;
import static com.revos.android.fleet.utils.ConstantsKt.VEHICLE_LAST_KNOWN_LONGITUDE;
import static com.revos.android.fleet.utils.ConstantsKt.VEHICLE_LAST_KNOWN_SPEED;
import static com.revos.android.fleet.utils.ConstantsKt.VEHICLE_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.VEHICLE_STATUS_BATTERY_KEY;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.revos.android.fleet.data.DataFeedVehicleSnapshot;
import com.revos.android.fleet.data.Device;
import com.revos.android.fleet.data.Model;
import com.revos.android.fleet.data.TripDataFeed;
import com.revos.android.fleet.data.legacy.RentalVehicle;
import com.revos.android.fleet.data.User;
import com.revos.android.fleet.data.Vehicle;
import com.revos.android.fleet.utils.ConstantsKt;
import com.revos.scripts.FetchDeviceQuery;
import com.revos.scripts.FetchVehicleQuery;
import com.revos.scripts.SetOdoResetFlagMutation;
import com.revos.scripts.SetPinMutation;
import com.revos.scripts.SetVehicleConfigMutation;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

public class PrefUtils {

    //holds the single instance
    private static PrefUtils mInstance = null;

    //private constructor
    private PrefUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static PrefUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PrefUtils(context);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    //stores the context
    private Context mContext = null;

    private void setContext(Context context) {
        mContext = context;
    }

    String PREF_KEY_USER_OBJ = "userObj";
    String USER_PREFERENCES = "userSharedPrefs";

    public void saveAuthTokenToPrefs(String authToken){
        SharedPreferences prefs = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
        prefs.edit().putString(USER_TOKEN_KEY, authToken).apply();
    }

    public String getAuthTokenFromPrefs() {
        return mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                .getString(USER_TOKEN_KEY, null);
    }

    public User getUserFromPrefs() {
        String userObj = mContext.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE)
                .getString(PREF_KEY_USER_OBJ, null);
        return new Gson().fromJson(userObj, User.class);
    }

    public void saveUserToPrefs(User user){
        SharedPreferences prefs = mContext.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        prefs.edit().putString(PREF_KEY_USER_OBJ, new Gson().toJson(user)).apply();
    }

    public Vehicle getVehicleObjectFromPrefs() {
        String vehicleStr = mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).getString(VEHICLE_JSON_KEY, null);

        if (vehicleStr == null) {
            return null;
        }
        try {
            return new Gson().fromJson(vehicleStr, Vehicle.class);
        } catch (Exception e) {
            return null;
        }
    }

    public DataFeedVehicleSnapshot getLivePacketObjectFromPrefs() {
        String livePacketStr = mContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).getString(LIVE_PACKET_JSON_KEY, null);

        if (livePacketStr == null) {
            return null;
        }
        try {
            return new Gson().fromJson(livePacketStr, DataFeedVehicleSnapshot.class);
        } catch (Exception e) {
            return null;
        }
    }

    public Device getDeviceObjectFromPrefs() {
        String deviceStr = mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_JSON_DATA_KEY, null);

        if (deviceStr == null) {
            return null;
        }
        try {
            return new Gson().fromJson(deviceStr, Device.class);
        } catch (Exception e) {
            return null;
        }
    }

    public User getUserObjectFromPrefs() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE);
        String userDataJSON = sharedPreferences.getString(ConstantsKt.USER_JSON_DATA_KEY, null);
        if (userDataJSON == null) {
            return null;
        } else {
            try {
                User user = new Gson().fromJson(userDataJSON, User.class);
                return user;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public HashMap<String, String> getModelImageHashMapObjectFromPrefs() {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE);
        String modelImagesStr = sharedPreferences.getString(MODEL_IMAGE_HASHMAP_KEY, null);

        if (modelImagesStr == null || modelImagesStr.isEmpty()) {
            return null;
        } else {
            try {
                HashMap<String, String> modelsImagesHashMap = new HashMap<>();
                modelsImagesHashMap = new Gson().fromJson(modelImagesStr, new TypeToken<HashMap<String, String>>() {
                }.getType());

                return modelsImagesHashMap;

            } catch (Exception e) {
                return null;
            }
        }
    }

    public void updateModelImageHashMap(String modelId, String imageBuffer) {

        HashMap<String, String> modelImagesHashMap = PrefUtils.getInstance(mContext).getModelImageHashMapObjectFromPrefs();

        if (modelImagesHashMap == null) {
            modelImagesHashMap = new HashMap<>();
        }

        if (imageBuffer == null || imageBuffer.isEmpty()) {
            modelImagesHashMap.put(modelId, null);
        } else {
            modelImagesHashMap.put(modelId, imageBuffer);
        }

        mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(MODEL_IMAGE_HASHMAP_KEY, new Gson().toJson(modelImagesHashMap)).apply();
    }

    public void updateTripData(ArrayList<TripDataFeed> tripListToBeSaved) {
        mContext.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE)
                .edit().putString(TRIP_DATA_KEY, new Gson().toJson(tripListToBeSaved)).apply();
    }

    public ArrayList<TripDataFeed> getTripData() {
        String tripData = mContext.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE)
                .getString(TRIP_DATA_KEY, null);

        return new Gson().fromJson(tripData,
                new TypeToken<ArrayList<TripDataFeed>>() {}.getType());
    }

    public void clearAllPrefs() {
        mContext.getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(PHONE_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();
    }

    public void clearOnGoingRentalPrefs() {
        mContext.getSharedPreferences(ConstantsKt.DEVICE_PREFS, Context.MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(ConstantsKt.VEHICLE_PREFS, Context.MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(ConstantsKt.TRIP_PREFS, Context.MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(ConstantsKt.GEO_FENCING_PREFS, Context.MODE_PRIVATE).edit().clear().apply();
        mContext.getSharedPreferences(ConstantsKt.USER_PREFS, Context.MODE_PRIVATE).edit().putString(ConstantsKt.ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
    }

    public Vehicle saveVehicleVariablesInPrefs(FetchVehicleQuery.Get fetchedVehicle) {
        try {

            Vehicle vehicle = new Vehicle();
            Model model = new Model();

            if (fetchedVehicle != null) {

                if (fetchedVehicle.vin() != null) {
                    vehicle.setVin(fetchedVehicle.vin());
                }
                if (fetchedVehicle.metrics() != null && fetchedVehicle.metrics().odometer() != null) {
                    vehicle.setOdoValue(fetchedVehicle.metrics().odometer());
                }
                if (fetchedVehicle.status() != null) {
                    vehicle.setStatus(fetchedVehicle.status());
                }
                if (fetchedVehicle.owner() != null && fetchedVehicle.owner().id() != null) {
                    vehicle.setOwnerId(fetchedVehicle.owner().id());
                }
                if (fetchedVehicle.owner() != null && fetchedVehicle.owner().firstName() != null) {
                    vehicle.setOwnerFirstName(fetchedVehicle.owner().firstName());
                }
                if (fetchedVehicle.owner() != null && fetchedVehicle.owner().lastName() != null) {
                    vehicle.setOwnerLastName(fetchedVehicle.owner().lastName());
                }
                if (fetchedVehicle.owner() != null && fetchedVehicle.owner().email() != null) {
                    vehicle.setOwnerEmailId(fetchedVehicle.owner().email());
                }
                if (fetchedVehicle.owner() != null && fetchedVehicle.owner().phone() != null) {
                    vehicle.setOwnerPhoneNo(fetchedVehicle.owner().phone());
                }
                if (fetchedVehicle.buyer() != null && fetchedVehicle.buyer().firstName() != null) {
                    vehicle.setBuyerFirstName(fetchedVehicle.buyer().firstName());
                }
                if (fetchedVehicle.buyer() != null && fetchedVehicle.buyer().lastName() != null) {
                    vehicle.setBuyerLastName(fetchedVehicle.buyer().lastName());
                }
                if (fetchedVehicle.buyer() != null && fetchedVehicle.buyer().phone() != null) {
                    vehicle.setBuyerPhoneNo(fetchedVehicle.buyer().phone());
                }
                if (fetchedVehicle.id() != null) {
                    vehicle.setVehicleId(fetchedVehicle.id());
                }
                if (fetchedVehicle.distributor() != null) {
                    if (fetchedVehicle.distributor().name() != null) {
                        vehicle.setDistributorName(fetchedVehicle.distributor().name());
                    }
                    if (fetchedVehicle.distributor().phone() != null) {
                        vehicle.setDistributorPhoneNo1(fetchedVehicle.distributor().phone());
                    }
                    if (fetchedVehicle.distributor().phone1() != null) {
                        vehicle.setDistributorPhoneNo2(fetchedVehicle.distributor().phone1());
                    }
                    if (fetchedVehicle.distributor().address() != null) {
                        vehicle.setDistributorAddress(fetchedVehicle.distributor().address());
                    }
                }

                //set vehicle config
                if (fetchedVehicle.config() != null && fetchedVehicle.config().childSpeedLock() != null) {
                    vehicle.setChildSpeedLock(fetchedVehicle.config().childSpeedLock());
                } else {
                    int speedLockValue = 100;
                    if (fetchedVehicle.device() != null
                            && fetchedVehicle.device().vehicle() != null
                            && fetchedVehicle.device().vehicle().model() != null
                            && fetchedVehicle.device().vehicle().model().config() != null
                            && fetchedVehicle.device().vehicle().model().config().speedLimit() != null) {
                        speedLockValue = fetchedVehicle.device().vehicle().model().config().speedLimit().intValue();
                    }
                    vehicle.setChildSpeedLock(speedLockValue);
                }
                if (fetchedVehicle.config() != null && fetchedVehicle.config().followMeHomeHeadLamp() != null) {
                    vehicle.setFollowMeHeadlamp(fetchedVehicle.config().followMeHomeHeadLamp());
                }

                //set vehicle warranty
                if (fetchedVehicle.warranty() != null && fetchedVehicle.warranty().expiry() != null) {
                    vehicle.setWarrantyExpiryTime(fetchedVehicle.warranty().expiry());
                }
                if (fetchedVehicle.warranty() != null && fetchedVehicle.warranty().status() != null) {
                    vehicle.setWarrantyStatus(fetchedVehicle.warranty().status());
                }

                //set vehicle invoice details
                if (fetchedVehicle.invoice() != null && fetchedVehicle.invoice().createdAt() != null) {
                    vehicle.setInvoiceCreationDate(fetchedVehicle.invoice().createdAt());
                }
                if (fetchedVehicle.invoice() != null && fetchedVehicle.invoice().passbook() != null
                        && !fetchedVehicle.invoice().passbook().isEmpty() && fetchedVehicle.invoice().passbook().get(0).status() != null) {
                    vehicle.setInvoiceStatus(fetchedVehicle.invoice().passbook().get(0).status());
                }

                //set model specific data
                if (fetchedVehicle.model() != null && fetchedVehicle.model().id() != null) {
                    model.setId(fetchedVehicle.model().id());
                }
                if (fetchedVehicle.model() != null && fetchedVehicle.model().name() != null) {
                    model.setName(fetchedVehicle.model().name());
                }
                if (fetchedVehicle.model() != null && fetchedVehicle.model().protocol() != null) {
                    model.setProtocol(fetchedVehicle.model().protocol());
                }
            }

            vehicle.setModel(model);

            String vehicleJSONStr = new Gson().toJson(vehicle);

            mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_JSON_KEY, vehicleJSONStr).apply();

            return vehicle;
        } catch (Exception e) {
            return null;
        }
    }

    private Vehicle saveVehicleVariablesInPrefs(FetchDeviceQuery.Get fetchedDevice) {
        try {
            Vehicle vehicle = new Vehicle();
            Model model = new Model();

            if (fetchedDevice.vehicle() != null) {

                if (fetchedDevice.vehicle().vin() != null) {
                    vehicle.setVin(fetchedDevice.vehicle().vin());
                }
                if (fetchedDevice.vehicle().status() != null) {
                    vehicle.setStatus(fetchedDevice.vehicle().status());
                }
                if (fetchedDevice.vehicle().metrics() != null && fetchedDevice.vehicle().metrics().odometer() != null) {
                    vehicle.setOdoValue(fetchedDevice.vehicle().metrics().odometer());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().id() != null) {
                    vehicle.setOwnerId(fetchedDevice.vehicle().owner().id());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().firstName() != null) {
                    vehicle.setOwnerFirstName(fetchedDevice.vehicle().owner().firstName());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().lastName() != null) {
                    vehicle.setOwnerLastName(fetchedDevice.vehicle().owner().lastName());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().email() != null) {
                    vehicle.setOwnerEmailId(fetchedDevice.vehicle().owner().email());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().phone() != null) {
                    vehicle.setOwnerPhoneNo(fetchedDevice.vehicle().owner().phone());
                }
                if (fetchedDevice.vehicle().buyer() != null && fetchedDevice.vehicle().buyer().firstName() != null) {
                    vehicle.setBuyerFirstName(fetchedDevice.vehicle().buyer().firstName());
                }
                if (fetchedDevice.vehicle().buyer() != null && fetchedDevice.vehicle().buyer().lastName() != null) {
                    vehicle.setBuyerLastName(fetchedDevice.vehicle().buyer().lastName());
                }
                if (fetchedDevice.vehicle().buyer() != null && fetchedDevice.vehicle().buyer().phone() != null) {
                    vehicle.setBuyerPhoneNo(fetchedDevice.vehicle().buyer().phone());
                }
                if (fetchedDevice.vehicle().id() != null) {
                    vehicle.setVehicleId(fetchedDevice.vehicle().id());
                }
                if (fetchedDevice.vehicle().distributor() != null) {
                    if (fetchedDevice.vehicle().distributor().name() != null) {
                        vehicle.setDistributorName(fetchedDevice.vehicle().distributor().name());
                    }
                    if (fetchedDevice.vehicle().distributor().phone() != null) {
                        vehicle.setDistributorPhoneNo1(fetchedDevice.vehicle().distributor().phone());
                    }
                    if (fetchedDevice.vehicle().distributor().phone1() != null) {
                        vehicle.setDistributorPhoneNo2(fetchedDevice.vehicle().distributor().phone1());
                    }
                    if (fetchedDevice.vehicle().distributor().address() != null) {
                        vehicle.setDistributorAddress(fetchedDevice.vehicle().distributor().address());
                    }
                }

                //set vehicle config
                if (fetchedDevice.vehicle().config() != null && fetchedDevice.vehicle().config().childSpeedLock() != null) {
                    vehicle.setChildSpeedLock(fetchedDevice.vehicle().config().childSpeedLock());
                } else {
                    int speedLockValue = 100;
                    if (fetchedDevice != null
                            && fetchedDevice.vehicle() != null
                            && fetchedDevice.vehicle().model() != null
                            && fetchedDevice.vehicle().model().config() != null
                            && fetchedDevice.vehicle().model().config().speedLimit() != null) {
                        speedLockValue = fetchedDevice.vehicle().model().config().speedLimit().intValue();
                    }
                    vehicle.setChildSpeedLock(speedLockValue);
                }
                if (fetchedDevice.vehicle().config() != null && fetchedDevice.vehicle().config().followMeHomeHeadLamp() != null) {
                    vehicle.setFollowMeHeadlamp(fetchedDevice.vehicle().config().followMeHomeHeadLamp());
                }

                //set vehicle warranty
                if (fetchedDevice.vehicle().warranty() != null && fetchedDevice.vehicle().warranty().expiry() != null) {
                    vehicle.setWarrantyExpiryTime(fetchedDevice.vehicle().warranty().expiry());
                }
                if (fetchedDevice.vehicle().warranty() != null && fetchedDevice.vehicle().warranty().status() != null) {
                    vehicle.setWarrantyStatus(fetchedDevice.vehicle().warranty().status());
                }

                //set vehicle invoice details
                if (fetchedDevice.vehicle().invoice() != null && fetchedDevice.vehicle().invoice().createdAt() != null) {
                    vehicle.setInvoiceCreationDate(fetchedDevice.vehicle().invoice().createdAt());
                }
                if (fetchedDevice.vehicle().invoice() != null && fetchedDevice.vehicle().invoice().passbook() != null
                        && !fetchedDevice.vehicle().invoice().passbook().isEmpty() && fetchedDevice.vehicle().invoice().passbook().get(0).status() != null) {
                    vehicle.setInvoiceStatus(fetchedDevice.vehicle().invoice().passbook().get(0).status());
                }

                //set model specific data
                if (fetchedDevice.vehicle().model() != null && fetchedDevice.vehicle().model().id() != null) {
                    model.setId(fetchedDevice.vehicle().model().id());
                }
                if (fetchedDevice.vehicle().model() != null && fetchedDevice.vehicle().model().name() != null) {
                    model.setName(fetchedDevice.vehicle().model().name());
                }
                if (fetchedDevice.vehicle().model() != null && fetchedDevice.vehicle().model().protocol() != null) {
                    model.setProtocol(fetchedDevice.vehicle().model().protocol());
                }
            }

            vehicle.setModel(model);

            String vehicleJSONStr = new Gson().toJson(vehicle);

            mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_JSON_KEY, vehicleJSONStr).apply();

            return vehicle;
        } catch (Exception e) {
            return null;
        }
    }

    private Vehicle saveVehicleVariablesInPrefs(SetPinMutation.Device1 fetchedDevice) {
        try {
            Vehicle vehicle = new Vehicle();
            Model model = new Model();

            if (fetchedDevice.vehicle() != null) {

                if (fetchedDevice.vehicle().vin() != null) {
                    vehicle.setVin(fetchedDevice.vehicle().vin());
                }
                if (fetchedDevice.vehicle().status() != null) {
                    vehicle.setStatus(fetchedDevice.vehicle().status());
                }
                if (fetchedDevice.vehicle().metrics() != null && fetchedDevice.vehicle().metrics().odometer() != null) {
                    vehicle.setOdoValue(fetchedDevice.vehicle().metrics().odometer());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().id() != null) {
                    vehicle.setOwnerId(fetchedDevice.vehicle().owner().id());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().firstName() != null) {
                    vehicle.setOwnerFirstName(fetchedDevice.vehicle().owner().firstName());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().lastName() != null) {
                    vehicle.setOwnerLastName(fetchedDevice.vehicle().owner().lastName());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().email() != null) {
                    vehicle.setOwnerEmailId(fetchedDevice.vehicle().owner().email());
                }
                if (fetchedDevice.vehicle().owner() != null && fetchedDevice.vehicle().owner().phone() != null) {
                    vehicle.setOwnerPhoneNo(fetchedDevice.vehicle().owner().phone());
                }
                if (fetchedDevice.vehicle().buyer() != null && fetchedDevice.vehicle().buyer().firstName() != null) {
                    vehicle.setBuyerFirstName(fetchedDevice.vehicle().buyer().firstName());
                }
                if (fetchedDevice.vehicle().buyer() != null && fetchedDevice.vehicle().buyer().lastName() != null) {
                    vehicle.setBuyerLastName(fetchedDevice.vehicle().buyer().lastName());
                }
                if (fetchedDevice.vehicle().buyer() != null && fetchedDevice.vehicle().buyer().phone() != null) {
                    vehicle.setBuyerPhoneNo(fetchedDevice.vehicle().buyer().phone());
                }
                if (fetchedDevice.vehicle().id() != null) {
                    vehicle.setVehicleId(fetchedDevice.vehicle().id());
                }
                if (fetchedDevice.vehicle().distributor() != null) {
                    if (fetchedDevice.vehicle().distributor().name() != null) {
                        vehicle.setDistributorName(fetchedDevice.vehicle().distributor().name());
                    }
                    if (fetchedDevice.vehicle().distributor().phone() != null) {
                        vehicle.setDistributorPhoneNo1(fetchedDevice.vehicle().distributor().phone());
                    }
                    if (fetchedDevice.vehicle().distributor().phone1() != null) {
                        vehicle.setDistributorPhoneNo2(fetchedDevice.vehicle().distributor().phone1());
                    }
                    if (fetchedDevice.vehicle().distributor().address() != null) {
                        vehicle.setDistributorAddress(fetchedDevice.vehicle().distributor().address());
                    }
                }

                //set vehicle config
                if (fetchedDevice.vehicle().config() != null && fetchedDevice.vehicle().config().childSpeedLock() != null) {
                    vehicle.setChildSpeedLock(fetchedDevice.vehicle().config().childSpeedLock());
                } else {
                    int speedLockValue = 100;
                    if (fetchedDevice != null
                            && fetchedDevice.vehicle() != null
                            && fetchedDevice.vehicle().model() != null
                            && fetchedDevice.vehicle().model().config() != null
                            && fetchedDevice.vehicle().model().config().speedLimit() != null) {
                        speedLockValue = fetchedDevice.vehicle().model().config().speedLimit().intValue();
                    }
                    vehicle.setChildSpeedLock(speedLockValue);
                }
                if (fetchedDevice.vehicle().config() != null && fetchedDevice.vehicle().config().followMeHomeHeadLamp() != null) {
                    vehicle.setFollowMeHeadlamp(fetchedDevice.vehicle().config().followMeHomeHeadLamp());
                }

                //set vehicle warranty
                if (fetchedDevice.vehicle().warranty() != null && fetchedDevice.vehicle().warranty().expiry() != null) {
                    vehicle.setWarrantyExpiryTime(fetchedDevice.vehicle().warranty().expiry());
                }
                if (fetchedDevice.vehicle().warranty() != null && fetchedDevice.vehicle().warranty().status() != null) {
                    vehicle.setWarrantyStatus(fetchedDevice.vehicle().warranty().status());
                }

                //set vehicle invoice details
                if (fetchedDevice.vehicle().invoice() != null && fetchedDevice.vehicle().invoice().createdAt() != null) {
                    vehicle.setInvoiceCreationDate(fetchedDevice.vehicle().invoice().createdAt());
                }
                if (fetchedDevice.vehicle().invoice() != null && fetchedDevice.vehicle().invoice().passbook() != null
                        && !fetchedDevice.vehicle().invoice().passbook().isEmpty() && fetchedDevice.vehicle().invoice().passbook().get(0).status() != null) {
                    vehicle.setInvoiceStatus(fetchedDevice.vehicle().invoice().passbook().get(0).status());
                }

                //set model specific data
                if (fetchedDevice.vehicle().model() != null && fetchedDevice.vehicle().model().id() != null) {
                    model.setId(fetchedDevice.vehicle().model().id());
                }
                if (fetchedDevice.vehicle().model() != null && fetchedDevice.vehicle().model().name() != null) {
                    model.setName(fetchedDevice.vehicle().model().name());
                }
                if (fetchedDevice.vehicle().model() != null && fetchedDevice.vehicle().model().protocol() != null) {
                    model.setProtocol(fetchedDevice.vehicle().model().protocol());
                }
            }

            vehicle.setModel(model);

            String vehicleJSONStr = new Gson().toJson(vehicle);

            mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_JSON_KEY, vehicleJSONStr).apply();

            return vehicle;
        } catch (Exception e) {
            return null;
        }
    }

    public Vehicle saveVehicleVariablesInPrefs(SetVehicleConfigMutation.SetConfig setVehicleConfigResponse) {
        try {

            Vehicle vehicle = new Vehicle();
            Model model = new Model();

            if (setVehicleConfigResponse != null && setVehicleConfigResponse.vehicle() != null) {

                if (setVehicleConfigResponse.vehicle().vin() != null) {
                    vehicle.setVin(setVehicleConfigResponse.vehicle().vin());
                }
                if (setVehicleConfigResponse.vehicle().status() != null) {
                    vehicle.setStatus(setVehicleConfigResponse.vehicle().status());
                }
                if (setVehicleConfigResponse.vehicle().metrics() != null && setVehicleConfigResponse.vehicle().metrics().odometer() != null) {
                    vehicle.setOdoValue(setVehicleConfigResponse.vehicle().metrics().odometer());
                }
                if (setVehicleConfigResponse.vehicle().owner() != null && setVehicleConfigResponse.vehicle().owner().id() != null) {
                    vehicle.setOwnerId(setVehicleConfigResponse.vehicle().owner().id());
                }
                if (setVehicleConfigResponse.vehicle().owner() != null && setVehicleConfigResponse.vehicle().owner().firstName() != null) {
                    vehicle.setOwnerFirstName(setVehicleConfigResponse.vehicle().owner().firstName());
                }
                if (setVehicleConfigResponse.vehicle().owner() != null && setVehicleConfigResponse.vehicle().owner().lastName() != null) {
                    vehicle.setOwnerLastName(setVehicleConfigResponse.vehicle().owner().lastName());
                }
                if (setVehicleConfigResponse.vehicle().owner() != null && setVehicleConfigResponse.vehicle().owner().email() != null) {
                    vehicle.setOwnerEmailId(setVehicleConfigResponse.vehicle().owner().email());
                }
                if (setVehicleConfigResponse.vehicle().owner() != null && setVehicleConfigResponse.vehicle().owner().phone() != null) {
                    vehicle.setOwnerPhoneNo(setVehicleConfigResponse.vehicle().owner().phone());
                }
                if (setVehicleConfigResponse.vehicle().buyer() != null && setVehicleConfigResponse.vehicle().buyer().firstName() != null) {
                    vehicle.setBuyerFirstName(setVehicleConfigResponse.vehicle().buyer().firstName());
                }
                if (setVehicleConfigResponse.vehicle().buyer() != null && setVehicleConfigResponse.vehicle().buyer().lastName() != null) {
                    vehicle.setBuyerLastName(setVehicleConfigResponse.vehicle().buyer().lastName());
                }
                if (setVehicleConfigResponse.vehicle().buyer() != null && setVehicleConfigResponse.vehicle().buyer().phone() != null) {
                    vehicle.setBuyerPhoneNo(setVehicleConfigResponse.vehicle().buyer().phone());
                }
                if (setVehicleConfigResponse.vehicle().id() != null) {
                    vehicle.setVehicleId(setVehicleConfigResponse.vehicle().id());
                }
                if (setVehicleConfigResponse.vehicle().distributor() != null) {
                    if (setVehicleConfigResponse.vehicle().distributor().name() != null) {
                        vehicle.setDistributorName(setVehicleConfigResponse.vehicle().distributor().name());
                    }
                    if (setVehicleConfigResponse.vehicle().distributor().phone() != null) {
                        vehicle.setDistributorPhoneNo1(setVehicleConfigResponse.vehicle().distributor().phone());
                    }
                    if (setVehicleConfigResponse.vehicle().distributor().phone1() != null) {
                        vehicle.setDistributorPhoneNo2(setVehicleConfigResponse.vehicle().distributor().phone1());
                    }
                    if (setVehicleConfigResponse.vehicle().distributor().address() != null) {
                        vehicle.setDistributorAddress(setVehicleConfigResponse.vehicle().distributor().address());
                    }
                }

                //set vehicle config
                if (setVehicleConfigResponse.vehicle().config() != null && setVehicleConfigResponse.vehicle().config().childSpeedLock() != null) {
                    vehicle.setChildSpeedLock(setVehicleConfigResponse.vehicle().config().childSpeedLock());
                } else {
                    int speedLockValue = 100;
                    if (setVehicleConfigResponse.vehicle().device() != null
                            && setVehicleConfigResponse.vehicle().device().vehicle() != null
                            && setVehicleConfigResponse.vehicle().device().vehicle().model() != null
                            && setVehicleConfigResponse.vehicle().device().vehicle().model().config() != null
                            && setVehicleConfigResponse.vehicle().device().vehicle().model().config().speedLimit() != null) {
                        speedLockValue = setVehicleConfigResponse.vehicle().device().vehicle().model().config().speedLimit().intValue();
                    }
                    vehicle.setChildSpeedLock(speedLockValue);
                }
                if (setVehicleConfigResponse.vehicle().config() != null && setVehicleConfigResponse.vehicle().config().followMeHomeHeadLamp() != null) {
                    vehicle.setFollowMeHeadlamp(setVehicleConfigResponse.vehicle().config().followMeHomeHeadLamp());
                }

                //set vehicle warranty
                if (setVehicleConfigResponse.vehicle().warranty() != null && setVehicleConfigResponse.vehicle().warranty().expiry() != null) {
                    vehicle.setWarrantyExpiryTime(setVehicleConfigResponse.vehicle().warranty().expiry());
                }
                if (setVehicleConfigResponse.vehicle().warranty() != null && setVehicleConfigResponse.vehicle().warranty().status() != null) {
                    vehicle.setWarrantyStatus(setVehicleConfigResponse.vehicle().warranty().status());
                }

                //set vehicle invoice details
                if (setVehicleConfigResponse.vehicle().invoice() != null && setVehicleConfigResponse.vehicle().invoice().createdAt() != null) {
                    vehicle.setInvoiceCreationDate(setVehicleConfigResponse.vehicle().invoice().createdAt());
                }
                if (setVehicleConfigResponse.vehicle().invoice() != null && setVehicleConfigResponse.vehicle().invoice().passbook() != null
                        && !setVehicleConfigResponse.vehicle().invoice().passbook().isEmpty() && setVehicleConfigResponse.vehicle().invoice().passbook().get(0).status() != null) {
                    vehicle.setInvoiceStatus(setVehicleConfigResponse.vehicle().invoice().passbook().get(0).status());
                }

                //set model specific data
                if (setVehicleConfigResponse.vehicle().model() != null && setVehicleConfigResponse.vehicle().model().id() != null) {
                    model.setId(setVehicleConfigResponse.vehicle().model().id());
                }
                if (setVehicleConfigResponse.vehicle().model() != null && setVehicleConfigResponse.vehicle().model().name() != null) {
                    model.setName(setVehicleConfigResponse.vehicle().model().name());
                }
                if (setVehicleConfigResponse.vehicle().model() != null && setVehicleConfigResponse.vehicle().model().protocol() != null) {
                    model.setProtocol(setVehicleConfigResponse.vehicle().model().protocol());
                }
            }

            vehicle.setModel(model);

            String vehicleJSONStr = new Gson().toJson(vehicle);

            mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_JSON_KEY, vehicleJSONStr).apply();

            return vehicle;
        } catch (Exception e) {
            return null;
        }
    }

    public Vehicle saveVehicleVariablesInPrefs(SetOdoResetFlagMutation.Update odoResetFlagMutationResponse) {
        try {

            Vehicle vehicle = new Vehicle();
            Model model = new Model();

            if (odoResetFlagMutationResponse != null && odoResetFlagMutationResponse.device().vehicle() != null) {

                if (odoResetFlagMutationResponse.device().vehicle().vin() != null) {
                    vehicle.setVin(odoResetFlagMutationResponse.device().vehicle().vin());
                }
                if (odoResetFlagMutationResponse.device().vehicle().status() != null) {
                    vehicle.setStatus(odoResetFlagMutationResponse.device().vehicle().status());
                }
                if (odoResetFlagMutationResponse.device().vehicle().metrics() != null && odoResetFlagMutationResponse.device().vehicle().metrics().odometer() != null) {
                    vehicle.setOdoValue(odoResetFlagMutationResponse.device().vehicle().metrics().odometer());
                }
                if (odoResetFlagMutationResponse.device().vehicle().owner() != null && odoResetFlagMutationResponse.device().vehicle().owner().id() != null) {
                    vehicle.setOwnerId(odoResetFlagMutationResponse.device().vehicle().owner().id());
                }
                if (odoResetFlagMutationResponse.device().vehicle().owner() != null && odoResetFlagMutationResponse.device().vehicle().owner().firstName() != null) {
                    vehicle.setOwnerFirstName(odoResetFlagMutationResponse.device().vehicle().owner().firstName());
                }
                if (odoResetFlagMutationResponse.device().vehicle().owner() != null && odoResetFlagMutationResponse.device().vehicle().owner().lastName() != null) {
                    vehicle.setOwnerLastName(odoResetFlagMutationResponse.device().vehicle().owner().lastName());
                }
                if (odoResetFlagMutationResponse.device().vehicle().owner() != null && odoResetFlagMutationResponse.device().vehicle().owner().email() != null) {
                    vehicle.setOwnerEmailId(odoResetFlagMutationResponse.device().vehicle().owner().email());
                }
                if (odoResetFlagMutationResponse.device().vehicle().owner() != null && odoResetFlagMutationResponse.device().vehicle().owner().phone() != null) {
                    vehicle.setOwnerPhoneNo(odoResetFlagMutationResponse.device().vehicle().owner().phone());
                }
                if (odoResetFlagMutationResponse.device().vehicle().buyer() != null && odoResetFlagMutationResponse.device().vehicle().buyer().firstName() != null) {
                    vehicle.setBuyerFirstName(odoResetFlagMutationResponse.device().vehicle().buyer().firstName());
                }
                if (odoResetFlagMutationResponse.device().vehicle().buyer() != null && odoResetFlagMutationResponse.device().vehicle().buyer().lastName() != null) {
                    vehicle.setBuyerLastName(odoResetFlagMutationResponse.device().vehicle().buyer().lastName());
                }
                if (odoResetFlagMutationResponse.device().vehicle().buyer() != null && odoResetFlagMutationResponse.device().vehicle().buyer().phone() != null) {
                    vehicle.setBuyerPhoneNo(odoResetFlagMutationResponse.device().vehicle().buyer().phone());
                }
                if (odoResetFlagMutationResponse.device().vehicle().id() != null) {
                    vehicle.setVehicleId(odoResetFlagMutationResponse.device().vehicle().id());
                }
                if (odoResetFlagMutationResponse.device().vehicle().distributor() != null) {
                    if (odoResetFlagMutationResponse.device().vehicle().distributor().name() != null) {
                        vehicle.setDistributorName(odoResetFlagMutationResponse.device().vehicle().distributor().name());
                    }
                    if (odoResetFlagMutationResponse.device().vehicle().distributor().phone() != null) {
                        vehicle.setDistributorPhoneNo1(odoResetFlagMutationResponse.device().vehicle().distributor().phone());
                    }
                    if (odoResetFlagMutationResponse.device().vehicle().distributor().phone1() != null) {
                        vehicle.setDistributorPhoneNo2(odoResetFlagMutationResponse.device().vehicle().distributor().phone1());
                    }
                    if (odoResetFlagMutationResponse.device().vehicle().distributor().address() != null) {
                        vehicle.setDistributorAddress(odoResetFlagMutationResponse.device().vehicle().distributor().address());
                    }
                }

                //set vehicle config
                if (odoResetFlagMutationResponse.device().vehicle().config() != null && odoResetFlagMutationResponse.device().vehicle().config().childSpeedLock() != null) {
                    vehicle.setChildSpeedLock(odoResetFlagMutationResponse.device().vehicle().config().childSpeedLock());
                } else {
                    int speedLockValue = 100;
                    if (odoResetFlagMutationResponse.device() != null
                            && odoResetFlagMutationResponse.device().vehicle() != null
                            && odoResetFlagMutationResponse.device().vehicle().model() != null
                            && odoResetFlagMutationResponse.device().vehicle().model().config() != null
                            && odoResetFlagMutationResponse.device().vehicle().model().config().speedLimit() != null) {
                        speedLockValue = odoResetFlagMutationResponse.device().vehicle().model().config().speedLimit().intValue();
                    }
                    vehicle.setChildSpeedLock(speedLockValue);
                }
                if (odoResetFlagMutationResponse.device().vehicle().config() != null && odoResetFlagMutationResponse.device().vehicle().config().followMeHomeHeadLamp() != null) {
                    vehicle.setFollowMeHeadlamp(odoResetFlagMutationResponse.device().vehicle().config().followMeHomeHeadLamp());
                }

                //set vehicle warranty
                if (odoResetFlagMutationResponse.device().vehicle().warranty() != null && odoResetFlagMutationResponse.device().vehicle().warranty().expiry() != null) {
                    vehicle.setWarrantyExpiryTime(odoResetFlagMutationResponse.device().vehicle().warranty().expiry());
                }
                if (odoResetFlagMutationResponse.device().vehicle().warranty() != null && odoResetFlagMutationResponse.device().vehicle().warranty().status() != null) {
                    vehicle.setWarrantyStatus(odoResetFlagMutationResponse.device().vehicle().warranty().status());
                }

                //set vehicle invoice details
                if (odoResetFlagMutationResponse.device().vehicle().invoice() != null && odoResetFlagMutationResponse.invoice().createdAt() != null) {
                    vehicle.setInvoiceCreationDate(odoResetFlagMutationResponse.invoice().createdAt());
                }
                if (odoResetFlagMutationResponse.device().vehicle().invoice() != null && odoResetFlagMutationResponse.device().vehicle().invoice().passbook() != null
                        && !odoResetFlagMutationResponse.device().vehicle().invoice().passbook().isEmpty() && odoResetFlagMutationResponse.device().vehicle().invoice().passbook().get(0).status() != null) {
                    vehicle.setInvoiceStatus(odoResetFlagMutationResponse.device().vehicle().invoice().passbook().get(0).status());
                }

                //set model specific data
                if (odoResetFlagMutationResponse.device().vehicle().model() != null && odoResetFlagMutationResponse.device().vehicle().model().id() != null) {
                    model.setId(odoResetFlagMutationResponse.device().vehicle().model().id());
                }
                if (odoResetFlagMutationResponse.device().vehicle().model() != null && odoResetFlagMutationResponse.device().vehicle().model().name() != null) {
                    model.setName(odoResetFlagMutationResponse.device().vehicle().model().name());
                }
                if (odoResetFlagMutationResponse.device().vehicle().model() != null && odoResetFlagMutationResponse.device().vehicle().model().protocol() != null) {
                    model.setProtocol(odoResetFlagMutationResponse.device().vehicle().model().protocol());
                }
            }

            vehicle.setModel(model);

            String vehicleJSONStr = new Gson().toJson(vehicle);

            mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_JSON_KEY, vehicleJSONStr).apply();

            return vehicle;
        } catch (Exception e) {
            return null;
        }
    }

    public Device saveDeviceVariablesInPrefs(FetchDeviceQuery.Get fetchedDevice) {

        if (fetchedDevice == null) {
            return null;
        }

        //set device details
        Device device = new Device();
        device.setId(fetchedDevice.id());
        device.setDeviceId(fetchedDevice.deviceId());
        device.setMacId(fetchedDevice.macId());
        device.setKey(fetchedDevice.key());
        device.setStatus(fetchedDevice.status());
        //set device type
        if (fetchedDevice.type() != null) {
            device.setDeviceType(fetchedDevice.type());
        }

        //set vehicle details
        Vehicle vehicle = saveVehicleVariablesInPrefs(fetchedDevice);

        if (vehicle != null) {
            device.setVehicle(vehicle);
        }

        //set Model details
        Model model = new Model();
        if (vehicle != null && vehicle.getModel() != null) {
            model = vehicle.getModel();
        }
        if (fetchedDevice.vehicle().model().id() != null) {
            model.setId(fetchedDevice.vehicle().model().id());
        }
        if (fetchedDevice.vehicle().model().name() != null) {
            model.setName(fetchedDevice.vehicle().model().name());
        }
        if (fetchedDevice.vehicle().model().config().accessType() != null) {
            model.setModelAccessType(fetchedDevice.vehicle().model().config().accessType());
        }


        if (fetchedDevice.vehicle().model().config().speedDivisor() != null) {
            model.setSpeedDivisor((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().speedDivisor())));
        }
        if (fetchedDevice.vehicle().model().config().odoDivisor() != null) {
            model.setOdoDivisor((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().odoDivisor())));
        }
        if (fetchedDevice.vehicle().model().config().maxSpeed() != null) {
            model.setMaxSpeed((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().maxSpeed())));
        }
        if (fetchedDevice.vehicle().model().config().speedLimit() != null) {
            model.setSpeedLimit((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().speedLimit())));
        }
        if (fetchedDevice.vehicle().model().config().pickupControlLimit() != null) {
            model.setPickupLimit((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().pickupControlLimit())));
        }
        if (fetchedDevice.vehicle().model().config().brakeRegenLimit() != null) {
            model.setBrakeRegenLimit((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().brakeRegenLimit())));
        }
        if (fetchedDevice.vehicle().model().config().zeroThrottleRegenLimit() != null) {
            model.setZeroThrottleRegenLimit((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().zeroThrottleRegenLimit())));
        }
        if (fetchedDevice.vehicle().model().config().currentLimit() != null) {
            model.setCurrentLimit((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().currentLimit())));
        }
        if (fetchedDevice.vehicle().model().config().overVoltageLimit() != null) {
            model.setOverVoltageLimit((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().overVoltageLimit())));
        }
        if (fetchedDevice.vehicle().model().config().underVoltageLimit() != null) {
            model.setUnderVoltageLimit((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().underVoltageLimit())));
        }
        if (fetchedDevice.vehicle().model().config().wheelDiameter() != null) {
            model.setWheelDiameter((int) Math.round(Math.floor(fetchedDevice.vehicle().model().config().wheelDiameter())));
        }
        if (fetchedDevice.vehicle().model().config().batteryMinVoltage() != null) {
            model.setBatteryMinVoltageLimit(fetchedDevice.vehicle().model().config().batteryMinVoltage());
        }
        if (fetchedDevice.vehicle().model().config().batteryMaxVoltage() != null) {
            model.setBatteryMaxVoltageLimit(fetchedDevice.vehicle().model().config().batteryMaxVoltage());
        }


        if (fetchedDevice.vehicle().model().config().hillAssistStatus() != null) {
            model.setHillAssist(fetchedDevice.vehicle().model().config().hillAssistStatus());
        }
        if (fetchedDevice.vehicle().model().config().parkingStatus() != null) {
            model.setParking(fetchedDevice.vehicle().model().config().parkingStatus());
        }
        if (fetchedDevice.vehicle().model().config().eabsStatus() != null) {
            model.setEAbs(fetchedDevice.vehicle().model().config().eabsStatus());
        }
        if (fetchedDevice.vehicle().model().config().regenBrakingStatus() != null) {
            model.setRegenBraking(fetchedDevice.vehicle().model().config().regenBrakingStatus());
        }

        device.getVehicle().setModel(model);

        if (fetchedDevice.firmware() != null) {
            device.setFirmware(fetchedDevice.firmware());
        }

        if (fetchedDevice.expectedFirmware() != null) {
            device.setExpectedFirmware(fetchedDevice.expectedFirmware());
        }

        if (fetchedDevice.updateStatus() != null) {
            device.setDeviceUpdateStatus(fetchedDevice.updateStatus());
        }

        if (fetchedDevice.pinResetRequired() != null) {
            device.setPinResetRequired(fetchedDevice.pinResetRequired());
        }

        if (fetchedDevice.odoResetRequired() != null) {
            mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY, fetchedDevice.odoResetRequired()).apply();
        }

        //write this data to shared prefs
        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY, new Gson().toJson(device)).apply();

        return device;
    }

    public Device saveDeviceVariablesInPrefs(FetchVehicleQuery.Get fetchedVehicle) {
        if (fetchedVehicle == null) {
            return null;
        }

        Device device = null;
        if (fetchedVehicle.device() != null) {

            //set device details
            device = new Device();
            device.setId(fetchedVehicle.device().id());
            device.setDeviceId(fetchedVehicle.device().deviceId());
            device.setMacId(fetchedVehicle.device().macId());
            device.setKey(fetchedVehicle.device().key());
            device.setStatus(fetchedVehicle.device().status());
            //set device type
            if (fetchedVehicle.device().type() != null) {
                device.setDeviceType(fetchedVehicle.device().type());
            }

            //set vehicle details
            Vehicle vehicle = saveVehicleVariablesInPrefs(fetchedVehicle);

            if (vehicle != null) {
                device.setVehicle(vehicle);
            }

            //save model details
            Model model = new Model();
            if (vehicle != null && vehicle.getModel() != null) {
                model = vehicle.getModel();
            }

            if (fetchedVehicle.device().vehicle().model().id() != null) {
                model.setId(fetchedVehicle.device().vehicle().model().id());
            }
            if (fetchedVehicle.device().vehicle().model().name() != null) {
                model.setName(fetchedVehicle.device().vehicle().model().name());
            }
            if (fetchedVehicle.device().vehicle().model().config().accessType() != null) {
                model.setModelAccessType(fetchedVehicle.device().vehicle().model().config().accessType());
            }

            if (fetchedVehicle.device().vehicle().model().config().speedDivisor() != null) {
                model.setSpeedDivisor((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().speedDivisor())));
            }
            if (fetchedVehicle.device().vehicle().model().config().odoDivisor() != null) {
                model.setOdoDivisor((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().odoDivisor())));
            }
            if (fetchedVehicle.device().vehicle().model().config().maxSpeed() != null) {
                model.setMaxSpeed((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().maxSpeed())));
            }
            if (fetchedVehicle.device().vehicle().model().config().speedLimit() != null) {
                model.setSpeedLimit((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().speedLimit())));
            }
            if (fetchedVehicle.device().vehicle().model().config().pickupControlLimit() != null) {
                model.setPickupLimit((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().pickupControlLimit())));
            }
            if (fetchedVehicle.device().vehicle().model().config().brakeRegenLimit() != null) {
                model.setBrakeRegenLimit((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().brakeRegenLimit())));
            }
            if (fetchedVehicle.device().vehicle().model().config().zeroThrottleRegenLimit() != null) {
                model.setZeroThrottleRegenLimit((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().zeroThrottleRegenLimit())));
            }
            if (fetchedVehicle.device().vehicle().model().config().currentLimit() != null) {
                model.setCurrentLimit((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().currentLimit())));
            }
            if (fetchedVehicle.device().vehicle().model().config().overVoltageLimit() != null) {
                model.setOverVoltageLimit((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().overVoltageLimit())));
            }
            if (fetchedVehicle.device().vehicle().model().config().underVoltageLimit() != null) {
                model.setUnderVoltageLimit((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().underVoltageLimit())));
            }
            if (fetchedVehicle.device().vehicle().model().config().wheelDiameter() != null) {
                model.setWheelDiameter((int) Math.round(Math.floor(fetchedVehicle.device().vehicle().model().config().wheelDiameter())));
            }
            if (fetchedVehicle.device().vehicle().model().config().batteryMinVoltage() != null) {
                model.setBatteryMinVoltageLimit(fetchedVehicle.device().vehicle().model().config().batteryMinVoltage());
            }
            if (fetchedVehicle.device().vehicle().model().config().batteryMaxVoltage() != null) {
                model.setBatteryMaxVoltageLimit(fetchedVehicle.device().vehicle().model().config().batteryMaxVoltage());
            }

            if (fetchedVehicle.device().vehicle().model().config().hillAssistStatus() != null) {
                model.setHillAssist(fetchedVehicle.device().vehicle().model().config().hillAssistStatus());
            }
            if (fetchedVehicle.device().vehicle().model().config().parkingStatus() != null) {
                model.setParking(fetchedVehicle.device().vehicle().model().config().parkingStatus());
            }
            if (fetchedVehicle.device().vehicle().model().config().eabsStatus() != null) {
                model.setEAbs(fetchedVehicle.device().vehicle().model().config().eabsStatus());
            }
            if (fetchedVehicle.device().vehicle().model().config().regenBrakingStatus() != null) {
                model.setRegenBraking(fetchedVehicle.device().vehicle().model().config().regenBrakingStatus());
            }

            device.getVehicle().setModel(model);

            if (fetchedVehicle.device().firmware() != null) {
                device.setFirmware(fetchedVehicle.device().firmware());
            }

            if (fetchedVehicle.device().expectedFirmware() != null) {
                device.setExpectedFirmware(fetchedVehicle.device().expectedFirmware());
            }

            if (fetchedVehicle.device().updateStatus() != null) {
                device.setDeviceUpdateStatus(fetchedVehicle.device().updateStatus());
            }

            if (fetchedVehicle.device().pinResetRequired() != null) {
                device.setPinResetRequired(fetchedVehicle.device().pinResetRequired());
            }

            if (fetchedVehicle.device().odoResetRequired() != null) {
                mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY, fetchedVehicle.device().odoResetRequired()).apply();
            }
        }
        //write this data to shared prefs
        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY, new Gson().toJson(device)).apply();

        return device;

    }

    public Device saveDeviceVariablesInPrefs(SetPinMutation.Device1 setPinMutationResponse) {
        if (setPinMutationResponse == null) {
            return null;
        }

        //set device details
        Device device = new Device();
        device.setId(setPinMutationResponse.id());
        device.setDeviceId(setPinMutationResponse.deviceId());
        device.setMacId(setPinMutationResponse.macId());
        device.setKey(setPinMutationResponse.key());
        device.setStatus(setPinMutationResponse.status());
        //set device type
        if (setPinMutationResponse.type() != null) {
            device.setDeviceType(setPinMutationResponse.type());
        }

        //set vehicle details
        Vehicle vehicle = saveVehicleVariablesInPrefs(setPinMutationResponse);

        if (vehicle != null) {
            device.setVehicle(vehicle);
        }

        //set Model details
        Model model = new Model();
        if (vehicle != null && vehicle.getModel() != null) {
            model = vehicle.getModel();
        }
        if (setPinMutationResponse.vehicle().model().id() != null) {
            model.setId(setPinMutationResponse.vehicle().model().id());
        }
        if (setPinMutationResponse.vehicle().model().name() != null) {
            model.setName(setPinMutationResponse.vehicle().model().name());
        }
        if (setPinMutationResponse.vehicle().model().config().accessType() != null) {
            model.setModelAccessType(setPinMutationResponse.vehicle().model().config().accessType());
        }

        if (setPinMutationResponse.vehicle().model().config().speedDivisor() != null) {
            model.setSpeedDivisor((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().speedDivisor())));
        }
        if (setPinMutationResponse.vehicle().model().config().odoDivisor() != null) {
            model.setOdoDivisor((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().odoDivisor())));
        }
        if (setPinMutationResponse.vehicle().model().config().maxSpeed() != null) {
            model.setMaxSpeed((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().maxSpeed())));
        }
        if (setPinMutationResponse.vehicle().model().config().speedLimit() != null) {
            model.setSpeedLimit((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().speedLimit())));
        }
        if (setPinMutationResponse.vehicle().model().config().pickupControlLimit() != null) {
            model.setPickupLimit((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().pickupControlLimit())));
        }
        if (setPinMutationResponse.vehicle().model().config().brakeRegenLimit() != null) {
            model.setBrakeRegenLimit((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().brakeRegenLimit())));
        }
        if (setPinMutationResponse.vehicle().model().config().zeroThrottleRegenLimit() != null) {
            model.setZeroThrottleRegenLimit((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().zeroThrottleRegenLimit())));
        }
        if (setPinMutationResponse.vehicle().model().config().currentLimit() != null) {
            model.setCurrentLimit((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().currentLimit())));
        }
        if (setPinMutationResponse.vehicle().model().config().overVoltageLimit() != null) {
            model.setOverVoltageLimit((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().overVoltageLimit())));
        }
        if (setPinMutationResponse.vehicle().model().config().underVoltageLimit() != null) {
            model.setUnderVoltageLimit((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().underVoltageLimit())));
        }
        if (setPinMutationResponse.vehicle().model().config().wheelDiameter() != null) {
            model.setWheelDiameter((int) Math.round(Math.floor(setPinMutationResponse.vehicle().model().config().wheelDiameter())));
        }
        if (setPinMutationResponse.vehicle().model().config().batteryMinVoltage() != null) {
            model.setBatteryMinVoltageLimit(setPinMutationResponse.vehicle().model().config().batteryMinVoltage());
        }
        if (setPinMutationResponse.vehicle().model().config().batteryMaxVoltage() != null) {
            model.setBatteryMaxVoltageLimit(setPinMutationResponse.vehicle().model().config().batteryMaxVoltage());
        }


        if (setPinMutationResponse.vehicle().model().config().hillAssistStatus() != null) {
            model.setHillAssist(setPinMutationResponse.vehicle().model().config().hillAssistStatus());
        }
        if (setPinMutationResponse.vehicle().model().config().parkingStatus() != null) {
            model.setParking(setPinMutationResponse.vehicle().model().config().parkingStatus());
        }
        if (setPinMutationResponse.vehicle().model().config().eabsStatus() != null) {
            model.setEAbs(setPinMutationResponse.vehicle().model().config().eabsStatus());
        }
        if (setPinMutationResponse.vehicle().model().config().regenBrakingStatus() != null) {
            model.setRegenBraking(setPinMutationResponse.vehicle().model().config().regenBrakingStatus());
        }

        device.getVehicle().setModel(model);

        if (setPinMutationResponse.firmware() != null) {
            device.setFirmware(setPinMutationResponse.firmware());
        }

        if (setPinMutationResponse.expectedFirmware() != null) {
            device.setExpectedFirmware(setPinMutationResponse.expectedFirmware());
        }

        if (setPinMutationResponse.updateStatus() != null) {
            device.setDeviceUpdateStatus(setPinMutationResponse.updateStatus());
        }

        if (setPinMutationResponse.pinResetRequired() != null) {
            device.setPinResetRequired(setPinMutationResponse.pinResetRequired());
        }

        if (setPinMutationResponse.odoResetRequired() != null) {
            mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY, setPinMutationResponse.odoResetRequired()).apply();
        }

        //write this data to shared prefs
        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY, new Gson().toJson(device)).apply();

        return device;
    }

    public Device saveDeviceVariablesInPrefs(SetVehicleConfigMutation.SetConfig vehicleConfigMutationResponse) {
        if (vehicleConfigMutationResponse == null) {
            return null;
        }

        Device device = null;
        if (vehicleConfigMutationResponse.vehicle().device() != null) {

            //set device details
            device = new Device();
            device.setId(vehicleConfigMutationResponse.vehicle().device().id());
            device.setDeviceId(vehicleConfigMutationResponse.vehicle().device().deviceId());
            device.setMacId(vehicleConfigMutationResponse.vehicle().device().macId());
            device.setKey(vehicleConfigMutationResponse.vehicle().device().key());
            device.setStatus(vehicleConfigMutationResponse.vehicle().device().status());
            //set device type
            if (vehicleConfigMutationResponse.vehicle().device().type() != null) {
                device.setDeviceType(vehicleConfigMutationResponse.vehicle().device().type());
            }

            //set vehicle details
            Vehicle vehicle = saveVehicleVariablesInPrefs(vehicleConfigMutationResponse);

            if (vehicle != null) {
                device.setVehicle(vehicle);
            }

            //save model details
            Model model = new Model();
            if (vehicle != null && vehicle.getModel() != null) {
                model = vehicle.getModel();
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().id() != null) {
                model.setId(vehicleConfigMutationResponse.vehicle().device().vehicle().model().id());
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().name() != null) {
                model.setName(vehicleConfigMutationResponse.vehicle().device().vehicle().model().name());
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().accessType() != null) {
                model.setModelAccessType(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().accessType());
            }

            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().speedDivisor() != null) {
                model.setSpeedDivisor((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().speedDivisor())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().odoDivisor() != null) {
                model.setOdoDivisor((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().odoDivisor())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().maxSpeed() != null) {
                model.setMaxSpeed((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().maxSpeed())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().speedLimit() != null) {
                model.setSpeedLimit((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().speedLimit())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().pickupControlLimit() != null) {
                model.setPickupLimit((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().pickupControlLimit())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().brakeRegenLimit() != null) {
                model.setBrakeRegenLimit((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().brakeRegenLimit())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().zeroThrottleRegenLimit() != null) {
                model.setZeroThrottleRegenLimit((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().zeroThrottleRegenLimit())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().currentLimit() != null) {
                model.setCurrentLimit((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().currentLimit())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().overVoltageLimit() != null) {
                model.setOverVoltageLimit((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().overVoltageLimit())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().underVoltageLimit() != null) {
                model.setUnderVoltageLimit((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().underVoltageLimit())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().wheelDiameter() != null) {
                model.setWheelDiameter((int) Math.round(Math.floor(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().wheelDiameter())));
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().batteryMinVoltage() != null) {
                model.setBatteryMinVoltageLimit(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().batteryMinVoltage());
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().batteryMaxVoltage() != null) {
                model.setBatteryMaxVoltageLimit(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().batteryMaxVoltage());
            }

            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().hillAssistStatus() != null) {
                model.setHillAssist(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().hillAssistStatus());
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().parkingStatus() != null) {
                model.setParking(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().parkingStatus());
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().eabsStatus() != null) {
                model.setEAbs(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().eabsStatus());
            }
            if (vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().regenBrakingStatus() != null) {
                model.setRegenBraking(vehicleConfigMutationResponse.vehicle().device().vehicle().model().config().regenBrakingStatus());
            }

            device.getVehicle().setModel(model);

            if (vehicleConfigMutationResponse.vehicle().device().firmware() != null) {
                device.setFirmware(vehicleConfigMutationResponse.vehicle().device().firmware());
            }

            if (vehicleConfigMutationResponse.vehicle().device().expectedFirmware() != null) {
                device.setExpectedFirmware(vehicleConfigMutationResponse.vehicle().device().expectedFirmware());
            }

            if (vehicleConfigMutationResponse.vehicle().device().updateStatus() != null) {
                device.setDeviceUpdateStatus(vehicleConfigMutationResponse.vehicle().device().updateStatus());
            }

            if (vehicleConfigMutationResponse.vehicle().device().pinResetRequired() != null) {
                device.setPinResetRequired(vehicleConfigMutationResponse.vehicle().device().pinResetRequired());
            }

            if (vehicleConfigMutationResponse.vehicle().device().odoResetRequired() != null) {
                mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY, vehicleConfigMutationResponse.vehicle().device().odoResetRequired()).apply();
            }
        }
        //write this data to shared prefs
        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY, new Gson().toJson(device)).apply();

        return device;
    }

    public void setVehicleLastKnownLocation(String lat, String lng) {
        mContext.getSharedPreferences(VEHICLE_PREFS,Context.MODE_PRIVATE).edit().putString(
                VEHICLE_LAST_KNOWN_LATITUDE, lat).apply();
        mContext.getSharedPreferences(VEHICLE_PREFS,Context.MODE_PRIVATE).edit().putString(
                VEHICLE_LAST_KNOWN_LONGITUDE, lng).apply();
    }
    public LatLng getVehicleLastKnownLocation() {
        SharedPreferences prefs = mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE);
        String lat = prefs.getString(ConstantsKt.VEHICLE_LAST_KNOWN_LATITUDE, null);
        String lng = prefs.getString(ConstantsKt.VEHICLE_LAST_KNOWN_LONGITUDE, null);
        if (lat != null && lng != null) {
            return new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
        }
        return null;
    }

    public void setBatteryStatus(Long status) {
        mContext.getSharedPreferences(VEHICLE_PREFS,Context.MODE_PRIVATE).edit().putLong(
                VEHICLE_STATUS_BATTERY_KEY, status).apply();
    }

    public Long getBatteryStatus() {
        SharedPreferences prefs = mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE);
        return  prefs.getLong(ConstantsKt.VEHICLE_STATUS_BATTERY_KEY, 0);
    }

    public void setLastKnownSpeed(Float speed) {
        mContext.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE).edit().putFloat(
                VEHICLE_LAST_KNOWN_SPEED, speed).apply();
    }
    public Float getLastKnownSpeed() {
        SharedPreferences prefs = mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE);
        return  prefs.getFloat(ConstantsKt.VEHICLE_LAST_KNOWN_SPEED, 0);
    }

    public Device saveDeviceVariablesInPrefs(SetOdoResetFlagMutation.Update odoResetFlagMutationResponse) {
        if (odoResetFlagMutationResponse == null) {
            return null;
        }

        Device device = null;
        if (odoResetFlagMutationResponse.device() != null) {

            //set device details
            device = new Device();
            device.setId(odoResetFlagMutationResponse.device().id());
            device.setDeviceId(odoResetFlagMutationResponse.device().deviceId());
            device.setMacId(odoResetFlagMutationResponse.device().macId());
            device.setKey(odoResetFlagMutationResponse.device().key());
            device.setStatus(odoResetFlagMutationResponse.device().status());
            //set device type
            if (odoResetFlagMutationResponse.device().type() != null) {
                device.setDeviceType(odoResetFlagMutationResponse.device().type());
            }

            //set vehicle details
            Vehicle vehicle = saveVehicleVariablesInPrefs(odoResetFlagMutationResponse);

            if (vehicle != null) {
                device.setVehicle(vehicle);
            }

            //save model details
            Model model = new Model();
            if (vehicle != null && vehicle.getModel() != null) {
                model = vehicle.getModel();
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().id() != null) {
                model.setId(odoResetFlagMutationResponse.device().vehicle().model().id());
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().name() != null) {
                model.setName(odoResetFlagMutationResponse.device().vehicle().model().name());
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().accessType() != null) {
                model.setModelAccessType(odoResetFlagMutationResponse.device().vehicle().model().config().accessType());
            }

            if (odoResetFlagMutationResponse.device().vehicle().model().config().speedDivisor() != null) {
                model.setSpeedDivisor((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().speedDivisor())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().odoDivisor() != null) {
                model.setOdoDivisor((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().odoDivisor())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().maxSpeed() != null) {
                model.setMaxSpeed((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().maxSpeed())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().speedLimit() != null) {
                model.setSpeedLimit((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().speedLimit())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().pickupControlLimit() != null) {
                model.setPickupLimit((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().pickupControlLimit())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().brakeRegenLimit() != null) {
                model.setBrakeRegenLimit((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().brakeRegenLimit())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().zeroThrottleRegenLimit() != null) {
                model.setZeroThrottleRegenLimit((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().zeroThrottleRegenLimit())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().currentLimit() != null) {
                model.setCurrentLimit((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().currentLimit())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().overVoltageLimit() != null) {
                model.setOverVoltageLimit((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().overVoltageLimit())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().underVoltageLimit() != null) {
                model.setUnderVoltageLimit((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().underVoltageLimit())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().wheelDiameter() != null) {
                model.setWheelDiameter((int) Math.round(Math.floor(odoResetFlagMutationResponse.device().vehicle().model().config().wheelDiameter())));
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().batteryMinVoltage() != null) {
                model.setBatteryMinVoltageLimit(odoResetFlagMutationResponse.device().vehicle().model().config().batteryMinVoltage());
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().batteryMaxVoltage() != null) {
                model.setBatteryMaxVoltageLimit(odoResetFlagMutationResponse.device().vehicle().model().config().batteryMaxVoltage());
            }

            if (odoResetFlagMutationResponse.device().vehicle().model().config().hillAssistStatus() != null) {
                model.setHillAssist(odoResetFlagMutationResponse.device().vehicle().model().config().hillAssistStatus());
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().parkingStatus() != null) {
                model.setParking(odoResetFlagMutationResponse.device().vehicle().model().config().parkingStatus());
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().eabsStatus() != null) {
                model.setEAbs(odoResetFlagMutationResponse.device().vehicle().model().config().eabsStatus());
            }
            if (odoResetFlagMutationResponse.device().vehicle().model().config().regenBrakingStatus() != null) {
                model.setRegenBraking(odoResetFlagMutationResponse.device().vehicle().model().config().regenBrakingStatus());
            }

            device.getVehicle().setModel(model);

            if (odoResetFlagMutationResponse.device().firmware() != null) {
                device.setFirmware(odoResetFlagMutationResponse.device().firmware());
            }

            if (odoResetFlagMutationResponse.device().expectedFirmware() != null) {
                device.setExpectedFirmware(odoResetFlagMutationResponse.device().expectedFirmware());
            }

            if (odoResetFlagMutationResponse.device().updateStatus() != null) {
                device.setDeviceUpdateStatus(odoResetFlagMutationResponse.device().updateStatus());
            }

            if (odoResetFlagMutationResponse.device().pinResetRequired() != null) {
                device.setPinResetRequired(odoResetFlagMutationResponse.device().pinResetRequired());
            }

            if (odoResetFlagMutationResponse.device().odoResetRequired() != null) {
                mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY, odoResetFlagMutationResponse.device().odoResetRequired()).apply();
            }
        }
        //write this data to shared prefs
        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY, new Gson().toJson(device)).apply();

        return device;
    }

    public void saveOnGoingRentalVehicle(@NotNull RentalVehicle rentalVehicle) {
        mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, new Gson().toJson(rentalVehicle)).apply();
    }

    public RentalVehicle getOnGoingRentalVehicle() {
        String rentalVehicleStr = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null);
        if(rentalVehicleStr == null) {
            return null;
        }
        try {
            return new Gson().fromJson(rentalVehicleStr, RentalVehicle.class);
        } catch (Exception e) {
            return null;
        }
    }
}