package com.revos.android.fleet.data

data class RentalVehicleModel (
    var name: String? = null,
    var company: RentalVehicleCompany? = null,
    var config: RentalVehicleModelConfig? = null,
)