package com.revos.android.fleet.data.legacy

import com.revos.android.fleet.data.RentalVehicleCompany
import com.revos.android.fleet.data.RentalVehicleModel
import com.revos.scripts.FetchRentalVehicleDetailsAndPricingInfoQuery

data class RentalVehicle (
    var isDeliverable : Boolean = false,
    var vin: String? = null,
    var model: RentalVehicleModel? = null,
    var oem: RentalVehicleCompany? = null,
    var company: RentalVehicleCompany? = null,
    var distributor: RentalVehicleCompany? = null,
    var rentalStatus: String? = null,
    var status: String? = null,
    var latitude : Double = 0.0,
    var longitude : Double = 0.0,
    var batteryVoltageAdc : Double = 0.0,
    var batteryVoltage : Double  = 0.0,
    var rentalPricingInfoList: List<FetchRentalVehicleDetailsAndPricingInfoQuery.Pricing>? = null,
    var startTime : String? = null,
    var endTime : String? = null,
    var amount : Double = 0.0,
    var bookingId : String? = null,
    var soc : Long = 0L,
    var distanceFromCurrentLoc : Long = 0L
)