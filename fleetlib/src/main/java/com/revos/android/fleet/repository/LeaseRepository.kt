package com.revos.android.fleet.repository

import com.revos.android.fleet.api.v2.data.*
import com.revos.android.fleet.api.v2.data.BookingModel
import com.revos.android.fleet.api.v2.data.BookingResponseModel
import com.revos.android.fleet.api.v2.data.GetAllVehiclesModel
import com.revos.android.fleet.api.v2.data.RentalVehicleModel
import com.revos.android.fleet.api.v2.lease.RevosLeaseService
import com.revos.android.fleet.api.v2.payment.RevosPaymentService
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

internal class LeaseRepository {
    private var revosLeaseService : RevosLeaseService = RevosLeaseService.create()
    private var revosPaymentService : RevosPaymentService = RevosPaymentService.create()

    suspend fun getAllRentalVehicles(first : Int, orderBy : String?, skip : Int,
                                     appToken : String, bearerToken : String) : List<GetAllVehiclesModel.Data.Vehicle> {
        val response = revosLeaseService.getAllRentalVehicles(first, orderBy, skip, appToken , bearerToken)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data.vehicles
    }

    suspend fun getRentalVehicle(vin : String, appToken : String, bearerToken : String) : RentalVehicleModel {
        val response = revosLeaseService.getRentalVehicle(vin, appToken , bearerToken)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response
    }

    suspend fun getAllBookings(first : Int, skip : Int,
                               appToken : String, bearerToken : String) : List<BookingDetails> {
        val response = revosLeaseService.getAllBookings(first, skip, appToken , bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data.bookings
    }

    suspend fun getBooking(bookingId : String, appToken : String,
                           bearerToken : String) : BookingModel {
        val response = revosLeaseService.getUserBooking(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response
    }

    suspend fun createBooking(appToken : String, bearerToken : String,
                              requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.createBooking(appToken, bearerToken, requestBody )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun startBooking(bookingId: String, appToken : String,
                             bearerToken : String) : BookingDetails {
        val response = revosLeaseService.startBooking(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun modifyBooking(bookingId: String, appToken : String, bearerToken : String,
                              requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.modifyBooking(bookingId, appToken, bearerToken, requestBody )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun paymentInit(bookingId: String, appToken : String, bearerToken : String,
                            requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.paymentInit(bookingId, appToken, bearerToken, requestBody )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun getOrderStatus(appToken: String,
                               bearerToken : String,
                               requestBody: RequestBody):
            GetOrderStatusResponseModel.Details.OrderDetails {

        val response = revosPaymentService.getOrderStatus(appToken, bearerToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.details.orderDetails
    }

    suspend fun paymentConfirm(bookingId: String, appToken : String,
                               bearerToken : String, requestBody: RequestBody?) : BookingDetails {
        var response : BookingResponseModel
        if(requestBody != null){
            response = revosLeaseService.paymentConfirm(bookingId, appToken, bearerToken, requestBody )
        }else{
            response = revosLeaseService.paymentConfirm(bookingId, appToken, bearerToken )
        }
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun paymentFailed(bookingId: String, appToken : String,
                              bearerToken : String) : BookingDetails {
        val response = revosLeaseService.paymentFailed(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun cancelBooking(bookingId: String, appToken : String,
                              bearerToken : String, requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.cancelBooking(bookingId, appToken, bearerToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    fun lockVehicle(vin: String, appToken : String,
                    bearerToken : String) : Call<ResponseBody> {

        return revosLeaseService.lockVehicle(vin, appToken, bearerToken )
    }

    fun unlockVehicle(vin: String, appToken : String,
                      bearerToken : String) : Call<ResponseBody> {
        return revosLeaseService.unlockVehicle(vin, appToken, bearerToken )
    }

    suspend fun endBooking(bookingId: String, appToken : String,
                           bearerToken : String) : BookingDetails {
        val response = revosLeaseService.endBooking(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }
}