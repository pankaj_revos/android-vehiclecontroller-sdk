package com.revos.android.fleet.data

import com.revos.scripts.type.PassbookTxStatus
import com.revos.scripts.type.PassbookTxType

data class PassbookTxn (
    var id: String? = null,
    var amount: String? = null,
    var dueDate: String? = null,
    var paymentDate: String? = null,
    var clearanceDate: String? = null,
    var remark: String? = null,
    var passbookTxStatus: PassbookTxStatus? = null,
    var passbookTxType: PassbookTxType? = null,
    var creationDate: String? = null,
    var payeeUpiId: String? = null,
    var vin: String? = null,
)