package com.revos.android.fleet.data

data class VehicleHomeItem(
    val drawableId : Int,
    val menuTitle : String
)
