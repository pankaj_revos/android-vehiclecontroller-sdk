package com.revos.android.fleet.graphql

import com.apollographql.apollo.api.CustomTypeAdapter
import com.apollographql.apollo.api.CustomTypeValue
import org.joda.time.format.ISODateTimeFormat

class GraphQLDateTimeCustomAdapter : CustomTypeAdapter<String?> {

    override fun decode(value: CustomTypeValue<*>): String? {
        return if (value != null) {
            val dateStr = value.value as String
            val dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(dateStr)
            dateTime.toString()
        } else {
            null
        }
    }

    override fun encode(value: String?): CustomTypeValue<*> {
        return CustomTypeValue.GraphQLString(value!!)
    }
}