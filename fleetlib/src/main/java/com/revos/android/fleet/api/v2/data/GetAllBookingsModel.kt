package com.revos.android.fleet.api.v2.data


import com.google.gson.annotations.SerializedName

internal data class GetAllBookingsModel(
    @SerializedName("data")
    val data : Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int) {

    data class Data(
        @SerializedName("bookings")
        val bookings: List<BookingDetails>,
        @SerializedName("count")
        val count: Int )
}