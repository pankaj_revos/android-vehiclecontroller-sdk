package com.revos.android.fleet.api.v2.data


import com.google.gson.annotations.SerializedName

internal data class BookingModel(
    @SerializedName("data")
    val data: Data,
    @SerializedName("status")
    val status: Int,
    @SerializedName("message")
    val message: String) {

    data class Data(
        @SerializedName("asset")
        val asset: Asset,
        @SerializedName("bookingDetails")
        val bookingDetails: BookingDetails,
        @SerializedName("leasee")
        val leasee: Leasee,
        @SerializedName("leasor")
        val leasor: Leasor) {

        data class Asset(
            @SerializedName("id")
            val id: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("vehicle")
            val vehicle: Vehicle) {

            data class Vehicle(
                @SerializedName("id")
                val id: String,
                @SerializedName("status")
                val status: String,
                @SerializedName("vin")
                val vin: String
            )
        }

        data class Leasee(
            @SerializedName("id")
            val id: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("user")
            val user: User) {

            data class User(
                @SerializedName("email")
                val email: String,
                @SerializedName("firstName")
                val firstName: String,
                @SerializedName("id")
                val id: String,
                @SerializedName("phone")
                val phone: String
            )
        }

        data class Leasor(
            @SerializedName("company")
            val company: Company,
            @SerializedName("id")
            val id: String,
            @SerializedName("type")
            val type: String) {

            data class Company(
                @SerializedName("id")
                val id: String,
                @SerializedName("name")
                val name: String
            )
        }
    }
}