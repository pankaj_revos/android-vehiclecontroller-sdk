package com.revos.android.fleet.data

data class DataFeedSnapshotLocation (
    var latitude : Double? = 0.0,
    var longitude : Double? = 0.0,
    var altitude : Double? = 0.0,
    var satellites : Int? = 0,
    private var xAcc : Int? = 0,
    private var yAcc : Int? = 0,
    private var zAcc : Int? = 0,
    var gpsSpeed : Double? = 0.0,
    var type: String? = null,
    var timestamp: String? = null,
    var terminalID: String? = null,
    var vin: String? = null,
)