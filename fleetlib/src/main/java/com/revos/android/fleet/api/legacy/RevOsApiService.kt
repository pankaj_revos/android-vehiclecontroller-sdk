package com.revos.android.fleet.api.legacy

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface RevOsApiService {

    companion object {
        private const val BASE_URL = "https://api.revos.in/v2/"

        fun create(): RevOsApiService {
            val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }
            val client = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
                .create(RevOsApiService::class.java)
        }
    }

    @POST("vehicles/{VIN}/exec")
    fun vehicleExecuteCommand(@Path("VIN") VIN: String?,
                              @Header("token") token: String?,
                              @Header("Authorization") bearerToken: String?,
                              @Body requestBody: RequestBody?) : Call<ResponseBody>

    @POST("mobile")
    fun uploadLog(@Query("authToken") authToken: String?,
                  @Body requestBody: RequestBody?) : Call<ResponseBody>

}