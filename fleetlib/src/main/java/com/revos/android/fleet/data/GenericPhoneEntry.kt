package com.revos.android.fleet.data

import java.util.*

data class GenericPhoneEntry (
    var phoneNumber: String? = null,
    var entryType: String? = null,
    var date: Date? = null,
    var name: String? = null,
)