package com.revos.android.fleet.utils.general;

public class VersionUtils {

    public static String removePatchVersion(String versionStr) {
        try {
            String[] tokens = versionStr.split("\\.");
            return tokens[0].concat(".").concat(tokens[1]);
        } catch (Exception e) {
            return null;
        }
    }
}
