package com.revos.android.fleet.utils.vehicleDataTransfer

data class BatteryAdcParameters (
    var timestamp: Long = 0,
    var isTimeStampTrue: Long = 0,
    var batteryAdcVoltage : Float = 0f,
)