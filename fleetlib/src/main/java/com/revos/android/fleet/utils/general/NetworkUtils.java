package com.revos.android.fleet.utils.general;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BT_EVENT_WRONG_PIN;
import static com.revos.android.fleet.utils.ConstantsKt.IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.IS_SOS_SMS_IN_PROGRESS_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.IS_SOS_SMS_SENT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.NETWORKING_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.VER_1_0;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.fleet.eventbus.EventBusMessage;
import com.revos.android.fleet.repository.MainRepository;
import com.revos.android.fleet.utils.security.MetadataDecryptionHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class NetworkUtils {

    private String TAG_READ = "read";
    private String TAG_LOG = "log";
    private String TAG_WRITE = "write";
    private String TAG_VEHICLE_CONTROL = "vehicleControl";
    private String TAG_BATTERY_ADC_LOG = "batteryAdcLog";
    private String FIRMWARE_METADATA_KEY = "firmwareMetadata";
    private String VERSION_NO_KEY = "versionNo";

    private boolean mProfilePicDownloadInProgress, mDLPic1DownloadInProgress, mDLPic2DownloadInProgress;
    public boolean mTripDataDownloadInProgress = false;

    private Context mContext;
    //holds the single instance
    private static NetworkUtils mInstance = null;
    private MainRepository mainRepository = new MainRepository();

    //private constructor
    private NetworkUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static NetworkUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new NetworkUtils(context);
            //clear download flags
            mInstance.setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, false);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    private void setContext(Context context) {
        mContext = context;
    }

    public void writeByteArrayAsFile(byte[] data, File file) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (IOException e) {
        }
    }


    public boolean isSosSMSInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_SOS_SMS_IN_PROGRESS_KEY, false);
    }

    public boolean isSosSMSSent() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_SOS_SMS_SENT_KEY, false);
    }

    public boolean isReadDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isReadDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null);
        return (jsonData != null);
    }

    public boolean isLogDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isLogDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null);
        return (jsonData != null);
    }

    public boolean isParamWriteDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isParamWriteDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null);
        return (jsonData != null);
    }

    public boolean isVehicleControlDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isVehicleControlDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null);
        return (jsonData != null);
    }

    public boolean isBatteryAdcLogDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isDeviceDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isBatteryAdcLogDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null);
        return (jsonData != null);
    }

    public void setBooleanNetworkPref(String prefKey, boolean prefValue) {
        if(mContext == null) {
            return;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(prefKey, prefValue).apply();
    }

    public boolean checkAndStartVehicleMetadataDownloadIfRequired(String firmwareVersion) {
        if(firmwareVersion == null) {
            return false;
        }

        String noPatchFirmwareVersion = VersionUtils.removePatchVersion(firmwareVersion);

        if(noPatchFirmwareVersion == null) {
            return false;
        }

        boolean mIsReadDataPresentAndVerified, mIsLogDataPresentAndVerified, mIsWriteDataPresentAndVerified, mIsControlDataPresentAndVerified, mIsBatteryAdcLogDataPresentAndVerified;
        mIsReadDataPresentAndVerified = mIsLogDataPresentAndVerified = mIsWriteDataPresentAndVerified = mIsControlDataPresentAndVerified = mIsBatteryAdcLogDataPresentAndVerified = false;

        MetadataDecryptionHelper metadataDecryptionHelper = MetadataDecryptionHelper.getInstance(mContext);

        String parameterReadJSON = metadataDecryptionHelper.decryptControllerReadMetadata();
        String parameterLogJSON = metadataDecryptionHelper.decryptControllerLogMetadata();
        String parameterWriteJSON = metadataDecryptionHelper.decryptControllerWriteMetadata();
        String vehicleControlJSON = metadataDecryptionHelper.decryptVehicleControlMetadata();

        if(parameterReadJSON == null) {
            if(!isReadDataDownloadInProgress() && !isReadDataDownloadComplete()) {
               mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_READ, noPatchFirmwareVersion);
            }
        } else if(!isReadDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject parameterReadJsonObject = null;
            try {
                parameterReadJsonObject = new JsonParser().parse(parameterReadJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(parameterReadJsonObject != null && parameterReadJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(parameterReadJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = parameterReadJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_READ, noPatchFirmwareVersion);
                    } else {
                        mIsReadDataPresentAndVerified = true;
                    }
                } else {
                    mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_READ, noPatchFirmwareVersion);
                }
            } else {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_READ, noPatchFirmwareVersion);
            }
        }

        if(parameterLogJSON == null) {
            if(!isLogDataDownloadInProgress() && !isLogDataDownloadComplete()) {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_LOG, noPatchFirmwareVersion);
            }
        } else if(!isLogDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject parameterLogJsonObject = null;
            try {
                parameterLogJsonObject = new JsonParser().parse(parameterLogJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(parameterLogJsonObject != null && parameterLogJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(parameterLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = parameterLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_LOG, noPatchFirmwareVersion);
                    } else {
                        mIsLogDataPresentAndVerified = true;
                    }
                } else {
                    mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_LOG, noPatchFirmwareVersion);
                }
            } else {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_LOG, noPatchFirmwareVersion);
            }
        }

        if(parameterWriteJSON == null) {
            if(!isParamWriteDataDownloadInProgress() && !isParamWriteDataDownloadComplete()) {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_WRITE, noPatchFirmwareVersion);
            }
        } else if(!isParamWriteDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject parameterWriteJsonObject = null;
            try {
                parameterWriteJsonObject = new JsonParser().parse(parameterWriteJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(parameterWriteJsonObject != null && parameterWriteJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(parameterWriteJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = parameterWriteJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_WRITE, noPatchFirmwareVersion);
                    } else {
                        mIsWriteDataPresentAndVerified = true;
                    }
                } else {
                    mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_WRITE, noPatchFirmwareVersion);
                }
            } else {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_WRITE, noPatchFirmwareVersion);
            }
        }

        if(vehicleControlJSON == null) {
            if(!isVehicleControlDataDownloadInProgress() && !isVehicleControlDataDownloadComplete()) {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
            }
        } else if(!isVehicleControlDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject vehicleControlJsonObject = null;
            try {
                vehicleControlJsonObject = new JsonParser().parse(vehicleControlJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(vehicleControlJsonObject != null && vehicleControlJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(vehicleControlJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = vehicleControlJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
                    } else {
                        mIsControlDataPresentAndVerified = true;
                    }
                } else {
                    mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
                }
            } else {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
            }
        }

        if(noPatchFirmwareVersion != null && !noPatchFirmwareVersion.isEmpty() && new Version(noPatchFirmwareVersion).equals(new Version(VER_1_0))) {
            return mIsReadDataPresentAndVerified && mIsLogDataPresentAndVerified &&mIsWriteDataPresentAndVerified && mIsControlDataPresentAndVerified;
        }

        String batteryAdcLogJSON =  metadataDecryptionHelper.decryptBatteryAdcLogMetadata();

        if(batteryAdcLogJSON == null) {
            if(!isBatteryAdcLogDataDownloadInProgress() && !isBatteryAdcLogDataDownloadComplete()) {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
            }
        } else if(!isBatteryAdcLogDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject batteryAdcLogJsonObject = null;
            try {
                batteryAdcLogJsonObject = new JsonParser().parse(batteryAdcLogJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(batteryAdcLogJsonObject != null && batteryAdcLogJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(batteryAdcLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = batteryAdcLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
                    } else {
                        mIsBatteryAdcLogDataPresentAndVerified = true;
                    }
                } else {
                    mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
                }
            } else {
                mainRepository.downloadAndStoreControllerMetadata(mContext, TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
            }
        }

        return mIsReadDataPresentAndVerified && mIsLogDataPresentAndVerified &&mIsWriteDataPresentAndVerified && mIsControlDataPresentAndVerified && mIsBatteryAdcLogDataPresentAndVerified;

    }
}