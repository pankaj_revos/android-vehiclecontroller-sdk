package com.revos.android.fleet.utils

import android.content.Context
import com.google.gson.Gson
import com.revos.android.fleet.data.User

fun String?.isNull() : Boolean {
    if(this.isNullOrEmpty() || this.isNullOrBlank() || this == "null") return true
    return false
}

fun getUserFromPrefs(context: Context): User? {
    val userObj = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
        .getString(USER_JSON_DATA_KEY, null)
    return Gson().fromJson(userObj, User::class.java)
}

fun saveUserToPrefs(context: Context, user : User){
    val prefs = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
    prefs?.edit()?.putString(USER_JSON_DATA_KEY, Gson().toJson(user))?.apply()
}

fun clearUserPrefsToPerformSignOut(context: Context) {
    val prefs = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
    prefs?.edit()?.putString(USER_JSON_DATA_KEY, null)?.apply()
}