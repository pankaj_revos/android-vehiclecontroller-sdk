package com.revos.android.fleet.utils.vehicleDataTransfer;

import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.VER_1_0_0;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.fleet.utils.security.MetadataDecryptionHelper;
import com.revos.android.fleet.utils.general.Version;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SpeedometerDataExchanger {
    public static int PACKET_TYPE_LIVE = 0;
    public static int PACKET_TYPE_OFFLINE = 1;
    public static int PACKET_TYPE_BATTERY_ADC = 2;

    private static SpeedometerDataExchanger mInstance;
    private Context mContext;

    private int mReadParameterPacketLength, mWriteParameterPacketLength;

    private JsonObject mParameterWriteFormatJsonObject, mParameterLogFormatJsonObject, mParameterReadFormatJsonObject, mVehicleControlJsonObject, mBatteryAdcFormatJsonObject;

    private HashMap<String, JsonObject> mReadParameterIdToMetadataHashMap;
    private HashMap<String, JsonObject> mWriteParameterIdToMetadataHashMap;
    private HashMap<String, JsonObject> mVehicleControlIdToMetadataHashMap;
    private HashMap<String, JsonObject> mBatteryAdcLogParameterIdToMetadataHashMap;

    //JSON Keys
    private final String LOG_PACKET_HEADER_FORMAT_KEY = "logPacketHeaderFormat";
    private final String PARAMETERS_KEY = "parameters";
    private final String PARAMETER_CONTROL_FORMAT_KEY = "parameterControlFormat";
    private final String VEHICLE_CONTROL_KEY = "vehicleControl";
    private final String ID_KEY = "id";
    private final String TYPE_KEY = "type";
    private final String SECTION_NO_KEY = "sectionNo";
    private final String START_POSITION_KEY = "startPosition";
    private final String ACTUAL_START_POSITION_KEY = "actualStartPosition";
    private final String LENGTH_KEY = "length";
    private final String SIGNED_KEY = "signed";
    private final String BIT_STRUCTURE_KEY = "bitStructure";
    private final String BIT_POSITION_KEY = "bitPosition";
    private final String PARAMETER_FORMAT_KEY = "parameterFormat";
    private final String BYTE_ORDER_KEY = "byteOrder";
    private final String DIVISOR_KEY = "divisor";
    private String FIRMWARE_METADATA_KEY = "firmwareMetadata";
    private String VERSION_NO_KEY = "versionNo";

    //JSON consts
    private final String TYPE_NUMBER = "number";
    private final String TYPE_BIT_FLAG = "bitFlag";
    private final String TYPE_CONST_HEX = "constHex";
    private final String TYPE_RESERVED = "reserved";
    private final String TRIP_ID = "tripId";
    private final String TIMESTAMP = "timeStamp";
    private final String LSB_MSB = "LSB,MSB";
    private final String MSB_LSB = "MSB,LSB";

    private SpeedometerDataExchanger() {

    }

    public static void clearInstance() {
        mInstance = null;
    }

    //should be called after vehicle metadata has been downloaded
    //assumes vehicle metadata is present
    public static SpeedometerDataExchanger getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new SpeedometerDataExchanger();
            //set the context
            mInstance.mContext = context;

            if(mInstance.setupAndProcessJSONStrings()) {
                if(!mInstance.processParameterReadFormatData()) {
                    mInstance = null;
                    return null;
                }
                if(!mInstance.processParameterWriteFormatData()) {
                    mInstance = null;
                    return null;
                }
                if(!mInstance.processVehicleControlData()) {
                    mInstance = null;
                    return null;
                }
                if(mInstance.getMetadataFirmwareVersion() == null || mInstance.getMetadataFirmwareVersion().isEmpty()) {
                    mInstance = null;
                    return null;
                }
                Version version_1_0_0 = new Version(VER_1_0_0);
                Version currentVersion = new Version(mInstance.getMetadataFirmwareVersion());

                if(currentVersion.compareTo(version_1_0_0) > 0) {
                    if(!mInstance.processBatteryAdcLogData()) {
                        mInstance = null;
                        return null;
                    }
                }
            } else {
                mInstance = null;
                return null;
            }

        }
        return mInstance;
    }

    //populate member variable strings from shared preferences
    private boolean setupAndProcessJSONStrings() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE);
        String parameterReadEncryptedJSON = sharedPreferences.getString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null);
        String parameterLogEncryptedJSON = sharedPreferences.getString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null);
        String parameterWriteEncryptedJSON = sharedPreferences.getString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null);
        String vehicleControlEncryptedJSON = sharedPreferences.getString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null);

        if(parameterReadEncryptedJSON != null && parameterLogEncryptedJSON != null && parameterWriteEncryptedJSON != null && vehicleControlEncryptedJSON != null) {
            MetadataDecryptionHelper metadataDecryptionHelper = MetadataDecryptionHelper.getInstance(mContext);

            String parameterReadDecryptedJson = metadataDecryptionHelper.decryptControllerReadMetadata();
            String parameterLogDecryptedJson = metadataDecryptionHelper.decryptControllerLogMetadata();
            String parameterWriteDecryptedJson = metadataDecryptionHelper.decryptControllerWriteMetadata();
            String vehicleControlDecryptedJson = metadataDecryptionHelper.decryptVehicleControlMetadata();

            if(parameterReadDecryptedJson == null || parameterLogDecryptedJson == null || parameterWriteDecryptedJson == null || vehicleControlDecryptedJson == null) {
                return false;
            }

            mParameterReadFormatJsonObject = new JsonParser().parse(parameterReadDecryptedJson).getAsJsonObject();
            mParameterLogFormatJsonObject = new JsonParser().parse(parameterLogDecryptedJson).getAsJsonObject();
            mParameterWriteFormatJsonObject = new JsonParser().parse(parameterWriteDecryptedJson).getAsJsonObject();
            mVehicleControlJsonObject = new JsonParser().parse(vehicleControlDecryptedJson).getAsJsonObject();

            if(getMetadataFirmwareVersion() == null || getMetadataFirmwareVersion().isEmpty()) {
                return false;
            }

            if(new Version(getMetadataFirmwareVersion()).compareTo(new Version(VER_1_0_0)) == 0) {
                return true;
            }


            String batteryAdcLogEncryptedJSON = sharedPreferences.getString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null);
            if(batteryAdcLogEncryptedJSON == null) {
                return false;
            }

            String batteryAdcLogDecryptedJson = metadataDecryptionHelper.decryptBatteryAdcLogMetadata();
            if(batteryAdcLogDecryptedJson == null) {
                return false;
            }
            mBatteryAdcFormatJsonObject = new JsonParser().parse(batteryAdcLogDecryptedJson).getAsJsonObject();

            return true;
        } else {
            return false;
        }
    }

    private boolean processVehicleControlData() {
        try {
            createVehicleControlIdToMetadataHashMap();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    private boolean processParameterReadFormatData() {
        try {
            createReadParameterIdToMetadataHashMap();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean processParameterWriteFormatData() {
        try {
            createWriteParameterIdToMetadataHashMap();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean processBatteryAdcLogData() {
        try {
            createBatteryVoltageADCIdToMetadataHashMap();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    JsonArray getParameterWriteFormatJsonArray() {
        if(mParameterWriteFormatJsonObject != null && mParameterWriteFormatJsonObject.has(PARAMETER_CONTROL_FORMAT_KEY)) {
            return mParameterWriteFormatJsonObject.get(PARAMETER_CONTROL_FORMAT_KEY).getAsJsonObject().getAsJsonArray(PARAMETERS_KEY).deepCopy();
        } else {
            return null;
        }
    }

    JsonObject getWriteParameterMetadata(String paramId) {
        if(mWriteParameterIdToMetadataHashMap != null && mWriteParameterIdToMetadataHashMap.containsKey(paramId)) {
            return mWriteParameterIdToMetadataHashMap.get(paramId);
        } else {
            return null;
        }
    }

    JsonObject getParameterLogFormatJsonObject() {
        return mParameterLogFormatJsonObject;
    }

    public int getOfflineLogPacketHeaderLength() {
        if(mParameterLogFormatJsonObject != null && mParameterLogFormatJsonObject.has(LOG_PACKET_HEADER_FORMAT_KEY)) {
            JsonObject logPacketHeaderFormat = mParameterLogFormatJsonObject.get(LOG_PACKET_HEADER_FORMAT_KEY).getAsJsonObject();
            if(logPacketHeaderFormat.has(LENGTH_KEY)) {
                return logPacketHeaderFormat.get(LENGTH_KEY).getAsInt();
            } else {
                return -1;
            }

        } else {
            return -1;
        }
    }

    public int getOfflineBatteryAdcLogPacketHeaderLength() {
        if(mBatteryAdcFormatJsonObject != null && mBatteryAdcFormatJsonObject.has(LOG_PACKET_HEADER_FORMAT_KEY)) {
            JsonObject logPacketHeaderFormat = mBatteryAdcFormatJsonObject.get(LOG_PACKET_HEADER_FORMAT_KEY).getAsJsonObject();
            if(logPacketHeaderFormat.has(LENGTH_KEY)) {
                return logPacketHeaderFormat.get(LENGTH_KEY).getAsInt();
            } else {
                return -1;
            }

        } else {
            return -1;
        }
    }

    public int getOfflineBatteryAdcLogParameterLength() {
        if(mBatteryAdcFormatJsonObject != null && mBatteryAdcFormatJsonObject.has(PARAMETER_FORMAT_KEY)) {
            JsonObject logPacketHeaderFormat = mBatteryAdcFormatJsonObject.get(PARAMETER_FORMAT_KEY).getAsJsonObject();
            if(logPacketHeaderFormat.has(LENGTH_KEY)) {
                return logPacketHeaderFormat.get(LENGTH_KEY).getAsInt();
            } else {
                return -1;
            }

        } else {
            return -1;
        }
    }

    public int getParameterFormatLength() {
        if(mParameterReadFormatJsonObject != null && mParameterReadFormatJsonObject.has(PARAMETER_FORMAT_KEY)) {
            JsonObject parameterFormat = mParameterReadFormatJsonObject.get(PARAMETER_FORMAT_KEY).getAsJsonObject();
            if(parameterFormat.has(LENGTH_KEY)) {
                return parameterFormat.get(LENGTH_KEY).getAsInt();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public String getMetadataFirmwareVersion() {
        if(mParameterReadFormatJsonObject.has(FIRMWARE_METADATA_KEY)) {
            JsonObject firmwareMetadataJsonObject = mParameterReadFormatJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY);
            if(firmwareMetadataJsonObject.has(VERSION_NO_KEY)) {
                return firmwareMetadataJsonObject.get(VERSION_NO_KEY).getAsString();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private void createVehicleControlIdToMetadataHashMap() {
        mVehicleControlIdToMetadataHashMap = new HashMap<>();

        //get vehicle control object
        JsonObject vehicleControlJsonObject = mVehicleControlJsonObject.getAsJsonObject(VEHICLE_CONTROL_KEY);
        //get vehicle control parameters array
        JsonArray parametersArray = vehicleControlJsonObject.getAsJsonArray(PARAMETERS_KEY);

        for(JsonElement jsonElement : parametersArray) {
            mVehicleControlIdToMetadataHashMap.put(jsonElement.getAsJsonObject().get(ID_KEY).getAsString(), jsonElement.getAsJsonObject());
        }
    }

    private void createBatteryVoltageADCIdToMetadataHashMap() {
        mBatteryAdcLogParameterIdToMetadataHashMap = new HashMap<>();

        //get the parameters JSON array
        JsonArray parameterJsonArray = mBatteryAdcFormatJsonObject.getAsJsonObject(PARAMETER_FORMAT_KEY).getAsJsonArray(PARAMETERS_KEY);

        for (JsonElement jsonElement : parameterJsonArray) {
            JsonObject parameterJsonObject = jsonElement.getAsJsonObject();
            String parameterType = parameterJsonObject.get(TYPE_KEY).getAsString();


            if (parameterType.equals(TYPE_NUMBER)) {
                //perform a deepCopy
                JsonObject metadataJsonObject = parameterJsonObject.deepCopy();
                metadataJsonObject.addProperty(TYPE_KEY, parameterType);
                //add start position value
                metadataJsonObject.addProperty(START_POSITION_KEY, parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                //put this object inside the hashMap
                mBatteryAdcLogParameterIdToMetadataHashMap.put(metadataJsonObject.get(ID_KEY).getAsString(), metadataJsonObject);
            } else if (parameterType.equals(TYPE_BIT_FLAG)) {
                //iterate over the bitStructure array
                JsonArray bitStructureArray = parameterJsonObject.getAsJsonArray(BIT_STRUCTURE_KEY);
                for (int bitStructureIndex = 0; bitStructureIndex < bitStructureArray.size(); ++bitStructureIndex) {
                    //perform a deepCopy
                    JsonObject metadataJsonObject = bitStructureArray.get(bitStructureIndex).getAsJsonObject().deepCopy();
                    metadataJsonObject.addProperty(TYPE_KEY, parameterType);
                    //add start position value for bit flag as well, this will be used for log packets
                    metadataJsonObject.addProperty(START_POSITION_KEY, parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                    //put this object inside the hashMap
                    mBatteryAdcLogParameterIdToMetadataHashMap.put(metadataJsonObject.get(ID_KEY).getAsString(), metadataJsonObject);
                }
            }
        }
    }


    private void createReadParameterIdToMetadataHashMap() {
        mReadParameterIdToMetadataHashMap = new HashMap<>();
        mReadParameterPacketLength = 0;

        //iterate over all the sections and create a parameterId to metadataJsonObject hashmap
        Set<Map.Entry<String, JsonElement>> outerEntrySet = mParameterReadFormatJsonObject.entrySet();
        for(Map.Entry<String,JsonElement> outerEntry : outerEntrySet) {

            JsonObject sectionJsonObject = outerEntry.getValue().getAsJsonObject();
            if(!sectionJsonObject.has(SECTION_NO_KEY)) {
                continue;
            }
            int sectionNo = sectionJsonObject.get(SECTION_NO_KEY).getAsInt();
            int sectionStartPosition = 0;
            int sectionLength = sectionJsonObject.get(LENGTH_KEY).getAsInt();
            mReadParameterPacketLength += sectionLength;

            //now find all the sections before this section to calculate the startPosition of this section
            //iterate over map
            Set<Map.Entry<String, JsonElement>> entrySet = mParameterReadFormatJsonObject.entrySet();
            for (Map.Entry<String, JsonElement> entry : entrySet) {
                JsonObject currentObject = entry.getValue().getAsJsonObject();

                //check if section no is less that parameterFormatObject section no
                if (currentObject.has(SECTION_NO_KEY)) {
                    int currentSectionNo = currentObject.get(SECTION_NO_KEY).getAsInt();
                    if (currentSectionNo < sectionNo) {
                        sectionStartPosition += currentObject.get(LENGTH_KEY).getAsInt();
                    }
                }
            }

            //find parameter JSON array
            JsonArray parameterJsonArray = sectionJsonObject.getAsJsonArray(PARAMETERS_KEY);

            for (JsonElement jsonElement : parameterJsonArray) {
                JsonObject parameterJsonObject = jsonElement.getAsJsonObject();
                String parameterType = parameterJsonObject.get(TYPE_KEY).getAsString();


                if (parameterType.equals(TYPE_NUMBER)) {
                    //perform a deepCopy
                    JsonObject metadataJsonObject = parameterJsonObject.deepCopy();
                    metadataJsonObject.addProperty(TYPE_KEY, parameterType);

                    //add start position value
                    metadataJsonObject.addProperty(START_POSITION_KEY, parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                    //add actualStartPostion value
                    metadataJsonObject.addProperty(ACTUAL_START_POSITION_KEY, sectionStartPosition + parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                    //put this object inside the hashMap
                    mReadParameterIdToMetadataHashMap.put(metadataJsonObject.get(ID_KEY).getAsString(), metadataJsonObject);
                } else if (parameterType.equals(TYPE_BIT_FLAG)) {
                    //iterate over the bitStructure array
                    JsonArray bitStructureArray = parameterJsonObject.getAsJsonArray(BIT_STRUCTURE_KEY);
                    for (int bitStructureIndex = 0; bitStructureIndex < bitStructureArray.size(); ++bitStructureIndex) {
                        //perform a deepCopy
                        JsonObject metadataJsonObject = bitStructureArray.get(bitStructureIndex).getAsJsonObject().deepCopy();
                        metadataJsonObject.addProperty(TYPE_KEY, parameterType);

                        //add start position value for bit flag as well, this will be used for log packets
                        metadataJsonObject.addProperty(START_POSITION_KEY, parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                        //add actualStartPosition value
                        metadataJsonObject.addProperty(ACTUAL_START_POSITION_KEY, sectionStartPosition + parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                        //put this object inside the hashMap
                        mReadParameterIdToMetadataHashMap.put(metadataJsonObject.get(ID_KEY).getAsString(), metadataJsonObject);
                    }
                }
            }
        }

    }

    Object decodeParamValue(String parameterId, String[] dataArray, int packetType) {
        //get metadata Json object
        JsonObject metadataJsonObject = mReadParameterIdToMetadataHashMap.get(parameterId);
        if(packetType == PACKET_TYPE_BATTERY_ADC) {
            metadataJsonObject = mBatteryAdcLogParameterIdToMetadataHashMap.get(parameterId);
        }

        if(metadataJsonObject == null) {
            return null;
        }

        String startPositionKey = ACTUAL_START_POSITION_KEY;
        //set the key value that will be used to extract the start position
        if(packetType == PACKET_TYPE_OFFLINE || packetType == PACKET_TYPE_BATTERY_ADC) {
            startPositionKey = START_POSITION_KEY;
        }

        //check type of parameter
        String paramType = metadataJsonObject.get(TYPE_KEY).getAsString();
        Object paramValue = null;
        if(paramType.equals(TYPE_NUMBER)) {
            //extract the actualStartPosition and find out the order of bytes
            int startPosition = metadataJsonObject.get(startPositionKey).getAsInt();
            int length = metadataJsonObject.get(LENGTH_KEY).getAsInt();
            boolean isMSBOnLeftSide = true;
            //check if param length is greater than 1
            if(length > 1) {
                if(metadataJsonObject.has(BYTE_ORDER_KEY) && metadataJsonObject.get(BYTE_ORDER_KEY).getAsString().equals(LSB_MSB)) {
                    isMSBOnLeftSide = false;
                }

                //construct byteString list
                ArrayList<String> byteStringArrayList = new ArrayList<>();
                for(int arrayIndex = startPosition; arrayIndex < startPosition + length; ++arrayIndex) {
                    byteStringArrayList.add(dataArray[arrayIndex]);
                }

                //reverse the list if required
                if(!isMSBOnLeftSide) {
                    Collections.reverse(byteStringArrayList);
                }

                //construct hex string to be parsed from the list
                String hexString = "";
                for(String dataString : byteStringArrayList) {
                    hexString += dataString;
                }

                //parse this value
                paramValue = Long.parseLong(hexString, 16);

                if(metadataJsonObject.has(SIGNED_KEY) && metadataJsonObject.get(SIGNED_KEY).getAsBoolean() && length == 2) {
                    int signedInt = (short)Integer.parseInt(hexString, 16);
                    paramValue = Integer.valueOf(signedInt).longValue();
                }

            } else if(metadataJsonObject.get(LENGTH_KEY).getAsInt() == 1){
                paramValue = Long.parseLong(dataArray[metadataJsonObject.get(startPositionKey).getAsInt()], 16);
            }

            //use the divisor, if any
            if(metadataJsonObject.has(DIVISOR_KEY)) {
                float divisor = metadataJsonObject.get(DIVISOR_KEY).getAsFloat();
                paramValue = (long)paramValue / divisor;
            }

        } else if(paramType.equals(TYPE_BIT_FLAG)) {
            //extract the byte value first
            paramValue = Integer.parseInt(dataArray[metadataJsonObject.get(startPositionKey).getAsInt()], 16);
            int length = metadataJsonObject.get(LENGTH_KEY).getAsInt();
            int bitPosition = metadataJsonObject.get(BIT_POSITION_KEY).getAsInt();

            //now extract the bit flag value
            //shift the number right according to length
            paramValue = (int)paramValue >> bitPosition;

            //construct mask string
            int maskInt = 0;
            for(int shiftIndex = 0; shiftIndex < length; ++shiftIndex) {
                maskInt = maskInt << 1;
                maskInt = maskInt | 1;
            }

            //apply bitmask to get the actual param value
            paramValue = (int)paramValue & maskInt;
        }

        return paramValue;
    }

    private void createWriteParameterIdToMetadataHashMap() {
        mWriteParameterIdToMetadataHashMap = new HashMap<>();
        mWriteParameterPacketLength = 0;

        //iterate over all the sections and create a parameterId to metadataJsonObject hashmap
        Set<Map.Entry<String, JsonElement>> outerEntrySet = mParameterWriteFormatJsonObject.entrySet();
        for(Map.Entry<String,JsonElement> outerEntry : outerEntrySet) {

            JsonObject sectionJsonObject = outerEntry.getValue().getAsJsonObject();
            if(!sectionJsonObject.has(SECTION_NO_KEY)) {
                continue;
            }
            int sectionNo = sectionJsonObject.get(SECTION_NO_KEY).getAsInt();
            int sectionStartPosition = 0;
            int sectionLength = sectionJsonObject.get(LENGTH_KEY).getAsInt();
            mWriteParameterPacketLength += sectionLength;

            //now find all the sections before this section to calculate the startPosition of this section
            //iterate over map
            Set<Map.Entry<String, JsonElement>> entrySet = mParameterWriteFormatJsonObject.entrySet();
            for (Map.Entry<String, JsonElement> entry : entrySet) {
                JsonObject currentObject = entry.getValue().getAsJsonObject();

                //check if section no is less that parameterFormatObject section no
                if (currentObject.has(SECTION_NO_KEY)) {
                    int currentSectionNo = currentObject.get(SECTION_NO_KEY).getAsInt();
                    if (currentSectionNo < sectionNo) {
                        sectionStartPosition += currentObject.get(LENGTH_KEY).getAsInt();
                    }
                }
            }

            //now we know the start position of ParameterFormatJsonObject
            //iterate over parameterFormat, extract parameter metadata, create metadata object and store it inside the hashmap

            //find parameter JSON array
            JsonArray parameterJsonArray = sectionJsonObject.getAsJsonArray(PARAMETERS_KEY);

            for (JsonElement jsonElement : parameterJsonArray) {
                JsonObject parameterJsonObject = jsonElement.getAsJsonObject();
                String parameterType = parameterJsonObject.get(TYPE_KEY).getAsString();


                if (parameterType.equals(TYPE_NUMBER) || parameterType.equals(TYPE_CONST_HEX)) {
                    //perform a deepCopy
                    JsonObject metadataJsonObject = parameterJsonObject.deepCopy();
                    metadataJsonObject.addProperty(TYPE_KEY, parameterType);

                    //add actualStartPostion value
                    metadataJsonObject.addProperty(ACTUAL_START_POSITION_KEY, sectionStartPosition + parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                    //put this object inside the hashMap
                    mWriteParameterIdToMetadataHashMap.put(metadataJsonObject.get(ID_KEY).getAsString(), metadataJsonObject);
                } else if (parameterType.equals(TYPE_BIT_FLAG)) {
                    //iterate over the bitStructure array
                    JsonArray bitStructureArray = parameterJsonObject.getAsJsonArray(BIT_STRUCTURE_KEY);
                    for (int bitStructureIndex = 0; bitStructureIndex < bitStructureArray.size(); ++bitStructureIndex) {
                        //perform a deepCopy
                        JsonObject metadataJsonObject = bitStructureArray.get(bitStructureIndex).getAsJsonObject().deepCopy();
                        metadataJsonObject.addProperty(TYPE_KEY, parameterType);

                        //add actualStartPosition value
                        metadataJsonObject.addProperty(ACTUAL_START_POSITION_KEY, sectionStartPosition + parameterJsonObject.get(START_POSITION_KEY).getAsInt());
                        //put this object inside the hashMap
                        mWriteParameterIdToMetadataHashMap.put(metadataJsonObject.get(ID_KEY).getAsString(), metadataJsonObject);
                    }
                }
            }
        }

    }

    int getReadParameterPacketLength() {
        return mReadParameterPacketLength;
    }

    int getWriteParameterPacketLength() {
        return mWriteParameterPacketLength;
    }

    JsonObject getParameterWriteFormatJsonObject() {
        return mParameterWriteFormatJsonObject;
    }

    JsonObject getVehicleControlMetadata(String paramId) {
        if(mVehicleControlIdToMetadataHashMap.containsKey(paramId)) {
            return mVehicleControlIdToMetadataHashMap.get(paramId);
        } else {
            return null;
        }
    }
}
