package com.revos.android.fleet.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DataFeedAllTripsLocation (
    var timestamp: String? = null,
    var lat : Double = 0.0,
    var lng : Double = 0.0,
) : Parcelable