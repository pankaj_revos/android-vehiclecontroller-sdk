package com.revos.android.fleet.storage.prefs

import android.content.Context
import com.google.gson.Gson
import com.revos.android.fleet.data.Device
import com.revos.android.fleet.data.Model
import com.revos.android.fleet.data.Vehicle
import com.revos.android.fleet.utils.*
import com.revos.scripts.FetchDeviceQuery
import com.revos.scripts.FetchVehicleQuery
import com.revos.scripts.SetPinMutation
import java.lang.Exception
import kotlin.math.floor
import kotlin.math.round

class LocalPrefs {
    companion object {

        fun saveDeviceDetailsToPrefs(context: Context, fetchedVehicle : FetchVehicleQuery.Get) : Device {
            val vehicle = saveVehicleVariablesInPrefs(context, fetchedVehicle)
            val deviceFromServer = fetchedVehicle.device()
            val device = Device(
                deviceId = deviceFromServer?.deviceId(),
                id = deviceFromServer?.id(),
                macId = deviceFromServer?.macId(),
                key = deviceFromServer?.key(),
                status = deviceFromServer?.status(),
                deviceType = deviceFromServer?.type() ,
                vehicle = vehicle,
                firmware = deviceFromServer?.firmware(),
                expectedFirmware = deviceFromServer?.expectedFirmware(),
                deviceUpdateStatus = deviceFromServer?.updateStatus(),
                isPinResetRequired = deviceFromServer?.pinResetRequired(),
                odoResetRequired = deviceFromServer?.odoResetRequired(),
                pin = deviceFromServer?.pin()
                )
            //write this data to shared prefs
            context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                .putString(DEVICE_JSON_DATA_KEY, Gson().toJson(device)).apply()

            return device
        }

        fun getDeviceObjectFromPrefs(context: Context): Device? {
            val deviceStr: String = context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE)
                .getString(DEVICE_JSON_DATA_KEY, null)?: return null
            return try {
                Gson().fromJson(deviceStr, Device::class.java)
            } catch (e: Exception) {
                null
            }
        }

        fun saveDeviceDetailsToPrefs(context: Context, deviceFromServer:  SetPinMutation.Device1) : Device {
            val device = Device(
                id =  deviceFromServer.id(),
                deviceId = deviceFromServer.deviceId(),
                macId = deviceFromServer.macId(),
                key = deviceFromServer.key(),
                status = deviceFromServer.status(),
                deviceType = deviceFromServer.type(),
                vehicle = saveVehicleVariablesInPrefs(context, deviceFromServer),
                firmware = deviceFromServer.firmware(),
                expectedFirmware = deviceFromServer.expectedFirmware(),
                deviceUpdateStatus = deviceFromServer.updateStatus(),
                isPinResetRequired = deviceFromServer.pinResetRequired(),
            )

            if (deviceFromServer.odoResetRequired() != null) {
                context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                    .putBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY,deviceFromServer.odoResetRequired()!!).apply()
            }

            context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                .putString(DEVICE_JSON_DATA_KEY, Gson().toJson(device)).apply()

            return device
        }

        private fun saveVehicleVariablesInPrefs(context: Context, deviceFromServer: SetPinMutation.Device1): Vehicle? {
            val model = Model(
                id = deviceFromServer.vehicle()?.model()?.id(),
                name = deviceFromServer.vehicle()?.model()?.name(),
                protocol = deviceFromServer.vehicle()?.model()?.protocol(),
                modelAccessType = deviceFromServer.vehicle()?.model()?.config()?.accessType(),
                batteryMaxVoltageLimit = deviceFromServer.vehicle()?.model()?.config()?.batteryMaxVoltage()!!,
                batteryMinVoltageLimit = deviceFromServer.vehicle()?.model()?.config()?.batteryMinVoltage()!!,
                isHillAssist = deviceFromServer.vehicle()?.model()?.config()?.hillAssistStatus(),
                isParking = deviceFromServer.vehicle()?.model()?.config()?.parkingStatus(),
                eAbs = deviceFromServer.vehicle()?.model()?.config()?.eabsStatus(),
                isRegenBraking = deviceFromServer.vehicle()?.model()?.config()?.regenBrakingStatus()
            )
            if(deviceFromServer.vehicle()?.model()?.config()?.speedDivisor() != null){
                model.speedDivisor = round(floor(deviceFromServer.vehicle()?.model()?.config()?.speedDivisor()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.odoDivisor() != null){
                model.odoDivisor = round(floor(deviceFromServer.vehicle()?.model()?.config()?.odoDivisor()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.maxSpeed() != null){
                model.maxSpeed = round(floor(deviceFromServer.vehicle()?.model()?.config()?.maxSpeed()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.speedLimit() != null){
                model.speedLimit = round(floor(deviceFromServer.vehicle()?.model()?.config()?.speedLimit()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.pickupControlLimit() != null){
                model.pickupLimit = round(floor(deviceFromServer.vehicle()?.model()?.config()?.pickupControlLimit()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.brakeRegenLimit() != null){
                model.brakeRegenLimit = round(floor(deviceFromServer.vehicle()?.model()?.config()?.brakeRegenLimit()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.maxSpeed() != null){
                model.zeroThrottleRegenLimit = round(floor(deviceFromServer.vehicle()?.model()?.config()?.zeroThrottleRegenLimit()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.maxSpeed() != null){
                model.currentLimit = round(floor(deviceFromServer.vehicle()?.model()?.config()?.currentLimit()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.maxSpeed() != null){
                model.overVoltageLimit = round(floor(deviceFromServer.vehicle()?.model()?.config()?.overVoltageLimit()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.underVoltageLimit() != null){
                model.underVoltageLimit = round(floor(deviceFromServer.vehicle()?.model()?.config()?.underVoltageLimit()!!)).toInt()
            }
            if(deviceFromServer.vehicle()?.model()?.config()?.wheelDiameter() != null){
                model.wheelDiameter = round(floor(deviceFromServer.vehicle()?.model()?.config()?.wheelDiameter()!!)).toInt()
            }

            var vehicle = Vehicle(
                vin = deviceFromServer.vehicle()?.vin(),
                status = deviceFromServer.vehicle()?.status(),
                odoValue = deviceFromServer.vehicle()?.metrics()?.odometer(),
                ownerId = deviceFromServer.vehicle()?.owner()?.id(),
                ownerFirstName = deviceFromServer.vehicle()?.owner()?.firstName(),
                ownerLastName = deviceFromServer.vehicle()?.owner()?.lastName(),
                ownerEmailId = deviceFromServer.vehicle()?.owner()?.email(),
                ownerPhoneNo = deviceFromServer.vehicle()?.owner()?.phone(),
                buyerFirstName = deviceFromServer.vehicle()?.buyer()?.firstName(),
                buyerLastName = deviceFromServer.vehicle()?.buyer()?.lastName(),
                buyerPhoneNo = deviceFromServer.vehicle()?.buyer()?.phone(),
                vehicleId = deviceFromServer.vehicle()?.id(),
                distributorName = deviceFromServer.vehicle()?.distributor()?.name(),
                distributorAddress = deviceFromServer.vehicle()?.distributor()?.address(),
                distributorPhoneNo1 = deviceFromServer.vehicle()?.distributor()?.phone(),
                distributorPhoneNo2 = deviceFromServer.vehicle()?.distributor()?.phone1(),
                followMeHeadlamp = deviceFromServer.vehicle()?.config()?.followMeHomeHeadLamp(),
                warrantyExpiryTime = deviceFromServer.vehicle()?.warranty()?.expiry(),
                warrantyStatus = deviceFromServer.vehicle()?.warranty()?.status(),
                invoiceCreationDate = deviceFromServer.vehicle()?.invoice()?.createdAt(),
                model = model,
            )

            if(deviceFromServer.vehicle()?.config()?.childSpeedLock() != null){
                vehicle.childSpeedLock = deviceFromServer.vehicle()?.config()?.childSpeedLock()
            }else{
                var speedLockValue = 100
                if (deviceFromServer.vehicle() != null
                    && deviceFromServer.vehicle()?.model() != null
                    && deviceFromServer.vehicle()?.model()?.config() != null
                    && deviceFromServer.vehicle()?.model()?.config()?.speedLimit() != null) {
                    speedLockValue = deviceFromServer.vehicle()?.model()?.config()?.speedLimit()!!.toInt()
                }
                vehicle.childSpeedLock = (speedLockValue)
            }
            if(!deviceFromServer.vehicle()?.invoice()?.passbook().isNullOrEmpty()){
                vehicle.invoiceStatus = deviceFromServer.vehicle()?.invoice()?.passbook()!![0].status()
            }

            context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE)
                .edit().putString(VEHICLE_JSON_KEY,Gson().toJson(vehicle)).apply()

            return vehicle
        }

        fun saveDeviceDetailsToPrefs(context: Context, deviceFromServer : FetchDeviceQuery.Get) : Device {
            val vehicle = saveVehicleVariablesInPrefs(context, deviceFromServer)
            val device = Device(
                deviceId = deviceFromServer.deviceId(),
                id = deviceFromServer.id(),
                macId = deviceFromServer.macId(),
                key = deviceFromServer.key(),
                status = deviceFromServer.status(),
                deviceType = deviceFromServer.type() ,
                vehicle = vehicle,
                firmware = deviceFromServer.firmware(),
                expectedFirmware = deviceFromServer.expectedFirmware(),
                deviceUpdateStatus = deviceFromServer.updateStatus(),
                isPinResetRequired = deviceFromServer.pinResetRequired(),
                odoResetRequired = deviceFromServer.odoResetRequired(),
                pin = deviceFromServer.pin()
            )
            //write this data to shared prefs
            context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                .putString(DEVICE_JSON_DATA_KEY, Gson().toJson(device)).apply()

            return device
        }

        private fun saveVehicleVariablesInPrefs(context: Context, fetchedDevice: FetchDeviceQuery.Get): Vehicle? {
            return try {
                val vehicleObjFromServer = fetchedDevice.vehicle() ?: return null

                val vehicle = Vehicle(
                    vin = vehicleObjFromServer.vin(),
                    status = vehicleObjFromServer.status(),
                    vehicleId = vehicleObjFromServer.id()
                )

                if(vehicleObjFromServer.metrics() != null){
                    vehicle.odoValue = vehicleObjFromServer.metrics()!!.odometer()
                }
                if(vehicleObjFromServer.owner() != null){
                    vehicle.ownerId = vehicleObjFromServer.owner()!!.id()
                    vehicle.ownerFirstName = vehicleObjFromServer.owner()!!.firstName()
                    vehicle.ownerLastName = vehicleObjFromServer.owner()!!.lastName()
                    vehicle.ownerEmailId = vehicleObjFromServer.owner()!!.email()
                    vehicle.ownerFirstName = vehicleObjFromServer.owner()!!.firstName()
                    vehicle.ownerPhoneNo = vehicleObjFromServer.owner()!!.phone()
                }

                if(vehicleObjFromServer.buyer() != null){
                    vehicle.buyerFirstName = vehicleObjFromServer.buyer()!!.firstName()
                    vehicle.buyerLastName = vehicleObjFromServer.buyer()!!.lastName()
                    vehicle.buyerPhoneNo = vehicleObjFromServer.buyer()!!.phone()
                }

                if(vehicleObjFromServer.distributor() != null){
                    vehicle.distributorName = vehicleObjFromServer.distributor()!!.name()
                    vehicle.distributorAddress = vehicleObjFromServer.distributor()!!.address()
                    vehicle.distributorPhoneNo1 = vehicleObjFromServer.distributor()!!.phone()
                    vehicle.distributorPhoneNo2 = vehicleObjFromServer.distributor()!!.phone1()
                }

                if(vehicleObjFromServer.config() != null
                    && vehicleObjFromServer.config()!!.childSpeedLock() != null ){

                    vehicle.childSpeedLock = vehicleObjFromServer.config()!!.childSpeedLock()
                }else{
                    vehicle.childSpeedLock = 100
                    if(vehicleObjFromServer.model()?.config()?.speedLimit() != null){
                        vehicle.childSpeedLock = vehicleObjFromServer.model()?.config()?.speedLimit()?.toInt()
                    }
                }

                if(vehicleObjFromServer.config() != null){
                    vehicle.followMeHeadlamp = vehicleObjFromServer.config()?.followMeHomeHeadLamp()
                }

                if(vehicleObjFromServer.warranty() != null){
                    vehicle.warrantyExpiryTime = vehicleObjFromServer.warranty()?.expiry()
                    vehicle.warrantyStatus = vehicleObjFromServer.warranty()?.status()
                }

                val invoice = vehicleObjFromServer.invoice()
                if(invoice != null){
                    vehicle.invoiceCreationDate = invoice.createdAt()
                    if(!invoice.passbook().isNullOrEmpty()){
                        vehicle.invoiceStatus = invoice.passbook()?.get(0)?.status()
                    }
                }

                val modelFromServer = vehicleObjFromServer.model()
                val modelConfig = modelFromServer?.config()
                if(modelFromServer != null){
                    var model = Model(
                        id = modelFromServer.id(),
                        name = modelFromServer.name(),
                        protocol = modelFromServer.protocol(),
                        modelAccessType = modelConfig?.accessType(),
                        speedDivisor = modelConfig?.speedDivisor()?.toInt(),
                        odoDivisor = modelConfig?.odoDivisor()?.toInt(),
                        maxSpeed = modelConfig?.maxSpeed()?.toInt(),
                        speedLimit = modelConfig?.speedLimit()?.toInt(),
                        pickupLimit = modelConfig?.pickupControlLimit()?.toInt(),
                        brakeRegenLimit = modelConfig?.brakeRegenLimit()?.toInt(),
                        zeroThrottleRegenLimit = modelConfig?.zeroThrottleRegenLimit()?.toInt(),
                        currentLimit = modelConfig?.currentLimit()?.toInt(),
                        overVoltageLimit = modelConfig?.overVoltageLimit()?.toInt(),
                        underVoltageLimit = modelConfig?.underVoltageLimit()?.toInt(),
                        wheelDiameter = modelConfig?.wheelDiameter()?.toInt(),
                        batteryMinVoltageLimit = modelConfig?.batteryMinVoltage()!!,
                        batteryMaxVoltageLimit = modelConfig?.batteryMaxVoltage()!!,
                        isHillAssist = modelConfig?.hillAssistStatus(),
                        isParking = modelConfig?.parkingStatus(),
                        eAbs = modelConfig?.eabsStatus(),
                        isRegenBraking = modelConfig?.regenBrakingStatus(),
                    )
                    vehicle.model = model
                }

                val vehicleJSONStr = Gson().toJson(vehicle)

                context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE).edit()
                    .putString(VEHICLE_JSON_KEY, vehicleJSONStr).apply()

                return vehicle
            } catch (e: Exception) {
                null
            }
        }

        fun getVehicleObjectFromPrefs(context: Context): Vehicle? {
            val vehicleStr: String = context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE)
                    .getString(VEHICLE_JSON_KEY, null) ?: return null
            return try {
                Gson().fromJson(vehicleStr, Vehicle::class.java)
            } catch (e: Exception) {
                null
            }
        }

        private fun saveVehicleVariablesInPrefs(context: Context, vehicleObjFromServer: FetchVehicleQuery.Get): Vehicle? {
            return try {
                val vehicle = Vehicle(
                    vin = vehicleObjFromServer.vin(),
                    status = vehicleObjFromServer.status(),
                    vehicleId = vehicleObjFromServer.id()
                )

                if(vehicleObjFromServer.metrics() != null){
                    vehicle.odoValue = vehicleObjFromServer.metrics()!!.odometer()
                }
                if(vehicleObjFromServer.owner() != null){
                    vehicle.ownerId = vehicleObjFromServer.owner()!!.id()
                    vehicle.ownerFirstName = vehicleObjFromServer.owner()!!.firstName()
                    vehicle.ownerLastName = vehicleObjFromServer.owner()!!.lastName()
                    vehicle.ownerEmailId = vehicleObjFromServer.owner()!!.email()
                    vehicle.ownerFirstName = vehicleObjFromServer.owner()!!.firstName()
                    vehicle.ownerPhoneNo = vehicleObjFromServer.owner()!!.phone()
                }

                if(vehicleObjFromServer.buyer() != null){
                    vehicle.buyerFirstName = vehicleObjFromServer.buyer()!!.firstName()
                    vehicle.buyerLastName = vehicleObjFromServer.buyer()!!.lastName()
                    vehicle.buyerPhoneNo = vehicleObjFromServer.buyer()!!.phone()
                }

                if(vehicleObjFromServer.distributor() != null){
                    vehicle.distributorName = vehicleObjFromServer.distributor()!!.name()
                    vehicle.distributorAddress = vehicleObjFromServer.distributor()!!.address()
                    vehicle.distributorPhoneNo1 = vehicleObjFromServer.distributor()!!.phone()
                    vehicle.distributorPhoneNo2 = vehicleObjFromServer.distributor()!!.phone1()
                }

                if(vehicleObjFromServer.config() != null
                    && vehicleObjFromServer.config()!!.childSpeedLock() != null ){
                    vehicle.childSpeedLock = vehicleObjFromServer.config()!!.childSpeedLock()
                }else{
                    vehicle.childSpeedLock = 100
                    if(vehicleObjFromServer.device()?.vehicle()?.model()?.config()?.speedLimit() != null){
                        vehicle.childSpeedLock = vehicleObjFromServer.device()?.vehicle()?.model()?.config()?.speedLimit()?.toInt()
                    }
                }

                if(vehicleObjFromServer.config() != null){
                    vehicle.followMeHeadlamp = vehicleObjFromServer.config()?.followMeHomeHeadLamp()
                }

                if(vehicleObjFromServer.warranty() != null){
                    vehicle.warrantyExpiryTime = vehicleObjFromServer.warranty()?.expiry()
                    vehicle.warrantyStatus = vehicleObjFromServer.warranty()?.status()
                }

                val invoice = vehicleObjFromServer.invoice()
                if(invoice != null){
                    vehicle.invoiceCreationDate = invoice.createdAt()
                    if(!invoice.passbook().isNullOrEmpty()){
                        vehicle.invoiceStatus = invoice.passbook()?.get(0)?.status()
                    }
                }

                val modelFromServer = vehicleObjFromServer.device()?.vehicle()?.model()
                val modelConfig = modelFromServer?.config()
                if(modelFromServer != null){
                    var model = Model(
                        id = vehicleObjFromServer.model()?.id(),
                        name = vehicleObjFromServer.model()?.name(),
                        protocol = vehicleObjFromServer.model()?.protocol(),
                        modelAccessType = modelConfig?.accessType(),
                        speedDivisor = modelConfig?.speedDivisor()?.toInt(),
                        odoDivisor = modelConfig?.odoDivisor()?.toInt(),
                        maxSpeed = modelConfig?.maxSpeed()?.toInt(),
                        speedLimit = modelConfig?.speedLimit()?.toInt(),
                        pickupLimit = modelConfig?.pickupControlLimit()?.toInt(),
                        brakeRegenLimit = modelConfig?.brakeRegenLimit()?.toInt(),
                        zeroThrottleRegenLimit = modelConfig?.zeroThrottleRegenLimit()?.toInt(),
                        currentLimit = modelConfig?.currentLimit()?.toInt(),
                        overVoltageLimit = modelConfig?.overVoltageLimit()?.toInt(),
                        underVoltageLimit = modelConfig?.underVoltageLimit()?.toInt(),
                        wheelDiameter = modelConfig?.wheelDiameter()?.toInt(),
                        batteryMinVoltageLimit = modelConfig?.batteryMinVoltage()!!,
                        batteryMaxVoltageLimit = modelConfig?.batteryMaxVoltage()!!,
                        isHillAssist = modelConfig?.hillAssistStatus(),
                        isParking = modelConfig?.parkingStatus(),
                        eAbs = modelConfig?.eabsStatus(),
                        isRegenBraking = modelConfig?.regenBrakingStatus(),
                    )
                    vehicle.model = model
                }

                val vehicleJSONStr = Gson().toJson(vehicle)

                context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE).edit()
                    .putString(VEHICLE_JSON_KEY, vehicleJSONStr).apply()

                return vehicle
            } catch (e: Exception) {
                null
            }
        }

        fun getLastConnectedDeviceVin(context: Context): String? {
            return context.getSharedPreferences(LAST_VEHICLE_PREFS, Context.MODE_PRIVATE)
                .getString(LAST_CONNECTED_DEVICE_VIN, null)
        }

        fun clearVehicleAndDeviceRelatedPrefs(context: Context) {
            context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply()
            context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply()
            context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE)
                .edit().putString(DEVICE_PIN_KEY, null).apply()
            context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE)
                .edit().putString(DEVICE_JSON_DATA_KEY, null).apply()
            context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE)
                .edit().clear().apply()
            context.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE)
                .edit().clear().apply()
            context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE)
                .edit().clear().apply()
        }

        fun clearBluetoothPrefs(context: Context){
            //clear previous metadata
            context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply()
            context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply()
            context.getSharedPreferences(BLUETOOTH_PREFS,Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply()
            context.getSharedPreferences(BLUETOOTH_PREFS,Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply()
            context.getSharedPreferences(BLUETOOTH_PREFS,Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply()
            context.getSharedPreferences(BLUETOOTH_PREFS,Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply()
            context.getSharedPreferences(BLUETOOTH_PREFS,Context.MODE_PRIVATE)
                .edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply()
        }

    }
}