package com.revos.android.fleet.api.v2.lease

import com.google.gson.GsonBuilder
import com.revos.android.fleet.BuildConfig
import com.revos.android.fleet.api.v2.data.*
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

internal interface RevosLeaseService {

    companion object {
                private val BASE_URL = if(BuildConfig.DEBUG) "https://bookings.dev.revos.in/" else "https://bookings.revos.in/"

                fun create(): RevosLeaseService {
                    val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

                    val clientBuilder = OkHttpClient.Builder()
                        .connectTimeout(100, TimeUnit.SECONDS)
                        .readTimeout(100, TimeUnit.SECONDS)

                    val client = if(BuildConfig.DEBUG){
                        clientBuilder.addInterceptor(logger).build()
                    }else{
                        clientBuilder.build()
                    }

                    return Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                        .build()
                        .create(RevosLeaseService::class.java)
        }
    }

    @GET("vehicles")
    suspend fun getAllRentalVehicles(@Query("first") first: Int,
                                     @Query("orderby") orderBy : String?,
                                     @Query("skip") skip : Int,
                                     @Header("token") token: String,
                                     @Header("Authorization") bearerToken: String) : GetAllVehiclesModel

    @GET("vehicles/{vin}")
    suspend fun getRentalVehicle(@Path("vin") vin : String,
                       @Header("token") token: String,
                       @Header("Authorization") bearerToken: String) : RentalVehicleModel


    @GET("users/bookings")
    suspend fun getAllBookings(@Query("first") first : Int,
                               @Query("skip") skip : Int,
                               @Header("token") token: String,
                               @Header("Authorization") bearerToken: String) : GetAllBookingsModel

    @GET("users/booking/{bookingId}")
    suspend fun getUserBooking(@Path("bookingId") bookingId : String,
                               @Header("token") token: String,
                               @Header("Authorization") bearerToken: String) : BookingModel

    @POST("users/booking/create")
    suspend fun createBooking(@Header("token") token: String,
                              @Header("Authorization") bearerToken: String,
                              @Body requestBody: RequestBody) : BookingResponseModel

    @POST("users/booking/{bookingId}/start")
    suspend fun startBooking(@Path("bookingId") bookingId : String,
                             @Header("token") token: String,
                             @Header("Authorization") bearerToken: String) : BookingResponseModel

    @POST("vehicles/{vin}/unlock")
    fun unlockVehicle(@Path("vin") vin : String,
                              @Header("token") token: String,
                              @Header("Authorization") bearerToken: String) : Call<ResponseBody>

    @POST("vehicles/{vin}/lock")
    fun lockVehicle(@Path("vin") vin : String,
                              @Header("token") token: String,
                              @Header("Authorization") bearerToken: String) : Call<ResponseBody>

    /*
    * Before start booking, vehicle should be available for modified time slots
    * */
    @POST("users/booking/{bookingId}/modify")
    suspend fun modifyBooking(@Path("bookingId") bookingId : String,
                              @Header("token") token: String,
                              @Header("Authorization") bearerToken: String,
                              @Body requestBody: RequestBody) : BookingResponseModel

    @POST("users/booking/{bookingId}/payment/init")
    suspend fun paymentInit(@Path("bookingId") bookingId : String,
                              @Header("token") token: String,
                              @Header("Authorization") bearerToken: String,
                              @Body requestBody: RequestBody) : BookingResponseModel

    @POST("users/booking/{bookingId}/payment/confirm")
    suspend fun paymentConfirm(@Path("bookingId") bookingId : String,
                            @Header("token") token: String,
                            @Header("Authorization") bearerToken: String) : BookingResponseModel

    @POST("users/booking/{bookingId}/payment/confirm")
    suspend fun paymentConfirm(@Path("bookingId") bookingId : String,
                               @Header("token") token: String,
                               @Header("Authorization") bearerToken: String,
                               @Body requestBody: RequestBody) : BookingResponseModel

    @POST("users/booking/{bookingId}/payment/fail")
    suspend fun paymentFailed(@Path("bookingId") bookingId : String,
                               @Header("token") token: String,
                               @Header("Authorization") bearerToken: String) : BookingResponseModel

    @POST("users/booking/{bookingId}/cancel")
    suspend fun cancelBooking(@Path("bookingId") bookingId : String,
                              @Header("token") token: String,
                              @Header("Authorization") bearerToken: String,
                              @Body requestBody: RequestBody) : BookingResponseModel

    @POST("users/booking/{bookingId}/end")
    suspend fun endBooking(@Path("bookingId") bookingId : String,
                           @Header("token") token: String,
                           @Header("Authorization") bearerToken: String) : BookingResponseModel
}