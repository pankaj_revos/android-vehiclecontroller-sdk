package com.revos.android.fleet.utils.vehicleDataTransfer;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.revos.android.fleet.ble.BleUtils;

public class ControllerWriteCommandHelper {
    private long mControlCheckByte;
    private long mSpeedCheckByte;
    private long mCurrentCheckByte;
    private long mUnderVoltageCheckByte;
    private long mOverVoltageCheckByte;
    private long mThrottleZeroRegenCheckByte;
    private long mBrakeRegenCheckByte;
    private long mPickupControlCheckByte;

    private VehicleParameters mCurrentVehicleParameters;
    private SpeedometerDataExchanger mSpeedometerDataExchanger;

    private final String RESERVED = "reserved";

    //JSON keys
    private final String BIT_POSITION_KEY = "bitPosition";
    private final String PACKET_HEADER_KEY = "packetHeader";
    private final String VALUE_KEY = "value";
    private final String ID_KEY = "id";
    private final String TYPE_KEY = "type";
    private final String START_POSITION_KEY = "startPosition";
    private final String PACKET_HEADER_FORMAT_KEY = "packetHeaderFormat";
    private final String LENGTH_KEY = "length";
    private final String MARKER_KEY = "marker";


    private final String BIT_MODE_CONTROL_KEY = "bitModeControl";
    private final String BIT_EABS_CONTROL_KEY = "bitEAbsControl";
    private final String BIT_REGEN_BRAKING_CONTROL_KEY = "bitRegenBrakingControl";
    private final String BIT_PARKING_CONTROL_KEY = "bitParkingControl";
    private final String BIT_HILL_ASSIST_CONTROL_KEY = "bitHillAssistControl";
    private final String BIT_SOFT_LOCK_CONTROL_KEY = "bitSoftLockControl";
    private final String BIT_WHEEL_LOCK_CONTROL_KEY = "bitWheelLockControl";

    private final String BIT_CONTROL1_KEY = "bitControl1";
    private final String SPEED_PERCENT_KEY = "speedPercent";
    private final String OVER_CURRENT_LIMIT_KEY = "overCurrentLimit";
    private final String UNDER_VOLTAGE_LIMIT_KEY = "underVoltageLimit";
    private final String OVER_VOLTAGE_LIMIT_KEY = "overVoltageLimit";
    private final String THROTTLE_ZERO_REGEN_LIMIT_KEY = "throttleZeroRegen";
    private final String BRAKE_REGEN_LIMIT_KEY = "brakeRegen";
    private final String PICK_UP_CONTROL_LIMIT_KEY = "pickupControl";


    public ControllerWriteCommandHelper (VehicleParameters vehicleParameters, Context context) {
        mCurrentVehicleParameters = vehicleParameters;
        mSpeedometerDataExchanger = SpeedometerDataExchanger.getInstance(context);

        mControlCheckByte = vehicleParameters.getControlCheckByte();
        mSpeedCheckByte = vehicleParameters.getSpeedCheckByte();
        mCurrentCheckByte = vehicleParameters.getCurrentCheckByte();
        mUnderVoltageCheckByte = vehicleParameters.getUnderVoltageCheckByte();
        mOverVoltageCheckByte = vehicleParameters.getOverVoltageCheckByte();
        mThrottleZeroRegenCheckByte = vehicleParameters.getThrottleZeroRegenCheckByte();
        mBrakeRegenCheckByte = vehicleParameters.getBrakeRegenCheckByte();
        mPickupControlCheckByte = vehicleParameters.getPickupPercentageCheckByte();
    }

    private void setControlCheckByteForBitFlag(String bitFlagId, int value) {
        JsonObject parameterMetadataJsonObject = mSpeedometerDataExchanger.getWriteParameterMetadata(bitFlagId);

        int bitPosition = parameterMetadataJsonObject.get(BIT_POSITION_KEY).getAsInt();
        int length = parameterMetadataJsonObject.get(LENGTH_KEY).getAsInt();

        //reset the bits to zero first
        //for this create a reset mask

        int resetMask = 0xFF;
        //introduce zeros
        resetMask = resetMask << length;
        resetMask = resetMask & 0xFF;

        //flip the bits
        resetMask = ~resetMask;
        resetMask = resetMask & 0xFF;

        //shift by bitPosition
        resetMask = resetMask << bitPosition;
        resetMask = resetMask & 0xFF;

        //flip the bits again
        resetMask = ~resetMask;
        resetMask = resetMask & 0xFF;

        byte resetByteMask = (byte)(resetMask);

        //reset the required bits to zero
        mControlCheckByte = mControlCheckByte & resetByteMask;

        //left shift this byte and OR it with mControlCheckByte
        value = value << bitPosition;

        value = value & 0xFF;

        mControlCheckByte = mControlCheckByte | value;
    }

    public byte[] constructWriteCommandByteArray() {
        byte[] commandByteArray = new byte[mSpeedometerDataExchanger.getWriteParameterPacketLength()];

        //first packet is always the header
        String valueString = mSpeedometerDataExchanger.getWriteParameterMetadata(PACKET_HEADER_KEY)
                                .get(VALUE_KEY).getAsString();

        commandByteArray[0] = BleUtils.hexStringWithZeroXToByte(valueString);

        //header length
        int headerLength = mSpeedometerDataExchanger.getParameterWriteFormatJsonObject().get(PACKET_HEADER_FORMAT_KEY).getAsJsonObject().get(LENGTH_KEY).getAsInt();

        JsonArray parameterSequenceArray = mSpeedometerDataExchanger.getParameterWriteFormatJsonArray();

        //iterate over control parameter array and populate the command byte array
        for(JsonElement jsonElement : parameterSequenceArray) {
            JsonObject metadataJsonObject = jsonElement.getAsJsonObject();

            String parameterId = metadataJsonObject.get(ID_KEY).getAsString();
            String type = metadataJsonObject.get(TYPE_KEY).getAsString();
            int paramStartPosition = metadataJsonObject.get(START_POSITION_KEY).getAsInt();

            if(parameterId.equals(RESERVED)) {
                commandByteArray[paramStartPosition + headerLength] = BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(VALUE_KEY).getAsString());
            } else {
                //set the marker
                commandByteArray[paramStartPosition + headerLength] = BleUtils.hexStringWithZeroXToByte(metadataJsonObject.get(MARKER_KEY).getAsString());

                switch (parameterId) {
                    case BIT_CONTROL1_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mControlCheckByte;
                        break;

                    case SPEED_PERCENT_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mSpeedCheckByte;
                        break;

                    case OVER_CURRENT_LIMIT_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mCurrentCheckByte;
                        break;

                    case UNDER_VOLTAGE_LIMIT_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mUnderVoltageCheckByte;
                        break;

                    case OVER_VOLTAGE_LIMIT_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mOverVoltageCheckByte;
                        break;

                    case THROTTLE_ZERO_REGEN_LIMIT_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mThrottleZeroRegenCheckByte;
                        break;

                    case BRAKE_REGEN_LIMIT_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mBrakeRegenCheckByte;
                        break;

                    case PICK_UP_CONTROL_LIMIT_KEY: commandByteArray[paramStartPosition + headerLength + 1] = (byte)mPickupControlCheckByte;
                        break;
                }
            }
        }

        //calculate checksum
        int checkSum = 0;
        for(int byteIndex = headerLength; byteIndex < commandByteArray.length - 1; ++byteIndex) {
            int unsignedInt = (int)commandByteArray[byteIndex] & 0xFF;
            checkSum += unsignedInt;
        }
        checkSum = checkSum & 0xFF;

        commandByteArray[commandByteArray.length - 1] = (byte)checkSum;

        return commandByteArray;
    }

    public boolean setMode(int value) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BIT_MODE_CONTROL_KEY) != null) {
            setControlCheckByteForBitFlag(BIT_MODE_CONTROL_KEY, value);
            return true;
        } else {
            return false;
        }
    }

    public boolean setParking(boolean parking) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BIT_PARKING_CONTROL_KEY) != null) {
            //todo::this value is flipped
            int value = parking ? 0 : 1;
            setControlCheckByteForBitFlag(BIT_PARKING_CONTROL_KEY, value);
            return true;
        } else {
            return false;
        }
    }

    public boolean setHillAssist(boolean hillAssist) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BIT_HILL_ASSIST_CONTROL_KEY) != null) {
            int value = hillAssist ? 1 : 0;
            setControlCheckByteForBitFlag(BIT_HILL_ASSIST_CONTROL_KEY, value);
            return true;
        } else {
            return false;
        }
    }

    public boolean setSoftLock(boolean lock) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BIT_SOFT_LOCK_CONTROL_KEY) != null) {
            int value = lock ? 1 : 0;
            setControlCheckByteForBitFlag(BIT_SOFT_LOCK_CONTROL_KEY, value);
            return true;
        } else {
            return false;
        }
    }

    public boolean setWheelLock(boolean lock) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BIT_WHEEL_LOCK_CONTROL_KEY) != null) {
            int value = lock ? 1 : 0;
            setControlCheckByteForBitFlag(BIT_WHEEL_LOCK_CONTROL_KEY, value);
            return true;
        } else {
            return false;
        }
    }

    public boolean setEAbs(boolean eAbs) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BIT_EABS_CONTROL_KEY) != null) {
            int value = eAbs ? 1 : 0;
            setControlCheckByteForBitFlag(BIT_EABS_CONTROL_KEY, value);
            return true;
        } else {
            return false;
        }
    }

    public boolean setRegenBraking(boolean regenBraking) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BIT_REGEN_BRAKING_CONTROL_KEY) != null) {
            int value = regenBraking ? 1 : 0;
            setControlCheckByteForBitFlag(BIT_REGEN_BRAKING_CONTROL_KEY, value);
            return true;
        } else {
            return false;
        }
    }

    public boolean setBitControl1(long bitControl1) {
        //check if bitControl is present
        JsonArray jsonArray = mSpeedometerDataExchanger.getParameterWriteFormatJsonArray();

        boolean bitControl1Found = false;
        for(JsonElement jsonElement : jsonArray) {
            if(jsonElement.getAsJsonObject().get(ID_KEY).getAsString().equals(BIT_CONTROL1_KEY)) {
                bitControl1Found = true;
                break;
            }
        }

        if(bitControl1Found) {
            mControlCheckByte = bitControl1;
            return true;
        } else {
            return false;
        }
    }

    public boolean setSpeedPercent(long speedPercent) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(SPEED_PERCENT_KEY) != null) {
            mSpeedCheckByte = speedPercent;
            return true;
        } else {
            return false;
        }
    }

    public boolean setOverCurrentLimit(long overCurrentLimit) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(OVER_CURRENT_LIMIT_KEY) != null) {
            mCurrentCheckByte = overCurrentLimit;
            return true;
        } else {
            return false;
        }
    }

    public boolean setUnderVoltageLimit(long underVoltageLimit) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(UNDER_VOLTAGE_LIMIT_KEY) != null) {
            mUnderVoltageCheckByte = underVoltageLimit;
            return true;
        } else {
            return false;
        }
    }

    public boolean setOverVoltageLimit(long overVoltageLimit) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(OVER_VOLTAGE_LIMIT_KEY) != null) {
            mOverVoltageCheckByte = overVoltageLimit;
            return true;
        } else {
            return false;
        }
    }

    public boolean setThrottleZeroRegenLimit(long throttleZeroRegenLimit) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(THROTTLE_ZERO_REGEN_LIMIT_KEY) != null) {
            mThrottleZeroRegenCheckByte = throttleZeroRegenLimit;
            return true;
        } else {
            return false;
        }
    }

    public boolean setBrakeRegenLimit(long brakeRegenLimit) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(BRAKE_REGEN_LIMIT_KEY) != null) {
            mBrakeRegenCheckByte = brakeRegenLimit;
            return true;
        } else {
            return false;
        }
    }

    public boolean setPickupControlLimit(long pickupControlLimit) {
        if(mSpeedometerDataExchanger.getWriteParameterMetadata(PICK_UP_CONTROL_LIMIT_KEY) != null) {
            mPickupControlCheckByte = pickupControlLimit;
            return true;
        } else {
            return false;
        }
    }

}
