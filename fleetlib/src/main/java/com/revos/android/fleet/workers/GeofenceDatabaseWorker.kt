package com.revos.android.fleet.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.revos.android.fleet.data.GeoFencing
import com.revos.android.fleet.storage.database.AppDatabase
import com.revos.android.fleet.utils.WORK_MANAGER_KEY_GEOFENCE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GeofenceDatabaseWorker(
    context: Context,
    workers: WorkerParameters
) : CoroutineWorker(context, workers) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            val geoFencingStrObj = inputData.getString(WORK_MANAGER_KEY_GEOFENCE)
            val geofence = Gson().fromJson(geoFencingStrObj, GeoFencing::class.java)
            val database = AppDatabase.getInstance(applicationContext)
            database.geofenceDao().insert(geofence)
            Result.success()
        } catch (ex: Exception) {
            Log.e(TAG, "Error adding geofence to database", ex)
            Result.failure()
        }
    }

    companion object {
        private const val TAG = "GeofenceDatabaseWorker"
    }
}
