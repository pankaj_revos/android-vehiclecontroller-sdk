package com.revos.android.fleet.data

import java.util.*

data class BatteryRegressionData (
    var version : Int = 0,
    var dataPoints: ArrayList<BatteryRegressionDataPoint>? = null,
)