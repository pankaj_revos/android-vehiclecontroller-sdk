package com.revos.android.fleet

import android.app.ActivityManager
import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import com.revos.android.fleet.managers.LeaseManager
import com.revos.android.fleet.managers.LoginManager
import com.revos.android.fleet.managers.VehicleManager
import com.revos.android.fleet.services.NordicBleService

object FleetSDK {

    private var loginManager : LoginManager? = null
    fun getLoginManager() : LoginManager {
        if(loginManager == null){
            loginManager = LoginManager()
        }
        return loginManager!!
    }

    private var leaseManager = LeaseManager
    fun getLeaseManager() : LeaseManager {
        return leaseManager
    }

    private var vehicleManager = VehicleManager
    fun getVehicleManager() : VehicleManager {
        return vehicleManager
    }

    private var appInstance : Application? = null
    fun initialize(appInstance: Application){
        this.appInstance = appInstance
        startServices(appInstance.applicationContext)
    }

    fun getAppInstance() : Application? {
        return appInstance
    }

    private fun startServices(context: Context) {
        val nordicBleService = Intent(context, NordicBleService::class.java)
        if (!isServiceRunning(context, NordicBleService::class.java)) {
            ContextCompat.startForegroundService(context, nordicBleService)
        }
    }

    private fun isServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
            ?: return false
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}