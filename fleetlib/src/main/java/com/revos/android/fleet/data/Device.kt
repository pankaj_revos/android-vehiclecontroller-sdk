package com.revos.android.fleet.data

import android.os.Parcelable
import com.revos.android.fleet.data.Vehicle
import com.revos.scripts.type.DeviceStatus
import com.revos.scripts.type.DeviceType
import com.revos.scripts.type.DeviceUpdateStatus
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Device (
    var id: String? = null,
    var macId: String? = null,
    var deviceId: String? = null,
    var deviceType: DeviceType? = null,
    var status: DeviceStatus? = null,
    var key: String? = null,
    var isPinResetRequired : Boolean? = false,
    var vehicle: Vehicle? = null,
    var firmware: String? = null,
    var expectedFirmware: String? = null,
    var deviceUpdateStatus: DeviceUpdateStatus? = null,
    var odoResetRequired : Boolean? = false,
    var pin : String? = null
) : Parcelable