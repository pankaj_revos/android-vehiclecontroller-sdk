package com.revos.android.fleet.payment

import dev.shreyaspatil.easyupipayment.model.TransactionDetails

internal object PaymentStore {
    val transactionDetailsMap = hashMapOf<String, TransactionDetails>()
    val cashFreeResponseMap = hashMapOf<String, String>()
}