package com.revos.android.fleet.data

data class DataFeedBattery (
    var batteryVoltageAdc : Double = 0.0,
    var batteryCurrent : Double = 0.0,
    var deviceBatteryVoltage : Double = 0.0,
    var batterySOC : Double = 0.0,
    var batterySOH : Double = 0.0,
    var type: String? = null,
    var timestamp: String? = null,
    var terminalID: String? = null,
    var vin: String? = null,
)