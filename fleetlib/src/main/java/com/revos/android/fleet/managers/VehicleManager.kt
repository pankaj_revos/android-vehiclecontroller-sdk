package com.revos.android.fleet.managers

import android.app.ActivityManager
import android.bluetooth.BluetoothProfile
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import androidx.annotation.UiThread
import androidx.core.content.ContextCompat
import com.revos.android.fleet.FleetSDK
import com.revos.android.fleet.R
import com.revos.android.fleet.data.Device
import com.revos.android.fleet.data.Direction
import com.revos.android.fleet.data.TripDataFeed
import com.revos.android.fleet.data.Vehicle
import com.revos.android.fleet.data.legacy.RentalVehicle
import com.revos.android.fleet.eventbus.EventBusMessage
import com.revos.android.fleet.pnp.IconCoxDevice
import com.revos.android.fleet.pnp.PnpDevice
import com.revos.android.fleet.pnp.TbitDevice
import com.revos.android.fleet.repository.MainRepository
import com.revos.android.fleet.repository.VehicleRepository
import com.revos.android.fleet.services.NordicBleService
import com.revos.android.fleet.services.NotificationReaderService
import com.revos.android.fleet.utils.*
import com.revos.android.fleet.utils.general.PrefUtils
import com.revos.scripts.type.DeviceStatus
import com.revos.scripts.type.DeviceType
import com.revos.scripts.type.ModelProtocol
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import timber.log.Timber
import kotlin.math.floor
import kotlin.math.round
import kotlin.math.roundToInt
import kotlin.math.roundToLong

object VehicleManager {
    private val vehicleRepository = VehicleRepository()
    private val mainRepository = MainRepository()
    private val callbacks = HashSet<Callback>()
    private var fleetManagerJob: Job? = null
    private var isFindVehicleCmdExec = false
    private var isLockVehicleCmdExec = false
    private var isUnlockVehicleCmdExec = false
    private var currentPNPDevice : PnpDevice? = null

    interface Callback {
        fun onConnectionStatusUpdate(status: ConnectionStatus) {}
        fun onVehicleLocked(status: CmdResponse) {}
        fun onVehicleUnLocked(status: CmdResponse) {}
        fun onVehicleFound(status: CmdResponse) {}
        fun onVehicleAvailable(devices: ArrayList<Device>) {}
        fun onPinChange(status: PinStatus) {}
        fun onPinVerified(status: PinStatus) {}
        fun onOnGoingRental(status: BaseResponse) {}
        fun onDeviceDataRefreshed(status: BaseResponse) {}
        fun onVehicleDataRefreshed(status: BaseResponse) {}
        fun onNavigation(direction: Direction)
        fun onTripDetailsAvailable(tripDetails : List<TripDataFeed>){}
        fun onError(message: String?) {}
    }

    fun addCallback(callback: Callback) {
        callbacks.add(callback)
    }

    fun removeCallback(callback: Callback) {
        callbacks.remove(callback)
    }

    init {
        //register with the EventBus
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    @Subscribe
    fun onEventBusMessage(event: EventBusMessage) {
        when (event.status) {
            PNP_BT_EVENT_STATE_CONNECTING -> {
                for (callback in callbacks) {
                    callback.onConnectionStatusUpdate(ConnectionStatus.Connecting)
                }
            }
            PNP_BT_EVENT_STATE_CONNECTED -> {
                val device = PrefUtils.getInstance(FleetSDK.getAppInstance()!!.applicationContext).deviceObjectFromPrefs
                createPnpDevice(device, FleetSDK.getAppInstance()!!.applicationContext)
                for (callback in callbacks) {
                    callback.onConnectionStatusUpdate(ConnectionStatus.Connected)
                }
            }
            PNP_BT_EVENT_STATE_DISCONNECTED -> {
                for (callback in callbacks) {
                    callback.onConnectionStatusUpdate(ConnectionStatus.Disconnected)
                }
            }

            BT_GPS_ICONCOX_CMD_RESPONSE -> {
                try {
                    val command = event.status.split(GEN_DELIMITER)[1]
                    val responseStatus = event.status.split(GEN_DELIMITER)[2]
                    val isSuccess = responseStatus.toBoolean()

                    when (command) {
                        CMD_SEND_REMOTE_LOCK -> {
                            isLockVehicleCmdExec = isSuccess
                            if (isSuccess){
                                for (callback in callbacks) {
                                    callback.onVehicleLocked(CmdResponse.Success)
                                }
                            }
                        }
                        CMD_SEND_REMOTE_IGNITION -> {
                            isUnlockVehicleCmdExec = isSuccess
                            if (isSuccess){
                                for (callback in callbacks) {
                                    callback.onVehicleUnLocked(CmdResponse.Success)
                                }
                            }
                        }
                        CMD_SEND_FIND_VEHICLE -> {
                            isFindVehicleCmdExec = isSuccess
                            if (isSuccess){
                                for (callback in callbacks) {
                                    callback.onVehicleFound(CmdResponse.Success)
                                }
                            }
                        }
                    }
                } catch (e: Exception){
                    e.printStackTrace()
                }
            }

            NAV_EVENT_TRIP_ADVICE_DATA_UPDATED -> {
                val direction = parseNavData(event.data!!)
                for(callback in callbacks){
                    callback.onNavigation(direction)
                }
            }

            BT_EVENT_STATE_CONNECTED -> {
                for (callback in callbacks) {
                    callback.onConnectionStatusUpdate(ConnectionStatus.Connected)
                }
            }

            BT_EVENT_STATE_CONNECTING -> {
                for (callback in callbacks) {
                    callback.onConnectionStatusUpdate(ConnectionStatus.Connecting)
                }
            }

            BT_EVENT_STATE_DISCONNECTED -> {
                for (callback in callbacks) {
                    callback.onConnectionStatusUpdate(ConnectionStatus.Disconnected)
                }
            }

            NETWORK_EVENT_TRIP_DATA_REFRESHED -> {
                val listOfTrips = PrefUtils.getInstance(FleetSDK.getAppInstance()?.applicationContext).tripData
                for (callback in callbacks) {
                    callback.onTripDetailsAvailable(listOfTrips)
                }
            }

            NETWORK_EVENT_ERROR -> {
                for (callback in callbacks) {
                    callback.onError(event.message)
                }
            }
        }
    }

    fun getTripsForVehicle(vin : String) {
        CoroutineScope(Dispatchers.IO).launch {
            mainRepository.refreshTripData(FleetSDK.getAppInstance()!!.applicationContext, vin)
        }
    }

    private fun parseNavData(data : Any) : Direction {
        val tripHashMap = data as HashMap<*, *>
        var nextRoad : String = ""
        var expectedTimeToArrive : String = ""
        var turnIconBitmap : Bitmap? = null
        var distanceFromTurn : String = ""
        var currentDestination : String = ""
        var remainingDistance : String = ""
        var remainingTime : String = ""

        //updating nav advice if rerouting
        if (tripHashMap.containsKey(TRIP_ADVICE_IS_REROUTING_KEY)) {
            val isReroutingKeyValue = tripHashMap[TRIP_ADVICE_IS_REROUTING_KEY] as String
            if (isReroutingKeyValue == "yes") {
                currentDestination = "Rerouting.."
                return Direction(currentDestination = currentDestination,
                    nextRoad = nextRoad,
                    expectedTimeToArrive = expectedTimeToArrive,
                    turnIconBitmap = null,
                    remainingDistance = remainingDistance,
                    remainingTime = remainingTime,
                    distanceFromTurn = distanceFromTurn)
            }
        }
        if(tripHashMap.containsKey(TRIP_ADVICE_DESTINATION_NAME_KEY)){
            currentDestination = tripHashMap[TRIP_ADVICE_DESTINATION_NAME_KEY] as String
        }
        if(NotificationReaderService.mCachedNavigationBitmap != null){
            turnIconBitmap = NotificationReaderService.mCachedNavigationBitmap
        }
        if(tripHashMap.containsKey(TRIP_ADVICE_NEXT_ROAD_KEY)){
            nextRoad = tripHashMap[TRIP_ADVICE_NEXT_ROAD_KEY] as String
        }
        if(tripHashMap.containsKey(TRIP_ADVICE_DISTANCE_FROM_TURN_KEY)){
            val distanceFromTurnInMeters = tripHashMap[TRIP_ADVICE_DISTANCE_FROM_TURN_KEY] as Float
            val distanceFromTurnStr: String
            val unitStr: String
            if (distanceFromTurnInMeters >= 1000) {
                val distanceFromTurnInKilos = distanceFromTurnInMeters / 1000
                distanceFromTurnStr = String.format("%.1f", distanceFromTurnInKilos)
                unitStr = "km"
            } else {
                distanceFromTurnStr = distanceFromTurnInMeters.roundToLong().toString()
                unitStr = "m"
            }
            distanceFromTurn = "$distanceFromTurnStr $unitStr"
        }
        if(tripHashMap.containsKey(TRIP_ADVICE_ETA_KEY)){
            expectedTimeToArrive = tripHashMap[TRIP_ADVICE_ETA_KEY] as String
        }
        if(tripHashMap.containsKey(TRIP_ADVICE_REMAINING_DISTANCE_KEY)){
            var remainingDistanceInMeters = tripHashMap[TRIP_ADVICE_REMAINING_DISTANCE_KEY] as Float
            val remainingDistanceValueStr: String
            val unitStr: String
            if (remainingDistanceInMeters >= 1000) {
                val distanceFromTurnInKilos = remainingDistanceInMeters / 1000
                remainingDistanceValueStr = String.format("%.1f", distanceFromTurnInKilos)
                unitStr = FleetSDK.getAppInstance()!!.getString(R.string.kilometers)
            } else {
                remainingDistanceValueStr = remainingDistanceInMeters.roundToLong().toString()
                unitStr = FleetSDK.getAppInstance()!!.getString(R.string.meters)
            }
            remainingDistance = "$remainingDistanceValueStr $unitStr"
        }
        if (tripHashMap.containsKey(TRIP_ADVICE_REMAINING_TIME_KEY)) {
            val remainingTimeInMinutes = tripHashMap[TRIP_ADVICE_REMAINING_TIME_KEY].toString().toFloat().roundToInt()
            val hours = remainingTimeInMinutes / 60
            val minutes = remainingTimeInMinutes % 60
            val hoursText =
                if (hours > 0) "" + hours + FleetSDK.getAppInstance()!!.getString(R.string.hours) + " " else ""
            val minuteText = minutes.toString() + FleetSDK.getAppInstance()!!.getString(R.string.minutes)
            remainingTime = hoursText + minuteText
        }

        return Direction(nextRoad, expectedTimeToArrive, turnIconBitmap,
            distanceFromTurn,  currentDestination, remainingTime, remainingDistance)
    }

    @UiThread
    fun connectUsingDeviceId( vinNumber: String, context: Context) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        startServiceIfRequired(context)
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val status =
                        vehicleRepository.fetchDeviceDetails(
                            vinNumber,
                            context
                        )
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onConnectionStatusUpdate(status)
                    }
                }

            } catch (e: Exception) {
                Timber.e(e)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(e.message)
                    }
                }
            }
        }
    }

    fun disconnect(){
        if(currentPNPDevice != null){
            currentPNPDevice?.disconnect()
            currentPNPDevice = null
        }
        EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_DISCONNECT))
    }

    @UiThread
    fun connectUsingVin( vinNumber: String, context: Context) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }

        startServiceIfRequired(FleetSDK.getAppInstance()!!.applicationContext)

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val status =
                        vehicleRepository.connectUsingVin(
                            vinNumber,
                            context
                        )
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onConnectionStatusUpdate(status)
                    }
                }

            } catch (e: Exception) {
                Timber.e(e)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(e.message)
                    }
                }
            }
        }
    }

    private fun startServiceIfRequired(context: Context) {
        val nordicBleService = Intent(context, NordicBleService::class.java)
        if (!isServiceRunning(context, NordicBleService::class.java)) {
            ContextCompat.startForegroundService(context, nordicBleService)
        }
    }

    private fun isServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
            ?: return false
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    @UiThread
    fun refreshDeviceData( context: Context) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val status =
                        vehicleRepository.refreshDeviceData(
                            context
                        )


                for (callback in callbacks) {
                    callback.onDeviceDataRefreshed(status)
                }


            } catch (e: Exception) {
                Timber.e(e)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(e.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun refreshVehicleData( context: Context) {

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val status =
                        vehicleRepository.refreshVehicleLiveLogs(
                            context
                        )
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onVehicleDataRefreshed(status)
                    }
                }


            } catch (e: Exception) {
                Timber.e(e)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(e.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun getMyVehicles( context: Context ) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val devices = vehicleRepository.getMyVehicles( context )
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onVehicleAvailable(devices)
                    }
                }
            } catch (e: Exception) {
                Timber.e(e)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(e.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun changeVehiclePin(
        context: Context, oldPin: String, newPin: String,
        phone: String, vin: String
    ) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val status =
                    vehicleRepository.changeVehiclePin(context, oldPin, newPin, phone, vin)

                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onPinChange(status)
                    }
                }
            } catch (exc: Exception) {
                Timber.e(exc)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun verifyPinToConnect(context: Context, pin: String) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val status =
                    vehicleRepository.verifyPinToConnect(pin, context)

                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onPinVerified(status)
                    }
                }
            } catch (exc: Exception) {
                Timber.e(exc)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    fun checkForOnGoingRentals(context: Context) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val status =
                        vehicleRepository.checkForOnGoingRentals(context)

                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onOnGoingRental(status)
                    }
                }
            } catch (exc: Exception) {
                Timber.e(exc)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    @UiThread
    internal fun triggerCommandFromServer(context: Context, commandType: String) {
        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
            fleetManagerJob?.cancel()
        }
        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                val cmdResponse =
                        vehicleRepository.triggerCommandFromServer(context, commandType)

                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        when (commandType) {
                            PnpDevice.COMMAND.IGNITION_OFF.name -> {
                                callback.onVehicleLocked(cmdResponse)
                            }
                            PnpDevice.COMMAND.IGNITION_ON.name -> {
                                callback.onVehicleUnLocked(cmdResponse)
                            }
                            PnpDevice.COMMAND.FIND.name -> {
                                callback.onVehicleFound(cmdResponse)
                            }
                        }
                    }
                }
            } catch (exc: Exception) {
                Timber.e(exc)
                withContext(Dispatchers.Main) {
                    for (callback in callbacks) {
                        callback.onError(exc.message)
                    }
                }
            }
        }
    }

    private fun createPnpDevice(device: Device, context: Context): PnpDevice? {
        if (device.deviceType == DeviceType.TBIT) {
            currentPNPDevice = TbitDevice(context)
            EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TBIT))
        } else if (device.deviceType == DeviceType.ICONCOX) {
            currentPNPDevice = IconCoxDevice(context)
            EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_ICONCOX))
        }
        return currentPNPDevice
    }

    fun lockVehicle() {
        if(currentPNPDevice != null){
            currentPNPDevice?.lock()
        }
    }

    fun unlockVehicle() {
        if(currentPNPDevice != null){
            currentPNPDevice?.unlock()
        }
    }

    fun isVehicleConnected() : Boolean {
        if(currentPNPDevice != null){
            return currentPNPDevice!!.isConnected()
        }
        return (NordicBleService.getConnectionState() == BluetoothProfile.STATE_CONNECTED)
    }

    fun findMyVehicle() {
        if(currentPNPDevice != null){
            currentPNPDevice?.findVehicle()
        }else{
            triggerCommandFromServer(FleetSDK.getAppInstance()!!.applicationContext, PnpDevice.COMMAND.FIND.name)
        }
    }


    @UiThread
    fun reconnectToPreviouslyConnectedVehicle(context: Context) {
        val vin = vehicleRepository.getLastConnectedDevice(context)
        if(vin != null){
            if (fleetManagerJob != null && fleetManagerJob?.isActive!!) {
                fleetManagerJob?.cancel()
            }
            startServiceIfRequired(context)
            fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
                try {
                    val status =
                            vehicleRepository.connectUsingVin(
                                vin,
                                context
                            )

                    withContext(Dispatchers.Main) {
                        for (callback in callbacks) {
                            callback.onConnectionStatusUpdate(status)
                        }
                    }

                } catch (e: Exception) {
                    Timber.e(e)
                    withContext(Dispatchers.Main) {
                        for (callback in callbacks) {
                            callback.onError(e.message)
                        }
                    }
                }
            }
        }
        for (callback in callbacks) {
            callback.onConnectionStatusUpdate(ConnectionStatus.NoDeviceFound)
        }
    }

    fun isPnpDevice(device: Device?): Boolean {
        return device?.vehicle?.model?.protocol == ModelProtocol.PNP
    }

    fun isDeviceActivated(device: Device?): Boolean {
        return device?.status == DeviceStatus.ACTIVATED
    }

    internal fun checkCmdResponse(context: Context, event: String) {
        when (event) {
            BT_EVENT_REQUEST_LOCK_VEHICLE -> {
                //if command not executed then, send command via api
                isLockVehicleCmdExec= false
                Handler(Looper.getMainLooper()).postDelayed({
                    if (!isLockVehicleCmdExec) {
                        CoroutineScope(Dispatchers.IO).launch {
                            triggerCommandFromServer(context,
                                PnpDevice.COMMAND.IGNITION_OFF.name
                            )
                        }
                    }
                }, 5000)
            }
            BT_EVENT_REQUEST_UNLOCK_VEHICLE ->  {
                //if command not executed then, send command via api
                isUnlockVehicleCmdExec = false
                Handler(Looper.getMainLooper()).postDelayed({
                    if (!isUnlockVehicleCmdExec) {
                        CoroutineScope(Dispatchers.IO).launch {
                            triggerCommandFromServer(context,
                                PnpDevice.COMMAND.IGNITION_ON.name
                            )
                        }
                    }
                }, 5000)
            }
            BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND -> {
                //if command not executed then, send command via api
                isFindVehicleCmdExec = false
                Handler(Looper.getMainLooper()).postDelayed({
                    if (!isFindVehicleCmdExec) {
                        CoroutineScope(Dispatchers.IO).launch {
                            triggerCommandFromServer(context,
                                PnpDevice.COMMAND.FIND.name
                            )
                        }
                    }
                }, 5000)
            }
        }

    }

    internal fun postCmdResponse(status: CmdResponse, event: String) {
        when (event) {
            BT_EVENT_REQUEST_LOCK_VEHICLE -> for (callback in callbacks) {
                callback.onVehicleLocked(status)
            }
            BT_EVENT_REQUEST_UNLOCK_VEHICLE -> for (callback in callbacks) {
                callback.onVehicleUnLocked(status)
            }
            BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND -> for (callback in callbacks) {
                callback.onVehicleFound(status)
            }
        }

    }

    /**
     * Shared preference
     * */
    fun getOnGoingRentalVehicle(context: Context): RentalVehicle? {
        return vehicleRepository.getOnGoingRentalVehicle(context)
    }

    fun getDevice(context: Context): Device? {
        val device : Device = vehicleRepository.getDevice(context)?: return null
        return deviceMapper(device)
    }

    fun getConnectedVehicle(context: Context): Vehicle? {
        val vehicle = vehicleRepository.getVehicle(context)?: return null
        return vehicleMapper(vehicle)
    }

    fun getVehicleLastKnownLocation(context: Context): DoubleArray {
        val latLng = vehicleRepository.getVehicleLastKnownLocation(context)
        val latLngRes = DoubleArray(2)
        if(latLng == null) return latLngRes
        latLngRes[0] = latLng.latitude
        latLngRes[1] = latLng.longitude
        return latLngRes
    }

    fun getBatteryStatus(context: Context): Long? {
        return if(NordicBleService.mVehicleLiveParameters != null
            && currentPNPDevice != null && currentPNPDevice!!.isConnected())   {
            getBatteryStatus(getConnectedVehicle(context))
        } else {
            vehicleRepository.getBatteryStatus(context)
        }
    }

    fun getSpeed(context: Context): Float? {
         return if(NordicBleService.mVehicleLiveParameters != null
             && currentPNPDevice != null && currentPNPDevice!!.isConnected())   {
             NordicBleService.mVehicleLiveParameters.speed
        } else {
            vehicleRepository.getSpeed(context)
        }
    }

    fun getVehicleHealthStatus() : VehicleHealth {
        val vehicleParameter = NordicBleService.mVehicleLiveParameters
        return if(vehicleParameter!= null) {
            VehicleHealth(
                isThrottleError = vehicleParameter.isThrottleError,
                isControllerError = vehicleParameter.isControllerError,
                isMotorError = vehicleParameter.isMotorError,
                temperature = vehicleParameter.temperature,
                isOverLoad = vehicleParameter.isOverLoad,
                isVehicleConnected = currentPNPDevice?.isConnected() ?: false
            )
        } else {
            VehicleHealth(
                isVehicleConnected = false
            )
        }
    }



    /**
     * Client Data mapper
     * Data required by the client should be added below
     * */
    private fun vehicleMapper(vehicleData: Vehicle): Vehicle {
        return Vehicle(
            vin = vehicleData.vin,
            model = vehicleData.model
        )
    }

    private fun deviceMapper(device: Device): Device {
        return Device(
            deviceType = device.deviceType,
            status = device.status,
            isPinResetRequired = device.isPinResetRequired,
            vehicle = device.vehicle,
            pin = device.pin
        )
    }

    /**
     * Client response Enums
     * */
    enum class ConnectionStatus {
        UnableToFetchData,
        NotMarkedSold,
        MarkedRental,
        EnterPin,
        DataFetched,
        NoDeviceFound,
        Connected,
        Connecting,
        Disconnected,
        Failed,
        NeedPermissions
    }


    enum class BaseResponse {
        Success,
        Failed,
        NoData
    }

    enum class PinStatus {
        Success,
        IncorrectPin,
        UnableToVerify,
        Failed,
        WrongOldPin,
        BadRequest
    }

    enum class CmdResponse {
        Success,
        Failed,
        VinNotFound,
        VehicleNotFound,
        SetPin
    }

    data class VehicleHealth(
        var isThrottleError : Boolean = false,
        var isControllerError : Boolean = false,
        var isMotorError : Boolean = false,
        var temperature : Float = 0f,
        var isOverLoad : Boolean = false,
        var isVehicleConnected : Boolean = false,
    )

    private fun getBatteryStatus(vehicle: Vehicle?) : Long {
        try {

            var batteryVoltageAdc =
                NordicBleService.mVehicleLiveParameters.batteryAdcVoltage.toDouble()

            val maxVoltage = vehicle?.model?.batteryMaxVoltageLimit
            val minVoltage = vehicle?.model?.batteryMinVoltageLimit

            var socValue: Long = 0
            var batteryPercentage = 0.0

            if (minVoltage != null && maxVoltage != null &&
                batteryVoltageAdc != 0.0 && maxVoltage != 0.0 && minVoltage != 0.0
            ) {
                batteryPercentage =
                    (batteryVoltageAdc - minVoltage) / (maxVoltage - minVoltage) * 100
                socValue = round(floor(batteryPercentage)).toLong()
            }

            if (socValue <= 0) {
                socValue = 0
            } else if (socValue > 100) {
                socValue = 100
            }

            return socValue
        } catch (e: Exception){
            e.printStackTrace()
            return 0
        }
    }
}