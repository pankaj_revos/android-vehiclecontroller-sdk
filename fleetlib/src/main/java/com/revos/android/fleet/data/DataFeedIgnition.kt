package com.revos.android.fleet.data

data class DataFeedIgnition (
    var ignition : Int = 0,
    var movement : Int = 0,
    var tripId: Long = 0,
    var type: String? = null,
    var timestamp: String? = null,
    var terminalID: String? = null,
    var vin: String? = null,
)