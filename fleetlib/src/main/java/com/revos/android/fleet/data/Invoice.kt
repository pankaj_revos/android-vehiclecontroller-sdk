package com.revos.android.fleet.data

import com.revos.scripts.type.InvoiceStatus

data class Invoice (
    var invoiceId: String? = null,
    var invoiceNumber: String? = null,
    var invoiceStatus: InvoiceStatus? = null,
    var payeeName: String? = null,
    var payeePhone: String? = null,
    var payerFirstName: String? = null,
    var payerLastName: String? = null,
    var payerPhone: String? = null,
    var passbooksList: List<PassbookTxn>? = null,
    var invoiceCreationDate: String? = null,
)