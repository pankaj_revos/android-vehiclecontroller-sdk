package com.revos.android.fleet.data

import com.revos.scripts.type.GenderType
import com.revos.scripts.type.Role
import com.revos.scripts.type.StatusType

data class User(
    val id : String,
    var role : Role?,
    var firstName : String?,
    var lastName : String?,
    var email : String?,
    var phone : String?,
    var altPhone1 : String?,
    val altPhone2 : String?,
    val genderType : GenderType?,
    val address : String?,
    var dob : String?,
    val status : String,
)
