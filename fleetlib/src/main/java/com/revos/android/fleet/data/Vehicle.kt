package com.revos.android.fleet.data

import android.os.Parcelable
import com.revos.scripts.type.PassbookTxStatus
import com.revos.scripts.type.VehicleStatus
import com.revos.scripts.type.WarrantyStatus
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Vehicle (
    var vin: String? = null,
    var status: VehicleStatus? = null,
    var ownerId: String? = null,
    var ownerFirstName: String? = null,
    var ownerLastName: String? = null,
    var ownerEmailId: String? = null,
    var ownerPhoneNo: String? = null,
    var buyerFirstName: String? = null,
    var buyerLastName: String? = null,
    var buyerPhoneNo: String? = null,
    var vehicleId: String? = null,
    var model: Model? = null,
    var distributorName: String? = null,
    var distributorPhoneNo1: String? = null,
    var distributorPhoneNo2: String? = null,
    var distributorAddress: String? = null,
    var followMeHeadlamp : Int? = 0,
    var childSpeedLock : Int? = 0,
    var warrantyStatus: WarrantyStatus? = null,
    var warrantyExpiryTime: String? = null,
    var invoiceStatus: PassbookTxStatus? = null,
    var invoiceCreationDate: String? = null,
    var odoValue : Double? = 0.0,
) : Parcelable