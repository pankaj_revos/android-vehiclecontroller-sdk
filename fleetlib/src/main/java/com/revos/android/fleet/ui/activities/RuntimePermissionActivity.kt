package com.revos.android.fleet.ui.activities

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import org.greenrobot.eventbus.EventBus
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.revos.android.fleet.eventbus.EventBusMessage
import com.revos.android.fleet.utils.EVENT_MESSAGE_PERMISSION_DENIED
import com.revos.android.fleet.utils.EVENT_MESSAGE_PERMISSION_GRANTED

class RuntimePermissionActivity : AppCompatActivity() {

    private val permissions = arrayOf( Manifest.permission.ACCESS_FINE_LOCATION,
                                       Manifest.permission.ACCESS_COARSE_LOCATION,
                                       Manifest.permission.READ_CONTACTS)

    private val GRANTED : Int = PackageManager.PERMISSION_GRANTED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ActivityCompat.checkSelfPermission(this, permissions[0]) != GRANTED
            || ActivityCompat.checkSelfPermission(this, permissions[1]) != GRANTED
            || ActivityCompat.checkSelfPermission(this, permissions[2]) != GRANTED) {

            ActivityCompat.requestPermissions(this, permissions, 101)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 101){
            var allPermissionsGranted = true
            for(results in grantResults){
                if(results != GRANTED){
                    allPermissionsGranted = false
                    break
                }
            }
            if(allPermissionsGranted){
                EventBus.getDefault().post(EventBusMessage(EVENT_MESSAGE_PERMISSION_GRANTED, "All permissions are granted"))
                finish()
            }else{
                showPermissionRequestDialog()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun showPermissionRequestDialog(){
        var dialog = MaterialAlertDialogBuilder(this)
            .setTitle("Permissions Required")
            .setMessage("Bolt needs LOCATION and Contacts permissions to discover nearby BLE devices ")
            .setPositiveButton("Grant") { _, _ ->
                requestPermissions(permissions, 101)
            }
            .setNegativeButton("Denied") { _, _ ->
                EventBus.getDefault().post(EventBusMessage(EVENT_MESSAGE_PERMISSION_DENIED, "All permissions are not granted"))
                finish()
            }
            .show()
    }
}