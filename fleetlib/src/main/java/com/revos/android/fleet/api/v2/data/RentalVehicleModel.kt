package com.revos.android.fleet.api.v2.data


import com.google.gson.annotations.SerializedName

internal data class RentalVehicleModel(
    @SerializedName("data")
    val data: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int) {

    data class Data(
        @SerializedName("createdAt")
        val createdAt: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("isDeliverable")
        val isDeliverable: Boolean,
        @SerializedName("isHealthy")
        val isHealthy: Boolean,
        @SerializedName("lastMarkedLatitude")
        val lastMarkedLatitude: Double,
        @SerializedName("lastMarkedLongitude")
        val lastMarkedLongitude: Double,
        @SerializedName("modelTag")
        val modelTag: String,
        @SerializedName("pricingInfo")
        val pricingInfo: List<PricingInfo>,
        @SerializedName("rentalStatus")
        val rentalStatus: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("updatedAt")
        val updatedAt: String,
        @SerializedName("vin")
        val vin: String) {

        data class PricingInfo(
            @SerializedName("amountPayable")
            val amountPayable: Float,
            @SerializedName("baseAmount")
            val baseAmount: Int,
            @SerializedName("costPerUnit")
            val costPerUnit: Int,
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("minimumPayableAmount")
            val minimumPayableAmount: Float,
            @SerializedName("name")
            val name: String,
            @SerializedName("status")
            val status: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("unit")
            val unit: String,
            @SerializedName("updatedAt")
            val updatedAt: String
        )
    }
}