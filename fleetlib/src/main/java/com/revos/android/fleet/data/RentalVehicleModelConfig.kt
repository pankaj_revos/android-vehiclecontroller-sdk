package com.revos.android.fleet.data

data class RentalVehicleModelConfig (
    var batteryMinVoltage : Double = 0.0,
    var batteryMaxVoltage : Double = 0.0,
)