package com.revos.android.fleet.api.legacy

import android.content.Context
import com.apollographql.apollo.ApolloClient
import com.revos.android.fleet.R
import com.revos.android.fleet.utils.general.PrefUtils
import com.revos.android.fleet.graphql.GraphQLDateTimeCustomAdapter
import com.revos.android.fleet.graphql.GraphQLInterceptor
import com.revos.scripts.type.CustomType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class ApolloClientService {

    companion object {
        fun create(context : Context): ApolloClient? {
            val authToken = PrefUtils.getInstance(context).authTokenFromPrefs ?: return null
            val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val client = OkHttpClient.Builder()
                .addInterceptor(GraphQLInterceptor(authToken))
                .addInterceptor(logger)
                .build()

            return ApolloClient.builder()
                .serverUrl(context.getString(R.string.server_link))
                .okHttpClient(client)
                .addCustomTypeAdapter(CustomType.DATETIME, GraphQLDateTimeCustomAdapter())
                .build()
        }
    }
}