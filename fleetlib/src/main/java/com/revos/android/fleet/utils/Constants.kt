package com.revos.android.fleet.utils

import android.Manifest
import java.util.HashMap

const val SERVICE_KILL_THRESHOLD = 3000
const val SOS_COOL_OFF_IN_MILLIS = 60000
const val SOS_COOL_OFF = 60
const val OVER_TEMPERATURE_LIMIT = 90.0f

const val REVOS_DEVICE_ID_LENGTH = 37

//tab positions
const val POS_NAV_TAB = 0
const val POS_OBD_TAB = 1
const val POS_ANALYTICS_TAB = 2

//general prefs
const val GENERAL_PREFS = "generalPrefs"
const val FIREBASE_INSTANCE_ID_KEY = "firebase_instance_id_key"
const val COMPANY_LOGO_FILE_PATH_KEY = "company_logo_file_path_key"
const val KILL_SWITCH_FLAG_KEY = "kill_switch_flag_key"
const val LOG_OUT_FLAG_KEY = "log_out_flag_key"
const val CONTACT_LIST_BUILT_FLAG_KEY = "contact_list_built_flag_key"
const val NOTIFICATION_LISTENER_CONNECTED_KEY = "notification_listener_connected_key"
const val SHARED_PREFS_CLEARED_VERSION_CODE_KEY = "app_prefs_cleared_version_code_key"

//app prefs
const val APP_PREFS = "appPrefs"
const val IS_APP_IN_GUEST_MODE = "is_app_in_guest_mode_key"
const val NOTIFICATION_SERVICE_BINDED_KEY = "notification_service_binded"
const val LIGHT_SENSOR_AVAILABLE = "light_sensor_available_key"
const val CRASH_TIMESTAMP_KEY = "crash_timestamp_key"
const val IS_APP_IN_INDIA_KEY = "is_app_in_india_key"

//phone prefs
const val PHONE_PREFS = "phonePrefs"
const val CONTACT_LIST_KEY = "contact_list_key"
const val NUMBER_IN_ACTION_KEY = "number_in_action_key"
const val CONTACT_IN_ACTION_KEY = "contact_in_action_key"
const val PHONE_STATE_KEY = "phone_state_key"
const val CALL_LOG_HASHMAP_KEY = "call_log_hashmap_key"

//direct booking
const val BOOK_RENTAL_VEHICLE_OBJECT_KEY = "book_rental_vehicle_object_key"

//lease prefs
const val LEASE_ID_PREFS = "devicePrefs"
const val LEASE_ID_JSON_DATA_KEY = "device_json_data_key"

//token prefs
const val TOKEN_ID_PREFS = "devicePrefs"
const val TOKEN_ID_JSON_DATA_KEY = "device_json_data_key"

//token prefs
const val VENDOR_ID_PREFS = "devicePrefs"
const val VENDOR_ID_JSON_DATA_KEY = "device_json_data_key"

//user prefs
const val USER_PREFS = "userPrefs"
const val USER_VERIFIED_PHONE_NUMBER = "user_verified_phone_number"
const val IS_USER_PHONE_NUMBER_VERIFIED = "is_user_phone_number_verified"
const val USER_JSON_DATA_KEY = "user_json_data_key"
const val USER_TOKEN_KEY = "user_token_key"
const val USER_PROFILE_LOCAL_FILE_PATH_KEY = "user_profile_file_path_key"
const val USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY = "user_dl_side_1_file_path_key"
const val USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY = "user_dl_side_2_file_path_key"
const val USER_PROFILE_UPLOAD_FILE_NAME_KEY = "user_profile_upload_file_name_key"
const val USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY = "user_dl_side_1_upload_file_name_key"
const val USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY = "user_dl_side_2_upload_file_name_key"
const val ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY = "active_rental_booking_for_user_key"
const val USER_KYC_STATUS_DATA_KEY = "user_kyc_status_data_key"
const val KYC_PROFILE_UPLOAD_FILE_NAME_KEY = "kyc_profile_upload_file_name_key"
const val KYC_DL_SIDE_1_UPLOAD_FILE_NAME_KEY = "kyc_dl_side_1_upload_file_name_key"
const val KYC_DL_SIDE_2_UPLOAD_FILE_NAME_KEY = "kyc_dl_side_2_upload_file_name_key"

//image prefs
const val IMAGE_PREFS = "imagePrefs"
const val KYC_PROFILE_IMG_BUFFER_KEY = "kyc_profile_img_buffer_key"
const val KYC_DL_SIDE_1_IMG_BUFFER_KEY = "kyc_dl_side_1_img_buffer_key"
const val KYC_DL_SIDE_2_IMG_BUFFER_KEY = "kyc_dl_side_2_img_buffer_key"

//user settings prefs
const val USER_SETTINGS_PREFS = "userSettingsPrefs"
const val DAY_NIGHT_AUTO_SWITCH_THEME_KEY = "day_night_auto_switch_theme_key"
const val DISTRACTION_FREE_MODE_KEY = "distraction_free_mode_key"
const val USE_DARK_THEME_KEY = "use_dark_theme_key"
const val ENABLE_CONNECT_USING_VIN_KEY = "enable_connect_using_vin_key"

//bluetooth prefs
const val BLUETOOTH_PREFS = "bluetoothPrefs"
const val BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY = "bluetooth_last_connected_device_mac_key"
const val BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY = "bluetooth_last_connected_device_name_key"
const val BLUETOOTH_CONNECTION_STATUS_KEY = "bluetooth_connection_status_key"
const val BLUETOOTH_SERVICES_DISCOVERED_FLAG_KEY = "bluetooth_services_discovered_flag_key"
const val BLUETOOTH_IMAGE_SEQUENCE_KEY = "bluetooth_image_sequence_key"
const val BLUETOOTH_EXPECTED_IMAGE_CODE_KEY = "bluetooth_expected_image_code_key"
const val BLUETOOTH_LAST_IMAGE_TX_ATTEMPT_TIMESTAMP = "bluetooth_last_image_tx_attempt_timestamp"
const val BLUETOOTH_LAST_READ_VEHICLE_CONTROL_PARAMS_KEY =
    "bluetooth_last_read_vehicle_control_params_key" //byte array list

const val BLUETOOTH_EXPECTED_CONTROLLER_PARAMS_KEY =
    "bluetooth_expected_vehicle_control_params_key" //byte array list

const val BLUETOOTH_CONTROL_PARAMS_WRITE_LAST_ATTEMPT_TIMESTAMP =
    "bluetooth_control_params_write_last_attempt_timestamp"
const val BLUETOOTH_SPEEDOMETER_MESSAGE_EXPIRY_TIME = "bluetooth_speedometer_message_expiry_time"
const val BLUETOOTH_SPEEDOMETER_MESSAGE_ERASED = "bluetooth_speedometer_message_erased"
const val BLUETOOTH_SERVICE_LAST_ACTIVE_TIMESTAMP = "bluetooth_service_last_active_time_stamp"
const val BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY = "bluetooth_user_manually_disconnected_flag_key"

const val BLUETOOTH_CONTROLLER_READ_FORMAT_KEY = "bluetooth_controller_read_format_key"
const val BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY = "bluetooth_controller_log_format_key"
const val BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY = "bluetooth_controller_write_format_key"
const val BLUETOOTH_PARAMID_TO_LOGID_KEY = "bluetooth_paramid_to_logid_key"
const val BLUETOOTH_VEHICLE_CONTROL_JSON_KEY = "bluetooth_vehicle_control_json_key"
const val BLUETOOTH_MODEL_CONFIG_JSON_KEY = "bluetooth_model_config_json_key"
const val BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY = "bluetooth_battery_adc_log_json_key"

//iconcox
const val BT_GPS_ICONCOX_CMD_RESPONSE = "bt_gps_iconcox_cmd_response"


//iconcox commands strings
//String CMD_SEND_MAC = "";
const val CMD_SEND_REMOTE_LOCK = "23240104" + "0000" + "0D0A"
const val CMD_SEND_REMOTE_IGNITION = "23240103" + "0000" + "0D0A"
const val CMD_SEND_FIND_VEHICLE = "23240105" + "0000" + "0D0A"
const val CMD_SEND_LOCK = "23240101" + "0000" + "0D0A"
const val CMD_SEND_UNLOCK = "23240102" + "0000" + "0D0A"

//trip prefs
const val TRIP_PREFS = "tripPrefs"
const val TRIP_DATA_KEY = "trip_data_key"
const val TRIP_DATA_NEEDS_REFRESH_KEY = "trip_data_needs_refresh"
const val TRIP_DATA_IS_REFRESHING_KEY = "trip_data_is_refreshing"
const val AVERAGE_PETROL_PRICE_KEY = "average_petrol_price_key"

const val  GEO_FENCING_PREFS = "geo_fencing_prefs"
const val GEO_FENCING_JSON_DATA_KEY = "geo_fencing_data_key"

const val DATABASE_NAME = "revos-db"
const val WORK_MANAGER_KEY_GEOFENCE = "geofenceObj"

//log prefs
const val LOG_PREFS = "logPrefs"

const val WORKING_LIVE_LOG_FILE_NAME_KEY = "working_live_log_file_name_key"
const val VEHICLE_LIVE_LOG_DIR_KEY = "vehicle_live_log_dir_key"
const val VEHICLE_DATA_LIVE_LOG_DIR_PATH = "vehicle_data_live"


const val WORKING_OFFLINE_LOG_FILE_NAME_KEY = "working_offline_log_file_name_key"
const val VEHICLE_OFFLINE_LOG_DIR_KEY = "vehicle_offline_log_dir_key"
const val VEHICLE_DATA_OFFLINE_LOG_DIR_PATH = "vehicle_data_offline"

const val WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY = "working_offline_battery_adc_log_file_name_key"
const val VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY = "vehicle_offline_battery_adc_log_dir_key"
const val VEHICLE_DATA_OFFLINE_BATTERY_ADC_LOG_DIR_PATH = "vehicle_data_offline_battery_adc"

//directory constants
const val TRIP_DATA_DIR_PATH = "trip_data"


//File extension constants
const val VEHICLE_DATA_LIVE_LOG_EXTENSION = ".rvdl"
const val VEHICLE_DATA_OFFLINE_LOG_EXTENSION = ".rvdo"
const val VEHICLE_DATA_OFFLINE_BATTERY_ADC_LOG_EXTENSION = ".rvdba"

//firmware version strings
const val VER_1_0 = "1.0"
const val VER_1_1 = "1.1"
const val VER_5_0 = "5.0"

const val VER_1_0_0 = "1.0.0"
const val VER_1_1_0 = "1.1.0"
const val VER_5_0_0 = "5.0.0"
const val VER_5_1_0 = "5.1.0"


const val LOG_TYPE_OFFLINE = "0"
const val LOG_TYPE_LIVE = "1"
const val LOG_TYPE_OFFLINE_BATTERY_ADC = "2"

//battery adc log constants
const val LOG_BATTERY_VOLTAGE_ADC = "batteryVoltageAdc"

//vehicle log entry constants
const val LOG_RIDER_ID = "riderId"
const val LOG_IS_TIMESTAMP_VERIFIED = "timestampVerified"
const val LOG_TYPE = "type"
const val LOG_VIN = "vin"
const val LOG_TRIP_ID = "tripId"
const val LOG_MANUFACTURER_ID = "manufacturerId"
const val LOG_FIRMWARE_VERSION = "firmware"

const val LOG_GPS_SPEED = "gpsSpeed"
const val LOG_LAT = "latitude"
const val LOG_LNG = "longitude"
const val LOG_GPS_ACCURACY = "gpsAccuracy"
const val LOG_ELEVATION = "elevation"
const val LOG_HEADING = "heading"

const val LOG_TIMESTAMP = "timestamp"
const val LOG_WHEEL_RPM = "wheelRpm"
const val LOG_BATTERY_VOLTAGE = "batteryVoltage"
const val LOG_BATTERY_CURRENT = "batteryCurrent"
const val LOG_BATTERY_SOC = "batterySoc"
const val LOG_CONTROLLER_TEMP = "controllerTemperature"
const val LOG_THROTTLE = "throttle"
const val LOG_ODO = "odometer"
const val LOG_MODE = "mode"
const val LOG_HILL_ASSIST_STATUS = "hillAssistStatus"
const val LOG_ANTI_THEFT_STATUS = "antiTheftStatus"
const val LOG_CHARGING_STATUS = "chargingStatus"
const val LOG_PARKING_STATUS = "parkingStatus"
const val LOG_OVERLOAD_STATUS = "overloadStatus"
const val LOG_OVERCURRENT_STATUS = "overCurrentStatus"
const val LOG_THROTTLE_STATUS = "throttleStatus"
const val LOG_BRAKE_STATUS = "brakeStatus"
const val LOG_REVERSE_STATUS = "reverseStatus"
const val LOG_SOFT_LOCK_STATUS = "softLockStatus"
const val LOG_MOTOR_STATUS = "motorStatus"
const val LOG_CONTROLLER_STATUS = "controllerStatus"
const val LOG_EABS_STATUS = "eabsStatus"
const val LOG_REGEN_BRAKING_STATUS = "regenBrakingStatus"

const val LOG_BRAKE_REGEN_LIMIT = "brakeRegenLimit"
const val LOG_ZERO_THROTTLE_REGEN_LIMIT = "zeroThrottleRegenLimit"
const val LOG_CURRENT_LIMIT = "currentLimit"
const val LOG_OVER_VOLTAGE_LIMIT = "overVoltageLimit"
const val LOG_UNDER_VOLTAGE_LIMIT = "underVoltageLimit"
const val LOG_SPEED_LIMIT = "speedLimit"
const val LOG_PICKUP_CONTROL_LIMIT = "pickupControlLimit"

const val LOG_LEFT_INDICATOR = "leftIndicator"
const val LOG_RIGHT_INDICATOR = "rightIndicator"
const val LOG_HEADLIGHT = "headlight"
const val LOG_BATTERY_DTE = "batteryDte"
const val LOG_BMS_SPEED = "bmsSpeed"

const val LOG_X_ACC = "xAcc"
const val LOG_Y_ACC = "yAcc"
const val LOG_Z_ACC = "zAcc"

//bluetooth low energy service uuid
const val BLUETOOTH_OBD_DATA_SERVICE_UUID = "699aa9da-fe76-4b13-bda5-07f38795776b"
const val BLUETOOTH_IMAGE_TRANSFER_SERVICE_UUID = "653d1e34-885a-4324-8d35-a6eb1266e008"

//a hash map of the commands and the number of retries attempted for that command
const val BLUETOOTH_COMMAND_STATUS_HASH_MAP_KEY = "bluetooth_command_status_hash_map_key"

//a count of the number of retries made for a connection attempt
const val BLUETOOTH_RETRY_ATTEMPT_COUNT_KEY = "bluetooth_retry_attempt_count_key"

//max retries allowed in case of attempting a connection
const val MAX_RETRY_ATTEMPT_COUNT = 2

//networking prefs
const val NETWORKING_PREFS = "networkingPrefs"
const val IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isReadDataDownloadInProgressKey"
const val IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isLogDataDownloadInProgressKey"
const val IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isParamWriteDataDownloadInProgressKey"
const val IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isVehicleControlDataDownloadInProgressKey"
const val IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isBatteryAdcLogDataDownloadInProgressKey"
const val IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isDeviceDataDownloadInProgressKey"

const val IS_SOS_SMS_IN_PROGRESS_KEY = "isSosSmsInProgressKey"
const val IS_SOS_SMS_SENT_KEY = "isSosSmsSentKey"
const val SMS_SENT_TIMESTAMP_KEY = "smsSentTimestampKey"
const val GENERAL_EVENT_START_SOS = "EVENT_START_SOS"

//navigation prefs
const val NAV_PREFS = "navPrefs"
const val LAST_GOOGLE_NOTIFICATION_TIMESTAMP = "last_google_notification_timestamp"
const val TRIP_ADVICE_DATA_KEY = "trip_advice_data_key"
const val SAVED_PLACES_DATA_KEY = "saved_places_key"
const val IS_GOOGLE_NAV_ACTIVE_KEY = "is_google_nav_active_key"

//device prefs
const val DEVICE_PREFS = "devicePrefs"

//this is pertaining to the cloud status
const val DEVICE_JSON_DATA_KEY = "device_json_data_key"

//this is just to store device PIN
const val DEVICE_PIN_KEY = "device_pin_key"
const val DEVICE_IS_ODO_RESET_REQUIRED_KEY = "device_is_od_reset_required_key"
const val DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS = "device_firmware_version_update_in_progress"

//vehicle prefs
const val VEHICLE_PREFS = "vehiclePrefs"
const val VEHICLE_STATUS_FUEL_KEY = "vehicle_status_fuel_key"
const val VEHICLE_STATUS_BATTERY_KEY = "vehicle_status_battery_key"
const val VEHICLE_JSON_KEY = "vehicle_json_key"
const val VEHICLE_MODEL_IMAGE_FILE_PATH_KEY = "vehicle_model_image_file_path_key"
const val VEHICLE_MODEL_IMAGE_DIR_PATH = "vehicle_model_image"
const val VEHICLE_LAST_KNOWN_LATITUDE = "vehicle_last_known_latitude"
const val VEHICLE_LAST_KNOWN_LONGITUDE = "vehicle_last_known_longitude"
const val VEHICLE_LAST_KNOWN_SPEED = "vehicle_last_known_speed"
const val LAST_CONNECTED_DEVICE_VIN = "last_connected_device_vin"

//onwer's vehicles prefs
const val OWNERS_VEHICLES_LIST = "owners_vehicles_list"
const val OWNERS_VEHICLES_PIN_LIST = "owners_vehicles_pin_list"
const val MODEL_IMAGE_HASHMAP_KEY = "owners_vehicles_models_image_list"

//battery prefs
const val BATTERY_PREFS = "batteryPrefs"
const val BATTERY_REGRESSION_DATA_KEY = "battery_regression_data_key"

//tracking prefs
const val TRACKING_PREFS = "trackingPrefs"
const val LIVE_PACKET_JSON_KEY = "live_packet_json_key"
const val LAST_LIVE_PACKET_REFRESHED_TIMESTAMP = "last_live_packet_refreshed_timestamp"
const val HISTORICAL_DATA_LIST_KEY = "historical_data_list_key"
const val HISTORICAL_DATA_IS_REFRESHING_KEY = "historical_data_refreshing_key"

//ota prefs
const val OTA_DIR_PATH = "ota"
const val OTA_FILE_PATH_KEY = "ota_file_path_key"

//temp image prefs
const val TEMP_IMAGE_PREFS = "temp_image_prefs"
const val GENERIC_TEMP_IMAGE_KEY = "generic_temp_image_key"

//Previous vehicle details
const val LAST_VEHICLE_PREFS = "lastVehiclePrefs"

//permissions required
/*
const permissionsRequired = arrayOf(
    Manifest.permission.ACCESS_FINE_LOCATION,
    Manifest.permission.ACCESS_BACKGROUND_LOCATION,
    Manifest.permission.READ_CONTACTS,
    Manifest.permission.WRITE_EXTERNAL_STORAGE
)
*/

//String delimiter for messages
const val GEN_DELIMITER = "-1del-1m-1ter"

//General Events
const val GEN_EVENT_TAB_CHANGE = "gen_event_tab_change"
const val GEN_EVENT_CHANGE_HAMBURGER_TO_BACK = "gen_event_change_hamburger_to_back"
const val GEN_EVENT_NAV_BANNER_ANIM_UPDATED = "gen_event_navigation_banner_updated"
const val GEN_EVENT_PHONE_LOCATION_UPDATED = "gen_event_location_updated"
const val GEN_EVENT_VEHICLE_RENTAL_TAB_CHANGE = "gen_event_vehicle_rental_tab_change"


//Phone constants
const val ENTRY_TYPE_CALL_INCOMING = "call_incoming"
const val ENTRY_TYPE_CALL_OUTGOING = "call_outgoing"
const val ENTRY_TYPE_CALL_MISSED = "call_missed"
const val ENTRY_TYPE_CONTACT_SEARCH_RESULT = "contact_search_result"
const val PHONE_NUMBER_REGEX = "[0-9 ()+-]*"

//Kill switch event
const val KILL_SWITCH = "kill_switch"

//CAN constants
const val CAN_SPEED_ID = 71
const val CAN_ODO_ID = 72
const val CAN_RANGE_ID = 73
const val CAN_SOC_ID = 74

//vehicle constants
const val WHEEL_RPM_ID = 0x57
const val THROTTLE_ID = 0x54
const val VOLTAGE_ID = 0x56
const val CURRENT_ID = 0x43
const val MODE_AND_LOCK_ID = 0x4D
const val ODO_ID = 0x4F
const val TEMP_ID = 0x54
const val RESISTANCE_ID = 0x52
const val OTHER_INFO_ID = 0x49


//OBD Constants
const val OBD_EVENT_LIVE_DATA = "obd_event_live_data"
const val OBD_EVENT_OFFLINE_DATA = "obd_event_offline_data"
const val OBD_EVENT_BATTERY_ADC_OFFLINE_DATA = "obd_event_battery_adc_offline_data"

//Vehicle control command strings
const val HEADLIGHT_ON = "16"
const val HEADLIGHT_OFF = "17"

const val LEFT_INDICATOR_ON = "32"
const val LEFT_INDICATOR_OFF = "33"

const val RIGHT_INDICATOR_ON = "48"
const val RIGHT_INDICATOR_OFF = "49"

const val VEHICLE_LOCK = "64"
const val VEHICLE_UNLOCK = "65"

const val COMMAND_STRING_HEADER = "DA"
const val SOFT_LOCK_HEADER = "A5" //0xA5

const val SPEED_LIMIT_HEADER = "53" //0x53, 'S'

const val CURRENT_LIMIT_HEADER = "43" //0x43, 'C'

const val VOLTAGE_LIMIT_HEADER = "56" //0x56, 'V'

const val CONTROL_PARAMS_DELIMITER = "55" //0x55


//Dynamic Image type
const val IMAGE_TYPE_BLANK = 0
const val IMAGE_TYPE_INTERNAL = 1
const val IMAGE_TYPE_CUSTOM = 2

//Login events
const val LOGIN_EVENT_SIGN_IN_GOOGLE = "login_event_sign_in_google"
const val LOGIN_EVENT_SIGN_IN_FB = "login_event_sign_in_fb"
const val LOGIN_EVENT_SIGN_IN_PHONE = "login_event_sign_in_phone"
const val LOGIN_EVENT_SIGN_IN_EMAIL = "login_event_sign_in_email"
const val LOGIN_EVENT_SIGN_IN_GUEST = "login_event_sign_in_guest"

//Navigation events
const val NAV_EVENT_TRIP_ADVICE_DATA_UPDATED = "nav_event_trip_advice_data_updated"
const val NAV_EVENT_SAVED_PLACE_LIST_UPDATED = "nav_event_saved_place_list_updated"

//Bluetooth events
const val BT_EVENT_REQUEST_CONNECT_TO_DEVICE = "bt_event_request_connect_to_device"
const val BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE = "bt_event_request_connect_to_last_device"
const val BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS = "bt_event_request_write_controller_params"
const val BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS = "bt_event_request_write_vehicle_params"
const val BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND = "bt_event_request_send_find_my_vehicle_command"
const val BT_EVENT_REQUEST_RECONNECT = "bt_event_request_reconnect"
const val BT_EVENT_REQUEST_DISCONNECT = "bt_event_request_disconnect"
const val BT_EVENT_REQUEST_CANCEL_PAIRING = "bt_event_request_cancel_pairing"

const val BT_EVENT_REQUEST_ROTATE_TEST_IMAGE = "bt_event_rotate_test_image"
const val BT_EVENT_IMAGE_CHECK_COMPLETE = "bt_event_image_check_complete"
const val BT_EVENT_REQUEST_CLEAR_IMAGE = "bt_event_clear_image"
const val BT_EVENT_DECREASE_CUSTOM_IMAGE_PRIORITY = "bt_event_decrease_custom_image_priority"
const val BT_EVENT_REQUEST_SEND_UBER_SCENARIO_1_IMAGE = "bt_send_uber_scenario_1_image"
const val BT_EVENT_REQUEST_SEND_UBER_SCENARIO_2_IMAGE = "bt_send_uber_scenario_2_image"
const val BT_EVENT_REQUEST_SEND_CALL_BITMAP = "bt_send_dynamic_bitmap"
const val BT_EVENT_REQUEST_SEND_FILLER_BYTES_OBD = "bt_request_send_filler_bytes_obd"
const val BT_EVENT_REQUEST_SEND_FILLER_BYTES_IMG = "bt_request_send_filler_bytes_img"
const val BT_EVENT_REQUEST_SEND_LARGE_BITMAP = "bt_request_send_large_bitmap"

const val BT_EVENT_REQUEST_LOCK_VEHICLE = "bt_request_lock_vehicle"
const val BT_EVENT_REQUEST_UNLOCK_VEHICLE = "bt_request_unlock_vehicle"
const val BT_EVENT_REQUEST_LOCK_HANDLE = "bt_request_lock_handle"
const val BT_EVENT_REQUEST_UNLOCK_HANDLE = "bt_request_unlock_handle"
const val BT_EVENT_REQUEST_UNLOCK_SEAT = "bt_request_seat_unlock"
const val BT_EVENT_REQUEST_SHOW_SETTINGS_CHANGED_ICON = "bt_request_show_settings_changed_icon"

const val BT_EVENT_REQUEST_DEVICE_SYNC = "bt_event_request_device_sync"
const val BT_EVENT_REQUEST_CONNECT_TBIT = "bt_event_request_connect_tbit"
const val BT_EVENT_REQUEST_CONNECT_ICONCOX = "bt_event_request_connect_iconcox"

const val BT_EVENT_OBD_CHAR_WRITE_SUCCESS = "bt_event_obd_char_write_success"
const val BT_EVENT_OBD_CHAR_WRITE_FAIL = "bt_event_obd_char_write_fail"
const val BT_EVENT_IMG_CHAR_WRITE_SUCCESS = "bt_event_img_char_write_success"
const val BT_EVENT_IMG_CHAR_WRITE_FAIL = "bt_event_img_char_write_fail"


const val BT_EVENT_SERVICES_DISCOVERED = "bt_event_state_services_discovered"
const val BT_EVENT_MTU_CHANGED = "bt_event_mtu_changed"
const val BT_EVENT_STATE_CONNECTED = "bt_event_state_connected"
const val BT_EVENT_STATE_AUTHENTICATING = "bt_event_state_authenticating"
const val BT_EVENT_STATE_AUTHENTICATION_SUCCESS = "bt_event_state_authentication_success"
const val BT_EVENT_STATE_AUTHENTICATION_FAIL = "bt_event_state_authentication_fail"
const val BT_EVENT_STATE_CONNECTING = "bt_event_state_connecting"
const val BT_EVENT_STATE_DISCONNECTED = "bt_event_state_disconnected"
const val BT_EVENT_SWITCH_ON_BLUETOOTH = "bt_event_switch_on_bluetooth"
const val BT_EVENT_SCAN_STARTED = "bt_event_scan_started"
const val BT_EVENT_DEVICE_NOT_FOUND = "bt_event_device_not_found"
const val BT_EVENT_DEVICE_START_ACTIVATION_PROCEDURE = "bt_event_device_start_activation_procedure"
const val BT_EVENT_VEHICLE_IDS_CHANGE_SUCCESS = "bt_event_vehicle_ids_change_success"
const val BT_EVENT_VEHICLE_CONSTANTS_CHANGE_SUCCESS = "bt_event_vehicle_constants_change_success"
const val BT_EVENT_VEHICLE_BATTERY_VOLTAGE_CHANGE_SUCCESS =
    "bt_event_vehicle_battery_voltage_change_success"
const val BT_EVENT_DEVICE_STATE_CHANGE_SUCCESS = "bt_event_device_state_change_success"
const val BT_EVENT_DEVICE_SYNC_FAIL = "bt_event_device_sync_fail"
const val BT_EVENT_DEVICE_SYNC_SUCCESS = "bt_event_device_sync_success"
const val BT_EVENT_BATTERY_REGRESSION_DATA_SYNC_SUCCESS = "bt_event_battery_regression_data_sync_success"
const val BT_EVENT_UNABLE_TO_READ_FIRMWARE_METADATA = "bt_event_unable_to_read_firmware_metadata"
const val BT_EVENT_DEVICE_LOCKED = "bt_event_device_locked"
const val BT_EVENT_NO_PIN_FOUND = "bt_event_no_pin_found"
const val BT_EVENT_WRONG_PIN = "bt_event_wrong_pin"
const val BT_EVENT_CHANGE_PIN_REQUIRED = "bt_event_change_pin_required"

//Phone event constants
const val PHONE_EVENT_CONTACT_LIST_BUILDING = "phone_event_contact_list_building"
const val PHONE_EVENT_CONTACT_LIST_BUILT = "phone_event_contact_list_built"
const val PHONE_EVENT_REQUEST_BUILD_CONTACT_LIST = "phone_event_request_build_contact_list"
const val PHONE_EVENT_REQUEST_ACCEPT_CALL_VIA_NOTIFICATION =
    "phone_event_request_accept_call_via_notification"
const val PHONE_EVENT_CREATE_CALL_LOG = "phone_event_create_call_log"
const val PHONE_EVENT_MULTIPLE_MATCHED_RESULTS = "phone_event_multiple_results"

//Networking events
const val NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED = "network_event_user_profile_pic_downloaded"
const val NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED = "network_event_user_dl_pic_1_downloaded"
const val NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED = "network_event_user_dl_pic_2_downloaded"
const val NETWORK_EVENT_CONTROLLER_METADATA_FILE_DOWNLOADED =
    "network_event_controller_metadata_file_downloaded"
const val NETWORK_EVENT_USER_DETAILS_DOWNLOAD_SUCCESS = "network_event_user_details_download_success"
const val NETWORK_EVENT_USER_DETAILS_DOWNLOAD_FAIL = "network_event_user_details_download_fail"
const val NETWORK_EVENT_TRIP_DATA_REFRESHED = "network_event_trip_data_refreshed"
const val NETWORK_EVENT_VEHICLE_MODEL_IMAGE_DOWNLOADED = "network_event_vehicle_model_image_downloaded"
const val NETWORK_EVENT_DEVICE_DATA_REFRESHED = "network_event_device_data_refreshed"
const val NETWORK_EVENT_BATTERY_REGRESSION_DATA_REFRESHED =
    "network_event_battery_regression_data_refreshed"
const val NETWORK_EVENT_VEHICLE_LAST_KNOWN_LOCATION_UPDATED =
    "network_event_vehicle_last_known_location_updated"
const val NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED = "network_event_vehicle_historical_data_updated"
const val NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED = "network_event_vehicle_live_log_data_updated"
const val NETWORK_EVENT_ENTER_PIN_TO_CONNECT = "network_event_enter_pin_to_connect"
const val NETWORK_EVENT_ERROR = "network_event_error"

//error event
const val GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT = "generic_message_activity_error_event"
const val GENERIC_MESSAGE_ACTIVITY_PERSISTENT_ERROR_EVENT =
    "generic_message_activity_persistent_error_event"
const val GENERIC_MESSAGE_ACTIVITY_INFO_EVENT = "generic_message_activity_info_event"
const val GENERIC_MESSAGE_ACTIVITY_PERSISTENT_INFO_EVENT =
    "generic_message_activity_persistent_info_event"

//User profile activity constants
const val USER_PROFILE_FRAG_EVENT_USER_UPDATED = "user_profile_frag_event_user_updated"
const val USER_PROFILE_FRAG_EVENT_LOAD_EDIT_USER_FRAGMENT =
    "user_profile_frag_event_load_edit_user_fragment"
const val USER_PROFILE_EVENT_BACK_PRESSED = "user_profile_event_back_pressed"

//Directory names constant
const val TEMP_PIC_DIR_PATH = "temp_pic_dir"
const val PIC_DIR_PATH = "pics"

//File prefix constants
const val PROFILE_PIC_TEMP_PREFIX = "tempProfilePic_"
const val DL_SIDE_1_TEMP_PREFIX = "tempDLPic1_"
const val DL_SIDE_2_TEMP_PREFIX = "tempDLPic2_"
const val SERVICE_TICKET_PIC_TEMP_PREFIX = "tempSRPic_"
const val KYC_PROFILE_PIC_TEMP_PREFIX = "tempKycProfilePic_"
const val KYC_DL_SIDE_1_PIC_TEMP_PREFIX = "tempKycDLSide1Pic_"
const val KYC_DL_SIDE_2_PIC_TEMP_PREFIX = "tempKycDLSide2Pic_"

//File provider string
const val FILE_PROVIDER = "com.revos.android.fileprovider"

const val SERVICE_REQUEST_ID = "service_request_id"
const val SERVICE_REQUEST_COMMENT = "service_request_comment"

//User profile pic and dl pic constants
const val PROFILE_PIC_TEMP_COMPRESSED_PREFIX = "tempCompressedProfilePic_"
const val DL_SIDE_1_TEMP_COMPRESSED_PREFIX = "tempCompressedDLPic1_"
const val DL_SIDE_2_TEMP_COMPRESSED_PREFIX = "tempCompressedDLPic2_"
const val SERVICE_TICKET_PIC_COMPRESSED_PREFIX = "tempCompressedSRPic_"
const val KYC_PROFILE_PIC_TEMP_COMPRESSED_PREFIX = "tempCompressedKycProfilePic_"
const val KYC_DL_SIDE_1_TEMP_COMPRESSED_PREFIX = "tempCompressedKycDLPic1_"
const val KYC_DL_SIDE_2_TEMP_COMPRESSED_PREFIX = "tempCompressedKycDLPic2_"

const val PROFILE_PIC_PREFIX = "ProfilePic_"
const val DL_SIDE_1_PREFIX = "DLSide1_"
const val DL_SIDE_2_PREFIX = "DLSide2_"
const val SERVICE_TICKET_PIC_PREFIX = "SRPic_"
const val KYC_PROFILE_PIC_PREFIX = "KYCProfilePic_"
const val KYC_DL_SIDE_1_PREFIX = "KYCDLSide1_"
const val KYC_DL_SIDE_2_PREFIX = "KYCDLSide2_"

const val UPLOAD_TYPE_PROFILE = "PROFILE"
const val UPLOAD_TYPE_DL_SIDE_1 = "DL_SIDE_1"
const val UPLOAD_TYPE_DL_SIDE_2 = "DL_SIDE_2"
const val UPLOAD_TYPE_SERVICE_TICKET = "SERVICE_TICKET"
const val UPLOAD_TYPE_KYC_PROFILE = "KYC_PROFILE"
const val UPLOAD_TYPE_KYC_DL_SIDE_1 = "KYC_DL_SIDE_1"
const val UPLOAD_TYPE_KYC_DL_SIDE_2 = "KYC_DL_SIDE_2"

//BITMAP TYPE
const val BITMAP_TYPE_NAVIGATION = "navigation"
const val BITMAP_TYPE_PHONE = "phone"

const val GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user"
const val PHONE_PAY_PACKAGE_NAME = "com.phonepe.app"


const val START_STM_INTERNAL_OTA = "start_stm_internal_ota"
const val START_STM_EXTERNAL_OTA = "start_stm_external_ota"
const val STM_OTA_STATUS_MESSAGE = "stm_ota_status_message"

const val IGNITION_ON = "IGNITION_ON"
const val IGNITION_OFF = "IGNITION_OFF"

const val TAG_READ = "read"
const val TAG_LOG = "log"
const val TAG_WRITE = "write"
const val TAG_VEHICLE_CONTROL = "vehicleControl"
const val TAG_BATTERY_ADC_LOG = "batteryAdcLog"
const val FIRMWARE_METADATA_KEY = "firmwareMetadata"
const val VERSION_NO_KEY = "versionNo"

internal const val EVENT_MESSAGE_TRANSACTION_SUCCESSFUL = "Transaction unsuccessful"
internal const val EVENT_MESSAGE_TRANSACTION_FAILED = "Transaction failed"
internal const val EVENT_MESSAGE_PERMISSION_GRANTED = "Permission granted"
internal const val EVENT_MESSAGE_STORAGE_PERMISSION_GRANTED = "Storage Permission granted"
internal const val EVENT_MESSAGE_PERMISSION_DENIED = "Permission Denied"

//TODO::tall right and tall straight, later
/* int[] turnIconIds = {
            R.drawable.da_turn_depart,
            R.drawable.da_turn_straight,
            R.drawable.da_turn_left, R.drawable.da_turn_right,
            R.drawable.da_turn_slight_left, R.drawable.da_turn_slight_right,
            R.drawable.da_turn_fork_left, R.drawable.da_turn_fork_right,
            R.drawable.da_turn_sharp_left, R.drawable.da_turn_sharp_right,
            R.drawable.da_turn_ramp_left, R.drawable.da_turn_ramp_right,
            R.drawable.da_lane_uturn_left, R.drawable.da_lane_uturn_right,
            R.drawable.da_lane_uturn_short_left, R.drawable.da_lane_uturn_short_right,
            R.drawable.da_turn_uturn_left, R.drawable.da_turn_uturn_right,
            R.drawable.da_turn_arrive, R.drawable.da_turn_arrive_left, R.drawable.da_turn_arrive_right,
            R.drawable.da_turn_generic_roundabout_left, R.drawable.da_turn_generic_roundabout_right,
            R.drawable.da_turn_roundabout_1_left, R.drawable.da_turn_roundabout_1_right,
            R.drawable.da_turn_roundabout_2_left, R.drawable.da_turn_roundabout_2_right,
            R.drawable.da_turn_roundabout_3_left, R.drawable.da_turn_roundabout_3_right,
            R.drawable.da_turn_roundabout_4_left, R.drawable.da_turn_roundabout_4_right,
            R.drawable.da_turn_roundabout_5_left, R.drawable.da_turn_roundabout_5_right,
            R.drawable.da_turn_roundabout_6_left, R.drawable.da_turn_roundabout_6_right,
            R.drawable.da_turn_roundabout_7_left, R.drawable.da_turn_roundabout_7_right,
            R.drawable.da_turn_roundabout_8_left, R.drawable.da_turn_roundabout_8_right,
            R.drawable.da_turn_roundabout_exit_left, R.drawable.da_turn_roundabout_exit_right,
            R.drawable.da_turn_unknown,
            R.drawable.da_turn_generic_merge,
            R.drawable.da_turn_ferry,
            R.drawable.da_turn_mylocation
    };
*/
//notification service constants
const val TRIP_ADVICE_DISTANCE_FROM_TURN_KEY = "tripAdvice_distanceFromTurn"
const val TRIP_ADVICE_ETA_KEY = "tripAdvice_eta"
const val TRIP_ADVICE_REMAINING_DISTANCE_KEY = "tripAdvice_remainingDistance"
const val TRIP_ADVICE_DESTINATION_NAME_KEY = "tripAdvice_destinationName"
const val TRIP_ADVICE_REMAINING_TIME_KEY = "tripAdvice_remainingTime"
const val TRIP_ADVICE_NEXT_ROAD_KEY = "tripAdvice_nextRoad"
const val TRIP_ADVICE_IS_REROUTING_KEY = "tripAdvice_isRerouting"
const val TRIP_ADVICE_TITLE_KEY = "tripAdvice_title"
const val TRIP_ADVICE_SUBTEXT_KEY = "tripAdvice_subtext"
const val TRIP_ADVICE_TEXT_KEY = "tripAdvice_text"

//google maps package name
const val GOOGLE_MAPS_PACKAGE_NAME = "com.google.android.apps.maps"
const val YOUTUBE_MUSIC_PACKAGE_NAME = "com.google.android.apps.youtube.music"
const val SPOTIFY_PACKAGE_NAME = "com.spotify.music"

//const valious arrow positions for center console
const val ARROW_STRAIGHT = 6
const val ARROW_SLIGHT_LEFT = 5
const val ARROW_LEFT = 4
const val ARROW_SHARP_LEFT = 3
const val ARROW_SLIGHT_RIGHT = 7
const val ARROW_RIGHT = 0
const val ARROW_SHARP_RIGHT = 1

//these two values are supposed to be used as they are but are to be used as a reference
const val ARROW_LEFT_UTURN = 10
const val ARROW_RIGHT_UTURN = 20

const val ARROW_ARRIVE = 30

const val ARROW_OFF = -1

//Speech Recognition
const val SPEECH_RECOGNITION_RESULT = "speech_recog_result"

//Generic error message activity arguments constants
const val GENERIC_MESSAGE_TEXT_KEY = "error_text_message"
const val GENERIC_MESSAGE_STATUS_KEY = "error_status"
const val GENERIC_MESSAGE_TIMER_KEY = "error_message_show_time"
const val GENERIC_MESSAGE_BUTTON_TEXT_KEY = "error_message_button_text_key"

//PNP Device connection state events
const val PNP_BT_EVENT_STATE_CONNECTED = "pnp_bt_event_state_connected"
const val PNP_BT_EVENT_STATE_CONNECTING = "pnp_bt_event_state_connecting"
const val PNP_BT_EVENT_STATE_DISCONNECTED = "pnp_bt_event_state_disconnected"
