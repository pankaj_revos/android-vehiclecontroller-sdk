package com.revos.android.fleet.api.v2.auth

import com.google.gson.GsonBuilder
import com.revos.android.fleet.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

internal interface RevosAuthService {

    companion object {

        private val BASE_URL = if(BuildConfig.DEBUG) "https://auth.dev.revos.in/user/" else "https://auth.revos.in/user/"

        fun create(): RevosAuthService {
            val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val clientBuilder = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)

            val client = if(BuildConfig.DEBUG){
                clientBuilder.addInterceptor(logger).build()
            }else{
                clientBuilder.build()
            }

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
                .create(RevosAuthService::class.java)
        }
    }

    @POST("register/open")
    suspend fun openRegisterUser(@Header("token") token: String,
                                 @Body requestBody: RequestBody): LoginResponseModel

    @POST("login/open")
    suspend fun openLoginUser(@Header("token") token: String,
                              @Body requestBody: RequestBody): LoginResponseModel

    @POST("register/firebase")
    suspend fun registerUserWithFirebase(@Header("token") token: String,
                                         @Body requestBody: RequestBody): LoginResponseModel

    @POST("login/firebase")
    suspend fun loginUserWithFirebase(@Header("token") token: String,
                                      @Body requestBody: RequestBody): LoginResponseModel
}