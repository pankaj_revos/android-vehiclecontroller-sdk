package com.revos.android.fleet.data

data class RentalVehicleCompany (
    var id: String? = null,
    var name: String? = null,
    var phone: String? = null,
)