package com.revos.android.fleet.data

import com.revos.scripts.type.PaymentType
import com.revos.scripts.type.UnitType

data class RentalPricingInfo (
    var pricingName: String? = null,
    var paymentType: PaymentType? = null,
    var unitType: UnitType? = null,
    var baseAmount : Float = 0f,
    var costPerUnit : Float = 0f,
    var providerUpiAddress: String? = null,
    var paymentServiceType: String? = null,
)