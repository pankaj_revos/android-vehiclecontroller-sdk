package com.revos.android.fleet.api.v2.auth

import com.google.gson.annotations.SerializedName

internal data class LoginResponseModel(
    @SerializedName("data") val data: Data,
    @SerializedName("status") val status: Int,
    @SerializedName("message") val message: String) {

    data class Data(
        @SerializedName("token") val token: String,
        @SerializedName("user") val user: User ) {

        data class User(
            @SerializedName("address")
            val address: String,
            @SerializedName("email")
            val email: String,
            @SerializedName("firebase")
            val firebase: Firebase,
            @SerializedName("firstName")
            val firstName: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("lastName")
            val lastName: String,
            @SerializedName("openId")
            val openId: String,
            @SerializedName("permissions")
            val permissions: List<String>,
            @SerializedName("phone")
            val phone: String,
            @SerializedName("status")
            val status: String,
            @SerializedName("id_token")
            val id_token: String,
            @SerializedName("company")
            val company: Company) {

            data class Firebase(
                @SerializedName("uid")
                val uid: String
            )
            data class Company(
                @SerializedName("address")
                val address: Any,
                @SerializedName("cinNumber")
                val cinNumber: Any,
                @SerializedName("gstNumber")
                val gstNumber: Any,
                @SerializedName("id")
                val id: String,
                @SerializedName("logoBuffer")
                val logoBuffer: Any,
                @SerializedName("name")
                val name: String,
                @SerializedName("panNumber")
                val panNumber: Any,
                @SerializedName("permissions")
                val permissions: List<String>,
                @SerializedName("phone")
                val phone: Any,
                @SerializedName("status")
                val status: String,
                @SerializedName("token")
                val token: String
            )
        }
    }
}