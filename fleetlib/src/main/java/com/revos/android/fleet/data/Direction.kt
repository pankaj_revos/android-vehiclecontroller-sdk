package com.revos.android.fleet.data

import android.graphics.Bitmap

data class Direction(
    var nextRoad : String,
    var expectedTimeToArrive : String,
    var turnIconBitmap : Bitmap?,
    var distanceFromTurn : String,
    var currentDestination : String,
    var remainingTime : String,
    var remainingDistance : String,
)
