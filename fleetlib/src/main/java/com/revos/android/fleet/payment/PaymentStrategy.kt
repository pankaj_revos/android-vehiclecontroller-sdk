package com.revos.android.fleet.payment

internal interface PaymentStrategy {
    fun pay(amount : Float)
}