package com.revos.android.fleet.api.v2.vehicle

import com.google.gson.GsonBuilder
import com.revos.android.fleet.BuildConfig
import com.revos.android.fleet.api.v2.data.VehicleCommandResponse
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


internal interface RevOsVehicleService {

    companion object {
        private val BASE_URL = if(BuildConfig.DEBUG) "https://api.dev.revos.in/v2/" else "https://api.revos.in/v2/"

        fun create(): RevOsVehicleService {
            val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val clientBuilder = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)

            val client = if(BuildConfig.DEBUG){
                clientBuilder.addInterceptor(logger).build()
            }else{
                clientBuilder.build()
            }

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
                .create(RevOsVehicleService::class.java)
        }
    }

    @POST("vehicles/{VIN}/exec")
    suspend fun vehicleExecuteCommand(@Path("VIN") VIN: String?,
                              @Header("token") token: String?,
                              @Header("Authorization") bearerToken: String?,
                              @Body requestBody: RequestBody?) : VehicleCommandResponse

    @POST("mobile")
    fun uploadLog(@Query("authToken") authToken: String?,
                  @Body requestBody: RequestBody?) : Call<ResponseBody>

}