package com.revos.android.fleet.repository

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloException
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.revos.android.fleet.api.legacy.ApolloClientService
import com.revos.android.fleet.api.legacy.DataFeedApiService
import com.revos.android.fleet.api.v2.vehicle.RevOsVehicleService
import com.revos.android.fleet.data.DataFeedVehicleSnapshot
import com.revos.android.fleet.data.Device
import com.revos.android.fleet.data.Model
import com.revos.android.fleet.data.Vehicle
import com.revos.android.fleet.data.legacy.RentalVehicle
import com.revos.android.fleet.eventbus.EventBusMessage
import com.revos.android.fleet.managers.VehicleManager.BaseResponse
import com.revos.android.fleet.managers.VehicleManager.CmdResponse
import com.revos.android.fleet.managers.VehicleManager.ConnectionStatus
import com.revos.android.fleet.managers.VehicleManager.PinStatus
import com.revos.android.fleet.pnp.PnpDevice
import com.revos.android.fleet.services.NordicBleService
import com.revos.android.fleet.storage.prefs.LocalPrefs
import com.revos.android.fleet.ui.activities.RuntimePermissionActivity
import com.revos.android.fleet.utils.*
import com.revos.android.fleet.utils.general.NetworkUtils
import com.revos.android.fleet.utils.general.PrefUtils
import com.revos.android.fleet.utils.general.Version
import com.revos.android.fleet.utils.general.VersionUtils
import com.revos.android.fleet.utils.security.MetadataDecryptionHelper
import com.revos.scripts.*
import com.revos.scripts.type.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import timber.log.Timber

internal class VehicleRepository {

    private val GRANTED : Int = PackageManager.PERMISSION_GRANTED
    private val permissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.READ_CONTACTS)

    suspend fun fetchDeviceDetails(deviceId : String, context : Context) : ConnectionStatus {

        val deviceWhereUniqueInput = DeviceWhereUniqueInput
            .builder()
            .deviceId(deviceId)
            .build()

        val fetchDeviceQuery = FetchDeviceQuery
            .builder()
            .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
            .build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            return ConnectionStatus.UnableToFetchData
        }

        var status: ConnectionStatus

        val response = apolloClientService.query(fetchDeviceQuery).toDeferred().await()

        if(response.errors != null && response.errors!!.isNotEmpty()){
            return ConnectionStatus.UnableToFetchData
        }
        if(response.data != null && response.data!!.device() != null && response.data!!.device()?.get() != null){
            val device = LocalPrefs.saveDeviceDetailsToPrefs(context, response.data?.device()?.get()!!)
            if(device.status == DeviceStatus.INITIALIZED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                return ConnectionStatus.NotMarkedSold
            }

            if(device.status == DeviceStatus.ACTIVATED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                if(device.vehicle!!.status == VehicleStatus.RENTAL){
                    val activeBookingPrefString: String? =
                        context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).
                        getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null)

                    if(activeBookingPrefString == null){
                        LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                        return ConnectionStatus.MarkedRental
                    }
                    val rentalVehicle: RentalVehicle = Gson().fromJson(activeBookingPrefString, RentalVehicle::class.java)
                    if(!rentalVehicle.vin.isNullOrEmpty()){
                        if(rentalVehicle.vin != device.vehicle!!.vin){
                        }
                    }
                    LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                    return  ConnectionStatus.MarkedRental
                }else{
                    return ConnectionStatus.NotMarkedSold
                }
            }

            if(device.vehicle?.model?.protocol == ModelProtocol.PNP) {
                if(response.data!!.device()!!.get()!!.pin().isNullOrEmpty()){
                    return ConnectionStatus.EnterPin
                }
                context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE)
                    .edit().putString(DEVICE_PIN_KEY, response.data!!.device()!!.get()!!.pin()).apply()

                if(device.deviceType != null) {
                    if(device.deviceType == DeviceType.TBIT) {
                        EventBus.getDefault().post(EventBusMessage("",BT_EVENT_REQUEST_CONNECT_TBIT))
                    }else if(device.deviceType == DeviceType.ICONCOX) {
                        EventBus.getDefault().post(EventBusMessage("",BT_EVENT_REQUEST_CONNECT_ICONCOX))
                    }
                }
            }else {
                requestNotificationAccess(context)
                CoroutineScope(Dispatchers.Main).launch {
                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.macId))
                }
            }
            status = ConnectionStatus.DataFetched
        }else{
            context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE)
                .edit().putString(DEVICE_PIN_KEY, null).apply()
            status = ConnectionStatus.UnableToFetchData
        }

        NordicBleService.setDeviceName(null)
        MetadataDecryptionHelper.getInstance(context).clearLocalCache()
        LocalPrefs.clearBluetoothPrefs(context)
        context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE)
            .edit().clear().apply()

        return status
    }

    suspend fun connectUsingVin(vinNumber : String,
                                context : Context) : ConnectionStatus {

        val vehicleWhereUniqueInput = VehicleWhereUniqueInput
            .builder()
            .vin(vinNumber)
            .build()

        val fetchVehicleQuery = FetchVehicleQuery
            .builder()
            .vehicleWhereInputVar(vehicleWhereUniqueInput)
            .build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            return ConnectionStatus.UnableToFetchData
        }

        try {
            val response = apolloClientService.query(fetchVehicleQuery).toDeferred().await()
            if(response.errors != null && response.errors!!.isNotEmpty()){
                return ConnectionStatus.UnableToFetchData
            }

            if(response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()?.get() != null
                && response.data!!.vehicles()?.get()?.device() != null){
                val device = LocalPrefs.saveDeviceDetailsToPrefs(context, response.data?.vehicles()?.get()!!)

                if(device.status == DeviceStatus.INITIALIZED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                    LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                    return ConnectionStatus.NotMarkedSold
                }

                if(device.status == DeviceStatus.ACTIVATED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                    if(device.vehicle!!.status == VehicleStatus.RENTAL){
                        val activeBookingPrefString: String? =
                            context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                                .getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null)

                        if(activeBookingPrefString == null){
                            LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                            return ConnectionStatus.MarkedRental
                        }
                        val rentalVehicle: RentalVehicle = Gson().fromJson(activeBookingPrefString, RentalVehicle::class.java)
                        if(!rentalVehicle.vin.isNullOrEmpty()){
                            if(rentalVehicle.vin != device.vehicle!!.vin){
                                LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                                return ConnectionStatus.MarkedRental
                            }
                        }
                    }else{
                        //return ConnectionStatus.NotMarkedSold
                    }
                }
                if(device.vehicle?.model?.protocol == ModelProtocol.PNP) {
                    if(response.data!!.vehicles()?.get()?.device()!!.pin().isNullOrEmpty()){
                        CoroutineScope(Dispatchers.Main).launch {
                            EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_ENTER_PIN_TO_CONNECT + GEN_DELIMITER + device.macId))
                        }
                        return ConnectionStatus.EnterPin
                    }
                    context.getSharedPreferences(DEVICE_PREFS,Context.MODE_PRIVATE)
                        .edit().putString(DEVICE_PIN_KEY, response.data!!.vehicles()?.get()?.device()!!.pin()).apply()

                    if(device.deviceType != null) {
                        if(device.deviceType == DeviceType.TBIT) {
                            EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TBIT))
                        }else if(device.deviceType == DeviceType.ICONCOX) {
                            EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_ICONCOX))
                        }
                    }
                }else {
                    val sharedPrefs = context.getSharedPreferences(GENERAL_PREFS, Context.MODE_PRIVATE)
                    val isNotificationListenerConnected = sharedPrefs.getBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, false)
                    if(!isNotificationListenerConnected){
                        requestNotificationAccess(context)
                        return ConnectionStatus.NeedPermissions
                    }
                    if(!isLocationPermissionGranted(context)){
                        return ConnectionStatus.NeedPermissions
                    }
                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.macId))
                }
            }else{
                return ConnectionStatus.UnableToFetchData
            }
            NordicBleService.setDeviceName(null)
            MetadataDecryptionHelper.getInstance(context).clearLocalCache()
            LocalPrefs.clearBluetoothPrefs(context)

            //clear previous tracking data
            context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE).edit().clear().apply()
            return ConnectionStatus.DataFetched
        }catch(e : ApolloException) {
            return ConnectionStatus.UnableToFetchData
        }
    }

    private fun requestNotificationAccess(context: Context){
        context.startActivity(Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"))
    }

    private fun isLocationPermissionGranted(context: Context) : Boolean {
        if (ContextCompat.checkSelfPermission(context, permissions[0]) != GRANTED ||
            ContextCompat.checkSelfPermission(context, permissions[1]) != GRANTED ||
            ContextCompat.checkSelfPermission(context, permissions[2]) != GRANTED) {
            val permissionActivity = Intent(context, RuntimePermissionActivity::class.java)
            context.startActivity(permissionActivity)
            return false
        }
        return true
    }

    fun refreshDeviceData(context: Context): BaseResponse {

        val apolloClientService = ApolloClientService.create(context) ?: return BaseResponse.Failed

        val device = LocalPrefs.getDeviceObjectFromPrefs(context) ?: return BaseResponse.Failed

        if(device.deviceId == null) return BaseResponse.Failed

        val deviceWhereUniqueInput = DeviceWhereUniqueInput
            .builder()
            .deviceId(device.deviceId)
            .build()

        val fetchDeviceQuery = FetchDeviceQuery
            .builder()
            .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
            .build()
        var Status = BaseResponse.Failed
        apolloClientService.query(fetchDeviceQuery)?.enqueue(object : ApolloCall.Callback<FetchDeviceQuery.Data>(){

            override fun onResponse(response: Response<FetchDeviceQuery.Data>) {
                if (response.data != null && response.data!!.device() != null && response.data!!.device()!!.get() != null) {
                    val cloudDevice = response.data!!.device()
                    //save device pin, no need to set it null if it's not provided as it is just a refresh
                    if (cloudDevice!!.get()!!.pin() != null) {
                        Status = BaseResponse.Success
                        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE)
                            .edit().putString(DEVICE_PIN_KEY, cloudDevice.get()!!.pin()).apply()
                    } else {
                        Status = BaseResponse.Failed
                    }
                }
            }

            override fun onFailure(e: ApolloException) {

            }
        })

        return Status
    }

    fun refreshVehicleLiveLogs(context: Context) : BaseResponse {
        val userToken: String = PrefUtils.getInstance(context).authTokenFromPrefs ?: return BaseResponse.Failed

        val bearerToken = "Bearer $userToken"

        val dataFeedApiInterface = DataFeedApiService.create()
        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context)
        if(vehicle == null || vehicle.vin.isNullOrEmpty()) {
            return BaseResponse.Failed
        }

        try {
            val response = dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.vin, bearerToken).execute()
            val responseBody = response.body()!!.string()
            val jsonObject = JsonParser.parseString(responseBody).asJsonObject

            val status = jsonObject["status"].asInt
            if (status != 200) {
                return BaseResponse.Failed
            }

            val dataStr = jsonObject["data"].toString()
            val vehicleSnapshot: DataFeedVehicleSnapshot = Gson().fromJson(dataStr, DataFeedVehicleSnapshot::class.java)

            if (vehicleSnapshot.battery != null) {
                context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE)
                    .edit().putString(LIVE_PACKET_JSON_KEY, Gson().toJson(vehicleSnapshot)).apply()
                context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE)
                    .edit().putLong(LAST_LIVE_PACKET_REFRESHED_TIMESTAMP, System.currentTimeMillis()).apply()
                EventBus.getDefault()
                    .post(EventBusMessage(NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED))
                return BaseResponse.Success
            }
            return BaseResponse.Failed
        }catch (e : Exception){
            return BaseResponse.Failed
        }
    }

    suspend fun getMyVehicles(context: Context): ArrayList<Device> {
        var myDeviceList = ArrayList<Device>()
        val user = PrefUtils.getInstance(context).userFromPrefs
        if(user == null){
            return myDeviceList
        }
        val userWhereInput = UserWhereInput.builder().id(user.id).build()
        val vehicleWhereInput = VehicleWhereInput.builder().owner(userWhereInput).build()
        val fetchOwnersVehiclesQuery = FetchOwnersVehiclesQuery.builder().vehicleWhereInput(vehicleWhereInput).build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            return myDeviceList
        }

        val response = apolloClientService.query(fetchOwnersVehiclesQuery).toDeferred().await()

        if(response.hasErrors()){
            return myDeviceList
        }
        if(response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()!!.all != null) {
            val vehicles = response.data!!.vehicles()!!.all ?: return myDeviceList

            for (vehicleFromServer in vehicles) {
                var vehicle = Vehicle()
                var model = Model()
                vehicle.vin = vehicleFromServer.vin()
                vehicle.status = vehicleFromServer.status()
                vehicle.odoValue = vehicleFromServer.metrics()?.odometer()
                vehicle.ownerId = vehicleFromServer.owner()?.id()
                vehicle.ownerFirstName = vehicleFromServer.owner()?.firstName()
                vehicle.ownerLastName = vehicleFromServer.owner()?.lastName()
                vehicle.ownerEmailId = vehicleFromServer.owner()?.email()
                vehicle.ownerPhoneNo = vehicleFromServer.owner()?.phone()
                vehicle.buyerFirstName = vehicleFromServer.buyer()?.firstName()
                vehicle.buyerLastName = vehicleFromServer.buyer()?.lastName()
                vehicle.buyerPhoneNo = vehicleFromServer.buyer()?.phone()
                vehicle.vehicleId = vehicleFromServer.id()
                vehicle.distributorName = vehicleFromServer.distributor()?.name()
                vehicle.distributorAddress = vehicleFromServer.distributor()?.address()
                vehicle.distributorPhoneNo1 = vehicleFromServer.distributor()?.phone()
                vehicle.distributorPhoneNo2 = vehicleFromServer.distributor()?.phone1()

                vehicle.childSpeedLock = vehicleFromServer.config()?.childSpeedLock()
                vehicle.followMeHeadlamp = vehicleFromServer.config()?.followMeHomeHeadLamp()
                vehicle.warrantyExpiryTime = vehicleFromServer.warranty()?.expiry()
                vehicle.warrantyStatus = vehicleFromServer.warranty()?.status()
                vehicle.invoiceCreationDate = vehicleFromServer.invoice()?.createdAt()

                if (vehicleFromServer.invoice()?.passbook() != null &&
                    vehicleFromServer.invoice()?.passbook()?.isNotEmpty()!!) {
                    vehicle.invoiceStatus =
                        vehicleFromServer.invoice()?.passbook()?.get(0)?.status()
                }
                model.id = vehicleFromServer.model()?.id()
                model.name = vehicleFromServer.model()?.name()
                model.protocol = vehicleFromServer.model()?.protocol()

                var device = Device()
                device.pin = vehicleFromServer.device()?.pin()

                vehicle.model = model
                device.vehicle = vehicle

                myDeviceList.add(device)
            }
        }
        return myDeviceList
    }

    fun changeVehiclePin(context: Context,
                         oldPin :String,
                         newPin : String,
                         phone : String,
                         vin : String): PinStatus {

        val inputSetPin = InputSetPin.builder().phone(phone).vin(vin).pin(newPin).previousPin(oldPin).build()

        val setPinMutation = SetPinMutation
            .builder()
            .inputSetPin(inputSetPin)
            .build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            return PinStatus.Failed
        }
        var status =  PinStatus.Failed
        apolloClientService.mutate(setPinMutation)?.enqueue(object : ApolloCall.Callback<SetPinMutation.Data>(){

            override fun onResponse(response: Response<SetPinMutation.Data>) {
                if(!response.errors.isNullOrEmpty()){
                    if (response.errors!![0].message == "400, Bad Request: Wrong PIN"){
                        status =  PinStatus.WrongOldPin
                    }else if (response.errors!![0].message == "Bad input"){
                        status =  PinStatus.BadRequest
                    }else{
                        status =  PinStatus.Failed
                    }
                    return
                }
                if(response.data != null && response.data!!.device() != null
                    && response.data!!.device()!!.setPin() != null
                    && response.data!!.device()!!.setPin()!!.device() != null){

                    val cloudDevice = response.data!!.device()!!.setPin()!!.device()!!

                    NordicBleService.setDeviceName(null)

                    val device = LocalPrefs.saveDeviceDetailsToPrefs(context, cloudDevice)

                    if(device.pin.isNullOrEmpty()){
                        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(DEVICE_PIN_KEY, null).apply()
                    }else{
                        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(DEVICE_PIN_KEY, device.pin).apply()
                    }

                    MetadataDecryptionHelper.getInstance(context).clearLocalCache()

                    //clear previous metadata
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply()

                    status =  PinStatus.Success

                    refreshControllerMetadata(context, NordicBleService.getDeviceFirmwareVersion())

                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.macId))

                }else{
                    status =  PinStatus.Failed
                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_DISCONNECT))
                }
            }

            override fun onFailure(e: ApolloException) {
                PrefUtils.getInstance(context).clearAllPrefs()
                status =  PinStatus.Failed
            }
        })

        return status
    }

    suspend fun verifyPinToConnect(pin : String,
                                   context: Context): PinStatus {

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            return PinStatus.Failed
        }

        val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs
        if(vehicle == null || vehicle.vin.isNullOrEmpty()){
            return PinStatus.Failed
        }

        val verifyPinQuery = VerifyPinQuery.builder()
            .inputVerifyPinVar(InputVerifyPin.builder().pin(pin).vin(vehicle.vin).build())
            .build()
        val response = apolloClientService.query(verifyPinQuery).toDeferred().await()

        if(!response.errors.isNullOrEmpty()){
            return PinStatus.Failed
        }
        if (response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()!!.verifyPin() != null) {
            if (response.data!!.vehicles()!!.verifyPin()!!) {
                context.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE).edit().clear().apply()
                context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                    .putString(DEVICE_PIN_KEY, pin).apply()

                return  PinStatus.Success
            }else{
                return  PinStatus.IncorrectPin
            }
        }else{
            return  PinStatus.UnableToVerify
        }
    }

    suspend fun checkForOnGoingRentals(context: Context): BaseResponse {

        val apolloClientService = ApolloClientService.create(context)
        val leaseWhereInput = LeaseWhereInput
            .builder()
            .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
            .status(LeaseStatus.ACTIVE)
            .build()

        val fetchLeaseForUserQuery =
            FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build()

        if(apolloClientService == null) {
            return BaseResponse.Failed
        }
        var status = BaseResponse.Failed
        try{
            val response = apolloClientService.query(fetchLeaseForUserQuery).toDeferred().await()
            if (response.data != null && response.data!!.lease() != null && response.data!!.lease()!!.leasesForUser != null) {
                val activeUserRentals = response.data!!.lease()!!.leasesForUser
                if (activeUserRentals.isNullOrEmpty()) {
                    PrefUtils.getInstance(context).clearOnGoingRentalPrefs()
                    status = BaseResponse.NoData
                }else{
                    status = BaseResponse.Success
                }
            }
        }catch (e : ApolloException){
            status = BaseResponse.NoData
        }

        return status
    }

    suspend fun triggerCommandFromServer(context: Context,
                                         commandType : String): CmdResponse {

        var authToken = PrefUtils.getInstance(context).authTokenFromPrefs
        authToken = "Bearer $authToken"

        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context) ?: return CmdResponse.VehicleNotFound
        if(vehicle.vin.isNullOrEmpty()) return CmdResponse.VinNotFound

        val devicePin = context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).getString(DEVICE_PIN_KEY,null)
        if(devicePin.isNullOrEmpty()) return CmdResponse.SetPin

        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("command", commandType)
        bodyJsonObject.addProperty("pin", devicePin)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        val revosApiService = RevOsVehicleService.create()
        val response = revosApiService.vehicleExecuteCommand(vehicle.vin,"1234", bearerToken = authToken, requestBody )
        if(response.status != 200){
            //throw Exception(response.message)
            return CmdResponse.Failed
        }
        return CmdResponse.Success
    }

    fun refreshControllerMetadata(context: Context, deviceFirmwareVersion: String?) {
        val noPatchVersion = VersionUtils.removePatchVersion(deviceFirmwareVersion)

        if(noPatchVersion.isNullOrEmpty()) return

        downloadAndStoreControllerMetadata(context, TAG_READ, noPatchVersion)
        downloadAndStoreControllerMetadata(context, TAG_WRITE, noPatchVersion)
        downloadAndStoreControllerMetadata(context, TAG_LOG, noPatchVersion)
        downloadAndStoreControllerMetadata(context, TAG_VEHICLE_CONTROL, noPatchVersion)

        if (!noPatchVersion.isNullOrEmpty() && Version(noPatchVersion) == Version(VER_1_0)) {
            return
        }
        downloadAndStoreControllerMetadata(context, TAG_BATTERY_ADC_LOG, noPatchVersion)
    }

    fun downloadAndStoreControllerMetadata(context: Context, tag : String, version : String){
        if (tag.isEmpty() || version.isEmpty()) {
            return
        }
        val apolloClientService = ApolloClientService.create(context) ?: return
        val cloudDevice = PrefUtils.getInstance(context).deviceObjectFromPrefs ?: return

        val downloadControllerMetadataQuery = DownloadControllerMetadataQuery.builder()
            .controllerInputVar(ControllerInput.builder().version(version).tag(tag).clientId(cloudDevice.id!!).build())
            .build()

        if (tag == TAG_READ) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_LOG) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_WRITE) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_VEHICLE_CONTROL) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_BATTERY_ADC_LOG) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        }

        apolloClientService.query(downloadControllerMetadataQuery)?.enqueue(object : ApolloCall.Callback<DownloadControllerMetadataQuery.Data>(){
            override fun onResponse(response: Response<DownloadControllerMetadataQuery.Data>) {

                val controllerMetadataJSON = response.data!!.metadata()!!.controller()

                if (tag == TAG_READ) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_LOG) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_WRITE) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit().putString(
                        BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_VEHICLE_CONTROL) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context)
                        .setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                } else if (tag == TAG_BATTERY_ADC_LOG) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context)
                        .setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                }
                EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_CONTROLLER_METADATA_FILE_DOWNLOADED + GEN_DELIMITER + tag))
            }

            override fun onFailure(e: ApolloException) {
                if (tag == TAG_READ) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_LOG) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_WRITE) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_VEHICLE_CONTROL) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                } else if (tag == TAG_BATTERY_ADC_LOG) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                }
            }
        })
    }

    fun refreshVehicleLastKnownLocation(context: Context) {

        val userToken = PrefUtils.getInstance(context).authTokenFromPrefs

        val bearerToken = "Bearer $userToken"

        val dataFeedApiInterface = DataFeedApiService.create()

        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context)

        if(vehicle == null || vehicle.vin.isNullOrEmpty()) {
            return
        }

        dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.vin, bearerToken).enqueue(
            object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    try {
                        val responseBody = response.body()!!.string()
                        val jsonObject = JsonParser.parseString(responseBody).asJsonObject
                        val status = jsonObject["status"].asInt
                        if (status != 200) {
                            return
                        }

                        val dataStr = jsonObject["data"].toString()
                        val vehicleSnapshot = Gson().fromJson(dataStr, DataFeedVehicleSnapshot::class.java)
                        if (vehicleSnapshot.location != null && vehicleSnapshot.location!!.latitude != null
                            && vehicleSnapshot.location!!.longitude != null) {

                            val latStr = vehicleSnapshot.location!!.latitude.toString()
                            val lngStr = vehicleSnapshot.location!!.longitude.toString()

                            PrefUtils.getInstance(context).setVehicleLastKnownLocation(latStr,lngStr)
                            Timber.d("Vehicle last known LOCATION updated")
                        }

                        if (vehicleSnapshot?.location?.gpsSpeed != null) {
                            val speed = vehicleSnapshot.location?.gpsSpeed!!.toFloat()
                            PrefUtils.getInstance(context).lastKnownSpeed = speed
                            Timber.d("Vehicle last known SPEED updated")
                        }

                        if (vehicleSnapshot?.battery?.batterySOC != null) {
                            var batterySoc = vehicleSnapshot.battery?.batterySOC?.toLong()
                            if (batterySoc != null) {
                                if(batterySoc < 0 ){
                                    batterySoc = 0
                                } else if(batterySoc > 100){
                                    batterySoc = 100
                                }
                                PrefUtils.getInstance(context).batteryStatus = batterySoc
                                Timber.d("Vehicle Battery status updated")
                            }else{
                                Timber.e("Vehicle Battery status null")
                            }
                        }
                    } catch (e: Exception) {
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }
            })
    }


    /**
     * Vehicle data
     * */
    fun getOnGoingRentalVehicle(context: Context): RentalVehicle? {
        return PrefUtils.getInstance(context).onGoingRentalVehicle
    }

    fun getDevice(context: Context): Device? {
        return LocalPrefs.getDeviceObjectFromPrefs(context)
    }

    fun getVehicle(context: Context): Vehicle? {
        return LocalPrefs.getVehicleObjectFromPrefs(context)
    }

    fun getVehicleLastKnownLocation(context: Context): LatLng? {
        return PrefUtils.getInstance(context).vehicleLastKnownLocation
    }

    fun getBatteryStatus(context: Context): Long? {
        return PrefUtils.getInstance(context).batteryStatus
    }

    fun getSpeed(context: Context): Float? {
        return PrefUtils.getInstance(context).lastKnownSpeed
    }

    fun getLastConnectedDevice(context: Context): String?{
        return LocalPrefs.getLastConnectedDeviceVin(context);
    }
}