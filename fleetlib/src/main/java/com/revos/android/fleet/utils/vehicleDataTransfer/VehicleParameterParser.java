package com.revos.android.fleet.utils.vehicleDataTransfer;

import static com.revos.android.fleet.utils.ConstantsKt.VER_1_0_0;
import static com.revos.android.fleet.utils.ConstantsKt.VER_5_1_0;

import android.content.Context;

import com.revos.android.fleet.utils.general.Version;

public class VehicleParameterParser {

    //holds the single instance
    private Context mContext;
    private String mDataString;

    public VehicleParameterParser(Context context, String dataString) {
        mContext = context;
        mDataString = dataString;
    }

    //JSON KEYS
    private final String PACKET_TYPE_KEY = "packetType";
    private final String TRIP_ID_KEY = "tripId";
    private final String TIME_STAMP_KEY = "timeStamp";
    private final String SPEED_KEY = "speed";
    private final String RPM_KEY = "rpm";
    private final String THROTTLE_KEY = "throttle";
    private final String VOLTAGE_KEY = "voltage";
    private final String CURRENT_KEY = "current";
    private final String MODE_KEY = "bitMode";
    private final String REVERSE_KEY = "bitReverseStatus";
    private final String PARKING_KEY = "bitParkingStatus";
    private final String CHARGING_KEY = "bitChargingStatus";
    private final String OVERCURRENT_STATUS_KEY = "bitOverCurrentStatus";
    private final String OVERLOAD_STATUS_KEY = "bitOverloadStatus";
    private final String HILL_ASSIST_STATUS_KEY = "bitHillAssistStatus";
    private final String LOCK_STATUS_KEY = "bitLockState";
    private final String ANTI_THEFT_STATUS_KEY = "bitAntiTheftStatus";
    private final String EABS_STATUS_KEY = "bitEAbsStatus";
    private final String REGEN_BRAKING_STATUS_KEY = "bitRegenBrakingStatus";
    private final String BRAKE_STATUS_KEY = "bitBrakeStatus";
    private final String THROTTLE_ERROR_STATUS_KEY = "bitThrottleErrorStatus";
    private final String CONTROLLER_ERROR_STATUS_KEY = "bitControllerErrorStatus";
    private final String MOTOR_ERROR_STATUS_KEY = "bitMotorErrorStatus";
    private final String ODO_KEY = "odometer";
    private final String TEMP_KEY = "temperature";
    private final String SOC_KEY = "socPercentage";

    private final String HEADLAMP_STATUS_KEY = "bitHeadLampStatus";
    private final String RIGHT_INDICATOR_STATUS_KEY = "bitRightIndicatorStatus";
    private final String LEFT_INDICATOR_STATUS_KEY = "bitLeftIndicatorStatus";
    private final String X_ACC_KEY = "xRAW";
    private final String Y_ACC_KEY = "yRAW";
    private final String Z_ACC_KEY = "zRAW";
    private final String ANTI_THEFT_MODE_KEY = "antiTheftMode";
    private final String ANTI_THEFT_SENSITIVITY_KEY = "antiTheftSensitivity";
    private final String BATTERY_VOLTAGE_ADC_KEY = "batteryVoltageADC";
    private final String FOLLOW_ME_HOME_HEADLAMP_ON_TIME_KEY = "followMeHomeHeadlampOnTime";
    private final String GENERIC_WHEEL_LOCK_ENGAGED_STATUS_KEY = "bitGenericWheelLockEngagedStatus";
    private final String GENERIC_WHEEL_LOCK_STATUS_KEY = "bitGenericWheelLockStatus";
    private final String GEO_FENCING_WHEEL_LOCK_STATUS_KEY = "bitGeofencingWheelLockStatus";

    private final String CONTROL_CHECK_BYTE_KEY = "controlCheckByte";
    private final String CURRENT_CHECK_BYTE_KEY = "currentCheckByte";
    private final String UNDER_VOLTAGE_CHECK_BYTE_KEY = "underVoltageCheckByte";
    private final String OVER_VOLTAGE_CHECK_BYTE_KEY = "overVoltageCheckByte";
    private final String SPEED_CHECK_BYTE_KEY = "speedCheckByte";
    private final String THROTTLE_ZERO_REGEN_CHECK_BYTE_KEY = "throttleZeroRegenCheckByte";
    private final String PICK_UP_PERCENTAGE_CHECK_BYTE_KEY = "pickupPercentageCheckByte";
    private final String BRAKE_REGEN_CHECK_BYTE_KEY = "brakeRegenCheckByte";

    private final String DISPLAYED_IMAGE_CODE_KEY = "displayedImageCode";
    private final String CHECKSUM_CODE_KEY = "checksumCode";


    /**
     * @return VehicleParametersObject if parsing is successful, else null
     */
    public VehicleParameters parseAndCreateVehicleParametersObject(int packetType) {
        try {

            VehicleParameters vehicleParameters = new VehicleParameters();

            SpeedometerDataExchanger dataExchanger = SpeedometerDataExchanger.getInstance(mContext);
            if(dataExchanger == null) {
                return null;
            }
            //first and foremost determine the firmware version
            String firmwareVersion = dataExchanger.getMetadataFirmwareVersion();

            if(firmwareVersion != null && !firmwareVersion.isEmpty()) {
                return populateVehicleParameter(vehicleParameters, dataExchanger, packetType, firmwareVersion);
            } else {
                return null;
            }

        } catch (Exception e) {
            return null;
        }
    }

    private VehicleParameters populateVehicleParameter(VehicleParameters vehicleParameters, SpeedometerDataExchanger dataExchanger, int packetType, String firmwareVersion) {
        String[] dataArray = mDataString.split(" ");
        Object paramValue;
        float floatValue = -1;
        int intValue = -1;
        long longValue = -1;
        boolean boolValue = false;

        /**
         * parse controller parameters first
         */
        //set float values
        paramValue = dataExchanger.decodeParamValue(SPEED_KEY, dataArray, packetType);
        floatValue = -1;
        try {
                if(paramValue != null) {
                floatValue = (float)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setSpeed(floatValue);
        } finally {
            vehicleParameters.setSpeed(floatValue);
        }

        paramValue = dataExchanger.decodeParamValue(RPM_KEY, dataArray, packetType);
        floatValue = -1;
        try {
                if(paramValue != null) {
                floatValue = (float)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setRpm(floatValue);
        } finally {
            vehicleParameters.setRpm(floatValue);
        }


        paramValue = dataExchanger.decodeParamValue(THROTTLE_KEY, dataArray, packetType);
        floatValue = -1;
        try {
                if(paramValue != null) {
                floatValue = (float)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setThrottle(floatValue);
        } finally {
            vehicleParameters.setThrottle(floatValue);
        }

        paramValue = dataExchanger.decodeParamValue(VOLTAGE_KEY, dataArray, packetType);
        floatValue = -1;
        try {
                if(paramValue != null) {
                floatValue = (float)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setVoltage(floatValue);
        } finally {
            vehicleParameters.setVoltage(floatValue);
        }

        paramValue = dataExchanger.decodeParamValue(CURRENT_KEY, dataArray, packetType);
        floatValue = -1;
        try {
                if(paramValue != null) {
                floatValue = (float)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setCurrent(floatValue);
        } finally {
            vehicleParameters.setCurrent(floatValue);
        }

        paramValue = dataExchanger.decodeParamValue(ODO_KEY, dataArray, packetType);
        floatValue = -1;
        try {
                if(paramValue != null) {
                floatValue = (float)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setOdo(floatValue);
        } finally {
            vehicleParameters.setOdo(floatValue);
        }

        paramValue = dataExchanger.decodeParamValue(TEMP_KEY, dataArray, packetType);
        floatValue = -1;
        try {
                if(paramValue != null) {
                floatValue = (float)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setTemperature(floatValue);
        } finally {
            vehicleParameters.setTemperature(floatValue);
        }

        //set int values
        paramValue = dataExchanger.decodeParamValue(MODE_KEY, dataArray, packetType);
        intValue = -1;
        try {
                if(paramValue != null) {
                intValue = (int)paramValue;
            }
        }
        catch (Exception e) {
            vehicleParameters.setMode(intValue);
        } finally {
            vehicleParameters.setMode(intValue);
        }

        //set bool values
        paramValue = dataExchanger.decodeParamValue(LOCK_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setLocked(boolValue);

        paramValue = dataExchanger.decodeParamValue(OVERLOAD_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setOverLoad(boolValue);

        paramValue = dataExchanger.decodeParamValue(HILL_ASSIST_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setHillAssistEngaged(boolValue);

        paramValue = dataExchanger.decodeParamValue(OVERCURRENT_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setOverCurrent(boolValue);

        paramValue = dataExchanger.decodeParamValue(REVERSE_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setReverse(boolValue);

        paramValue = dataExchanger.decodeParamValue(PARKING_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setParking(boolValue);

        paramValue = dataExchanger.decodeParamValue(ANTI_THEFT_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setAntiTheftEngaged(boolValue);

        paramValue = dataExchanger.decodeParamValue(EABS_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setEABSEngaged(boolValue);

        paramValue = dataExchanger.decodeParamValue(CHARGING_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setCharging(boolValue);

        paramValue = dataExchanger.decodeParamValue(BRAKE_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setBraking(boolValue);

        paramValue = dataExchanger.decodeParamValue(REGEN_BRAKING_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setRegenBraking(boolValue);

        paramValue = dataExchanger.decodeParamValue(THROTTLE_ERROR_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setThrottleError(boolValue);

        paramValue = dataExchanger.decodeParamValue(CONTROLLER_ERROR_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setControllerError(boolValue);

        paramValue = dataExchanger.decodeParamValue(MOTOR_ERROR_STATUS_KEY, dataArray, packetType);
        if(paramValue == null) {
            boolValue = false;
        } else {
            intValue = (int) paramValue;
            boolValue = (intValue == 1);
        }
        vehicleParameters.setMotorError(boolValue);

        /**
         * parse packet info
         */
        //set long values
        if(packetType == SpeedometerDataExchanger.PACKET_TYPE_LIVE) {
            paramValue = dataExchanger.decodeParamValue(TRIP_ID_KEY, dataArray, packetType);
            longValue = -1;
            if (paramValue != null) {
                longValue = (long) paramValue;
            }
            vehicleParameters.setTripId(longValue);

            paramValue = dataExchanger.decodeParamValue(TIME_STAMP_KEY, dataArray, packetType);
            longValue = -1;
            if (paramValue != null) {
                longValue = (long) paramValue;
            }
            vehicleParameters.setTimeStamp(longValue);
        }

        /**
         * parse check bytes
         */

        if(packetType == SpeedometerDataExchanger.PACKET_TYPE_LIVE) {
            paramValue = dataExchanger.decodeParamValue(CONTROL_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setControlCheckByte(longValue);
            } finally {
                vehicleParameters.setControlCheckByte(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(SPEED_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setSpeedCheckByte(longValue);
            } finally {
                vehicleParameters.setSpeedCheckByte(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(CURRENT_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setCurrentCheckByte(longValue);
            } finally {
                vehicleParameters.setCurrentCheckByte(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(UNDER_VOLTAGE_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setUnderVoltageCheckByte(longValue);
            } finally {
                vehicleParameters.setUnderVoltageCheckByte(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(OVER_VOLTAGE_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setOverVoltageCheckByte(longValue);
            } finally {
                vehicleParameters.setOverVoltageCheckByte(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(THROTTLE_ZERO_REGEN_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setThrottleZeroRegenCheckByte(longValue);
            } finally {
                vehicleParameters.setThrottleZeroRegenCheckByte(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(BRAKE_REGEN_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setBrakeRegenCheckByte(longValue);
            } finally {
                vehicleParameters.setBrakeRegenCheckByte(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(PICK_UP_PERCENTAGE_CHECK_BYTE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setPickupPercentageCheckByte(longValue);
            } finally {
                vehicleParameters.setPickupPercentageCheckByte(longValue);
            }
        }

        /**
         * parse checksum and image metadata code
         */
        if(packetType == SpeedometerDataExchanger.PACKET_TYPE_LIVE) {
            paramValue = dataExchanger.decodeParamValue(DISPLAYED_IMAGE_CODE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setDisplayedImageCode(longValue);
            } finally {
                vehicleParameters.setDisplayedImageCode(longValue);
            }

            paramValue = dataExchanger.decodeParamValue(CHECKSUM_CODE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                    if(paramValue != null) {
                    longValue = (long)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setChecksumCode(longValue);
            } finally {
                vehicleParameters.setChecksumCode(longValue);
            }
        }

        /**
         * parse vehicle peripheral info
         */
        if(packetType == SpeedometerDataExchanger.PACKET_TYPE_LIVE) {
            paramValue = dataExchanger.decodeParamValue(HEADLAMP_STATUS_KEY, dataArray, packetType);
            if (paramValue == null) {
                boolValue = false;
            } else {
                intValue = (int) paramValue;
                boolValue = (intValue == 1);
            }
            vehicleParameters.setHeadLampOn(boolValue);

            paramValue = dataExchanger.decodeParamValue(RIGHT_INDICATOR_STATUS_KEY, dataArray, packetType);
            if (paramValue == null) {
                boolValue = false;
            } else {
                intValue = (int) paramValue;
                boolValue = (intValue == 1);
            }
            vehicleParameters.setRightIndicatorOn(boolValue);

            paramValue = dataExchanger.decodeParamValue(LEFT_INDICATOR_STATUS_KEY, dataArray, packetType);
            if (paramValue == null) {
                boolValue = false;
            } else {
                intValue = (int) paramValue;
                boolValue = (intValue == 1);
            }
            vehicleParameters.setLeftIndicatorOn(boolValue);

            paramValue = dataExchanger.decodeParamValue(X_ACC_KEY, dataArray, packetType);
            floatValue = 0;
            try {
                    if(paramValue != null) {
                    floatValue = (float)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setXAcc(floatValue);
            } finally {
                vehicleParameters.setXAcc(floatValue);
            }

            paramValue = dataExchanger.decodeParamValue(Y_ACC_KEY, dataArray, packetType);
            floatValue = 0;
            try {
                    if(paramValue != null) {
                    floatValue = (float)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setYAcc(floatValue);
            } finally {
                vehicleParameters.setYAcc(floatValue);
            }

            paramValue = dataExchanger.decodeParamValue(Z_ACC_KEY, dataArray, packetType);
            floatValue = 0;
            try {
                    if(paramValue != null) {
                    floatValue = (float)paramValue;
                }
            }
            catch (Exception e) {
                vehicleParameters.setZAcc(floatValue);
            } finally {
                vehicleParameters.setZAcc(floatValue);
            }

            /**IMPORTANT**/
            // anti theft is represented as a byte in 1.0 and as a bit flag after 1.1, hence cast has to be done
            // to long in case of 1.0 and int in case of 1.1 and above
            paramValue = dataExchanger.decodeParamValue(ANTI_THEFT_MODE_KEY, dataArray, packetType);
            longValue = -1;
            try {
                if(paramValue != null) {
                    if(firmwareVersion != null && !firmwareVersion.isEmpty() && new Version(firmwareVersion).compareTo(new Version(VER_1_0_0)) > 0) {
                        longValue = (int) paramValue;
                    } else {
                        longValue = (long) paramValue;
                    }
                }
            }
            catch (Exception e) {
                vehicleParameters.setAntiTheftMode(longValue);
            } finally {
                vehicleParameters.setAntiTheftMode(longValue);
            }

            /**
             * Extra parameters introduced in 1.1.0
             */
            if(firmwareVersion != null && !firmwareVersion.isEmpty() && new Version(firmwareVersion).compareTo(new Version(VER_1_0_0)) > 0) {
                paramValue = dataExchanger.decodeParamValue(BATTERY_VOLTAGE_ADC_KEY, dataArray, packetType);
                floatValue = -1;
                try {
                    if(paramValue != null) {
                        floatValue = (float)paramValue;
                    }
                }
                catch (Exception e) {
                    vehicleParameters.setBatteryAdcVoltage(floatValue);
                } finally {
                    vehicleParameters.setBatteryAdcVoltage(floatValue);
                }

                paramValue = dataExchanger.decodeParamValue(FOLLOW_ME_HOME_HEADLAMP_ON_TIME_KEY, dataArray, packetType);
                longValue = -1;
                try {
                    if(paramValue != null) {
                        longValue = (long)paramValue;
                    }
                }
                catch (Exception e) {
                    vehicleParameters.setFollowMeHomeHeadlampOnTime(longValue);
                } finally {
                    vehicleParameters.setFollowMeHomeHeadlampOnTime(longValue);
                }

                paramValue = dataExchanger.decodeParamValue(ANTI_THEFT_SENSITIVITY_KEY, dataArray, packetType);
                longValue = -1;
                try {
                    if(paramValue != null) {
                        longValue = (int)paramValue;
                    }
                }
                catch (Exception e) {
                    vehicleParameters.setAntiTheftSensitivity(longValue);
                } finally {
                    vehicleParameters.setAntiTheftSensitivity(longValue);
                }

                paramValue = dataExchanger.decodeParamValue(GENERIC_WHEEL_LOCK_ENGAGED_STATUS_KEY, dataArray, packetType);
                if (paramValue == null) {
                    boolValue = false;
                } else {
                    intValue = (int) paramValue;
                    boolValue = (intValue == 1);
                }
                vehicleParameters.setGenericWheelLockEngaged(boolValue);

                paramValue = dataExchanger.decodeParamValue(GENERIC_WHEEL_LOCK_STATUS_KEY, dataArray, packetType);
                if (paramValue == null) {
                    boolValue = false;
                } else {
                    intValue = (int) paramValue;
                    boolValue = (intValue == 1);
                }
                vehicleParameters.setGenericWheelLockStatus(boolValue);

                paramValue = dataExchanger.decodeParamValue(GEO_FENCING_WHEEL_LOCK_STATUS_KEY, dataArray, packetType);
                if (paramValue == null) {
                    boolValue = false;
                } else {
                    intValue = (int) paramValue;
                    boolValue = (intValue == 1);
                }
                vehicleParameters.setGeoFencingLockStatus(boolValue);
            }

            if(firmwareVersion != null && !firmwareVersion.isEmpty() && new Version(firmwareVersion).compareTo(new Version(VER_5_1_0)) >= 0) {
                paramValue = dataExchanger.decodeParamValue(SOC_KEY, dataArray, packetType);
                floatValue = -1;
                try {
                    if(paramValue != null) {
                        floatValue = (float)paramValue;
                    }
                }
                catch (Exception e) {
                    vehicleParameters.setSoc(floatValue);
                } finally {
                    vehicleParameters.setSoc(floatValue);
                }

            } else {
                floatValue = -1;
                vehicleParameters.setSoc(floatValue);
            }

        }

        return vehicleParameters;
    }
}
