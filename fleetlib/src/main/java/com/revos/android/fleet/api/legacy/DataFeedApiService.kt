package com.revos.android.fleet.api.legacy

import com.google.gson.GsonBuilder
import com.revos.android.fleet.BuildConfig
import com.revos.android.fleet.api.legacy.data.GetTripsForVehicleResponse
import com.revos.android.fleet.api.v2.lease.RevosLeaseService
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface DataFeedApiService {


    companion object {
        private val BASE_URL = if(BuildConfig.DEBUG) "https://datafeed.dev.revos.in/" else "https://datafeed.revos.in/"

        fun create(): DataFeedApiService {
            val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val clientBuilder = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)

            val client = if(BuildConfig.DEBUG){
                clientBuilder.addInterceptor(logger).build()
            }else{
                clientBuilder.build()
            }

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
                .create(DataFeedApiService::class.java)
        }
    }

    @POST("v2/getAllRentalVehicles")
    fun getAllRentalVehicles(@Query("package") packageName : String,
                             @Header("Authorization") bearerToken : String,
                             @Body requestBody : RequestBody ) : Call<ResponseBody>

    @GET("/v2/trips")
    fun getAllTripsForVehicle(@Query("vin") vin: String?,
                              @Query("first") first: Int,
                              @Query("skip") skip: Int,
                              @Header("Authorization") bearerToken: String?) : Call<ResponseBody>

    @GET("tripV2")
    fun getTripForVehicle(@Query("tripid") tripId: String?,
                          @Header("Authorization") bearerToken: String?) : Call<ResponseBody>


    @GET("v2/vehicleSnapshot")
    fun getVehicleLocationSnapshot(@Query("vin") vin: String?,
                                   @Header("Authorization") bearerToken: String?) : Call<ResponseBody>


    /*
    * To show battery status graph, carbon emission and fuel saved
    * To show location history in live tracking
    * */
    @GET("v2/vehicleLogsV2")
    fun getVehicleLiveLogForCount(@Query("vin") vin: String?,
                                  @Query("startTime") startTime: String?,
                                  @Query("endTime") endTime: String?,
                                  @Query("tripId") tripId: String?,
                                  @Query("count") count: Int,
                                  @Query("mobile") mobile: Boolean,
                                  @Header("Authorization") bearerToken: String?) : Call<ResponseBody>

    @GET("v2/vehicleLogsV2")
    fun getVehicleLiveLogWithoutCount(@Query("vin") vin: String?,
                                      @Query("startTime") startTime: String?,
                                      @Query("endTime") endTime: String?,
                                      @Query("tripId") tripId: String?,
                                      @Query("mobile") mobile: Boolean,
                                      @Header("Authorization") bearerToken: String?) : Call<ResponseBody>
}