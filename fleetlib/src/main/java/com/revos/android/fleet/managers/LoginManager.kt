package com.revos.android.fleet.managers

import com.google.gson.JsonObject
import com.revos.android.fleet.FleetUser
import com.revos.android.fleet.data.User
import com.revos.android.fleet.repository.LoginRepository
import com.revos.android.fleet.utils.general.PrefUtils
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.util.*

class LoginManager  {

    private var fleetManagerJob: Job? = null
    private val loginRepository = LoginRepository()

    /**
     * Returns the response of apis on this callback methods
     */
    interface Callback {
        fun onLoginSuccessful()
        fun onRegistrationSuccessful()
        fun onLoginFailed(message: String)
        fun onRegistrationFailed(message: String)
    }

    private var callback : Callback? = null

    /**
     * Callback to be used to get the response asynchronously from apis
     * @param callback Callback for response from apis
     * @see com.boltCore.android.managers.LoginManager.Callback
     */
    fun setCallback(callback : Callback){
        this.callback = callback
    }

    fun login(fleetUser: FleetUser){
        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("UID", fleetUser.userId)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) { // Make sure we cancel any previous request
            fleetManagerJob?.cancel()
        }

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val response = if(fleetUser.context.packageName == "com.revos.android"){
                    loginRepository.loginUsingFirebase(fleetUser.appToken, requestBody)
                }else{
                    loginRepository.openLogin(fleetUser.appToken, requestBody)
                }
                Timber.e("Token ${response.token}")
                val user = User(
                    address = response.user.address,
                    id = response.user.id,
                    role = null,
                    firstName = response.user.firstName,
                    lastName = response.user.lastName,
                    email = response.user.email,
                    phone = response.user.phone,
                    altPhone1 = null,
                    altPhone2 = null,
                    genderType = null,
                    dob = null,
                    status = response.user.status,
                )
                val prefUtils = PrefUtils.getInstance(fleetUser.context)
                prefUtils.saveUserToPrefs(user)
                prefUtils.saveAuthTokenToPrefs(response.token)
                withContext(Dispatchers.Main){
                    callback?.onLoginSuccessful()
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    callback?.onLoginFailed(exc.toString())
                }
            }
        }
    }

    fun register(fleetUser: FleetUser){
        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("firstName", fleetUser.firstName)
        bodyJsonObject.addProperty("lastName", fleetUser.lastName)
        bodyJsonObject.addProperty("address", fleetUser.address)
        bodyJsonObject.addProperty("UID", fleetUser.userId)
        bodyJsonObject.addProperty("phone", fleetUser.phoneNumber)
        bodyJsonObject.addProperty("email", fleetUser.email)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())

        if (fleetManagerJob != null && fleetManagerJob?.isActive!!) { // Make sure we cancel any previous request
            fleetManagerJob?.cancel()
        }

        fleetManagerJob = CoroutineScope(Dispatchers.IO).launch {
            try{
                val response = if(fleetUser.context.packageName == "com.revos.android"){
                    loginRepository.registerUsingFirebase(fleetUser.appToken, requestBody)
                }else{
                    loginRepository.openRegister(fleetUser.appToken, requestBody)
                }

                Timber.d("Token ${response.token}")
                val user = User(
                    address = response.user.address,
                    id = response.user.id,
                    role = null,
                    firstName = response.user.firstName,
                    lastName = response.user.lastName,
                    email = response.user.email,
                    phone = response.user.phone,
                    altPhone1 = null,
                    altPhone2 = null,
                    genderType = null,
                    dob = null,
                    status = response.user.status,
                )
                val prefUtils = PrefUtils.getInstance(fleetUser.context)
                prefUtils.saveUserToPrefs(user)
                prefUtils.saveAuthTokenToPrefs(response.token)

                withContext(Dispatchers.Main){
                    callback?.onRegistrationSuccessful()
                }
            }catch (exc : Exception){
                Timber.e(exc)
                withContext(Dispatchers.Main){
                    callback?.onRegistrationFailed(exc.toString())
                }
            }
        }
    }
}