package com.revos.android.fleet.api.v2.data


import com.google.gson.annotations.SerializedName

internal data class BookingResponseModel(
    @SerializedName("data")
    val data: BookingDetails,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
    )
