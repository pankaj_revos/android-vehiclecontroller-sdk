package com.revos.android.fleet.data

data class DataFeedAlarm (
    var isTowing : Int = 0,
    var isCrashDetection : Int = 0,
    var isBatteryUnplug : Int = 0,
    var isDeviceBatteryLow : Int = 0,
    var isWheel : Int = 0,
    var isVibration : Int = 0,
    var type: String? = null,
    var timestamp: String? = null,
    var terminalID: String? = null,
    var vin: String? = null,
)