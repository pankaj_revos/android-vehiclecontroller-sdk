package com.revos.android.fleet.pnp

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revos.android.fleet.data.DataState
import com.revos.android.fleet.managers.VehicleManager
import com.revos.android.fleet.utils.*
import com.tbit.tbitblesdk.Bike.TbitBle
import com.tbit.tbitblesdk.Bike.services.command.callback.SimpleCommonCallback
import com.tbit.tbitblesdk.bluetooth.BleClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TbitDevice constructor(var context: Context): MutableLiveData<DataState<String>>(),
    PnpDevice {

    private val vehicleManager = VehicleManager
    override fun lock() {
        if(isConnected()) {

            TbitBle.lock { resultCode ->
                if(resultCode != 0){
                    // Error
                    CoroutineScope(Dispatchers.IO).launch {
                        vehicleManager.triggerCommandFromServer(context,
                            PnpDevice.COMMAND.IGNITION_OFF.name
                        )
                    }
                }else{
                    // Success
                    vehicleManager.postCmdResponse(VehicleManager.CmdResponse.Success,BT_EVENT_REQUEST_LOCK_VEHICLE)
                }
            }
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                vehicleManager.triggerCommandFromServer(context,
                    PnpDevice.COMMAND.IGNITION_OFF.name
                )
            }
        }
    }

    override fun unlock() {
        if(isConnected()) {

            TbitBle.unlock { resultCode ->
                if(resultCode != 0){
                    // Error
                    CoroutineScope(Dispatchers.IO).launch {
                        vehicleManager.triggerCommandFromServer(context,
                            PnpDevice.COMMAND.IGNITION_ON.name
                        )
                    }
                }else{
                    // Success
                    vehicleManager.postCmdResponse(VehicleManager.CmdResponse.Success,BT_EVENT_REQUEST_UNLOCK_VEHICLE)
                }
            }
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                vehicleManager.triggerCommandFromServer(context,
                    PnpDevice.COMMAND.IGNITION_ON.name
                )
            }
        }
    }

    override fun findVehicle() {
        if(isConnected()) {
            TbitBle.commonCommand(0x03.toByte(), 0x04.toByte(), arrayOf(0x01), SimpleCommonCallback { resultCode ->
                    if (resultCode != 0) {
                        // Error
                        CoroutineScope(Dispatchers.IO).launch {
                            vehicleManager.triggerCommandFromServer(context,
                                PnpDevice.COMMAND.FIND.name
                            )
                        }
                    } else {
                        // Success
                        vehicleManager.postCmdResponse(VehicleManager.CmdResponse.Success,BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND)
                    }
                }
            )
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                vehicleManager.triggerCommandFromServer(context,
                    PnpDevice.COMMAND.FIND.name
                )
            }
        }
    }

    override fun disconnect() {
        if(TbitBle.hasInitialized()
            && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED
                    || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {

            TbitBle.disConnect()
        }
        clearDeviceCacheOnDisconnect()
    }

    override fun get(): LiveData<DataState<String>> {
        return this
    }

    override fun isConnected(): Boolean {
        return (TbitBle.hasInitialized()
                && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED
                || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED))
    }

    private fun clearDeviceCacheOnDisconnect(){
        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
        context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
        context.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
        context.getSharedPreferences(GEO_FENCING_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
    }
}