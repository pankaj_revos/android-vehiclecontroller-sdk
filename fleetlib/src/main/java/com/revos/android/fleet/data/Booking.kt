package com.revos.android.fleet.data

import java.util.*

data class Booking (
    var bookingTime: Date,
    var startTime: Date,
    var endTime: Date,
    var createdAt: Date,
    val type: TYPE,
    val remarks : String,
    val vin: String,
    var id: String? = null,
    val amount : Float) {

    var leasee : String? = null
    var leasor: String? = null
    var priceInfo: String? = null
    var v: Int = 0
    var updatedAt: String? = null
    var status: Status? = null
    var trips: List<String>? = null
    var asset: String? = null

    enum class TYPE {
        SHORT_TERM, LONG_TERM
    }

    enum class Status {
        PENDING_PAYMENT, BOOKED, TERMINATED, ACTIVE, ENDED, PAYMENT_INITIALISED, TERMINATED_FAILED_PAYMENT
    }
}
