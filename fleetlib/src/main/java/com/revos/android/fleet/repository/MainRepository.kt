package com.revos.android.fleet.repository

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Base64
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.gson.JsonObject
import com.revos.android.fleet.api.legacy.FirebaseClientService
import com.revos.android.fleet.graphql.GraphQLDateTimeCustomAdapter
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.toDeferred
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.revos.android.fleet.api.legacy.DataFeedApiService
import com.revos.android.fleet.api.legacy.RevOsApiService
import com.revos.android.fleet.eventbus.EventBusMessage
import com.revos.android.fleet.R
import com.revos.android.fleet.api.legacy.ApolloClientService
import com.revos.android.fleet.api.v2.auth.LoginResponseModel
import com.revos.android.fleet.api.v2.auth.RevosAuthService
import com.revos.android.fleet.api.v2.data.*
import com.revos.android.fleet.api.v2.data.BookingModel
import com.revos.android.fleet.api.v2.data.GetAllVehiclesModel
import com.revos.android.fleet.api.v2.data.RentalVehicleModel as RentalVehicleV2
import com.revos.android.fleet.api.v2.lease.RevosLeaseService
import com.revos.android.fleet.api.v2.payment.RevosPaymentService
import com.revos.android.fleet.data.*
import com.revos.android.fleet.data.legacy.RentalVehicle
import com.revos.android.fleet.utils.*
import com.revos.android.fleet.pnp.PnpDevice
import com.revos.android.fleet.services.NordicBleService
import com.revos.android.fleet.storage.prefs.LocalPrefs
import com.revos.android.fleet.storage.prefs.LocalPrefs.Companion.getDeviceObjectFromPrefs
import com.revos.android.fleet.storage.database.AppDatabase
import com.revos.android.fleet.utils.general.NetworkUtils
import com.revos.android.fleet.utils.general.PrefUtils
import com.revos.android.fleet.utils.general.Version
import com.revos.android.fleet.utils.general.VersionUtils
import com.revos.android.fleet.utils.security.MetadataDecryptionHelper
import com.revos.android.fleet.workers.GeofenceDatabaseWorker
import com.revos.scripts.*
import com.revos.scripts.type.*
import com.tbit.tbitblesdk.Bike.TbitBle
import com.tbit.tbitblesdk.bluetooth.BleClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.greenrobot.eventbus.EventBus
import org.jetbrains.annotations.Nullable
import retrofit2.Call
import retrofit2.Callback
import timber.log.Timber
import java.io.File
import kotlin.collections.HashMap
import kotlin.math.floor
import kotlin.math.round


internal class MainRepository {

    private var revosAuthService : RevosAuthService = RevosAuthService.create()
    private var revosLeaseService : RevosLeaseService = RevosLeaseService.create()
    private var revosPaymentService : RevosPaymentService = RevosPaymentService.create()

    suspend fun openRegister(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.openRegisterUser(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun openLogin(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.openLoginUser(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun registerUsingFirebase(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.registerUserWithFirebase(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun loginUsingFirebase(appToken : String, requestBody: RequestBody) : LoginResponseModel.Data {
        val response = revosAuthService.loginUserWithFirebase(appToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun getAllRentalVehicles(first : Int, orderBy : String?, skip : Int,
                                     appToken : String, bearerToken : String) : List<GetAllVehiclesModel.Data.Vehicle> {
        val response = revosLeaseService.getAllRentalVehicles(first, orderBy, skip, appToken , bearerToken)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data.vehicles
    }

    suspend fun getRentalVehicle(vin : String, appToken : String, bearerToken : String) : RentalVehicleV2 {
        val response = revosLeaseService.getRentalVehicle(vin, appToken , bearerToken)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response
    }

    suspend fun getAllBookings(first : Int, skip : Int,
                                 appToken : String, bearerToken : String) : List<BookingDetails> {
        val response = revosLeaseService.getAllBookings(first, skip, appToken , bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data.bookings
    }

    suspend fun getBooking(bookingId : String, appToken : String,
                           bearerToken : String) : BookingModel {
        val response = revosLeaseService.getUserBooking(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response
    }

    suspend fun createBooking(appToken : String, bearerToken : String,
                              requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.createBooking(appToken, bearerToken, requestBody )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun startBooking(bookingId: String, appToken : String,
                             bearerToken : String) : BookingDetails {
        val response = revosLeaseService.startBooking(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun modifyBooking(bookingId: String, appToken : String, bearerToken : String,
                             requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.modifyBooking(bookingId, appToken, bearerToken, requestBody )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun paymentInit(bookingId: String, appToken : String, bearerToken : String,
                              requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.paymentInit(bookingId, appToken, bearerToken, requestBody )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun getOrderStatus(appToken: String,
                               bearerToken : String,
                               requestBody: RequestBody):
            GetOrderStatusResponseModel.Details.OrderDetails {

        val response = revosPaymentService.getOrderStatus(appToken, bearerToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.details.orderDetails
    }

    suspend fun paymentConfirm(bookingId: String, appToken : String,
                               bearerToken : String, requestBody: RequestBody?) : BookingDetails {
        var response : BookingResponseModel
        if(requestBody != null){
            response = revosLeaseService.paymentConfirm(bookingId, appToken, bearerToken, requestBody )
        }else{
            response = revosLeaseService.paymentConfirm(bookingId, appToken, bearerToken )
        }
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun paymentFailed(bookingId: String, appToken : String,
                               bearerToken : String) : BookingDetails {
        val response = revosLeaseService.paymentFailed(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    suspend fun cancelBooking(bookingId: String, appToken : String,
                              bearerToken : String, requestBody: RequestBody) : BookingDetails {
        val response = revosLeaseService.cancelBooking(bookingId, appToken, bearerToken, requestBody)
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

    fun lockVehicle(vin: String, appToken : String,
                            bearerToken : String) : Call<ResponseBody> {

        return revosLeaseService.lockVehicle(vin, appToken, bearerToken )
    }

    fun unlockVehicle(vin: String, appToken : String,
                              bearerToken : String) : Call<ResponseBody> {
        return revosLeaseService.unlockVehicle(vin, appToken, bearerToken )
    }

    suspend fun endBooking(bookingId: String, appToken : String,
                           bearerToken : String) : BookingDetails {
        val response = revosLeaseService.endBooking(bookingId, appToken, bearerToken )
        if(response.status != 200){
            throw Exception(response.message)
        }
        return response.data
    }

   suspend fun openLogin(firebaseToken : String, context : Context) {

        val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }
        val okHttpClient =  OkHttpClient.Builder()
            .addInterceptor(logger)
            .build()

        val apolloClient: ApolloClient = ApolloClient.builder()
            .serverUrl(context.getString(R.string.server_link))
            .okHttpClient(okHttpClient)
            .addCustomTypeAdapter(CustomType.DATETIME, GraphQLDateTimeCustomAdapter())
            .build()

        val loginWithFirebaseMutation = LoginWithFirebaseMutation
            .builder()
            .accessTokenVar(firebaseToken)
            .appPackageVar(context.packageName)
            .build()

        apolloClient.mutate(loginWithFirebaseMutation)?.enqueue(
            object : ApolloCall.Callback<LoginWithFirebaseMutation.Data>(){

                override fun onResponse(response: Response<LoginWithFirebaseMutation.Data>) {
                    if(response.errors != null && response.errors!!.isNotEmpty()){
                        //Error state
                        //dataState.postValue(DataState.Error("Unable to login"))
                        return
                    }
                    val loginWithFirebase = response.data!!.account()!!.loginWithFirebase()
                    val firebaseUser = response.data!!.account()!!.loginWithFirebase()!!.user()

                    val authToken = loginWithFirebase!!.token()
                    val id: String = firebaseUser!!.id()
                    val role: Role = firebaseUser.role()
                    val firstName: String = firebaseUser.firstName().toString()
                    val lastName: String = firebaseUser.lastName().toString()
                    val email: String = firebaseUser.email().toString()
                    val phoneNo: String = firebaseUser.phone().toString()
                    val altPhone1: String = firebaseUser.altPhone1().toString()
                    val altPhone2: String = firebaseUser.altPhone2().toString()
                    val statusType: @Nullable StatusType? = firebaseUser.status()
                    val dob: String = firebaseUser.dob().toString()

                    if(authToken == null) {
                        //dataState.postValue(DataState.Error("Authentication Token is null"))
                        return
                    }

                    PrefUtils.getInstance(context).saveAuthTokenToPrefs(authToken)

                    CoroutineScope(Dispatchers.IO).launch {
                        sendRegistrationToServer(phoneNo, email, authToken, firebaseToken )
                    }

                    if(statusType == null || statusType == StatusType.DELETED ||
                        statusType == StatusType.INACTIVE){
                        //dataState.postValue(DataState.Error("Please renew your subscription"))
                        return //Sign out
                    }
                    val user = User(id = id, role = role, firstName = firstName, lastName = lastName,
                        email = email, phone = phoneNo, status = "ACTIVE", altPhone1 = altPhone1,
                        altPhone2 = altPhone2, dob = dob, genderType = null, address = null )

                    CoroutineScope(Dispatchers.IO).launch {
                        getUserUploads(user, context)
                    }
                }

                override fun onFailure(e: ApolloException) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }
        )
    }

    fun getUserUploads(user : User, context: Context) {
        val userWhereInput = UserWhereInput.builder().id(user.id).build()
        val fileWhereInput = FileWhereInput.builder().user(userWhereInput).build()
        val userUploadsQuery = UserUploadsQuery.builder().fileInputVar(fileWhereInput).build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_get_user_uploads)))
            return
        }

        apolloClientService.query(userUploadsQuery).enqueue(
            object : ApolloCall.Callback<UserUploadsQuery.Data>() {
                override fun onResponse(response: Response<UserUploadsQuery.Data>) {
                    if(response.errors != null && response.errors!!.isNotEmpty()){
                        //dataState.postValue(DataState.Error("Error in getting user uploads"))
                        return //Sign out
                    }else if(response.data != null && response.data!!.files() != null){
                        if (response.data!!.files()!!.list() != null) {
                            val sharedPreferences: SharedPreferences =
                                context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                            val fileList = response.data!!.files()!!.list()
                            for (fileIndex in fileList!!.indices) {
                                val fileType = fileList[fileIndex].type()
                                val fileName = fileList[fileIndex].name()
                                if (fileType == FileType.PROFILE) {
                                    sharedPreferences.edit()
                                        .putString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, fileName)
                                        .apply()
                                } else if (fileType == FileType.DL_SIDE_1) {
                                    sharedPreferences.edit()
                                        .putString(USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, fileName)
                                        .apply()
                                } else if (fileType == FileType.DL_SIDE_2) {
                                    sharedPreferences.edit()
                                        .putString(USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, fileName)
                                        .apply()
                                }
                            }
                        }
                        validateUserAndProceed(context, user)
                    }else{
                        //Error
                        //dataState.postValue(DataState.Error("Error in getting user uploads"))
                        return //Sign out
                    }
                }

                override fun onFailure(e: ApolloException) {
                    //dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }
        )
    }

    private fun validateUserAndProceed(context : Context,
                                       user : User) {

        PrefUtils.getInstance(context).saveUserToPrefs(user)

        if(user.firstName.isNull() || user.lastName.isNull()
            || user.dob.isNull() || user.email.isNull()){

            //dataState.postValue(DataState.Error("Launch User Details"))
            return //Sign out
        }
        //dataState.postValue(DataState.Success(user))
    }

    suspend fun sendRegistrationToServer(phone: String, email : String,
                                         authToken : String, firebaseToken : String){

        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("token", firebaseToken)
        bodyJsonObject.addProperty("email", email)
        bodyJsonObject.addProperty("phone", phone)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        val firebaseClientService = FirebaseClientService.create()
        firebaseClientService.sendFirebaseToken("Bearer $authToken", requestBody)
        //todo : Error handling from response
    }

    fun fetchDeviceDetails(deviceId : String, context : Context, dataState : MutableLiveData<DataState<String>> ) : Boolean {

        //dataState.postValue(DataState.Loading)

        val deviceWhereUniqueInput = DeviceWhereUniqueInput
            .builder()
            .deviceId(deviceId)
            .build()

        val fetchDeviceQuery = FetchDeviceQuery
            .builder()
            .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
            .build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
            return false
        }

        var status = false

        apolloClientService.query(fetchDeviceQuery).enqueue(object : ApolloCall.Callback<FetchDeviceQuery.Data>() {

            override fun onResponse(response: Response<FetchDeviceQuery.Data>) {

                if(response.errors != null && response.errors!!.isNotEmpty()){
                    //Error state
                    //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
                    return
                }
                Timber.d(" onResponse ${response.data} ${response.data?.device()}")

                if(response.data != null && response.data!!.device() != null && response.data!!.device()?.get() != null){
                    val device = LocalPrefs.saveDeviceDetailsToPrefs(context, response.data?.device()?.get()!!)

                    if(device.status == DeviceStatus.INITIALIZED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                        LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                        //dataState.postValue(DataState.Error(context.getString(R.string.vehicle_not_marked_sold)))
                        return
                    }

                    if(device.status == DeviceStatus.ACTIVATED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                        if(device.vehicle!!.status == VehicleStatus.RENTAL){
                            val activeBookingPrefString: String? =
                                context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).
                                getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null)

                            if(activeBookingPrefString == null){
                                LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                                //dataState.postValue(DataState.Error(context.getString(R.string.vehicle_marked_for_rental_go_to_rental)))
                                return
                            }
                            val rentalVehicle: RentalVehicle = Gson().fromJson(activeBookingPrefString, RentalVehicle::class.java)
                            if(!rentalVehicle.vin.isNullOrEmpty()){
                                if(rentalVehicle.vin != device.vehicle!!.vin){
                                    //dataState.postValue(DataState.Error(context.getString(R.string.vehicle_marked_for_rental_go_to_rental)))
                                }
                            }
                            LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                            return
                        }else{
                            //dataState.postValue(DataState.Error(context.getString(R.string.vehicle_not_marked_sold)))
                        }
                    }

                    if(device.vehicle?.model?.protocol == ModelProtocol.PNP) {
                        if(response.data!!.device()!!.get()!!.pin().isNullOrEmpty()){
                            CoroutineScope(Dispatchers.Main).launch {
                                EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_ENTER_PIN_TO_CONNECT + GEN_DELIMITER + device.macId))
                            }
                            return
                        }
                        context.getSharedPreferences(DEVICE_PREFS,Context.MODE_PRIVATE)
                            .edit().putString(DEVICE_PIN_KEY, response.data!!.device()!!.get()!!.pin()).apply()

                        if(device.deviceType != null) {
                            if(device.deviceType == DeviceType.TBIT) {
                                EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TBIT))
                            }else if(device.deviceType == DeviceType.ICONCOX) {
                                EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_ICONCOX))
                            }
                            //dataState.postValue(DataState.Success(context.getString(R.string.success_to_fetch_vehicle_details)))
                        }
                    }else {
                        requestNotificationAccess(context)
                        CoroutineScope(Dispatchers.Main).launch {
                            EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.macId))
                        }
                    }
                    status = true
                }else{
                    context.getSharedPreferences(DEVICE_PREFS,Context.MODE_PRIVATE)
                        .edit().putString(DEVICE_PIN_KEY, null).apply()
                    //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
                }

                NordicBleService.setDeviceName(null)
                MetadataDecryptionHelper.getInstance(context).clearLocalCache()
                LocalPrefs.clearBluetoothPrefs(context)

                //clear previous tracking data
                context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE)
                    .edit().clear().apply()

            }

            override fun onFailure(e: ApolloException) {}
        })
        return status
    }

    private fun requestNotificationAccess(context: Context){
        context.startActivity(Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"))
    }

    //https://github.com/square/retrofit/issues/3226
    fun refreshVehicleLiveLogs(context: Context) : Boolean {
        val userToken: String = PrefUtils.getInstance(context).authTokenFromPrefs ?: return false

        val bearerToken = "Bearer $userToken"

        val dataFeedApiInterface = DataFeedApiService.create()
        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context)
        if(vehicle == null || vehicle.vin.isNullOrEmpty()) {
            return false
        }

        try {
            val response = dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.vin, bearerToken).execute()
            val responseBody = response.body()!!.string()
            val jsonObject = JsonParser.parseString(responseBody).asJsonObject

            val status = jsonObject["status"].asInt
            if (status != 200) {
                return false
            }

            val dataStr = jsonObject["data"].toString()
            val vehicleSnapshot: DataFeedVehicleSnapshot = Gson().fromJson(dataStr, DataFeedVehicleSnapshot::class.java)

            if (vehicleSnapshot.battery != null) {
                context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE)
                    .edit().putString(LIVE_PACKET_JSON_KEY, Gson().toJson(vehicleSnapshot)).apply()
                context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE)
                    .edit().putLong(LAST_LIVE_PACKET_REFRESHED_TIMESTAMP, System.currentTimeMillis()).apply()
                EventBus.getDefault()
                    .post(EventBusMessage(NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED))
                return true
            }
            return false
        }catch (e : Exception){
            return false
        }
    }

    fun refreshDeviceData(context: Context) {

        val apolloClientService = ApolloClientService.create(context) ?: return

        val device = getDeviceObjectFromPrefs(context) ?: return

        if(device.deviceId == null) return

        val deviceWhereUniqueInput = DeviceWhereUniqueInput
            .builder()
            .deviceId(device.deviceId)
            .build()

        val fetchDeviceQuery = FetchDeviceQuery
            .builder()
            .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
            .build()

        apolloClientService.query(fetchDeviceQuery)?.enqueue(object : ApolloCall.Callback<FetchDeviceQuery.Data>(){

            override fun onResponse(response: Response<FetchDeviceQuery.Data>) {
                if (response.data != null && response.data!!.device() != null && response.data!!.device()!!.get() != null) {
                    val cloudDevice = response.data!!.device()

                    //save device pin, no need to set it null if it's not provided as it is just a refresh
                    if (cloudDevice!!.get()!!.pin() != null) {
                        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE)
                            .edit().putString(DEVICE_PIN_KEY, cloudDevice.get()!!.pin()).apply()
                    }
                }
            }

            override fun onFailure(e: ApolloException) {

            }
        })
    }

    fun refreshGeofenceData(context: Context) : Boolean {

        var apiCallStatus = false

        val apolloClientService = ApolloClientService.create(context) ?: return apiCallStatus

        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context)

        if(vehicle == null || vehicle.vin.isNullOrEmpty()) {
            return apiCallStatus
        }

        val vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vehicle.vin).build()
        val fetchGeoFenceQuery = FetchGeoFenceQuery.builder().where(vehicleWhereUniqueInput).build()

        apolloClientService.query(fetchGeoFenceQuery)?.enqueue(object : ApolloCall.Callback<FetchGeoFenceQuery.Data>() {
            override fun onResponse(response: Response<FetchGeoFenceQuery.Data>) {
                if(response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()?.fence != null){

                    context.getSharedPreferences(GEO_FENCING_PREFS,Context.MODE_PRIVATE)
                        .edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply()

                    val fenceType = response.data!!.vehicles()!!.fence!!.type()
                    val geoFenceData = GeoFencing(response.data!!.vehicles()!!.fence!!.id())
                    geoFenceData.geoFenceType = fenceType

                    if(fenceType == FenceType.RADIAL){
                        geoFenceData.circleRadius = response.data!!.vehicles()!!.fence!!.radius()
                        val centre = LatLng(
                            response.data!!.vehicles()!!.fence!!.centre()!!.latitude()!!,
                            response.data!!.vehicles()!!.fence!!.centre()!!.longitude()!!
                        )
                        geoFenceData.circleGeoFenceCentre = centre
                    }else{
                        val getGeoFenceVerticesList = response.data!!.vehicles()!!.fence!!.vertices()
                        val polygonVerticesArrayList = ArrayList<LatLng>()
                        for (index in getGeoFenceVerticesList!!.indices) {
                            val latitude = getGeoFenceVerticesList[index].latitude()!!
                            val longitude = getGeoFenceVerticesList[index].longitude()!!
                            polygonVerticesArrayList.add(LatLng(latitude, longitude))
                        }
                        geoFenceData.polygonVerticesList = polygonVerticesArrayList
                    }

                    val geofenceType = object : TypeToken<GeoFencing>() {}.type
                    val jsonStr = Gson().toJson(geoFenceData, geofenceType)

                    val inputData = workDataOf(WORK_MANAGER_KEY_GEOFENCE to jsonStr)

                    val request = OneTimeWorkRequestBuilder<GeofenceDatabaseWorker>()
                        .setInputData(inputData)
                        .build()

                    WorkManager.getInstance(context)
                        .beginWith(request)
                        .enqueue()

                    //save to prefs
                    context.getSharedPreferences(GEO_FENCING_PREFS,Context.MODE_PRIVATE)
                        .edit().putString(GEO_FENCING_JSON_DATA_KEY, Gson().toJson(geoFenceData)).apply()
                    apiCallStatus = true
                }else{
                    context.getSharedPreferences(GEO_FENCING_PREFS,Context.MODE_PRIVATE)
                        .edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply()
                }
            }

            override fun onFailure(e: ApolloException) {

            }
        })
        return apiCallStatus
    }

    suspend fun connectUsingVin(vinNumber : String,
                                context : Context,
                                dataState : MutableLiveData<DataState<String>> ) : Boolean {

       // dataState.postValue(DataState.Loading)

        val vehicleWhereUniqueInput = VehicleWhereUniqueInput
            .builder()
            .vin(vinNumber)
            .build()

        val fetchVehicleQuery = FetchVehicleQuery
            .builder()
            .vehicleWhereInputVar(vehicleWhereUniqueInput)
            .build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
            return false
        }

        try {
            val response = apolloClientService.query(fetchVehicleQuery).toDeferred().await()
            if(response.errors != null && response.errors!!.isNotEmpty()){
                //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
                return false
            }

            if(response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()?.get() != null
                && response.data!!.vehicles()?.get()?.device() != null){
                val device = LocalPrefs.saveDeviceDetailsToPrefs(context, response.data?.vehicles()?.get()!!)

                if(device.status == DeviceStatus.INITIALIZED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                    LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                    //dataState.postValue(DataState.Error(context.getString(R.string.vehicle_not_marked_sold)))
                    return false
                }

                if(device.status == DeviceStatus.ACTIVATED && device.vehicle != null && device.vehicle?.status != VehicleStatus.SOLD){
                    if(device.vehicle!!.status == VehicleStatus.RENTAL){
                        val activeBookingPrefString: String? =
                            context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                                .getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null)

                        if(activeBookingPrefString == null){
                            LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                            //dataState.postValue(DataState.Error(context.getString(R.string.vehicle_marked_for_rental_go_to_rental)))
                            return false
                        }
                        val rentalVehicle: RentalVehicle = Gson().fromJson(activeBookingPrefString, RentalVehicle::class.java)
                        if(!rentalVehicle.vin.isNullOrEmpty()){
                            if(rentalVehicle.vin != device.vehicle!!.vin){
                               // dataState.postValue(DataState.Error(context.getString(R.string.vehicle_marked_for_rental_go_to_rental)))
                                LocalPrefs.clearVehicleAndDeviceRelatedPrefs(context)
                                return false
                            }
                        }
                    }else{
                        //dataState.postValue(DataState.Error(context.getString(R.string.vehicle_not_marked_sold)))
                        return false
                    }
                }
                if(device.vehicle?.model?.protocol == ModelProtocol.PNP) {
                    if(response.data!!.vehicles()?.get()?.device()!!.pin().isNullOrEmpty()){
                        CoroutineScope(Dispatchers.Main).launch {
                            EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_ENTER_PIN_TO_CONNECT + GEN_DELIMITER + device.macId))
                        }
                        return false
                    }
                    context.getSharedPreferences(DEVICE_PREFS,Context.MODE_PRIVATE)
                        .edit().putString(DEVICE_PIN_KEY, response.data!!.vehicles()?.get()?.device()!!.pin()).apply()

                    if(device.deviceType != null) {
                        if(device.deviceType == DeviceType.TBIT) {
                            EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TBIT))
                        }else if(device.deviceType == DeviceType.ICONCOX) {
                            EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_ICONCOX))
                        }
                       // dataState.postValue(DataState.Success(context.getString(R.string.success_to_fetch_vehicle_details)))
                    }
                }else {
                    requestNotificationAccess(context)
                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.macId))
                }
            }else{
                //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
            }
            NordicBleService.setDeviceName(null)
            MetadataDecryptionHelper.getInstance(context).clearLocalCache()
            LocalPrefs.clearBluetoothPrefs(context)

            //clear previous tracking data
            context.getSharedPreferences(TRACKING_PREFS, Context.MODE_PRIVATE).edit().clear().apply()
            return true
        }catch(e : ApolloException) {
            return false
        }
    }

    fun updateDeviceFirmwareOnServer(context: Context, firmwareVersion : String) {
        if(firmwareVersion.isNullOrEmpty()) return

        val apolloClientService = ApolloClientService.create(context) ?: return

        val device = LocalPrefs.getDeviceObjectFromPrefs(context) ?: return

        val deviceWhereUniqueInput = DeviceWhereUniqueInput.builder()
            .deviceId(device.deviceId)
            .build()

        val inputOtaStatus = InputOtaStatus.builder()
            .version(firmwareVersion)
            .build()

        val markOTAStatusMutation = MarkOTAStatusMutation.builder()
            .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
            .inputOtaStatusVar(inputOtaStatus)
            .build()

        apolloClientService.mutate(markOTAStatusMutation)?.enqueue(object : ApolloCall.Callback<MarkOTAStatusMutation.Data>(){

            override fun onResponse(response: Response<MarkOTAStatusMutation.Data>) {
                context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                    .putBoolean(DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS, false).apply()
            }

            override fun onFailure(e: ApolloException) {
                context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                    .putBoolean(DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS, false).apply()
            }
        })
    }

    fun refreshTripData(context: Context, vin: String) {

        var authToken: String = PrefUtils.getInstance(context).authTokenFromPrefs
        authToken = "Bearer $authToken"

        val dataFeedApiService = DataFeedApiService.create()

        dataFeedApiService.getAllTripsForVehicle(vin, 10, 0, authToken)
            .enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    if(response.code() != 200 || response.body() == null) {
                        return
                    }

                    val bodyStr = response.body()!!.string()
                    val jsonObject = JsonParser.parseString(bodyStr).asJsonObject
                    val trips : JsonArray = jsonObject.get("trips").asJsonArray

                    val dataStr: String = trips.toString()

                    val tripArrayList = Gson().fromJson<ArrayList<TripDataFeed>>(dataStr,
                        object : TypeToken<ArrayList<TripDataFeed?>?>() {}.type)
                    val tripListToBeSaved = ArrayList<TripDataFeed>()

                    for (fetchedTrip in tripArrayList) {
                        tripListToBeSaved.add(fetchedTrip)
                    }

                    context.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE)
                        .edit().putBoolean(TRIP_DATA_NEEDS_REFRESH_KEY, false).apply()

                    PrefUtils.getInstance(context).updateTripData(tripListToBeSaved)
                    Timber.e("Trip Data refreshed ${tripListToBeSaved.size}")

                    EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_TRIP_DATA_REFRESHED))
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                    context.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE)
                        .edit().putBoolean(TRIP_DATA_IS_REFRESHING_KEY, false).apply()

                    val eventMessage = EventBusMessage(NETWORK_EVENT_ERROR)
                    eventMessage.message = "Error fetching trip data"

                    EventBus.getDefault().post(eventMessage)
                }
            })
    }

    fun refreshVehicleHistoricalData(context: Context, vin : String,
                                     dataState : MutableLiveData<DataState<ArrayList<DataFeedVehicleLiveLog>>>) {

       // dataState.postValue(DataState.Loading)

        var authToken = PrefUtils.getInstance(context).authTokenFromPrefs
        authToken = "Bearer $authToken"

        val dataFeedApiService = DataFeedApiService.create()

        dataFeedApiService.getVehicleLiveLogForCount(vin, "", "", "", 2000, true, bearerToken = authToken)
            .enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    if(response.code() != 200 || response.body() == null) {
                       // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                        return
                    }

                    val bodyStr = response.body()!!.string()
                    val jsonObject = JsonParser.parseString(bodyStr).asJsonObject
                    val status : Int = jsonObject.get("status").asInt
                    if(status != 200) return

                    val data : String = jsonObject.get("data").toString()

                    val vehicleLiveLogList: ArrayList<DataFeedVehicleLiveLog> =
                        Gson().fromJson(data,object : TypeToken<ArrayList<DataFeedVehicleLiveLog?>?>() {}.type)

                    val vehicleLiveLogListToBeSaved = ArrayList<DataFeedVehicleLiveLog>()

                    if(!vehicleLiveLogList.isNullOrEmpty()){
                        for(liveLog in vehicleLiveLogList){
                            vehicleLiveLogListToBeSaved.add(liveLog)
                        }
                    }
                   // dataState.postValue(DataState.Success(vehicleLiveLogListToBeSaved))
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            })
    }

    fun triggerCommandFromServer(context: Context,
                                 dataState : MutableLiveData<DataState<String>>,
                                 commandType : PnpDevice.COMMAND) {

        var authToken = PrefUtils.getInstance(context).authTokenFromPrefs
        authToken = "Bearer $authToken"

        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context) ?: return
        if(vehicle.vin.isNullOrEmpty()) return

        val device = LocalPrefs.getDeviceObjectFromPrefs(context) ?: return
        if(device.pin.isNullOrEmpty()) return

        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("command", commandType.name)
        bodyJsonObject.addProperty("pin", device.pin)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        val revosApiService = RevOsApiService.create()

        revosApiService.vehicleExecuteCommand(vehicle.vin, "1234", bearerToken = authToken, requestBody).enqueue(
            object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    if(response.code() != 200 || response.body() == null) {
                        //Server error
                        dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                        return
                    }

                    val bodyStr = response.body()!!.string()
                    val bodyJsonObject = JsonParser.parseString(bodyStr).asJsonObject
                    if(bodyJsonObject.has("data")){
                        val dataStr = bodyJsonObject["data"].asString
                        if(dataStr.isNullOrEmpty() || dataStr != "SUCCESS.") {
                            //Server error
                           // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                        }else{
                            // Success
                          //  dataState.postValue(DataState.Success(context.getString(R.string.command_success)))
                        }
                    }
                    CoroutineScope(Dispatchers.IO).launch {
                        refreshVehicleLiveLogs(context)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                }
            })
    }

    fun createGeofence(inputFenceData : InputFenceData,
                       vehicle : Vehicle,
                       context: Context,
                       name : String,
                       dataState: MutableLiveData<DataState<String>>){

        val apolloClientService = ApolloClientService.create(context) ?: return

        val vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vehicle.vin).build()
        val setGeoFenceMutation = SetGeoFenceMutation.builder()
            .data(inputFenceData)
            .where(vehicleWhereUniqueInput)
            .build()

        apolloClientService.mutate(setGeoFenceMutation)
            .enqueue(object : ApolloCall.Callback<SetGeoFenceMutation.Data>(){

                override fun onResponse(response: Response<SetGeoFenceMutation.Data>) {

                    if(!response.errors.isNullOrEmpty()){
                       // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                        return
                    }
                    if (response.data != null
                        && response.data!!.vehicles() != null
                        && response.data!!.vehicles()!!.setFence() != null) {
                        //clear prefs before saving
                        context.getSharedPreferences(GEO_FENCING_PREFS,Context.MODE_PRIVATE)
                            .edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply()

                        val geoFencing = GeoFencing(response.data!!.vehicles()!!.setFence()!!.id())

                        val fenceType = response.data!!.vehicles()!!.setFence()!!.type()
                        if (fenceType == FenceType.RADIAL) {
                            geoFencing.geoFenceType = fenceType
                            geoFencing.circleRadius = response.data!!.vehicles()!!.setFence()!!.radius()

                            val centre = LatLng(response.data!!.vehicles()!!.setFence()!!.centre()!!.latitude()!!,
                                response.data!!.vehicles()!!.setFence()!!.centre()!!.longitude()!!)
                            geoFencing.circleGeoFenceCentre = centre
                            geoFencing.geoFenceName = name
                        } else {
                            geoFencing.geoFenceType = fenceType
                            val setGeoFenceVerticesList = response.data!!.vehicles()!!.setFence()!!.vertices()
                            val polygonVerticesArrayList = ArrayList<LatLng>()
                            for (index in setGeoFenceVerticesList!!.indices) {
                                val latitude = setGeoFenceVerticesList[index].latitude()!!
                                val longitude = setGeoFenceVerticesList[index].longitude()!!
                                polygonVerticesArrayList.add(LatLng(latitude, longitude))
                            }
                            geoFencing.polygonVerticesList = polygonVerticesArrayList
                            geoFencing.geoFenceName = name
                        }

                        val geofenceType = object : TypeToken<GeoFencing>() {}.type
                        val jsonStr = Gson().toJson(geoFencing, geofenceType)

                        val inputData = workDataOf(WORK_MANAGER_KEY_GEOFENCE to jsonStr)

                        val request = OneTimeWorkRequestBuilder<GeofenceDatabaseWorker>()
                            .setInputData(inputData)
                            .build()

                        WorkManager.getInstance(context)
                            .beginWith(request)
                            .enqueue()

                        //save to prefs
                        context.getSharedPreferences(GEO_FENCING_PREFS, Context.MODE_PRIVATE)
                            .edit().putString(GEO_FENCING_JSON_DATA_KEY, Gson().toJson(geoFencing)).apply()

                        //dataState.postValue(DataState.Success(context.getString(R.string.geo_fencing_changes_applied_successfully)))

                    }else{
                        //dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    }
                }

                override fun onFailure(e: ApolloException) {
                    //dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            })
    }

    fun removeGeofence(geofenceId: String,
                       context: Context,
                       dataState: MutableLiveData<DataState<String>>){

        val apolloClientService = ApolloClientService.create(context) ?: return

        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context)

        if(vehicle == null || vehicle.vin.isNullOrEmpty()) {
           // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_vin_for_geofence)))
            return
        }

        val vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vehicle.vin).build()
        val removeGeoFenceMutation = RemoveGeoFenceMutation.builder()
            .vehicleWhereUniqueInput(vehicleWhereUniqueInput).build()

        apolloClientService.mutate(removeGeoFenceMutation)
            .enqueue(object : ApolloCall.Callback<RemoveGeoFenceMutation.Data>(){

                override fun onResponse(response: Response<RemoveGeoFenceMutation.Data>) {

                    if(!response.errors.isNullOrEmpty()){
                        //dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                        return
                    }
                    if(response.data != null && response.data?.vehicles() != null){
                        context.getSharedPreferences(GEO_FENCING_PREFS,Context.MODE_PRIVATE)
                            .edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply()

                        CoroutineScope(Dispatchers.IO).launch {
                            deleteGeofenceZone(context, geofenceId)
                        }

                        //dataState.postValue(DataState.Success(context.getString(R.string.geo_fence_removed_successfully)))
                    }else{
                       // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    }
                }

                override fun onFailure(e: ApolloException) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            })
    }

    fun getMyGeofences(context: Context) : Flow<List<GeoFencing>> {
        val geofenceDao = AppDatabase.getInstance(context).geofenceDao()
        return geofenceDao.getAllGeofence()
    }

    suspend fun deleteGeofenceZone(context: Context, geofenceId: String) {
        val geofenceDao = AppDatabase.getInstance(context).geofenceDao()
        geofenceDao.deleteGeofenceById(geofenceId)
    }

    fun getGeofenceById(context: Context, geofenceId: String): LiveData<GeoFencing?> {
        val geofenceDao = AppDatabase.getInstance(context).geofenceDao()
        return geofenceDao.getGeofenceById(geofenceId).asLiveData()
    }

    fun getMyVehicles(context: Context, dataState: MutableLiveData<DataState<List<Device>>>) {
        //dataState.postValue(DataState.Loading)
        val user = PrefUtils.getInstance(context).getUserFromPrefs()
        if(user == null){
            //dataState.postValue(DataState.Error("No user Found"))
            return
        }
        val userWhereInput = UserWhereInput.builder().id(user.id).build()
        val vehicleWhereInput = VehicleWhereInput.builder().owner(userWhereInput).build()
        val fetchOwnersVehiclesQuery = FetchOwnersVehiclesQuery.builder().vehicleWhereInput(vehicleWhereInput).build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
           // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_locate_vehicle)))
            return
        }

        var myDeviceList = ArrayList<Device>()

        apolloClientService.query(fetchOwnersVehiclesQuery).enqueue(
            object : ApolloCall.Callback<FetchOwnersVehiclesQuery.Data>() {
                override fun onResponse(response: Response<FetchOwnersVehiclesQuery.Data>) {
                    if(!response.errors.isNullOrEmpty()){
                        return
                    }else if(response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()!!.all != null) {
                        val vehicles = response.data!!.vehicles()!!.all ?: return

                        for(vehicleFromServer in vehicles){
                            var vehicle = Vehicle()
                            var model = Model()
                            vehicle.vin = vehicleFromServer.vin()
                            vehicle.status = vehicleFromServer.status()
                            vehicle.odoValue = vehicleFromServer.metrics()?.odometer()
                            vehicle.ownerId = vehicleFromServer.owner()?.id()
                            vehicle.ownerFirstName = vehicleFromServer.owner()?.firstName()
                            vehicle.ownerLastName = vehicleFromServer.owner()?.lastName()
                            vehicle.ownerEmailId = vehicleFromServer.owner()?.email()
                            vehicle.ownerPhoneNo = vehicleFromServer.owner()?.phone()
                            vehicle.buyerFirstName = vehicleFromServer.buyer()?.firstName()
                            vehicle.buyerLastName = vehicleFromServer.buyer()?.lastName()
                            vehicle.buyerPhoneNo = vehicleFromServer.buyer()?.phone()
                            vehicle.vehicleId = vehicleFromServer.id()
                            vehicle.distributorName = vehicleFromServer.distributor()?.name()
                            vehicle.distributorAddress = vehicleFromServer.distributor()?.address()
                            vehicle.distributorPhoneNo1 = vehicleFromServer.distributor()?.phone()
                            vehicle.distributorPhoneNo2 = vehicleFromServer.distributor()?.phone1()

                            vehicle.childSpeedLock = vehicleFromServer.config()?.childSpeedLock()
                            vehicle.followMeHeadlamp = vehicleFromServer.config()?.followMeHomeHeadLamp()
                            vehicle.warrantyExpiryTime = vehicleFromServer.warranty()?.expiry()
                            vehicle.warrantyStatus = vehicleFromServer.warranty()?.status()
                            vehicle.invoiceCreationDate = vehicleFromServer.invoice()?.createdAt()

                            if(vehicleFromServer.invoice()?.passbook() != null &&
                                vehicleFromServer.invoice()?.passbook()?.isNotEmpty()!!) {
                                vehicle.invoiceStatus = vehicleFromServer.invoice()?.passbook()?.get(0)?.status()
                            }
                            model.id = vehicleFromServer.model()?.id()
                            model.name = vehicleFromServer.model()?.name()
                            model.protocol = vehicleFromServer.model()?.protocol()

                            var device = Device()
                            device.pin = vehicleFromServer.device()?.pin()

                            vehicle.model = model
                            device.vehicle = vehicle

                            myDeviceList.add(device)
                        }

                        if(myDeviceList.isNullOrEmpty()){
                          //  dataState.postValue(DataState.Error("Error fetching user vehicles"))
                        }else{
                         //   dataState.postValue(DataState.Success(myDeviceList))
                        }
                    }
                }

                override fun onFailure(e: ApolloException) {
                  //  dataState.postValue(DataState.Error("Error fetching user vehicles"))
                }
            }
        )
    }

    fun verifyPinToConnect(pin : String,
                           context: Context,
                           dataState: MutableLiveData<DataState<String>>){

       // dataState.postValue(DataState.Loading)

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
        //    dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            return
        }

        val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs
        if(vehicle == null || vehicle.vin.isNullOrEmpty()){
          //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            return
        }

        val verifyPinQuery = VerifyPinQuery.builder()
            .inputVerifyPinVar(InputVerifyPin.builder().pin(pin).vin(vehicle.vin).build())
            .build()

        apolloClientService.query(verifyPinQuery).enqueue(object : ApolloCall.Callback<VerifyPinQuery.Data>() {

            override fun onResponse(response: Response<VerifyPinQuery.Data>) {

                if(!response.errors.isNullOrEmpty()){
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    return
                }
                if (response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()!!.verifyPin() != null) {
                    if (response.data!!.vehicles()!!.verifyPin()!!) {
                        context.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE).edit().clear().apply()
                        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(DEVICE_PIN_KEY, pin).apply()

                     //   dataState.postValue(DataState.Success(context.getString(R.string.success)))
                    }else{
                       // dataState.postValue(DataState.Error(context.getString(R.string.incorrect_pin)))
                    }
                }else{
                  //  dataState.postValue(DataState.Error(context.getString(R.string.error)))
                }
            }

            override fun onFailure(e: ApolloException) {
              // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun changeVehiclePin(context: Context,
                         oldPin :String,
                         newPin : String,
                         phone : String,
                         vin : String,
                         dataState: MutableLiveData<DataState<String>>) {

       // dataState.postValue(DataState.Loading)

        val inputSetPin = InputSetPin.builder().phone(phone).vin(vin).pin(newPin).previousPin(oldPin).build()

        val setPinMutation = SetPinMutation
            .builder()
            .inputSetPin(inputSetPin)
            .build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
          //  dataState.postValue(DataState.Error(context.getString(R.string.unable_to_change_vehicle_pin)))
            return
        }

        apolloClientService.mutate(setPinMutation)?.enqueue(object : ApolloCall.Callback<SetPinMutation.Data>(){

            override fun onResponse(response: Response<SetPinMutation.Data>) {
                if(!response.errors.isNullOrEmpty()){
                    if (response.errors!![0].message == "400, Bad Request: Wrong PIN"){
                      //  dataState.postValue(DataState.Error(context.getString(R.string.old_pin_entered_is_wrong)))
                    }else if (response.errors!![0].message == "Bad input"){
                     //   dataState.postValue(DataState.Error(context.getString(R.string.insufficient_details)))
                    }else{
                        dataState.postValue(DataState.Error(context.getString(R.string.unable_to_change_vehicle_pin)))
                    }
                    return
                }
                if(response.data != null && response.data!!.device() != null
                    && response.data!!.device()!!.setPin() != null
                    && response.data!!.device()!!.setPin()!!.device() != null){

                    val cloudDevice = response.data!!.device()!!.setPin()!!.device()!!

                    NordicBleService.setDeviceName(null)

                    val device = LocalPrefs.saveDeviceDetailsToPrefs(context, cloudDevice)

                    if(device.pin.isNullOrEmpty()){
                        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(DEVICE_PIN_KEY, null).apply()
                    }else{
                        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(DEVICE_PIN_KEY, device.pin).apply()
                    }

                    MetadataDecryptionHelper.getInstance(context).clearLocalCache()

                    //clear previous metadata
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply()
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply()

                    dataState.postValue(DataState.Success(context.getString(R.string.vehicle_pin_changed_successfully)))

                    refreshControllerMetadata(context, NordicBleService.getDeviceFirmwareVersion())

                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.macId))

                }else{
                    dataState.postValue(DataState.Error(context.getString(R.string.unable_to_change_vehicle_pin)))
                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_DISCONNECT))
                }
            }

            override fun onFailure(e: ApolloException) {
                PrefUtils.getInstance(context).clearAllPrefs()
            }
        })
    }

    suspend fun fetchAllRentalVehicles(context: Context,
                               latLngBounds: LatLngBounds) : ArrayList<RentalVehicle>  {

        val userToken = PrefUtils.getInstance(context).authTokenFromPrefs

        val bearerToken = "Bearer $userToken"
        val dataFeedApiInterface = DataFeedApiService.create()
        val locationBoundJsonObject = JsonObject()

        locationBoundJsonObject.addProperty("latitudeTop", latLngBounds.northeast.latitude)
        locationBoundJsonObject.addProperty("longitudeRight", latLngBounds.northeast.longitude)
        locationBoundJsonObject.addProperty("latitudeBottom", latLngBounds.southwest.latitude)
        locationBoundJsonObject.addProperty("longitudeLeft", latLngBounds.southwest.longitude)

        val requestBody = locationBoundJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())

        try{
            val response = dataFeedApiInterface.getAllRentalVehicles(context.packageName, bearerToken, requestBody).execute()

            val responseBody = response.body()!!.string()
            val jsonObject = JsonParser.parseString(responseBody).asJsonObject

            val status = jsonObject["status"].asInt
            if (status != 200) {
                throw Exception("Failed to get rental vehicles")
            }
            val dataStr = jsonObject["data"].toString()
            val rentalVehicles : ArrayList<RentalVehicle> = Gson().fromJson(dataStr, object : TypeToken<ArrayList<RentalVehicle>>() {}.type)
            val listOfRentalVehicle = ArrayList<RentalVehicle>()
            for (rentalVehicle in rentalVehicles) {
                if (rentalVehicle.status.equals(VehicleStatus.RENTAL.name)) {
                    listOfRentalVehicle.add(rentalVehicle)
                }
            }
            return listOfRentalVehicle
        }catch (exc : Exception){
            throw exc
        }
    }

    fun uploadKycDocs(context: Context,
                      fileType: FileType,
                      dataState: MutableLiveData<DataState<FileType>>) {

        //dataState.postValue(DataState.Loading)

        var bufferKey = KYC_PROFILE_IMG_BUFFER_KEY

        if (fileType == FileType.KYC_DL_SIDE_1) {
            bufferKey = KYC_DL_SIDE_1_IMG_BUFFER_KEY
        }
        if (fileType == FileType.KYC_DL_SIDE_2) {
            bufferKey = KYC_DL_SIDE_2_IMG_BUFFER_KEY
        }

        val fileBuffer = context.getSharedPreferences(IMAGE_PREFS, Context.MODE_PRIVATE)
            .getString(bufferKey, null)

        if(fileBuffer == null) {
            //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_upload_file) + " No image buffer found"))
            return
        }

        val uploadInput = UploadInput.builder().uploadType(fileType).fileData(fileBuffer).build()

        val uploadFileMutation = UploadFileMutation.builder().uploadInputVar(uploadInput).build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_upload_file)))
            return
        }

        apolloClientService.mutate(uploadFileMutation)?.enqueue(object : ApolloCall.Callback<UploadFileMutation.Data>(){

            override fun onResponse(response: Response<UploadFileMutation.Data>) {
                if(!response.errors.isNullOrEmpty()){
                    //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_upload_file)))
                    return
                }
                if(response.data != null && response.data!!.files() != null &&
                    response.data!!.files()?.upload() != null){

                    val upload = response.data!!.files()?.upload()!!

                    when(upload.type()){
                        FileType.KYC_PROFILE -> {
                            context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                                .putString(KYC_PROFILE_UPLOAD_FILE_NAME_KEY,response.data!!.files()!!.upload()!!.name()).apply()
                        }
                        FileType.KYC_DL_SIDE_1 -> {
                            context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                                .putString(KYC_DL_SIDE_1_UPLOAD_FILE_NAME_KEY,response.data!!.files()!!.upload()!!.name()).apply()
                        }
                        FileType.KYC_DL_SIDE_2 -> {
                            context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                                .putString(KYC_DL_SIDE_2_UPLOAD_FILE_NAME_KEY,response.data!!.files()!!.upload()!!.name()).apply()
                        }
                    }
                   // dataState.postValue(DataState.Success(fileType))
                }else{
                    //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_upload_file)))
                }
            }

            override fun onFailure(e: ApolloException) {
                //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_upload_file)))
            }
        })
    }

    fun fetchAllRentalCompanies(context: Context,
                                dataState: MutableLiveData<DataState<ArrayList<RentalVehicleCompany>>>){

        val fetchAllRentalCompaniesQuery = FetchAllRentalCompaniesQuery.builder().build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
            //dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
            return
        }

        apolloClientService.query(fetchAllRentalCompaniesQuery).enqueue(object : ApolloCall.Callback<FetchAllRentalCompaniesQuery.Data>() {

            override fun onResponse(response: Response<FetchAllRentalCompaniesQuery.Data>) {

                if(!response.errors.isNullOrEmpty()){
                   // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_rental_providers_for_share_kyc)))
                    return
                }
                if (response.data != null && response.data!!.company() != null && !response.data!!.company()!!.allRentalCompanies.isNullOrEmpty()) {
                    val allRentalCompaniesList = response.data!!.company()!!.allRentalCompanies!!
                    val uniqueProviders = HashSet<String>()
                    val rentalCompanies = ArrayList<RentalVehicleCompany>()
                    for(companies in allRentalCompaniesList){
                        if(uniqueProviders.add(companies.id())){
                            rentalCompanies.add(RentalVehicleCompany(companies.id(), companies.name(), null))
                        }
                    }
                   // dataState.postValue(DataState.Success(rentalCompanies))
                }else{
                   // dataState.postValue(DataState.Error(context.getString(R.string.no_rental_providers_found)))
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.no_rental_providers_found)))
            }
        })
    }

    suspend fun checkKycWithRentalProviders(context: Context, providerId : String) : Boolean {

        val companyWhereUniqueInput = CompanyWhereUniqueInput
            .builder()
            .id(providerId)
            .build()

        val fetchKYCAssociationForUserQuery = FetchKYCAssociationForUserQuery
            .builder()
            .companyWhereUniqueInput(companyWhereUniqueInput)
            .build()

        val apolloClientService = ApolloClientService.create(context) ?: throw Exception("No user token found")

        try {
            val response =
                apolloClientService.query(fetchKYCAssociationForUserQuery).toDeferred().await()
            if (response.data != null && response.data!!.account() != null && response.data!!.account()!!.kyc != null) {
                if (response.data!!.account()!!.kyc!!.status() == KYCStatus.APPROVED) {
                    return true
                }
            }
            return false
        }catch (exc : Exception){
            return false
        }
    }

    suspend fun shareKycWithRentalProviders(context: Context, providerId : String) : Boolean {

        val apolloClientService = ApolloClientService.create(context) ?: return false
        val companyWhereUniqueInput = CompanyWhereUniqueInput.builder().id(providerId).build()

        val shareKycDocsMutation = ShareKycDocsMutation.builder()
            .shareKycDocsRentalUniqueInput(companyWhereUniqueInput)
            .build()

        try {
            val response = apolloClientService.mutate(shareKycDocsMutation).toDeferred().await()
            if(!response.errors.isNullOrEmpty()){
                return false
            }
            if (response.data != null && response.data!!.account() != null
                && response.data!!.account()!!.shareKYC() != null) {
                val kycStatus = response.data!!.account()!!.shareKYC()!!.status()
                return kycStatus != null
            }else{
                return false
            }
        }catch (exc : Exception){
            return false
        }
    }

    suspend fun fetchAllKycAssociation(context: Context) : ArrayList<KycAssociation> {

        var listOfAssociation = ArrayList<KycAssociation>()
        val apolloClientService = ApolloClientService.create(context)

        val fetchAllKYCAssociationsQuery = FetchAllKYCAssociationsQuery.builder().build()

        if(apolloClientService == null) {
            return listOfAssociation
        }

        try{
            val response = apolloClientService.query(fetchAllKYCAssociationsQuery).toDeferred().await()
            if(!response.errors.isNullOrEmpty()){
                return listOfAssociation
            }
            if (response.data != null && response.data!!.account() != null) {
                val kycAssociationList = ArrayList<KycAssociation>()
                if(response.data!!.account()!!.me() != null
                    && response.data!!.account()!!.me()!!.kyc() != null
                    && response.data!!.account()!!.me()!!.kyc()!!.associations() != null){

                    val associationList = response.data!!.account()!!.me()!!.kyc()!!.associations()

                    for(item in associationList!!){
                        val association = KycAssociation(
                            kycAssociationStatus = item.status(),
                            rentalProviderName = item.company()?.name(),
                            rentalProviderId = item.company()?.id(),
                            documentList = item.documentStatus(),
                            kycAssociationId = item.id(),
                        )
                        kycAssociationList.add(association)
                    }
                    return kycAssociationList
                }
            }
            return listOfAssociation
        }catch (ex : Exception){

        }
        return listOfAssociation
    }

    fun fetchRentalHistoryForUser(context: Context){

        val apolloClientService = ApolloClientService.create(context)

        val leaseWhereInput = LeaseWhereInput.builder()
            .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
            .status(LeaseStatus.ENDED)
            .build()

        if(apolloClientService == null) {
            return
        }

        val fetchLeaseForUserQuery = FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build()

        apolloClientService.query(fetchLeaseForUserQuery)?.enqueue(object : ApolloCall.Callback<FetchLeaseForUserQuery.Data>(){

            override fun onResponse(response: Response<FetchLeaseForUserQuery.Data>) {
                if (response.data != null && response.data!!.lease() != null && response.data!!.lease()!!.leasesForUser != null) {
                    val leaseForUser = response.data!!.lease()!!.leasesForUser
                    val listOfRentalVehicles = ArrayList<RentalVehicle>()
                    if(!leaseForUser.isNullOrEmpty()){
                        for(item in leaseForUser){
                            val rentalVehicle = RentalVehicle()
                            rentalVehicle.vin = item.asset()?.vehicle()?.vin()

                            val model = RentalVehicleModel()
                            model.name = item.asset()?.vehicle()?.model()?.name()
                            rentalVehicle.model = model

                            val company = RentalVehicleCompany()
                            company.name = item.leasor()?.company()?.name()
                            rentalVehicle.company = company

                            if(!item?.contract()?.txInfo()?.invoice()?.passbook()!!.isNullOrEmpty()){
                                rentalVehicle.amount = item.contract()?.txInfo()?.invoice()?.passbook()!![0].amount()!!
                            }

                            rentalVehicle.startTime = item.bookingTime()
                            rentalVehicle.endTime = item.endTime()

                            rentalVehicle.bookingId = item.id()

                            listOfRentalVehicles.add(rentalVehicle)
                        }
                    }
                 //   dataState.postValue(DataState.Success(listOfRentalVehicles))
                }else{
                 //   dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }

            override fun onFailure(e: ApolloException) {
             //   dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun fetchActiveLeaseForUser(context: Context,
                                dataState: MutableLiveData<DataState<List<RentalVehicle>>>){

     //   dataState.postValue(DataState.Loading)

        val apolloClientService = ApolloClientService.create(context)

        val leaseWhereInput = LeaseWhereInput.builder()
            .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
            .status(LeaseStatus.ACTIVE)
            .build()

        if(apolloClientService == null) {
         //   dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }

        val fetchLeaseForUserQuery = FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build()

        apolloClientService.query(fetchLeaseForUserQuery)?.enqueue(object : ApolloCall.Callback<FetchLeaseForUserQuery.Data>(){

            override fun onResponse(response: Response<FetchLeaseForUserQuery.Data>) {
                if (response.data != null && response.data!!.lease() != null && response.data!!.lease()!!.leasesForUser != null) {
                    val leaseForUser = response.data!!.lease()!!.leasesForUser
                    val listOfRentalVehicles = ArrayList<RentalVehicle>()
                    if(!leaseForUser.isNullOrEmpty()){
                        for(item in leaseForUser){
                            val rentalVehicle = RentalVehicle()
                            rentalVehicle.vin = item.asset()?.vehicle()?.vin()

                            val model = RentalVehicleModel()
                            model.name = item.asset()?.vehicle()?.model()?.name()
                            rentalVehicle.model = model

                            val company = RentalVehicleCompany()
                            company.name = item.leasor()?.company()?.name()
                            rentalVehicle.company = company

                            if(!item?.contract()?.txInfo()?.invoice()?.passbook()!!.isNullOrEmpty()){
                                rentalVehicle.amount = item.contract()?.txInfo()?.invoice()?.passbook()!![0].amount()!!
                            }

                            rentalVehicle.startTime = item.bookingTime()
                            rentalVehicle.endTime = item.endTime()

                            rentalVehicle.bookingId = item.id()

                            listOfRentalVehicles.add(rentalVehicle)
                        }
                    }
                  //  dataState.postValue(DataState.Success(listOfRentalVehicles))
                }else{
                 //   dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }

            override fun onFailure(e: ApolloException) {
             //   dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun fetchVehicleDetailsAndPricingInfo(context: Context,vin:String?,dataState : MutableLiveData<DataState<RentalVehicle>>)  {
     //   dataState.postValue(DataState.Loading)
        var inputGetRentalVehicles = InputGetRentalVehicles.builder().package_(context.packageName).vehicle(VehicleWhereInput.builder().vin(vin).build()).build()

        var fetchRentalVehicleDetailsAndPricingInfoQuery = FetchRentalVehicleDetailsAndPricingInfoQuery
            .builder()
            .inputGetRentalVehicles(inputGetRentalVehicles)
            .build()
        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
         //   dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }
        apolloClientService.query(fetchRentalVehicleDetailsAndPricingInfoQuery).enqueue(object : ApolloCall.Callback<FetchRentalVehicleDetailsAndPricingInfoQuery.Data>(){
            override fun onResponse(response: Response<FetchRentalVehicleDetailsAndPricingInfoQuery.Data>) {
                if (response.errors != null && !response.errors!!.isEmpty()) {
                //    dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_vehicle_details)))
                }else if(response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()?.rentalVehicles != null) {
                    var fetchedRentalVehicleDetails: FetchRentalVehicleDetailsAndPricingInfoQuery.GetRentalVehicle
                    if (response.data!!.vehicles()!!.rentalVehicles != null && !response.data!!.vehicles()!!.rentalVehicles!!.isEmpty() && response.data!!.vehicles()!!.rentalVehicles!![0] != null) {
                        fetchedRentalVehicleDetails =
                            response.data!!.vehicles()!!.rentalVehicles!![0]

                        if (fetchedRentalVehicleDetails != null && fetchedRentalVehicleDetails.rentalStatus() == RentalStatus.AVAILABLE) {

                            val rentalVehicle = RentalVehicle()

                            if (fetchedRentalVehicleDetails.isDeliverable != null) {
                                rentalVehicle.isDeliverable= fetchedRentalVehicleDetails.isDeliverable!!
                            }

                            if (fetchedRentalVehicleDetails.vin() != null && !fetchedRentalVehicleDetails.vin().isEmpty()) {
                                rentalVehicle.vin=fetchedRentalVehicleDetails.vin()
                            }

                            if (fetchedRentalVehicleDetails.rentalStatus() != null) {
                                rentalVehicle.rentalStatus=fetchedRentalVehicleDetails.rentalStatus().toString()
                            }

                            if (fetchedRentalVehicleDetails.model() != null) {
                                val model = RentalVehicleModel()
                                if (fetchedRentalVehicleDetails.model()!!.name() != null && !fetchedRentalVehicleDetails.model()!!.name()!!.isEmpty()) {
                                    model.name=fetchedRentalVehicleDetails.model()!!.name()
                                }
                                if (fetchedRentalVehicleDetails.model()!!.config() != null) {
                                    val modelConfig = RentalVehicleModelConfig()
                                    if (fetchedRentalVehicleDetails.model()!!.config()!!.batteryMaxVoltage() != null) {
                                        modelConfig.batteryMaxVoltage= fetchedRentalVehicleDetails.model()!!.config()!!
                                            .batteryMaxVoltage()!!
                                    }
                                    if (fetchedRentalVehicleDetails.model()!!.config()!!.batteryMinVoltage() != null) {
                                        modelConfig.batteryMinVoltage= fetchedRentalVehicleDetails.model()!!.config()!!
                                            .batteryMinVoltage()!!
                                    }
                                    model.config=modelConfig
                                }
                                if (fetchedRentalVehicleDetails.model()!!.company() != null) {
                                    val rentalVehicleCompany = RentalVehicleCompany()
                                    if (fetchedRentalVehicleDetails.model()!!.company()!!.id() != null
                                        && !fetchedRentalVehicleDetails.model()!!.company()!!.id()
                                            .isEmpty()
                                    ) {
                                        rentalVehicleCompany.id= fetchedRentalVehicleDetails.model()!!.company()!!.id()
                                    }
                                    if (fetchedRentalVehicleDetails.model()!!.company()!!
                                            .name() != null
                                        && !fetchedRentalVehicleDetails.model()!!.company()!!.name()
                                            .isEmpty()
                                    ) {
                                        rentalVehicleCompany.name= fetchedRentalVehicleDetails.model()!!.company()!!.name()

                                    }
                                    if (fetchedRentalVehicleDetails.model()!!.company()!!
                                            .phone() != null
                                        && !fetchedRentalVehicleDetails.model()!!.company()!!
                                            .phone()!!.isEmpty()
                                    ) {
                                        rentalVehicleCompany.phone=fetchedRentalVehicleDetails.model()!!.company()!!
                                            .phone()

                                    }
                                    model.company=rentalVehicleCompany
                                }
                                rentalVehicle.model=model
                            }

                            if (fetchedRentalVehicleDetails.company() != null) {
                                val company = RentalVehicleCompany()
                                if (fetchedRentalVehicleDetails.company()!!
                                        .id() != null && !fetchedRentalVehicleDetails.company()!!
                                        .id().isEmpty()
                                ) {
                                    company.id=fetchedRentalVehicleDetails.company()!!.id()
                                }
                                if (fetchedRentalVehicleDetails.company()!!
                                        .name() != null && !fetchedRentalVehicleDetails.company()!!
                                        .name().isEmpty()
                                ) {
                                    company.name=fetchedRentalVehicleDetails.company()!!.name()
                                }
                                if (fetchedRentalVehicleDetails.company()!!
                                        .phone() != null && !fetchedRentalVehicleDetails.company()!!
                                        .phone()!!.isEmpty()
                                ) {
                                    company.phone= fetchedRentalVehicleDetails.company()!!.phone()
                                }
                                rentalVehicle.company=company
                            }

                            if (fetchedRentalVehicleDetails.oem() != null) {
                                val oem = RentalVehicleCompany()
                                if (fetchedRentalVehicleDetails.oem()!!
                                        .id() != null && !fetchedRentalVehicleDetails.oem()!!.id()
                                        .isEmpty()
                                ) {
                                    oem.id=fetchedRentalVehicleDetails.oem()!!.id()
                                }
                                if (fetchedRentalVehicleDetails.oem()!!
                                        .name() != null && !fetchedRentalVehicleDetails.oem()!!
                                        .name().isEmpty()
                                ) {
                                    oem.name=fetchedRentalVehicleDetails.oem()!!.name()
                                }
                                if (fetchedRentalVehicleDetails.oem()!!
                                        .phone() != null && !fetchedRentalVehicleDetails.oem()!!
                                        .phone()!!
                                        .isEmpty()
                                ) {
                                    oem.phone=fetchedRentalVehicleDetails.oem()!!.phone()
                                }
                                rentalVehicle.oem=oem
                            }

                            if (fetchedRentalVehicleDetails.distributor() != null) {
                                val distributor = RentalVehicleCompany()
                                if (fetchedRentalVehicleDetails.distributor()!!
                                        .id() != null && !fetchedRentalVehicleDetails.distributor()!!
                                        .id().isEmpty()) {
                                    distributor.id=fetchedRentalVehicleDetails.distributor()!!.id()
                                }
                                if (fetchedRentalVehicleDetails.distributor()!!
                                        .name() != null && !fetchedRentalVehicleDetails.distributor()!!
                                        .name().isEmpty()) {
                                    distributor.name=fetchedRentalVehicleDetails.distributor()!!.name()
                                }
                                if (fetchedRentalVehicleDetails.distributor()!!
                                        .phone() != null && !fetchedRentalVehicleDetails.distributor()!!
                                        .phone()!!.isEmpty()
                                ) {
                                    distributor.phone=fetchedRentalVehicleDetails.distributor()!!.phone()
                                }
                                rentalVehicle.distributor=distributor
                            }

                            if (fetchedRentalVehicleDetails.txInfo() != null && fetchedRentalVehicleDetails.txInfo()!!
                                    .pricing() != null) {
                                val pricingList = fetchedRentalVehicleDetails.txInfo()!!.pricing()
                                if (pricingList != null && !pricingList.isEmpty()) {
                                    for (index in pricingList.indices) {
                                        val rentalPricingInfo = RentalPricingInfo()
                                        if (pricingList[index].name() != null && !pricingList[index].name()!!.isEmpty()) {
                                            rentalPricingInfo.pricingName =
                                                pricingList[index].name()
                                        }
                                        if (pricingList[index].type() != null) {
                                            rentalPricingInfo.paymentType =
                                                pricingList[index].type()
                                        }
                                        if (pricingList[index].baseAmount() != null) {
                                            rentalPricingInfo.baseAmount =
                                                pricingList[index].baseAmount()!!.toFloat()
                                        }
                                        if (pricingList[index].costPerUnit() != null) {
                                            rentalPricingInfo.costPerUnit =
                                                pricingList[index].costPerUnit()!!.toFloat()
                                        }
                                        if (pricingList[index].unit() != null) {
                                            rentalPricingInfo.unitType = pricingList[index].unit()
                                        }
                                        if (pricingList[index].paymentMeta() != null && pricingList[index].paymentMeta()!!
                                                .address() != null && !pricingList[index].paymentMeta()!!
                                                .address()!!.isEmpty()
                                        ) {
                                            rentalPricingInfo.providerUpiAddress =
                                                pricingList[0].paymentMeta()!!.address()

                                        }
                                    }
                                    rentalVehicle.rentalPricingInfoList = pricingList
                                }
                            }
                            fetchVehicleLiveLogsForBatteryDetails(context, rentalVehicle, dataState)
                        }
                    }
                }
            }

            override fun onFailure(e: ApolloException) {
             //   dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    private fun fetchVehicleLiveLogsForBatteryDetails(context: Context,
                                                      rentalVehicle: RentalVehicle,
                                                      dataState: MutableLiveData<DataState<RentalVehicle>>) {
      //  dataState.postValue(DataState.Loading)

        val userToken = PrefUtils.getInstance(context).authTokenFromPrefs
        val bearerToken = "Bearer $userToken"
        val dataFeedApiInterface = DataFeedApiService.create()

        if(rentalVehicle.vin.isNull()) {
          //  dataState.postValue(DataState.Error("Vin is null"))
            return
        }

        dataFeedApiInterface.getVehicleLocationSnapshot(rentalVehicle.vin, bearerToken).enqueue(
            object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    try {
                        val responseBody = response.body()!!.string()
                        val jsonObject = JsonParser.parseString(responseBody).asJsonObject
                        val status = jsonObject["status"].asInt
                        if (status != 200) {
                           // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                            return
                        }
                        val dataStr = jsonObject["data"].toString()
                        val vehicleSnapshot = Gson().fromJson(dataStr, DataFeedVehicleSnapshot::class.java)
                        if (vehicleSnapshot?.battery != null) {
                            var batteryVoltageAdc = vehicleSnapshot.battery!!.batteryVoltageAdc

                            val maxVoltage = rentalVehicle.model?.config?.batteryMaxVoltage
                            val minVoltage = rentalVehicle.model?.config?.batteryMinVoltage

                            var socValue: Long = 0
                            var batteryPercentage = 0.0

                            if (minVoltage != null && maxVoltage != null &&
                                batteryVoltageAdc != 0.0 && maxVoltage != 0.0 && minVoltage != 0.0) {
                                batteryPercentage = (batteryVoltageAdc - minVoltage) / (maxVoltage - minVoltage) * 100
                                socValue = round(floor(batteryPercentage)).toLong()
                            }

                            if (batteryVoltageAdc <= 0) {
                                batteryVoltageAdc = 0.0
                            } else if (batteryVoltageAdc > 100) {
                                batteryVoltageAdc = 100.0
                            }

                            if (socValue <= 0) {
                                socValue = 0
                            } else if (socValue > 100) {
                                socValue = 100
                            }
                            rentalVehicle.batteryVoltageAdc = round(floor(batteryVoltageAdc))
                            rentalVehicle.soc = socValue

                            checkKycStatusWithRentalProvider(context, getRentalProviderId(rentalVehicle))
                        }
                    } catch (e: Exception) {
                      //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            })
    }

    private fun getRentalProviderId(rentalVehicle: RentalVehicle): String {
        var rentalProviderId : String = ""
        if (rentalVehicle.company != null && rentalVehicle.company!!.id != null && !rentalVehicle.company!!.id?.isEmpty()!!) {
            rentalProviderId = rentalVehicle.company!!.id!!
        } else if (rentalVehicle.oem != null && rentalVehicle.oem!!.id != null && !rentalVehicle.oem!!.id?.isEmpty()!!) {
            rentalProviderId = rentalVehicle.oem!!.id.toString()
        } else if (rentalVehicle.distributor != null && rentalVehicle.distributor!!.id != null && !rentalVehicle.distributor!!.id?.isEmpty()!!) {
            rentalProviderId = rentalVehicle.distributor!!.id.toString()
        }
        return rentalProviderId
    }

    fun fetchDevice(context: Context,
                    deviceId:String,
                    dataState: MutableLiveData<DataState<RentalVehicle>>) : String? {

        var vin : String? = null
       // dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context)

        val deviceWhereUniqueInput = DeviceWhereUniqueInput
            .builder()
            .deviceId(deviceId)
            .build()

        val fetchDeviceQuery = FetchDeviceQuery
            .builder()
            .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
            .build()

        if(apolloClientService == null) {
           // dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return vin
        }

        apolloClientService.query(fetchDeviceQuery).enqueue(object : ApolloCall.Callback<FetchDeviceQuery.Data>() {
            override fun onResponse(response: Response<FetchDeviceQuery.Data>) {
                if (!response.errors.isNullOrEmpty()) {
                  //  dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_device_details)))
                    return
                }
                if (response.data != null && response.data!!.device() != null && response.data!!.device()!!.get() != null) {
                    val fetchedDevice = response.data!!.device()!!.get()
                    if (fetchedDevice?.vehicle() != null && fetchedDevice.vehicle()!!.status() == VehicleStatus.RENTAL) {
                        if (fetchedDevice.vehicle()!!.rentalStatus() != null
                            && fetchedDevice.vehicle()!!.rentalStatus() == RentalStatus.AVAILABLE) {
                            vin = fetchedDevice.vehicle()!!.vin()

                            checkForActiveBooking(context, fetchedDevice.vehicle()!!.vin(), dataState)

                        }else if(fetchedDevice.vehicle()!!.rentalStatus() != null
                            && fetchedDevice.vehicle()!!.rentalStatus() == RentalStatus.BOOKED){
                          //  dataState.postValue(DataState.Error("Vehicle already booked"))
                        }else{
                         //   dataState.postValue(DataState.Error("Vehicle not available for booking"))
                        }
                    }else{
                      //  dataState.postValue(DataState.Error("Vehicle not available for lease"))
                    }
                }else{
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
        return vin
    }

    fun checkForActiveBooking(context: Context,
                              vin : String,
                              dataState: MutableLiveData<DataState<RentalVehicle>>){
       // dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context)
        val leaseWhereInput = LeaseWhereInput
            .builder()
            .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
            .status(LeaseStatus.ACTIVE)
            .build()

        val fetchLeaseForUserQuery =
            FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build()

        if(apolloClientService == null) {
          //  dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }

        apolloClientService.query(fetchLeaseForUserQuery).enqueue(object : ApolloCall.Callback<FetchLeaseForUserQuery.Data>(){

            override fun onResponse(response: Response<FetchLeaseForUserQuery.Data>) {
                if (response.data != null && response.data!!.lease() != null && response.data!!.lease()!!.leasesForUser != null) {
                    val fetchedLeaseForUserList = response.data!!.lease()!!.leasesForUser
                    if (!fetchedLeaseForUserList.isNullOrEmpty()) {
                      //  dataState.postValue(DataState.Error(context.getString(R.string.end_current_booking)))
                    }else{
                        fetchVehicleDetailsAndPricingInfo(context, vin, dataState)
                    }
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    suspend fun checkForOnGoingRentals(context: Context,
                                       dataState: MutableLiveData<DataState<Boolean>>){

      //  dataState.postValue(DataState.Loading)

        val apolloClientService = ApolloClientService.create(context)
        val leaseWhereInput = LeaseWhereInput
            .builder()
            .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
            .status(LeaseStatus.ACTIVE)
            .build()

        val fetchLeaseForUserQuery =
            FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build()

        if(apolloClientService == null) {
          //  dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }
        try{
            val response = apolloClientService.query(fetchLeaseForUserQuery).toDeferred().await()
            if (response.data != null && response.data!!.lease() != null && response.data!!.lease()!!.leasesForUser != null) {
                val activeUserRentals = response.data!!.lease()!!.leasesForUser
                if (activeUserRentals.isNullOrEmpty()) {
                    PrefUtils.getInstance(context).clearOnGoingRentalPrefs()
                   // dataState.postValue(DataState.Error("No active rentals"))
                }else{
                   // dataState.postValue(DataState.Success(true))
                }
            }
        }catch (e : ApolloException){
           // dataState.postValue(DataState.Error("No active rentals"))
        }
    }


    fun generateLeaseWithPaymentPendingStatus(context: Context,
                                              dataState: MutableLiveData<DataState<Map<String, String>>>,
                                              endTime : String,
                                              rentalUnitType : UnitType,
                                              remarks : String,
                                              amountPayable : Double,
                                              rentalVehicle : RentalVehicle
    ){

      //  dataState.postValue(DataState.Loading)

        val apolloClientService = ApolloClientService.create(context)

        var priceInfoCreateOneInput: PriceInfoCreateOneInput? = null
        if (!rentalVehicle.rentalPricingInfoList.isNullOrEmpty()) {
            for(pricing in rentalVehicle.rentalPricingInfoList!!){
                if (rentalUnitType == pricing.unit()) {
                    priceInfoCreateOneInput = PriceInfoCreateOneInput.builder()
                        .connect(PriceInfoWhereUniqueInput.builder().id(pricing.id()).build())
                        .build()
                    break
                }
            }
        } else {
         //   dataState.postValue(DataState.Error(context.getString(R.string.unable_to_book_vehicle)))
            return
        }

        var stage = Stage.TEST
        if (context.getString(R.string.cf_stage) == "PROD") {
            stage = Stage.PROD
        }

        var leaseStatus = LeaseStatus.PENDING_PAYMENT
        if (amountPayable == 0.0) {
            leaseStatus = LeaseStatus.ACTIVE
        }
        val inputRegisterC2ULease = InputRegisterC2ULease
            .builder()
            .status(leaseStatus)
            .endTime(endTime)
            .vehicle(VehicleWhereUniqueInput.builder().vin(rentalVehicle.vin).build())
            .amount(amountPayable)
            .type(LeaseType.SHORT_TERM)
            .remarks(remarks)
            .priceInfo(priceInfoCreateOneInput)
            .stage(stage)
            .build()

        val createNewLeaseMutationWithCashfreeMutation = CreateNewLeaseMutationWithCashfreeMutation
            .builder()
            .leaseDataInput(inputRegisterC2ULease)
            .build()

        if(apolloClientService == null) {
          //  dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }

        apolloClientService.mutate(createNewLeaseMutationWithCashfreeMutation).enqueue(
            object :ApolloCall.Callback<CreateNewLeaseMutationWithCashfreeMutation.Data>() {

                override fun onResponse(response: Response<CreateNewLeaseMutationWithCashfreeMutation.Data>) {
                    if (!response.errors.isNullOrEmpty()) {
                       // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    } else if (response.data != null && response.data!!.lease() != null
                        && response.data!!.lease()!!.newRegisterC2ULease() != null
                        && response.data!!.lease()!!.newRegisterC2ULease()!!.status() != null){
                        val cfToken = response.data!!.lease()!!.newRegisterC2ULease()!!.cftoken()
                        val leaseId = response.data!!.lease()!!.newRegisterC2ULease()!!.id()
                        if(cfToken.isNullOrEmpty() || leaseId.isNullOrEmpty()){
                           // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                            return
                        }
                        val cashFreeParams = HashMap<String, String>()
                        cashFreeParams["leaseId"] = leaseId
                        cashFreeParams["cfToken"] = cfToken

                        if (amountPayable == 0.0) {
                          //  dataState.postValue(DataState.Success(emptyMap()))
                            return
                        }
                        if (response.data!!.lease()!!.newRegisterC2ULease()!!.status() == LeaseStatus.PENDING_PAYMENT) {
                            val vendorSplit = response.data!!.lease()!!.newRegisterC2ULease()!!.vendorSplit().toString()
                            cashFreeParams["vendorSplit"] = vendorSplit
                           // dataState.postValue(DataState.Success(cashFreeParams))
                        }
                    }else{
                      //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    }
                }

                override fun onFailure(e: ApolloException) {
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            })
    }

    fun updateLease(context: Context,
                    dataState: MutableLiveData<DataState<String>>,
                    amountPayable : Double,
                    leaseId : String,
                    leaseStatus : LeaseStatus,
                    remarks : String){

       // dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context)

        val inputRegisterC2ULease = InputRegisterC2ULease.builder().status(leaseStatus).amount(amountPayable)
            .remarks(remarks).build()
        val leaseWhereUniqueInput = LeaseWhereUniqueInput.builder().id(leaseId).build()

        val updateLeaseMutation = UpdateLeaseMutation.builder()
            .inputRegisterC2ULeaseVar(inputRegisterC2ULease)
            .leaseWhereUniqueInputVar(leaseWhereUniqueInput)
            .build()

        if(apolloClientService == null) {
         //   dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }
        apolloClientService.mutate(updateLeaseMutation).enqueue(object :ApolloCall.Callback<UpdateLeaseMutation.Data>() {
            override fun onResponse(response: Response<UpdateLeaseMutation.Data>) {
                if (!response.errors.isNullOrEmpty()) {
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }else if(response.data != null && response.data!!.lease() != null && response.data!!.lease()!!
                        .updateLease() != null && response.data!!.lease()!!.updateLease()!!
                        .status() == LeaseStatus.ACTIVE){
                  //  dataState.postValue(DataState.Success(context.getString(R.string.success)))
                }else{
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun setNewVehiclePin(context : Context, phone : String,
                         vin : String, pin : String, dataState: MutableLiveData<DataState<String>>){

        val apolloClientService = ApolloClientService.create(context) ?: return

        val inputSetPin = InputSetPin.builder().phone(phone).vin(vin).pin(pin).build()

        val setPinMutation = SetPinMutation.builder().inputSetPin(inputSetPin).build()

        apolloClientService.mutate(setPinMutation)
            .enqueue(object : ApolloCall.Callback<SetPinMutation.Data>(){

                override fun onResponse(response: Response<SetPinMutation.Data>) {

                    if(!response.errors.isNullOrEmpty()){
                        EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_DISCONNECT))
                     //   dataState.postValue(DataState.Error(context.getString(R.string.unable_to_set_pin)))
                        PrefUtils.getInstance(context).clearAllPrefs()
                        return
                    }
                    if (response.data != null && response.data!!.device() != null
                        && response.data!!.device()!!.setPin() != null
                        && response.data!!.device()!!.setPin()!!.device() != null) {

                        val cloudDevice = response.data!!.device()!!.setPin()!!.device()
                        if (cloudDevice!!.pin() != null) {
                            context.getSharedPreferences(DEVICE_PREFS,Context.MODE_PRIVATE)
                                .edit().putString(DEVICE_PIN_KEY, cloudDevice.pin()).apply()
                        } else {
                            context.getSharedPreferences(DEVICE_PREFS,Context.MODE_PRIVATE)
                                .edit().putString(DEVICE_PIN_KEY, null).apply()
                        }

                        NordicBleService.setDeviceName(null);

                        MetadataDecryptionHelper.getInstance(context).clearLocalCache()

                        //clear previous metadata
                        context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply()
                        context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply()
                        context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply()
                        context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply()
                        context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply()

                        refreshControllerMetadata(context, NordicBleService.getDeviceFirmwareVersion())

                        //request sync device
                        EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_DEVICE_SYNC))
                      //  dataState.postValue(DataState.Success(context.getString(R.string.vehicle_pin_changed_successfully)))
                    }else{
                        EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_DISCONNECT))
                      //  dataState.postValue(DataState.Error(context.getString(R.string.unable_to_change_vehicle_pin)))
                    }
                }

                override fun onFailure(e: ApolloException) {
                    EventBus.getDefault().post(EventBusMessage(BT_EVENT_REQUEST_DISCONNECT))
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    PrefUtils.getInstance(context).clearAllPrefs()
                }
            })
    }

    fun refreshControllerMetadata(context: Context, deviceFirmwareVersion: String?) {
        val noPatchVersion = VersionUtils.removePatchVersion(deviceFirmwareVersion)

        if(noPatchVersion.isNullOrEmpty()) return

        downloadAndStoreControllerMetadata(context, TAG_READ, noPatchVersion)
        downloadAndStoreControllerMetadata(context, TAG_WRITE, noPatchVersion)
        downloadAndStoreControllerMetadata(context, TAG_LOG, noPatchVersion)
        downloadAndStoreControllerMetadata(context, TAG_VEHICLE_CONTROL, noPatchVersion)

        if (!noPatchVersion.isNullOrEmpty() && Version(noPatchVersion) == Version(VER_1_0)) {
            return
        }
        downloadAndStoreControllerMetadata(context, TAG_BATTERY_ADC_LOG, noPatchVersion)
    }

    fun downloadAndStoreControllerMetadata(context: Context, tag : String, version : String){
        if (tag.isEmpty() || version.isEmpty()) {
            return
        }
        val apolloClientService = ApolloClientService.create(context) ?: return
        val cloudDevice = PrefUtils.getInstance(context).deviceObjectFromPrefs ?: return

        val downloadControllerMetadataQuery = DownloadControllerMetadataQuery.builder()
            .controllerInputVar(ControllerInput.builder().version(version).tag(tag).clientId(cloudDevice.id!!).build())
            .build()

        if (tag == TAG_READ) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_LOG) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_WRITE) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_VEHICLE_CONTROL) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        } else if (tag == TAG_BATTERY_ADC_LOG) {
            NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, true)
        }

        apolloClientService.query(downloadControllerMetadataQuery)?.enqueue(object : ApolloCall.Callback<DownloadControllerMetadataQuery.Data>(){
            override fun onResponse(response: Response<DownloadControllerMetadataQuery.Data>) {

                val controllerMetadataJSON = response.data!!.metadata()!!.controller()

                if (tag == TAG_READ) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_LOG) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_WRITE) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).edit().putString(
                        BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_VEHICLE_CONTROL) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context)
                        .setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                } else if (tag == TAG_BATTERY_ADC_LOG) {
                    context.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, controllerMetadataJSON).apply()
                    NetworkUtils.getInstance(context)
                        .setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                }
                EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_CONTROLLER_METADATA_FILE_DOWNLOADED + GEN_DELIMITER + tag))
            }

            override fun onFailure(e: ApolloException) {
                if (tag == TAG_READ) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_LOG) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_WRITE) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false)
                } else if (tag == TAG_VEHICLE_CONTROL) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                } else if (tag == TAG_BATTERY_ADC_LOG) {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY,false)
                }
            }
        })
    }

    fun refreshBatteryRegressionData(context: Context) {
        val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs ?: return
        if (vehicle.vin.isNullOrEmpty()) {
            return
        }

        val fetchBatteryRegressionDataByVinQuery = FetchBatteryRegressionDataByVinQuery
            .builder().vehicleWhereUniqueInput(VehicleWhereUniqueInput.builder().vin(vehicle.vin).build())
            .build()

        val apolloClientService = ApolloClientService.create(context)

        apolloClientService?.query(fetchBatteryRegressionDataByVinQuery)?.enqueue(object : ApolloCall.Callback<FetchBatteryRegressionDataByVinQuery.Data>(){

            override fun onResponse(response: Response<FetchBatteryRegressionDataByVinQuery.Data>) {

                if (response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()!!.batteryRegressionData() != null) {

                    val valueStr = response.data!!.vehicles()!!.batteryRegressionData()

                    val valueJsonObject = JsonParser.parseString(valueStr).asJsonObject
                    val dataJsonArray = valueJsonObject.getAsJsonArray("data")

                    val batteryRegressionData = BatteryRegressionData()
                    val regressionDataPoints = ArrayList<BatteryRegressionDataPoint>()
                    batteryRegressionData.version = (valueJsonObject["version"].asInt)

                    for (dataJsonElement in dataJsonArray) {
                        val batteryRegressionDataPoint = BatteryRegressionDataPoint()
                        batteryRegressionDataPoint.voltage = (dataJsonElement.asJsonObject["voltage"].asDouble)
                        batteryRegressionDataPoint.soc = (dataJsonElement.asJsonObject["soc"].asDouble)
                        batteryRegressionDataPoint.dte = (dataJsonElement.asJsonObject["dte"].asDouble)
                        regressionDataPoints.add(batteryRegressionDataPoint)
                    }

                    batteryRegressionData.dataPoints = (regressionDataPoints)
                    context.getSharedPreferences(BATTERY_PREFS, Context.MODE_PRIVATE)
                        .edit().putString(BATTERY_REGRESSION_DATA_KEY, Gson().toJson(batteryRegressionData)).apply()

                    EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_BATTERY_REGRESSION_DATA_REFRESHED))
                }
            }

            override fun onFailure(e: ApolloException) {

            }
        })
    }

    fun markOdometerResetFlag(context: Context, resetOdometerSuccessful : Boolean) {
        val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs ?: return
        if(vehicle.vin.isNullOrEmpty()) return

        val apolloClientService = ApolloClientService.create(context)

        val vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vehicle.vin).build()

        val updateVehicleInputVar = UpdateVehicleInput.builder()
            .device(DeviceUpdateInput.builder().odoResetRequired(resetOdometerSuccessful).build())
            .build()

        val setOdoResetFlagMutation = SetOdoResetFlagMutation.builder().inputResetOdoFlagVar(updateVehicleInputVar)
            .vehicleWhereUniqueInputVar(vehicleWhereUniqueInput).build()

        apolloClientService?.mutate(setOdoResetFlagMutation)?.enqueue(object :ApolloCall.Callback<SetOdoResetFlagMutation.Data>() {

            override fun onResponse(response: Response<SetOdoResetFlagMutation.Data>) {

                val odoResetFlagMutationResponse = response.data!!.vehicles()!!.update()

                PrefUtils.getInstance(context).saveDeviceVariablesInPrefs(odoResetFlagMutationResponse)
            }

            override fun onFailure(e: ApolloException) {

            }
        })
    }

    fun sendEmergencySMS(context: Context, message : String) {
        NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, true)

        val sendEmergencySMSMutation = SendEmergencySMSMutation.builder()
            .inputSMSVar(InputSMS.builder().message(message).build())
            .build()

        val apolloClientService = ApolloClientService.create(context) ?: return

        apolloClientService.mutate(sendEmergencySMSMutation)?.enqueue(object : ApolloCall.Callback<SendEmergencySMSMutation.Data>(){

            override fun onResponse(response: Response<SendEmergencySMSMutation.Data>) {
                NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, false)
                if (response.errors == null || response.errors!!.isEmpty()) {
                    //clear crash timestamp
                    context.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE).edit()
                        .putLong(CRASH_TIMESTAMP_KEY, 0).apply()

                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, true)
                    context.getSharedPreferences(NETWORKING_PREFS, Context.MODE_PRIVATE)
                        .edit().putLong(SMS_SENT_TIMESTAMP_KEY, System.currentTimeMillis()                        ).apply()
                } else {
                    NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, false)
                }
            }

            override fun onFailure(e: ApolloException) {
                NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, false)
                NetworkUtils.getInstance(context).setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, false)
            }
        })
    }

    fun refreshVehicleLastKnownLocation(context: Context , dataState: MutableLiveData<DataState<LatLng>>?) {

       // dataState?.postValue(DataState.Loading)

        val userToken = PrefUtils.getInstance(context).authTokenFromPrefs

        val bearerToken = "Bearer $userToken"

        val dataFeedApiInterface = DataFeedApiService.create()

        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context)

        if(vehicle == null || vehicle.vin.isNullOrEmpty()) {
           // dataState?.postValue(DataState.Error("Vin is null"))
            return
        }

        dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.vin, bearerToken).enqueue(
            object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    try {
                        val responseBody = response.body()!!.string()
                        val jsonObject = JsonParser.parseString(responseBody).asJsonObject
                        val status = jsonObject["status"].asInt
                        if (status != 200) {
                          //  dataState?.postValue(DataState.Error(context.getString(R.string.server_error)))
                            return
                        }
                        val dataStr = jsonObject["data"].toString()
                        val vehicleSnapshot = Gson().fromJson(dataStr, DataFeedVehicleSnapshot::class.java)
                        if (vehicleSnapshot.location != null && vehicleSnapshot.location!!.latitude != null
                            && vehicleSnapshot.location!!.longitude != null) {

                            val latStr = vehicleSnapshot.location!!.latitude.toString()
                            val lngStr = vehicleSnapshot.location!!.longitude.toString()

                            context.getSharedPreferences(VEHICLE_PREFS,Context.MODE_PRIVATE).edit().putString(
                                VEHICLE_LAST_KNOWN_LATITUDE, latStr).apply()
                            context.getSharedPreferences(VEHICLE_PREFS,Context.MODE_PRIVATE).edit().putString(
                                VEHICLE_LAST_KNOWN_LONGITUDE, lngStr).apply()

                         //   dataState?.postValue(DataState.Success(LatLng(latStr.toDouble(), lngStr.toDouble())))

                        }
                    } catch (e: Exception) {
                      //  dataState?.postValue(DataState.Error(context.getString(R.string.server_error)))
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                 //   dataState?.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            })
    }

    fun downloadVehicleModelImageIfRequired(context: Context, modelId: String?) {
        if (modelId.isNullOrEmpty()) {
            return
        }
        try {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE)
            val filePath = sharedPreferences.getString(VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, null)
            if (filePath == null) {
                downloadVehicleModelImage(context, modelId)
            } else {
                val imageFile = File(filePath)
                if (imageFile.exists() && imageFile.name.split("[.]".toRegex()).toTypedArray()[0] == modelId) {
                    return
                }
                downloadVehicleModelImage(context, modelId)
            }
        } catch (e: Exception) {

        }
    }

    private fun downloadVehicleModelImage(context: Context, modelId : String){

        val modelWhereUniqueInput = ModelWhereUniqueInput.builder().id(modelId).build()
        val fetchModelImageQuery = FetchModelImageQuery.builder().modelWhereUniqueInput(modelWhereUniqueInput).build()

        val apolloClientService = ApolloClientService.create(context) ?: return

        apolloClientService.query(fetchModelImageQuery).enqueue(object : ApolloCall.Callback<FetchModelImageQuery.Data?>() {
            override fun onResponse(response: Response<FetchModelImageQuery.Data?>) {
                if (response.errors != null && response.errors.isNullOrEmpty()) {
                    return
                }
                if (response.data != null && response.data!!.models() != null && response.data!!.models()!!
                        .get() != null && response.data!!.models()!!.get()!!.image() != null) {
                    val fileDataUri = response.data!!.models()!!.get()!!.image()
                    if (!fileDataUri.isNullOrEmpty()) {
                        try {
                            //extract file extension from data uri
                            var processedString = fileDataUri.split(";".toRegex()).toTypedArray()[0]
                            processedString = processedString.split(":".toRegex()).toTypedArray()[1]
                            processedString = processedString.split("/".toRegex()).toTypedArray()[1]
                            processedString = ".$processedString"
                            val imageFile = File(context.getExternalFilesDir(VEHICLE_MODEL_IMAGE_DIR_PATH), modelId + processedString)
                            val byteArray = Base64.decode(fileDataUri.substring(fileDataUri.indexOf(",")), Base64.DEFAULT)
                            NetworkUtils.getInstance(context).writeByteArrayAsFile(byteArray, imageFile)
                            EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_VEHICLE_MODEL_IMAGE_DOWNLOADED))
                            context.getSharedPreferences(VEHICLE_PREFS,Context.MODE_PRIVATE)
                                .edit().putString(VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, imageFile.absolutePath).apply()

                        } catch (e: Exception) {

                        }
                    } else {
                        context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE)                            .edit().putString(
                            VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, null).apply()
                    }
                }
            }

            override fun onFailure(e: ApolloException) {}
        })

    }

    private fun downloadProfilePic(context: Context) {

        val fileName = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).getString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, null)
        if(fileName.isNullOrEmpty()) return

        val downloadInput = DownloadInput.builder().name(fileName).build()
        val downloadFileQuery = DownloadFileQuery.builder().downloadInputVar(downloadInput).build()

        val apolloClientService = ApolloClientService.create(context) ?: return

        apolloClientService.query(downloadFileQuery).enqueue(object : ApolloCall.Callback<DownloadFileQuery.Data>() {
            override fun onResponse(response: Response<DownloadFileQuery.Data>) {

                val fileAsBase64Str = response.data!!.files()!!.download()!!.imageBuffer()
                val decodedFileByteArray = Base64.decode(fileAsBase64Str, Base64.NO_WRAP)

                val profilePicFile = File(context.getExternalFilesDir(PIC_DIR_PATH),"$PROFILE_PIC_PREFIX$fileName.jpg")
                NetworkUtils.getInstance(context).writeByteArrayAsFile(decodedFileByteArray, profilePicFile)

                //clear previous files
                val tempDir = profilePicFile.parentFile
                val tempFiles = tempDir.listFiles()
                for (fileIndex in tempFiles.indices) {
                    if (tempFiles[fileIndex].name.contains(PROFILE_PIC_TEMP_PREFIX) && tempFiles[fileIndex].name != profilePicFile.name) {
                        tempFiles[fileIndex].delete()
                    }
                }

                //write this data to prefs
                val sharedPreferences: SharedPreferences = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                if (profilePicFile.exists()) {
                    editor.putString(USER_PROFILE_LOCAL_FILE_PATH_KEY,profilePicFile.absolutePath)
                    editor.apply()
                }
                //send out a broadcast for others to listen
                EventBus.getDefault().post(EventBusMessage(NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED))
            }

            override fun onFailure(e: ApolloException) {

            }
        })
    }

    fun fetchVehicleLogsWithoutCount(vin : String,
                                     tripStartTime : String,
                                     tripEndTime : String,
                                     tripId : String,
                                     context: Context,
                                     dataState: MutableLiveData<DataState<ArrayList<DataFeedVehicleLiveLog>>>) {

        val userToken: String = PrefUtils.getInstance(context).authTokenFromPrefs ?: return

        val bearerToken = "Bearer $userToken"

        val dataFeedApiInterface = DataFeedApiService.create()

        val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs ?: return
        val device = PrefUtils.getInstance(context).deviceObjectFromPrefs ?: return

        dataFeedApiInterface.getVehicleLiveLogWithoutCount(vin, tripStartTime, tripEndTime, tripId, true, bearerToken).enqueue(
            object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    try {
                        val responseBody = response.body()!!.string()

                        val jsonObject = JsonParser.parseString(responseBody).asJsonObject

                        val status = jsonObject["status"].asInt
                        if (status != 200) {
                            //dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                            return
                        }
                        val dataStr = jsonObject["data"].toString()

                        val vehicleTripLogList = Gson().fromJson<ArrayList<DataFeedVehicleLiveLog>>(dataStr,
                            object : TypeToken<ArrayList<DataFeedVehicleLiveLog>>() {}.type)

                        if(vehicleTripLogList.isNullOrEmpty()){
                           // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                            return
                        }
                        val processedList = ArrayList<DataFeedVehicleLiveLog>()
                        for(item in vehicleTripLogList){
                            var speed = 0.0
                            if(vehicle.model?.protocol == ModelProtocol.PNP){
                                speed = item.gpsSpeed
                            }else{
                                speed = floor(item.wheelRpm / device.vehicle?.model?.speedDivisor!!)
                            }
                            if (item.latitude == 0.0 && item.longitude == 0.0) {
                                continue
                            }
                            if (speed < 5) {
                                continue
                            }
                            processedList.add(item)
                        }

                       // dataState.postValue(DataState.Success(processedList))

                    } catch (e: Exception) {
                      //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                 //   dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            })
    }

    fun uploadVehicleControlSettings(context: Context,
                                     speedValue : Int,
                                     followMeHeadlampTime : Int,
                                     dataState: MutableLiveData<DataState<String>>){

      //  dataState.postValue(DataState.Loading)

        val vehicleConfigCreateInput = VehicleConfigCreateInput
            .builder()
            .childSpeedLock(speedValue)
            .followMeHomeHeadLamp(followMeHeadlampTime)
            .build()

        val vehicle = LocalPrefs.getVehicleObjectFromPrefs(context)

        if(vehicle == null || vehicle.vin.isNullOrEmpty()) {
           // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            return
        }

        val vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vehicle.vin).build()

        val setVehicleConfigMutation = SetVehicleConfigMutation.builder()
            .data(vehicleConfigCreateInput)
            .where(vehicleWhereUniqueInput)
            .build()

        val apolloClientService = ApolloClientService.create(context) ?: return

        apolloClientService.mutate(setVehicleConfigMutation)?.enqueue(object : ApolloCall.Callback<SetVehicleConfigMutation.Data>(){

            override fun onResponse(response: Response<SetVehicleConfigMutation.Data>) {
                if (!response.errors.isNullOrEmpty()) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    return
                }
                if (response.data?.vehicles()?.setConfig() != null) {
                    val config = response.data?.vehicles()?.setConfig()
                    PrefUtils.getInstance(context).saveDeviceVariablesInPrefs(config)
                   // dataState.postValue(DataState.Success(context.getString(R.string.success)))
                }else{
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun makeEndActiveBookingCall(context: Context,
                                 rentalVehicle: RentalVehicle,
                                 dataState: MutableLiveData<DataState<String>>){

       // dataState.postValue(DataState.Loading)

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
         //   dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }

        val leaseWhereUniqueInput = LeaseWhereUniqueInput.builder().id(rentalVehicle.bookingId).build()

        val endRentalBookingMutation = EndRentalBookingMutation
            .builder()
            .endBookingWhereInput(leaseWhereUniqueInput)
            .build()

        apolloClientService.mutate(endRentalBookingMutation).enqueue(object : ApolloCall.Callback<EndRentalBookingMutation.Data>(){

            override fun onResponse(response: Response<EndRentalBookingMutation.Data>) {
                if (!response.errors.isNullOrEmpty()) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_end_active_booking)))
                    return
                } else {
                    if (response.data != null && response.data!!.lease() != null && response.data!!.lease()!!.endC2ULease() != null) {
                        if (response.data!!.lease()!!.endC2ULease()!!.status() != null && response.data!!.lease()!!.endC2ULease()!!.status() == LeaseStatus.ENDED) {
                            val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs
                            if(vehicle != null && vehicle.model?.protocol != null && vehicle.model?.protocol == ModelProtocol.PNP ){
                                val device = PrefUtils.getInstance(context).deviceObjectFromPrefs
                                if (device?.deviceType != null && device.deviceType == DeviceType.TBIT
                                    && TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED
                                            || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {
                                    TbitBle.disConnect()
                                }
                            }
                          //  dataState.postValue(DataState.Success(context.getString(R.string.end_vehicle_booking_success)))
                            makeLockVehicleOnRentalEndApiCall(context, rentalVehicle)
                        } else {
                           // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_end_active_booking)))
                        }
                    } else {
                       // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_end_active_booking)))
                    }
                }
            }

            override fun onFailure(e: ApolloException) {
               // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun makeLockVehicleOnRentalEndApiCall(context: Context,
                                          rentalVehicle: RentalVehicle
    ){
        var authToken = PrefUtils.getInstance(context).authTokenFromPrefs ?: return
        authToken = "Bearer $authToken"

        val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs ?: return
        val device = PrefUtils.getInstance(context).deviceObjectFromPrefs ?: return

        if(vehicle.vin.isNullOrEmpty() || device.pin.isNullOrEmpty()) {
            PrefUtils.getInstance(context).clearOnGoingRentalPrefs()
            return
        }

        val bodyJsonObject = JsonObject()
        bodyJsonObject.addProperty("command","LOCK")
        bodyJsonObject.addProperty("pin", device.pin)

        val requestBody = bodyJsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        val revosApiService = RevOsApiService.create()

        revosApiService.vehicleExecuteCommand(rentalVehicle.vin, "1234", bearerToken = authToken, requestBody).enqueue(
            object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    PrefUtils.getInstance(context).clearOnGoingRentalPrefs()
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    PrefUtils.getInstance(context).clearOnGoingRentalPrefs()
                }
            })
    }


    fun fetchActiveBookingsForUser(context: Context, dataState: MutableLiveData<DataState<String>>){
     //   dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context)
        val leaseWhereInput = LeaseWhereInput
            .builder()
            .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
            .status(LeaseStatus.ACTIVE)
            .build()

        val fetchLeaseForUserQuery =
            FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build()

        if(apolloClientService == null) {
          //  dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }

        apolloClientService.query(fetchLeaseForUserQuery).enqueue(object : ApolloCall.Callback<FetchLeaseForUserQuery.Data>(){
            override fun onResponse(response: Response<FetchLeaseForUserQuery.Data>) {
                if (response.data != null && response.data!!.lease() != null && response.data!!.lease()!!
                        .leasesForUser != null) {
                    val fetchedLeaseForUserList = response.data!!.lease()!!.leasesForUser
                    if (fetchedLeaseForUserList != null && !fetchedLeaseForUserList.isEmpty()) {
                        if (fetchedLeaseForUserList[0] != null) {
                            val rentalVehicle = RentalVehicle()
                            if (fetchedLeaseForUserList[0]!!.asset() != null && fetchedLeaseForUserList[0]!!
                                    .asset()!!.vehicle() != null) {
                                if (fetchedLeaseForUserList[0]!!.asset()!!.vehicle()!!.vin() != null
                                    && !fetchedLeaseForUserList[0]!!.asset()!!.vehicle()!!.vin()
                                        .isEmpty()
                                ) {
                                    rentalVehicle.vin= fetchedLeaseForUserList[0]!!.asset()!!.vehicle()!!.vin()

                                }
                                if (fetchedLeaseForUserList[0]!!.asset()!!.vehicle()!!
                                        .model() != null && fetchedLeaseForUserList[0]!!.asset()!!
                                        .vehicle()!!.model()!!
                                        .name() != null && !fetchedLeaseForUserList[0]!!.asset()!!
                                        .vehicle()!!.model()!!.name()!!.isEmpty()
                                ) {
                                    val model = RentalVehicleModel()
                                    model.name= fetchedLeaseForUserList[0]!!.asset()!!.vehicle()!!.model()!!
                                        .name()

                                    rentalVehicle.model=model
                                }
                                if (fetchedLeaseForUserList[0]!!.asset()!!.vehicle()!!
                                        .company() != null && fetchedLeaseForUserList[0]!!.asset()!!
                                        .vehicle()!!.company()!!
                                        .name() != null && !fetchedLeaseForUserList[0]!!.asset()!!
                                        .vehicle()!!.company()!!.name().isEmpty()
                                ) {
                                    val company = RentalVehicleCompany()
                                    company.name=fetchedLeaseForUserList[0]!!.asset()!!.vehicle()!!
                                        .company()!!.name()
                                    rentalVehicle.company=company
                                }
                            }
                            context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                                .putString(
                                    ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY,
                                    Gson().toJson(rentalVehicle)
                                ).apply()
                        } else {
                            context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                                .putString(
                                    ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null
                                ).apply()
                        }
                    } else {
                        context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                            .putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null)
                            .apply()
                    }
                } else {
                    context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                        .putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply()
                }
            }

            override fun onFailure(e: ApolloException) {
                context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit()
                    .putString(
                        ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null
                    ).apply()
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun downloadModelImage(context: Context,modelId:String?,dataState: MutableLiveData<DataState<String>>){
        if (modelId == null) {
            return
        }
        val apolloClientService = ApolloClientService.create(context)

        val modelWhereUniqueInput = ModelWhereUniqueInput.builder().id(modelId).build()
        val fetchModelImageQuery =
            FetchModelImageQuery.builder().modelWhereUniqueInput(modelWhereUniqueInput).build()

        if(apolloClientService == null) {
           // dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }

        apolloClientService.query(fetchModelImageQuery).enqueue(object : ApolloCall.Callback<FetchModelImageQuery.Data>(){
            override fun onResponse(response: Response<FetchModelImageQuery.Data>) {
                if (response.data != null && response.data!!.models() != null && response.data!!.models()!!
                        .get() != null && response.data!!.models()!!.get()!!.image() != null) {
                    PrefUtils.getInstance(context)
                        .updateModelImageHashMap(modelId, response.data!!.models()!!.get()!!.image())
                }
              //  dataState.postValue(DataState.Success(modelId))
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }


    fun checkKycStatusWithRentalProvider(context: Context,
                                         providerId : String){
      //  dataState.postValue(DataState.Loading)
        val companyWhereUniqueInput = CompanyWhereUniqueInput
            .builder()
            .id(providerId)
            .build()

        val fetchKYCAssociationForUserQuery = FetchKYCAssociationForUserQuery
            .builder()
            .companyWhereUniqueInput(companyWhereUniqueInput)
            .build()

        val apolloClientService = ApolloClientService.create(context)

        if(apolloClientService == null) {
          //  dataState.postValue(DataState.Error(context.getString(R.string.error)))
            return
        }

        apolloClientService.query(fetchKYCAssociationForUserQuery)?.enqueue(object : ApolloCall.Callback<FetchKYCAssociationForUserQuery.Data>(){

            override fun onResponse(response: Response<FetchKYCAssociationForUserQuery.Data>) {
                if (response.data != null && response.data!!.account() != null) {
                    if(response.data!!.account()!!.kyc != null &&
                        response.data!!.account()!!.kyc!!.status() == KYCStatus.APPROVED) {
                      //  dataState.postValue(DataState.Success(vehicle))
                    }else{
                     //   dataState.postValue(DataState.Error("KYC_PENDING"))
                    }
                }else{
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.kyc_submit_to_provider_failed)))
                return
            }
        })
    }

    fun updateUserDetails(context: Context,
                          user: User,
                          dataState: MutableLiveData<DataState<User>>) {

      //  dataState.postValue(DataState.Loading)

        var userUpdateInput = if (!user.phone.isNullOrEmpty()) {
            UserUpdateInput
                .builder()
                .firstName(user.firstName)
                .lastName(user.lastName)
                .email(user.email)
                .dob(user.dob)
                .gender(user.genderType)
                .role(user.role)
                .phone(user.phone)
                .build()
        } else {
            UserUpdateInput
                .builder()
                .firstName(user.firstName)
                .lastName(user.lastName)
                .email(user.email)
                .dob(user.dob)
                .gender(user.genderType)
                .role(user.role)
                .build()
        }
        val updateUserMutation = UpdateUserMutation.builder().data(userUpdateInput).build()
        val apolloClientService = ApolloClientService.create(context) ?: return

        apolloClientService.mutate(updateUserMutation)?.enqueue(object : ApolloCall.Callback<UpdateUserMutation.Data>(){

            override fun onResponse(response: Response<UpdateUserMutation.Data>) {
                if(!response.errors.isNullOrEmpty()){
                  //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
                    return
                }else{
                  //  dataState.postValue(DataState.Success(user))
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }
        })
    }

    fun fetchServiceTickets(context: Context, dataState: MutableLiveData<DataState<List<ListServiceRequestQuery.GetAll>?>>){
        // dataState.postValue(DataState.Loading)
         val listServiceRequestQuery = ListServiceRequestQuery.builder().build()
         val apolloClientService = ApolloClientService.create(context) ?: return
         apolloClientService.query(listServiceRequestQuery).enqueue(object : ApolloCall.Callback<ListServiceRequestQuery.Data>(){
             override fun onResponse(response: Response<ListServiceRequestQuery.Data>) {
                 if (response.errors != null && response.errors!!.isNotEmpty()) {
                 //    dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_service_requests)))
                 }else{
                     if (response.data != null && response.data!!.support() != null && response.data!!.support()!!.all != null && response.data!!.support()!!
                             .all!!.size > 0) {
                         val serviceTicketList = response.data!!.support()!!.all
                      //   dataState.postValue(DataState.Success(serviceTicketList))
                     }else{
                       //  dataState.postValue(DataState.Error(context.getString(R.string.no_service_requests_found)))
                     }
                 }
             }

             override fun onFailure(e: ApolloException) {
               //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
             }

         })
    }

    fun fetchServiceRequestDetails(context: Context,mTicketID:String,dataState: MutableLiveData<DataState<FetchServiceRequestDetailsQuery.Get?>>){
       // dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context) ?: return
        if(mTicketID.isNullOrEmpty()){
            return
        }
        val fetchServiceRequestDetails = FetchServiceRequestDetailsQuery.builder()
            .serviceTicketWhereUniqueInputVar(mTicketID)
            .build()

        apolloClientService.query(fetchServiceRequestDetails).enqueue(object : ApolloCall.Callback<FetchServiceRequestDetailsQuery.Data>(){
            override fun onResponse(response: Response<FetchServiceRequestDetailsQuery.Data>) {

                if (response.errors != null && response.errors!!.isNotEmpty()) {
                  //  dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_service_ticket_details)))
                }else{
                    if (response.data != null && response.data!!.support() != null && response.data!!.support()!!.get() != null) {
                        val serviceTicketDetailsObject = response.data!!.support()!!.get()
                        if(serviceTicketDetailsObject == null) return
                      //  dataState.postValue(DataState.Success(serviceTicketDetailsObject))

                    }else{
                     //   dataState.postValue(DataState.Error(context.getString(R.string.no_service_requests_found)))
                    }
                }

            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }

        })
    }

    fun makeCreateServiceRequestCall(
        context: Context,
        titleStr: String,
        descStr: String,
        imageLogoBuffer: List<String>?,
        dataState: MutableLiveData<DataState<String>>
    ){
       // dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context) ?: return
        if(titleStr.isEmpty() && descStr.isEmpty()){
            return
        }
        val vehicle = PrefUtils.getInstance(context).vehicleObjectFromPrefs ?: return
        val vehicleId= vehicle.vehicleId
        var serviceTicketInput = if (imageLogoBuffer != null && imageLogoBuffer.isNotEmpty()) {
            ServiceTicketInput
                .builder()
                .title(titleStr)
                .description(descStr)
                .logoBuffer(imageLogoBuffer)
                .vehicle(vehicleId)
                .build()
        } else {
            ServiceTicketInput
                .builder()
                .title(titleStr)
                .description(descStr)
                .vehicle(vehicleId)
                .build()
        }

        val createServiceTicketMutation = CreateServiceTicketMutation
            .builder()
            .serviceTicketInputVar(serviceTicketInput)
            .build()
        apolloClientService.mutate(createServiceTicketMutation).enqueue(object : ApolloCall.Callback<CreateServiceTicketMutation.Data>(){
            override fun onResponse(response: Response<CreateServiceTicketMutation.Data>) {
                if (response.errors != null && response.errors!!.isNotEmpty()) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_fetch_service_ticket_details)))
                }else{
                    if (response.data != null) {
                      //  dataState.postValue(DataState.Success(context.getString(R.string.ticket_created_successfully)))
                    }
                }
            }

            override fun onFailure(e: ApolloException) {
               // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }

        })
    }

    fun fetchVehicleObject(
        context: Context,
        vinEntered: String,
        dataState: MutableLiveData<DataState<FetchVehicleQuery.Get?>>) {
        //dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context) ?: return
        val vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder()
            .vin(vinEntered)
            .build()

        val fetchVehicleQuery = FetchVehicleQuery.builder()
            .vehicleWhereInputVar(vehicleWhereUniqueInput)
            .build()

        apolloClientService.query(fetchVehicleQuery).enqueue(object : ApolloCall.Callback<FetchVehicleQuery.Data>(){
            override fun onResponse(response: Response<FetchVehicleQuery.Data>) {
                if (response.errors != null && response.errors!!.isNotEmpty()) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.incorrect_vin)))
                }else{
                    if (response.data != null && response.data!!.vehicles() != null && response.data!!.vehicles()!!.get() != null) {
                        val vehicleDetailsObject = response.data!!.vehicles()!!.get()
                       // dataState.postValue(DataState.Success(vehicleDetailsObject))

                    }else{
                      //  dataState.postValue(DataState.Error(context.getString(R.string.no_vehicle_found)))
                    }
                }
            }

            override fun onFailure(e: ApolloException) {
               // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }

        })

    }


    fun makeAddTicketImagesCall(
        context: Context,
        imageLogoBuffer:List<String>?,
        mTicketID:String,
        dataState: MutableLiveData<DataState<String>>
    ) {
       // dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context) ?: return

        val serviceTicketImagesInput = ServiceTicketCommentsInput
            .builder()
            .logoBuffer(imageLogoBuffer)
            .build()

        val serviceTicketWhereUniqueInput = ServiceTicketWhereUniqueInput
            .builder()
            .id(mTicketID)
            .build()

        val updateServiceRequestMutation = UpdateServiceRequestMutation
            .builder()
            .data(serviceTicketImagesInput)
            .where(serviceTicketWhereUniqueInput)
            .build()

        apolloClientService.mutate(updateServiceRequestMutation).enqueue(object : ApolloCall.Callback<UpdateServiceRequestMutation.Data>(){
            override fun onResponse(response: Response<UpdateServiceRequestMutation.Data>) {
                if (response.errors != null && response.errors!!.isNotEmpty()) {
                   // dataState.postValue(DataState.Error(context.getString(R.string.unable_to_add_image)))
                }else if (response.data != null && response.data?.support() != null && response.data?.support()?.update() != null) {
                  //  dataState.postValue(DataState.Success(context.getString(R.string.image_added_successfully)))
                }
            }

            override fun onFailure(e: ApolloException) {
              //  dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }

        })
    }

    fun makeUpdateServiceTicketCall(context: Context,commentAdded: String,mTicketID:String,dataState: MutableLiveData<DataState<String>>) {
       // dataState.postValue(DataState.Loading)
        val apolloClientService = ApolloClientService.create(context) ?: return

        val serviceTicketCommentsInput = ServiceTicketCommentsInput
            .builder()
            .comment(commentAdded)
            .build()

        val serviceTicketWhereUniqueInput = ServiceTicketWhereUniqueInput
            .builder()
            .id(mTicketID)
            .build()

        val updateServiceRequestMutation = UpdateServiceRequestMutation
            .builder()
            .data(serviceTicketCommentsInput)
            .where(serviceTicketWhereUniqueInput)
            .build()

        apolloClientService.mutate(updateServiceRequestMutation).enqueue(object : ApolloCall.Callback<UpdateServiceRequestMutation.Data>(){
            override fun onResponse(response: Response<UpdateServiceRequestMutation.Data>) {
                if (response.errors != null && response.errors!!.isNotEmpty()) {
                  //  dataState.postValue(DataState.Error(context.getString(R.string.unable_to_add_comment)))
                }else if (response.data != null && response.data?.support() != null && response.data?.support()?.update() != null) {
                 //   dataState.postValue(DataState.Success(context.getString(R.string.comment_added_successfully)))
                }
            }

            override fun onFailure(e: ApolloException) {
               // dataState.postValue(DataState.Error(context.getString(R.string.server_error)))
            }

        })
    }
}