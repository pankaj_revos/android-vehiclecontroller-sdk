package com.revos.android.fleet.pnp

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.c.NativeController
import com.csy.bl.ble.IconCoxBleManager
import com.csy.bl.ble.common.utils.TypeConvert
import com.revos.android.fleet.data.DataState
import com.revos.android.fleet.managers.VehicleManager
import com.revos.android.fleet.utils.*
import com.revos.android.fleet.services.NordicBleService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class IconCoxDevice constructor(val context: Context) : MutableLiveData<DataState<String>>(),
    PnpDevice {
    private val vehicleManager = VehicleManager
    private val bleManager = IconCoxBleManager.getInstance(context)

    override fun lock(){
        if(NordicBleService.mIsIconcoxBleConnected){
            val cmd = NativeController.encode8String(TypeConvert.hexStringToBytes(CMD_SEND_REMOTE_LOCK))
            bleManager.sendHexStringCmd(cmd)
            vehicleManager.checkCmdResponse(context, BT_EVENT_REQUEST_LOCK_VEHICLE)
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                vehicleManager.triggerCommandFromServer(context, PnpDevice.COMMAND.IGNITION_OFF.name)
            }
        }
    }

    override fun unlock() {
        if(NordicBleService.mIsIconcoxBleConnected){
            val cmd = NativeController.encode8String(TypeConvert.hexStringToBytes(CMD_SEND_REMOTE_IGNITION))
            bleManager.sendHexStringCmd(cmd)
            vehicleManager.checkCmdResponse(context,BT_EVENT_REQUEST_UNLOCK_VEHICLE)
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                vehicleManager.triggerCommandFromServer(context,
                    PnpDevice.COMMAND.IGNITION_ON.name
                )
            }
        }
    }

    override fun findVehicle() {
        if(NordicBleService.mIsIconcoxBleConnected){
            val cmd = NativeController.encode8String(TypeConvert.hexStringToBytes(CMD_SEND_FIND_VEHICLE))
            bleManager.sendHexStringCmd(cmd)
            vehicleManager.checkCmdResponse(context, BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND)
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                vehicleManager.triggerCommandFromServer(context,
                    PnpDevice.COMMAND.FIND.name
                )
            }
        }
    }

    override fun disconnect() {
        if(NordicBleService.mIsIconcoxBleConnected){
            bleManager.disconnect()
        }
        clearDeviceCacheOnDisconnect()
    }

    override fun get(): LiveData<DataState<String>> {
        return this
    }

    override fun isConnected(): Boolean {
        return NordicBleService.mIsIconcoxBleConnected
    }

    private fun clearDeviceCacheOnDisconnect(){
        context.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
        context.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
        context.getSharedPreferences(TRIP_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
        context.getSharedPreferences(GEO_FENCING_PREFS, Context.MODE_PRIVATE).edit()
            .clear().apply()
    }
}