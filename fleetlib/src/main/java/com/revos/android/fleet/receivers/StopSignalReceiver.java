package com.revos.android.fleet.receivers;

import static com.revos.android.fleet.utils.ConstantsKt.GENERAL_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.KILL_SWITCH;
import static com.revos.android.fleet.utils.ConstantsKt.KILL_SWITCH_FLAG_KEY;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.revos.android.fleet.eventbus.EventBusMessage;

import org.greenrobot.eventbus.EventBus;

public class StopSignalReceiver extends BroadcastReceiver {
    public StopSignalReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus.getDefault().post(new EventBusMessage(KILL_SWITCH));
        context.getSharedPreferences(GENERAL_PREFS, Context.MODE_PRIVATE).edit().putBoolean(KILL_SWITCH_FLAG_KEY, true).apply();
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }
}
