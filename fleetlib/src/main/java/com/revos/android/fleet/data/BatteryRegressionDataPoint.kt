package com.revos.android.fleet.data

data class BatteryRegressionDataPoint (
    var voltage : Double = 0.0,
    var soc : Double = 0.0,
    var dte : Double = 0.0,
)