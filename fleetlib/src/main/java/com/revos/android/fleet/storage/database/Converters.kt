package com.revos.android.fleet.storage.database

import androidx.room.TypeConverter
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.revos.scripts.type.FenceType

/**
 * Type converters to allow Room to reference complex data types.
 */
class Converters {
    @TypeConverter fun fenceTypeToString(fenceType: FenceType): String = fenceType.name

    @TypeConverter fun stringToFenceType(value: String): FenceType {
        when(value){
            FenceType.RADIAL.name -> {
                return FenceType.RADIAL
            }
            FenceType.POLYGON.name -> {
                return FenceType.POLYGON
            }
        }
        return FenceType.`$UNKNOWN`
    }

    @TypeConverter fun latlngToString(latLng: LatLng?): String? {
        if(latLng == null) return null
        val coordinates = StringBuilder()
        coordinates.append(latLng.latitude)
        coordinates.append(",")
        coordinates.append(latLng.longitude)
        return coordinates.toString()
    }

    @TypeConverter fun stringToLatLng(value: String?): LatLng? {
        if(value.isNullOrEmpty()) return null
        val coord = value.split(",")
        return LatLng(coord[0].toDouble(), coord[1].toDouble())
    }

    @TypeConverter
    fun polygonVerticesListToString(vertices: ArrayList<LatLng>?): String? {
        val geofenceType = object : TypeToken<ArrayList<LatLng>>() {}.type
        return Gson().toJson(vertices, geofenceType)
    }

    @TypeConverter
    fun stringToPolygonVerticesList(vertices: String?): ArrayList<LatLng>? {
        val geofenceType = object : TypeToken<ArrayList<LatLng>>() {}.type
        return Gson().fromJson(vertices, geofenceType)
    }

}
