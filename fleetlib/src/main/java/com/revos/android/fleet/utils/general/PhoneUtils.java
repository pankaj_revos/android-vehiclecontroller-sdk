package com.revos.android.fleet.utils.general;

import static com.revos.android.fleet.utils.ConstantsKt.CALL_LOG_HASHMAP_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.PHONE_EVENT_REQUEST_BUILD_CONTACT_LIST;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.revos.android.fleet.utils.ConstantsKt;
import com.revos.android.fleet.data.Contact;
import com.revos.android.fleet.data.GenericPhoneEntry;
import com.revos.android.fleet.eventbus.EventBusMessage;
import org.greenrobot.eventbus.EventBus;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * Created by moyadav on 19-Nov-16.
 */
public class PhoneUtils {

    //holds the single instance
    private static PhoneUtils mInstance = null;

    //private constructor
    private PhoneUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static PhoneUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new PhoneUtils(context);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    //stores the context
    private Context mContext = null;

    private void setContext(Context context) {
        mContext = context;
    }

    // localContactList
    private ArrayList<Contact> mContactList;

    //builds the mContactList and updates the shared preferences
    public void buildContactList() {
        //build the localContactList
        buildContactListHelper();

        //convert mContactList to json via gson
        if(mContactList != null) {
            String contactListStr = new Gson().toJson(mContactList);

            //write this to the prefs
            writeToPhonePrefs(ConstantsKt.CONTACT_LIST_KEY, contactListStr);
        } else {
            buildContactList();
        }
    }

    private void writeToPhonePrefs(String key, String value) {
        if(mContext != null) {
            SharedPreferences sharedpreferences = mContext.getSharedPreferences(ConstantsKt.PHONE_PREFS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    //does the actual work, this method is synchronous
    private void buildContactListHelper() {
        if(mContactList == null) {
            mContactList = new ArrayList<>();
        }
        mContactList.clear();

        ContentResolver cr = mContext.getContentResolver();
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME +
                " COLLATE LOCALIZED ASC";
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, sortOrder);

        if(cur != null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if(Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    ArrayList<String> phoneNumbers = new ArrayList<String>();
                    if(pCur != null) {
                        while (pCur.moveToNext()) {
                            String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                            String cleanNumber = sanitizePhoneNumber(phoneNo);
                            phoneNumbers.add(cleanNumber);

                        }
                        pCur.close();
                    }

                    //remove duplicate numbers
                    Set<String> uniqueNumbers = new HashSet<>();
                    uniqueNumbers.addAll(phoneNumbers);
                    phoneNumbers.clear();
                    phoneNumbers.addAll(uniqueNumbers);

                    //create contact details
                    Contact contactJSON = new Contact();
                    contactJSON.setId(Integer.parseInt(id));
                    contactJSON.setName(name);
                    contactJSON.setPhoneNumbers(phoneNumbers);

                    //add to the contact list
                    mContactList.add(contactJSON);
                }
            }
            //free the cursor
            cur.close();
        }
    }

    public String sanitizePhoneNumber(String phoneNumber) {
        if(phoneNumber == null) {
            return "";
        }
        return phoneNumber.replaceAll("[^0-9+]", "");
    }

    public ArrayList<Contact> getContactList() {
        if(mContactList != null) {
            return mContactList;
        } else {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(ConstantsKt.PHONE_PREFS, Context.MODE_PRIVATE);
            String contactListStr = sharedPreferences.getString(ConstantsKt.CONTACT_LIST_KEY, null);

            if(contactListStr != null) {
                mContactList = new Gson().fromJson(contactListStr, new TypeToken<ArrayList<Contact>>(){}.getType());
            } else {
                EventBus.getDefault().post(new EventBusMessage(PHONE_EVENT_REQUEST_BUILD_CONTACT_LIST));
            }
            return mContactList;
        }
    }

    public String findContactName(String queryNumber) {
        if(queryNumber == null) {
            return null;
        }
        ArrayList<Contact> contactList = getContactList();

        if(contactList != null) {
            //iterate over the contact list and see if we have a matching contact
            for (int contactIndex = 0; contactIndex < contactList.size(); ++contactIndex) {
                ArrayList<String> phoneNumbers = contactList.get(contactIndex).getPhoneNumbers();
                for (int phoneNumberIndex = 0; phoneNumberIndex < phoneNumbers.size(); ++phoneNumberIndex) {
                    if(phoneNumbers.get(phoneNumberIndex).contains(queryNumber)) {
                        return contactList.get(contactIndex).getName();
                    }
                }
            }
        }

        return  null;
    }

    public ArrayList<String> findContactNumbers(String contactName) {
        ArrayList<Contact> contactList = getContactList();

        if(contactList != null) {
            //iterate over the contact list and see if we have a matching contact
            for (int contactIndex = 0; contactIndex < contactList.size(); ++contactIndex) {
                if(contactName.equals(contactList.get(contactIndex).getName())) {
                    return contactList.get(contactIndex).getPhoneNumbers();
                }
            }
        }

        return  null;
    }

    public ArrayList<GenericPhoneEntry> getBriefCallHistory() {

        ArrayList<GenericPhoneEntry> callHistoryList = new ArrayList<>();
        HashMap<String, GenericPhoneEntry> callLogHashMap = new HashMap<>();

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(ConstantsKt.PHONE_PREFS, Context.MODE_PRIVATE);
        String callLogHashMapStr = sharedPreferences.getString(CALL_LOG_HASHMAP_KEY, null);

        if(callLogHashMapStr != null) {
            callLogHashMap = new Gson().fromJson(callLogHashMapStr, new TypeToken<HashMap<String, GenericPhoneEntry>>(){}.getType());
        }

        //convert hashmap to arraylist
        for(Map.Entry<String, GenericPhoneEntry> entry : callLogHashMap.entrySet()) {
            callHistoryList.add(entry.getValue());
        }

        //sort this arraylist
        Collections.sort(callHistoryList, new Comparator<GenericPhoneEntry>() {
            @Override
            public int compare(GenericPhoneEntry entry1, GenericPhoneEntry entry2) {
                return entry2.getDate().compareTo(entry1.getDate());
            }
        });

        return callHistoryList;
    }

    public String getNumberInAction() {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(ConstantsKt.PHONE_PREFS, Context.MODE_PRIVATE);
            return sharedPreferences.getString(ConstantsKt.NUMBER_IN_ACTION_KEY, null);
    }

    public void setNumberInAction(String displayString) {
        writeToPhonePrefs(ConstantsKt.NUMBER_IN_ACTION_KEY, displayString);
    }

    public String getContactInAction() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(ConstantsKt.PHONE_PREFS, Context.MODE_PRIVATE);
        return sharedPreferences.getString(ConstantsKt.CONTACT_IN_ACTION_KEY, null);
    }

    public void setContactInAction(String displayString) {
        writeToPhonePrefs(ConstantsKt.CONTACT_IN_ACTION_KEY, displayString);
    }

    public Bitmap retrieveContactPhotoBitmap(String number) {
        Bitmap photo = null;
        try {
            ContentResolver contentResolver = mContext.getContentResolver();
            String contactId = number;
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

            String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

            Cursor cursor =
                    contentResolver.query(
                            uri,
                            projection,
                            null,
                            null,
                            null);

            if(cursor != null) {
                while (cursor.moveToNext()) {
                    contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                }
                cursor.close();
            } else {
                return null;
            }

            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(mContext.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId)));

            if(inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            }

        } catch (Exception e) {
            //todo::handle exception
        }

        return photo;
    }

    public Uri retrieveContactThumbPhotoUri(String number) {
        Uri thumbPhotoUri = null;
        try {
            ContentResolver contentResolver = mContext.getContentResolver();
            String contactId = number;
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

            String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

            Cursor cursor =
                    contentResolver.query(
                            uri,
                            projection,
                            null,
                            null,
                            null);

            if(cursor != null) {
                while (cursor.moveToNext()) {
                    contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                }
                cursor.close();
            } else {
                return null;
            }

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId));
            thumbPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {
            //todo::handle exception
        }

        return thumbPhotoUri;
    }

    public Uri retrieveContactDisplayPhotoUri(String number) {
        Uri displayPhotoUri = null;
        try {
            ContentResolver contentResolver = mContext.getContentResolver();
            String contactId = number;
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

            String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

            Cursor cursor =
                    contentResolver.query(
                            uri,
                            projection,
                            null,
                            null,
                            null);

            if(cursor != null) {
                while (cursor.moveToNext()) {
                    contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                }
                cursor.close();
            } else {
                return null;
            }

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId));
            displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);

        } catch (Exception e) {
            //todo::handle exception
        }

        return displayPhotoUri;
    }

    public void callNumber(String phoneNumber) {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
        mContext.startActivity(intent);

    }
}
