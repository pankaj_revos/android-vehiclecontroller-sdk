package com.revos.android.fleet.payment

import android.app.Activity
import com.revos.android.fleet.data.Booking
import com.revos.android.fleet.data.User

/**
* @suppress from generating docs
* */
abstract class PaymentManager {

    internal fun choosePaymentMethodAndPay(booking: Booking,
                                           activity: Activity,
                                           cftoken: String,
                                           vendorSplit : String,
                                           user: User) {

        pay(CashFreePaymentStrategy(activity, booking, cftoken, user, vendorSplit), booking.amount)
    }

    private fun pay(paymentStrategy: PaymentStrategy, amount: Float) {
        paymentStrategy.pay(amount)
    }
}