package com.revos.android.fleet.utils.general;

import static android.content.Context.MODE_PRIVATE;
import android.content.Context;
import android.content.SharedPreferences;
import com.revos.android.fleet.utils.ConstantsKt;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class RevosFileUtils {

    //holds the single instance
    private static RevosFileUtils mInstance = null;


    //private constructor
    private RevosFileUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static RevosFileUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new RevosFileUtils(context);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    //stores the context
    private Context mContext = null;

    private void setContext(Context context) {
        mContext = context;
    }

    public int getStoredLogFileCount(String vehicleLogType) {
        if(mContext == null) {
            return 0;
        }

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(ConstantsKt.LOG_PREFS, MODE_PRIVATE);

        //check if there are any local files to sync, if not then abort
        String logDirKey = ConstantsKt.VEHICLE_OFFLINE_LOG_DIR_KEY;

        if(vehicleLogType.equals(ConstantsKt.LOG_TYPE_LIVE)) {
            logDirKey = ConstantsKt.VEHICLE_LIVE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(ConstantsKt.LOG_TYPE_OFFLINE)) {
            logDirKey = ConstantsKt.VEHICLE_OFFLINE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(ConstantsKt.LOG_TYPE_OFFLINE_BATTERY_ADC)){
            logDirKey = ConstantsKt.VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY;
        }

        String logDirStr = sharedPreferences.getString(logDirKey, null);
        if(logDirStr == null) {
            return 0;
        }

        File logDir = new File(logDirStr);
        if(!logDir.exists()) {
            return 0;
        }

        File[] logFiles = logDir.listFiles();
        if(logFiles == null || logFiles.length == 0) {
            return 0;
        }

        ArrayList<String> logFileList = new ArrayList<>();

        //sort file with latest at position zero
        for (File logFile: logFiles) {
            logFileList.add(logFile.getName());
        }

        Collections.sort(logFileList);

        Collections.reverse(logFileList);

        File latestLogFile = new File(logDirStr, logFileList.get(0));

        //check if this file is not the one being written into
        String workingLogFileKey = ConstantsKt.WORKING_OFFLINE_LOG_FILE_NAME_KEY;
        if(vehicleLogType.equals(ConstantsKt.LOG_TYPE_LIVE)) {
            workingLogFileKey = ConstantsKt.WORKING_LIVE_LOG_FILE_NAME_KEY;
        } else if(vehicleLogType.equals(ConstantsKt.LOG_TYPE_OFFLINE)) {
            workingLogFileKey = ConstantsKt.WORKING_OFFLINE_LOG_FILE_NAME_KEY;
        } else if(vehicleLogType.equals(ConstantsKt.LOG_TYPE_OFFLINE_BATTERY_ADC)){
            workingLogFileKey = ConstantsKt.WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY;
        }
        String workingLogFile = mContext.getSharedPreferences(ConstantsKt.LOG_PREFS, MODE_PRIVATE).getString(workingLogFileKey, null);

        //if yes then take the next file to sync
        if(workingLogFile != null && workingLogFile.equals(latestLogFile.getName()) && logFileList.size() > 1) {
            return (logFileList.size() - 1);
        } else {
            return logFileList.size();
        }
    }

}
