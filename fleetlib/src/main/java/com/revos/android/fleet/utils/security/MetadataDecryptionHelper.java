package com.revos.android.fleet.utils.security;

import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_PREFS;
import static com.revos.android.fleet.utils.ConstantsKt.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.DEVICE_PIN_KEY;
import static com.revos.android.fleet.utils.ConstantsKt.DEVICE_PREFS;

import android.content.Context;
import android.util.Base64;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.fleet.data.Device;
import com.revos.android.fleet.utils.general.PrefUtils;

public class MetadataDecryptionHelper {
    private final String FIRMWARE_METADATA_KEY = "firmwareMetadata";
    private final String VERSION_NO_KEY = "versionNo";


    private Context mContext;
    private static MetadataDecryptionHelper mInstance;
    private String mControllerReadDecryptedString = null;
    private String mControllerLogDecryptedString = null;
    private String mControllerWriteDecryptedString = null;
    private String mVehicleControlDecryptedString = null;
    private String mBatteryAdcLogDecryptedString = null;

    private MetadataDecryptionHelper(Context context) {
        mContext = context;
    }

    private void setContext(Context context) {
        mContext = context;
    }

    public static MetadataDecryptionHelper getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new MetadataDecryptionHelper(context);
            mInstance.mControllerReadDecryptedString = null;
            mInstance.mControllerLogDecryptedString = null;
            mInstance.mControllerWriteDecryptedString = null;
            mInstance.mVehicleControlDecryptedString = null;
            mInstance.mBatteryAdcLogDecryptedString = null;
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    public String getMetadataFirmwareVersion() {
        if(mControllerReadDecryptedString == null) {
            return null;
        }

        try {
            JsonObject parameterReadFormatJsonObject = new JsonParser().parse(mControllerReadDecryptedString).getAsJsonObject();

            if (parameterReadFormatJsonObject.has(FIRMWARE_METADATA_KEY)) {
                JsonObject firmwareMetadataJsonObject = parameterReadFormatJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY);
                if (firmwareMetadataJsonObject.has(VERSION_NO_KEY)) {
                    return firmwareMetadataJsonObject.get(VERSION_NO_KEY).getAsString();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            return  null;
        }
    }

    public void clearLocalCache() {
        mControllerReadDecryptedString = null;
        mControllerLogDecryptedString = null;
        mControllerWriteDecryptedString = null;
        mVehicleControlDecryptedString = null;
        mBatteryAdcLogDecryptedString = null;
    }

    public String decryptControllerReadMetadata() {
        if(mControllerReadDecryptedString != null) {
            return mControllerReadDecryptedString;
        }

        String decryptedString = null;

        String encryptionKey = constructEncryptionKey();

        if(encryptionKey == null) {
            return decryptedString;
        }

        try {
            String base64ControllerReadMetadata = mContext.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null);
            if(base64ControllerReadMetadata == null) {
                return decryptedString;
            }

            byte[] controllerReadMetadataEncryptedBytes = Base64.decode(base64ControllerReadMetadata, Base64.DEFAULT);
            byte[] controllerReadMetadataDecryptedBytes = EncryptionHelper.decryptBytes(controllerReadMetadataEncryptedBytes, encryptionKey);

            decryptedString = new String(controllerReadMetadataDecryptedBytes);
        } catch (Exception e) {

        }

        mControllerReadDecryptedString = decryptedString;
        return decryptedString;
    }

    public String decryptControllerLogMetadata() {
        if(mControllerLogDecryptedString != null) {
            return mControllerLogDecryptedString;
        }

        String decryptedString = null;

        String encryptionKey = constructEncryptionKey();

        if(encryptionKey == null) {
            return decryptedString;
        }

        try {
            String base64ControllerLogMetadata = mContext.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null);
            if(base64ControllerLogMetadata == null) {
                return decryptedString;
            }

            byte[] controllerLogMetadataEncryptedBytes = Base64.decode(base64ControllerLogMetadata, Base64.DEFAULT);
            byte[] controllerLogMetadataDecryptedBytes = EncryptionHelper.decryptBytes(controllerLogMetadataEncryptedBytes, encryptionKey);

            decryptedString = new String(controllerLogMetadataDecryptedBytes);
        } catch (Exception e) {

        }

        mControllerLogDecryptedString = decryptedString;
        return decryptedString;
    }

    public String decryptControllerWriteMetadata() {
        if(mControllerWriteDecryptedString != null) {
            return mControllerWriteDecryptedString;
        }

        String decryptedString = null;

        String encryptionKey = constructEncryptionKey();

        if(encryptionKey == null) {
            return decryptedString;
        }

        try {
            String base64ControllerWriteMetadata = mContext.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null);
            if(base64ControllerWriteMetadata == null) {
                return decryptedString;
            }

            byte[] controllerWriteMetadataEncryptedBytes = Base64.decode(base64ControllerWriteMetadata, Base64.DEFAULT);
            byte[] controllerWriteMetadataDecryptedBytes = EncryptionHelper.decryptBytes(controllerWriteMetadataEncryptedBytes, encryptionKey);

            decryptedString = new String(controllerWriteMetadataDecryptedBytes);
        } catch (Exception e) {

        }

        mControllerWriteDecryptedString = decryptedString;
        return decryptedString;
    }

    public String decryptVehicleControlMetadata() {
        if(mVehicleControlDecryptedString != null) {
            return mVehicleControlDecryptedString;
        }

        String decryptedString = null;

        String encryptionKey = constructEncryptionKey();

        if(encryptionKey == null) {
            return decryptedString;
        }

        try {
            String base64VehicleControlMetadata = mContext.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).getString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null);
            if(base64VehicleControlMetadata == null) {
                return decryptedString;
            }

            byte[] vehicleControlMetadataEncryptedBytes = Base64.decode(base64VehicleControlMetadata, Base64.DEFAULT);
            byte[] vehicleControlMetadataDecryptedBytes = EncryptionHelper.decryptBytes(vehicleControlMetadataEncryptedBytes, encryptionKey);

            decryptedString = new String(vehicleControlMetadataDecryptedBytes);
        } catch (Exception e) {

        }

        mVehicleControlDecryptedString = decryptedString;
        return decryptedString;
    }

    public String decryptBatteryAdcLogMetadata() {
        if(mBatteryAdcLogDecryptedString != null) {
            return mBatteryAdcLogDecryptedString;
        }

        String decryptedString = null;

        String encryptionKey = constructEncryptionKey();

        if(encryptionKey == null) {
            return decryptedString;
        }

        try {
            String base64BatteryAdcLogMetadata = mContext.getSharedPreferences(BLUETOOTH_PREFS, Context.MODE_PRIVATE).getString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null);
            if(base64BatteryAdcLogMetadata == null) {
                return decryptedString;
            }

            byte[] batteryAdcLogMetadataEncryptedBytes = Base64.decode(base64BatteryAdcLogMetadata, Base64.DEFAULT);
            byte[] batteryAdcLogMetadataDecryptedBytes = EncryptionHelper.decryptBytes(batteryAdcLogMetadataEncryptedBytes, encryptionKey);

            decryptedString = new String(batteryAdcLogMetadataDecryptedBytes);
        } catch (Exception e) {

        }

        mBatteryAdcLogDecryptedString = decryptedString;
        return decryptedString;
    }

    private String constructEncryptionKey() {
        //replace the first n bytes of the key by PIN values

        Device cloudDevice = PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs();
        String devicePin = mContext.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        if(cloudDevice == null || devicePin == null) {
            return null;
        }

        byte[] aesKey = Base64.decode(cloudDevice.getKey(), Base64.NO_WRAP | Base64.URL_SAFE);

        for(int byteIndex = 0; byteIndex < devicePin.length(); ++byteIndex) {
            aesKey[aesKey.length - devicePin.length() + byteIndex] = (byte)devicePin.charAt(byteIndex);
        }

        return Base64.encodeToString(aesKey, Base64.NO_WRAP|Base64.URL_SAFE);
    }
}
