package com.revos.android.fleet.services;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_CONNECTING;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;
import static com.revos.android.fleet.utils.ConstantsKt.*;
import static com.revos.android.fleet.utils.ConstantsKt.GEN_EVENT_PHONE_LOCATION_UPDATED;
import static com.revos.android.fleet.utils.ConstantsKt.VEHICLE_LAST_KNOWN_LONGITUDE;
import static com.revos.android.fleet.utils.vehicleDataTransfer.SpeedometerDataExchanger.PACKET_TYPE_LIVE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.c.NativeController;
import com.csy.bl.ble.IconCoxBleManager;
import com.csy.bl.ble.common.utils.BleConfig;
import com.csy.bl.ble.common.utils.TypeConvert;
import com.csy.bl.ble.listener.OnBleManagerListener;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.revos.android.fleet.R;
import com.revos.android.fleet.receivers.PhoneStateReceiver;
import com.revos.android.fleet.ui.activities.RuntimePermissionActivity;
import com.revos.android.fleet.utils.ConstantsKt;
import com.revos.android.fleet.data.BatteryRegressionData;
import com.revos.android.fleet.data.Device;
import com.revos.android.fleet.data.GeoFencing;
import com.revos.android.fleet.data.Model;
import com.revos.android.fleet.data.User;
import com.revos.android.fleet.data.Vehicle;
import com.revos.android.fleet.eventbus.EventBusMessage;
import com.revos.android.fleet.utils.general.PrefUtils;
import com.revos.android.fleet.receivers.StopSignalReceiver;
import com.revos.android.fleet.repository.MainRepository;
import com.revos.android.fleet.ble.BleUtils;
import com.revos.android.fleet.ble.REVOSBleManager;
import com.revos.android.fleet.ble.RevosTbitProtocolAdapter;
import com.revos.android.fleet.utils.general.ImageUtils;
import com.revos.android.fleet.utils.general.NetworkUtils;
import com.revos.android.fleet.utils.geofence.GeoFenceUtil;
import com.revos.android.fleet.utils.general.Version;
import com.revos.android.fleet.utils.security.EncryptionHelper;
import com.revos.android.fleet.utils.security.MetadataDecryptionHelper;
import com.revos.android.fleet.utils.vehicleDataTransfer.BatteryAdcLogPacketParser;
import com.revos.android.fleet.utils.vehicleDataTransfer.BatteryAdcParameters;
import com.revos.android.fleet.utils.vehicleDataTransfer.ControllerWriteCommandHelper;
import com.revos.android.fleet.utils.vehicleDataTransfer.OfflineLogPacketParser;
import com.revos.android.fleet.utils.vehicleDataTransfer.SpeedometerDataExchanger;
import com.revos.android.fleet.utils.vehicleDataTransfer.VehicleControlHelper;
import com.revos.android.fleet.utils.vehicleDataTransfer.VehicleParameterParser;
import com.revos.android.fleet.utils.vehicleDataTransfer.VehicleParameters;
import com.revos.android.fleet.workers.VehicleLastKnownLocationWorker;
import com.revos.scripts.type.DeviceStatus;
import com.revos.scripts.type.DeviceType;
import com.revos.scripts.type.DeviceUpdateStatus;
import com.revos.scripts.type.FenceType;
import com.revos.scripts.type.ModelProtocol;
import com.tbit.tbitblesdk.Bike.TbitBle;
import com.tbit.tbitblesdk.Bike.model.BikeState;
import com.tbit.tbitblesdk.Bike.services.command.callback.StateCallback;
import com.tbit.tbitblesdk.bluetooth.BleClient;
import com.tbit.tbitblesdk.protocol.callback.ResultCallback;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import no.nordicsemi.android.ble.callback.FailCallback;
import no.nordicsemi.android.ble.callback.SuccessCallback;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import timber.log.Timber;

public class NordicBleService extends Service implements OnBleManagerListener {
    /**
     * Location variables
     */

    public static Location mCurrentLocation;

    public static long UPDATE_INTERVAL = 3 * 1000;  /* 10 secs */
    public static long FASTEST_INTERVAL = 1000; /* 1 sec */


    //IConcox variables
    private static final int MSG_CONNECT_FIRST_TIME_DELAY = 1010;
    private static final int MSG_SEND_TIME_OUT = 1001;
    private static final int MSG_SEND_TIME_OUT_DELAY = 1000 * 10;
    private static final int MSG_SEND_SCAN = 1004;
    private static final int MSG_SEND_DELAY_UPDATA_BL_STATUS_CONNECTED = 1005;
    private static final int MSG_SEND_DELAY_UPDATA_BL_STATUS_DISCONNECTED = 1006;
    private static final int MSG_SEND_DELAY_SEND_MAC = 1007;
    private static final int MSG_SEND_DELAY_SEND_MAC_TIME = 4 * 1000;
    private static final int MSG_SEND_MAC_TIME_OUT = 2001;
    private static final int TIME_MSG_SEND_MAC_TIME_OUT = 5 * 1000;
    private boolean mSendMacTimeOutShouldReconnect = false;
    private boolean mCurrentDeviceRealConnected = false;
    private static String CMD_SEND_MAC = "";
    public static boolean mIsIconcoxBleConnected = false;
    private MainRepository mainRepository = new MainRepository();


    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            // do work here
            if (locationResult.getLastLocation() != null) {
                mCurrentLocation = locationResult.getLastLocation();
                //Timber.d("Last location : " + mCurrentLocation.getLatitude() + ", " + mCurrentLocation.getLongitude());

                //write last location to prefs only if vehicle does not have a IOT device, else we will honor what is fetched from the server
                Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
                if(vehicle != null && vehicle.getModel() != null)  {
                    if(vehicle.getModel().getProtocol() == null || vehicle.getModel().getProtocol() !=  ModelProtocol.PNP) {
                        SharedPreferences sharedpreferences = getSharedPreferences(ConstantsKt.USER_PREFS, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(ConstantsKt.VEHICLE_LAST_KNOWN_LATITUDE, String.valueOf(mCurrentLocation.getLatitude())).apply();
                        editor.putString(ConstantsKt.VEHICLE_LAST_KNOWN_LONGITUDE, String.valueOf(mCurrentLocation.getLongitude())).apply();
                    }
                }
                EventBus.getDefault().post(new EventBusMessage(GEN_EVENT_PHONE_LOCATION_UPDATED));
                applyGeoFencingRulesIfRequired();
            }
        }
    };


    /**
     * Indicator status constants
     */
    private static int INDICATOR_LEFT = -1;
    private static int INDICATOR_OFF = 0;
    private static int INDICATOR_RIGHT = 1;

    private final int GENERIC_IMG_METADATA_CODE = 40;
    private final int NAVIGATION_IMG_METADATA_CODE = 41;
    private final int BLANK_IMG_METADATA_CODE = 42;
    private final int GEO_FENCE_IMG_CODE = 43;
    private final int SETTINGS_CHANGED_IMG_CODE = 44;

    /**
     * Flag values indicating different type of text messages that are sent to the speedometer
     */

    private final byte NAV_MSG_FLAG = (byte)0xA1;
    private final byte PHONE_MSG_FLAG = (byte)0xA2;
    private final byte GEO_FENCE_MSG_FLAG  = (byte)0xA3;
    private final byte SETTING_CHANGED_MSG_FLAG  = (byte)0xA4;
    private final byte GPS_LOCK_MSG_FLAG   = (byte)0xA5;
    private final byte GPS_WARN_MSG_FLAG = (byte)0xA6;
    private final byte VEHICLE_LOCKED_MSG_FLAG = (byte)0xA7;
    private final byte VEHICLE_UNLOCKED_MSG_FLAG  = (byte)0xA8;
    private final byte HANDLE_LOCKED_MSG_FLAG = (byte)0xA9;
    private final byte HANDLE_UNLOCKED_MSG_FLAG = (byte)0xAA;
    private final byte SEAT_UNLOCKED_MSG_FLAG = (byte)0xAB;
    private final byte SPOTIFY_MSG_FLAG = (byte)0xAC;
    private final byte YOUTUBE_MUSIC_MSG_FLAG = (byte)0xAD;

    /**
     * extra bluetooth states
     */

    private final int LIVE_LOG_FILE_SIZE_LIMIT = 10;

    public static final int STATE_AUTHENTICATING = 5000;
    public static final int STATE_AUTHENTICATION_SUCCESS = 5001;
    public static final int STATE_AUTHENTICATION_FAIL = 5002;


    /**
     * Speedometer dynamic aread dimensions
     */

    private static final int DYNAMIC_AREA_WIDTH = 48;
    private static final int DYNAMIC_AREA_HEIGHT = 32;

    /**
     * Ble manager to handle ble connections
     */
    private REVOSBleManager mRevosBleManager = null;

    public static VehicleParameters mVehicleLiveParameters = null;


    private double mLastSentDistanceFromTurn = -1;
    private Bitmap mLastSentBitmap = null;
    private String mLastSentNavAdviceTitle = null;
    private String mLastSentNavAdviceText = null;


    private String mLastSentMusicText = null;

    //image sequence hashmap
    private static HashMap<Integer, Integer> mImageSequenceHashMap;

    //lcd char to drawable hashmap
    private static HashMap<Character, Integer> mLCDCharToDrawableHashMap;

    private static HashMap<Character, Integer> mLCDCharToCompressedDrawableHashMap;

    private static final int GOOGLE_NOTIFICATION_COOL_DOWN_TIME = 2500;

    /**
     * Expected length of the array containing the command params
     */
    private final int WRITE_PARAMS_COMMAND_LENGTH = 11;

    //private ArrayList<Byte> mImageBufferArrayList;
    private static int BUFFER_LENGTH = 192;

    private static int BIG_BITMAP_BUFFER_LENGTH = 968;

    private static ArrayList<Byte> mLastAttemptedWriteByteArray;

    private static String mBluetoothDeviceAddress;
    private static String mBluetoothDeviceName;
    private static String mDeviceMetadataString;
    private static String mDeviceFirmwareVersion;
    private static int mConnectionState = STATE_DISCONNECTED;

    /**Variables and handler for scanner*/
    private final Handler mHandler = new Handler();
    public static boolean mIsScanning = false;
    private final static long SCAN_DURATION = 30000;
    public static boolean mDeviceFound = false;
    private BluetoothLeScannerCompat mScanner;

    private long mETACircularCount;

    private String mAesKey = null;

    /**
     * flag to show settings changed icon on speedometer
     */
    private boolean mSettingsChangeInitiated = false;

    /**
     * flag to apply model default config once
     */
    private boolean mModelDefaultConfigApplied = false;
    private boolean mModelDefaultConfigChangeInitiated = false;

    private boolean mResetOdometerApplied = false;
    private boolean mResetOdometerInitiated = false;

    /**
     * flag to transfer larger bitmap
     */
    private boolean mLargeBitmapFirstPartTransferInitiated = false;
    private boolean mLargeBitmapFirstPartTransferCompleted = false;
    private boolean mLargeBitmapSecondPartTransferInitiated = false;
    private boolean mLargeBitmapSecondPartTransferCompleted = false;
    private int LARGE_BITMAP_TRANSFER_CODE;

    /**
     * File object for logging the data
     */

    private File mWorkingLiveLogFile = null;
    private File mWorkingOfflineLogFile = null;
    private File mWorkingOfflineBatteryAdcLogFile = null;

    public final static int NOTIFICATION_ID = 1;

    /**
     * flags and variables to detect high beam changes
     */
    private boolean mLastKnownHighBeamState = false;
    private ArrayList<Long> mHighBeamStateChangeTimestampList;
    private final int PROCESS_HIGH_BEAM_STATE_CHANGES = 88;
    private final int HIGH_BEAM_STATE_CHANGE_COOL_OFF_TIME = 750;

    private Handler mHighBeamStateHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == PROCESS_HIGH_BEAM_STATE_CHANGES) {
                AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

                Timber.d("media : processing high beam changes");


                if(mHighBeamStateChangeTimestampList == null) {
                    return false;
                } else if(mHighBeamStateChangeTimestampList.size() == 2) {
                    KeyEvent keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
                    audioManager.dispatchMediaKeyEvent(keyEvent);
                    keyEvent = new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
                    audioManager.dispatchMediaKeyEvent(keyEvent);
                } else if(mHighBeamStateChangeTimestampList.size() == 4) {
                    KeyEvent keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_NEXT);
                    audioManager.dispatchMediaKeyEvent(keyEvent);
                    keyEvent = new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_NEXT);
                    audioManager.dispatchMediaKeyEvent(keyEvent);
                } else if(mHighBeamStateChangeTimestampList.size() == 6) {
                    KeyEvent keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PREVIOUS);
                    audioManager.dispatchMediaKeyEvent(keyEvent);
                    keyEvent = new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_PREVIOUS);
                    audioManager.dispatchMediaKeyEvent(keyEvent);
                } else {
                    mHighBeamStateChangeTimestampList.clear();
                }

                mHighBeamStateChangeTimestampList.clear();
            }
            return false;
        }
    });;

    /**
     * One second timer
     */
    private Timer mOneSecondTimer;

    private static long mLastLiveLogSyncedTimestamp;

    private ArrayList<Float> mAccelerationValuesList;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //clear connection state
        mConnectionState = STATE_DISCONNECTED;

        clearLastSentDistanceAndBitmap();

        Notification notification = createConnectionStatusNotification();

        if(notification == null) {
            EventBus.getDefault().post(new EventBusMessage(KILL_SWITCH));
            super.onStartCommand(intent, flags, startId);
        }

        //clear the notification listener connected flag
        getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, false).apply();

        //make the service foreground and attach a notification to it
        startForeground(NOTIFICATION_ID, createConnectionStatusNotification());

        initializeBleManagerAndScanner();

        initializeImageSequenceHashMap();

        generateLCDCharToDrawableHashMap();

        generateLCDCharToCompressedDrawableHashMap();

        //clear any stored vehicle params
        clearStoredControlParams();

        //register with the EventBus, if not registered already
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        //try connecting to previously saved device
        tryConnectingToLastSavedDevice(false);

        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBluetoothAdapterStateChangeReceiver, filter);

        //initialize one second timer
        initializeTimer();

        clearWorkingLogFiles();

        fetchDeviceDetailsFromServer();

        fetchBatteryRegressionDataFromServer();

        //start location updates
        startLocationUpdates();

        connectToTbitDeviceIfRequired();

        Timber.d("BLE service started");

        return super.onStartCommand(intent, flags, startId);
    }

    private void connectToTbitDeviceIfRequired() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(!TbitBle.hasInitialized()) {
                    TbitBle.initialize(getApplicationContext(), new RevosTbitProtocolAdapter());
                }

                Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

                if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.TBIT) {

                    int tbitBleState = TbitBle.getBleConnectionState();

                    if(tbitBleState != BleClient.STATE_CONNECTING
                            && tbitBleState != BleClient.STATE_CONNECTED
                            && tbitBleState != BleClient.STATE_SCANNING
                            && tbitBleState != BleClient.STATE_SERVICES_DISCOVERED) {
                        postEventDeviceConnectionState(PNP_BT_EVENT_STATE_CONNECTING);
                        TbitBle.connect(device.getDeviceId(), device.getKey(), new ResultCallback() {
                            @Override
                            public void onResult(int resultCode) {
                                //Timber.d("Connection resultCode:" + resultCode);

                                if(resultCode != 0) {
                                    if(TbitBle.hasInitialized()) {
                                        TbitBle.cancelAllCommand();
                                        new Timer().schedule(new TimerTask() {
                                            @Override
                                            public void run() {
                                                postEventDeviceConnectionState(PNP_BT_EVENT_STATE_DISCONNECTED);
                                                connectToTbitDeviceIfRequired();
                                            }
                                        }, 5000);
                                    }
                                } else {
                                    postEventDeviceConnectionState(PNP_BT_EVENT_STATE_CONNECTED);
                                    updateNotification();
                                }
                            }
                        }, new StateCallback() {
                            @Override
                            public void onStateUpdated(BikeState bikeState) {
                                Timber.d("bike state:" + bikeState.toString());
                            }
                        });
                    }
                }
            }
        });
    }

    private void connectToIConCoxDeviceIfRequired() {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

                if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.ICONCOX) {

                    initIConcoxBle();

                    if(!mIsIconcoxBleConnected) {
                        connectToIconcoxByMac(device.getMacId());
                    }
                }
            }
        });
    }

    private void initIConcoxBle() {
        Timber.d("iConcox init BLE");

        BleConfig.getInstance()
                //设置是否输出日志,默认关闭（可选）
                .setEnableLog(true)
                .setServiceUuid(UUID.fromString("0000FEE6-0000-1000-8000-00805f9b34fb"))
                .setReadUuid(UUID.fromString("0000FEC6-0000-1000-8000-00805f9b34fb"))
                .setWriteUuid(UUID.fromString("0000FEC5-0000-1000-8000-00805f9b34fb"))
                //设置扫描指定UUID[]服务的设备,默认扫描全部设备（可选）
                .setServiceUuids(new UUID[]{UUID.fromString("0000FEE6-0000-1000-8000-00805F9B34FB")})
                //设置扫描指定Mac地址
                //.setDeviceMac("B4:A8:28:04:4B:23")（可选）
                //模糊匹配 只要设备名字是String[]的任一元素子串（可选）
                //.setDeviceName(true,new String[]{"NoKeLock1","NoKeLock2"})
                //精准匹配 名字完全相同（可选）
                //.setDeviceName(false,new String[]{deviceName})
                //设置扫描超时时间,默认10s（可选）
                .setScanTimeout(10 * 1000)
                //设置连接超时时间 默认10s （可选）
                .setConnectTimeOut(10 * 1000)
                //只要失败一直重新连接
                .setConnectRetryAllTime(true)
                //设置重连次数 默认3次 （可选）
                //.setConnectRetryCount(3)
                //设置重连时间间隔 5 s 默认10s （可选）
                .setConnectRetryInterval(1 * 1000)
                //发送命令超时时间 默认10s （可选）
                .setSendCmdTimeOut(5 * 1000);
        IconCoxBleManager.getInstance(this);
        IconCoxBleManager.getInstance(this).addOnBleManagerListener(this);
    }

    private void connectToIconcoxByMac(String mac) {
        if (mSendMacTimeOutShouldReconnect) {
        } else if (mCurrentDeviceRealConnected) {
            return;
        }

        updateIsIconcoxConnectedState(false);

        IconCoxBleManager.getInstance(getApplicationContext())
                .disconnect();

        //check if Bluetooth is available or not
        if (IconCoxBleManager.getInstance(getApplicationContext()).isEnabled()) {
            startIconcoxBleScan();

            //Prevent switching devices and remove previous connections
            handler.removeMessages(MSG_CONNECT_FIRST_TIME_DELAY);
            Message msg = handler.obtainMessage();
            msg.what = MSG_CONNECT_FIRST_TIME_DELAY;
            handler.sendMessageDelayed(msg, 3 * 1000);
            Timber.d("iConcox BL ==doConnectBleByMac==%s", mac);
        } else {
            //TODO:: show BT not avaialble message?
        }
    }

    private void updateIsIconcoxConnectedState(boolean state) {
        mCurrentDeviceRealConnected = state;
        //getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putBoolean(BLUETOOTH_ICONCOX_IS_CONNECTED, state).apply();

        if(state) {
            Message msg = handler.obtainMessage();
            msg.obj = state;
            msg.what = MSG_SEND_DELAY_UPDATA_BL_STATUS_CONNECTED;
            handler.sendMessageDelayed(msg, 0);

        } else {
            handler.removeMessages(MSG_SEND_MAC_TIME_OUT);
            handler.removeMessages(MSG_SEND_DELAY_UPDATA_BL_STATUS_CONNECTED);
            Message msg = handler.obtainMessage();
            msg.obj = state;
            msg.what = MSG_SEND_DELAY_UPDATA_BL_STATUS_DISCONNECTED;
            handler.sendMessageDelayed(msg, 0);
        }
    }

    private void startIconcoxBleScan() {
        Timber.d(" BL=== iConcox Start Scan: ===startScan ===searchDevice ");
        IconCoxBleManager.getInstance(getApplicationContext()).searchDevice(false);
        IconCoxBleManager.getInstance(getApplicationContext()).searchDevice(true);
    }

    private void doSendMac() {
        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

        String mac = device.getMacId();

        if(mac == null) {
            return;
        }

        String[] macs = mac.split(":");
        String tempMac = "";
        for (int i = 0; i < macs.length; i++) {
            tempMac += macs[macs.length - 1 - i];
        }
        CMD_SEND_MAC = "23240600" + tempMac + "0D0A";

        doSendCmd(CMD_SEND_MAC);

    }

    private void doSendCmd(String cmd) {
        if (!CMD_SEND_MAC.equals(cmd)) {
            handler.sendEmptyMessageDelayed(MSG_SEND_TIME_OUT, MSG_SEND_TIME_OUT_DELAY);
        }
        if (!CMD_SEND_MAC.equals(cmd)) {
            Timber.d("Bluetooth send commands, raw data:%s", cmd);
        } else {
            Timber.d("Bluetooth send address verification, original data:%s", cmd);
        }

        Timber.d("BL===Send instructions, encrypted data:%s", cmd);

        cmd = NativeController.encode8String(TypeConvert.hexStringToBytes(cmd));

        Timber.d("BL===Send instructions after encoding, encrypted data:%s", cmd);

        IconCoxBleManager.getInstance(getApplicationContext()).sendHexStringCmd(cmd);
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SEND_DELAY_SEND_MAC:
                    mSendMacTimeOutShouldReconnect = false;
                    doSendMac();
                    handler.removeMessages(MSG_SEND_MAC_TIME_OUT);
                    Message msg2 = new Message();
                    msg2.what = MSG_SEND_MAC_TIME_OUT;
                    handler.sendMessageDelayed(msg2, TIME_MSG_SEND_MAC_TIME_OUT);
                    break;
                case MSG_SEND_MAC_TIME_OUT:
                    handler.removeMessages(MSG_SEND_MAC_TIME_OUT);
                    mSendMacTimeOutShouldReconnect = true;
                    Timber.d("Address verification timed out, actively disconnect and reconnect");
                    break;
                case MSG_SEND_DELAY_UPDATA_BL_STATUS_DISCONNECTED:
                    postEventDeviceConnectionState(PNP_BT_EVENT_STATE_DISCONNECTED);
                    mIsIconcoxBleConnected = (boolean) msg.obj;
                    break;

                case MSG_SEND_DELAY_UPDATA_BL_STATUS_CONNECTED:
                    postEventDeviceConnectionState(PNP_BT_EVENT_STATE_CONNECTED);
                    Timber.d("case MSG_SEND_DELAY_UPDATA_BL_STATUS_DISCONNECTED : | MSG_SEND_DELAY_UPDATA_BL_STATUS_CONNECTED");
                    mIsIconcoxBleConnected = (boolean) msg.obj;

                    //Not connected, you can do some ui processing logic
                    break;
                //Link Bluetooth
                case MSG_CONNECT_FIRST_TIME_DELAY:
                    postEventDeviceConnectionState(PNP_BT_EVENT_STATE_CONNECTING);
                    Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

                    if(device != null && device.getMacId() != null && !device.getMacId().isEmpty()) {
                        IconCoxBleManager.getInstance(getApplicationContext()).connect(device.getMacId());
                    }
                    break;
                case MSG_SEND_TIME_OUT:
                    //Toast.makeText(getApplicationContext(), "Command sending timeout", Toast.LENGTH_SHORT).show();
                    break;
                case MSG_SEND_SCAN:
                    if (!mIsIconcoxBleConnected) {
                        IconCoxBleManager.getInstance(getApplicationContext()).searchDevice();
                        Timber.d("iConcox BL=== Start scanning:===searchDevice===");
                    } else {
                        Timber.d("iConcox BL=== mConnected   Don't scan:===searchDevice===");
                    }
                    handler.removeMessages(MSG_SEND_SCAN);
                    Message msg1 = handler.obtainMessage();
                    msg1.what = MSG_SEND_SCAN;
                    handler.sendMessageDelayed(msg1, 10 * 1000);
                    break;
                default:
                    break;

            }
        }
    };

    private void applyGeoFencingRulesIfRequired() {
        String geoFencingDataStr = getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).getString(GEO_FENCING_JSON_DATA_KEY, null);
        if(geoFencingDataStr == null) {
            //this means geo fence is not set
            if(mDeviceFirmwareVersion != null && !mDeviceFirmwareVersion.isEmpty() && (new Version(mDeviceFirmwareVersion).compareTo(new Version(VER_1_1)) > 0)
                    && mConnectionState == STATE_CONNECTED && mVehicleLiveParameters != null) {

                //remove geo fence wheel lock if it is set
                if(mVehicleLiveParameters.isGeoFencingLockStatus()) {
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());
                            byte[] geoFenceLockByteArray = vehicleControlHelper.getGeoFencingWheelLockControlByteArray(false);
                            mRevosBleManager.writeDataToOBDCharacteristic(geoFenceLockByteArray);
                        }
                    });
                }

                //check if wheel lock is set, if yes then remove it
                if(mVehicleLiveParameters.isGenericWheelLockStatus()) {
                    new Handler(getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());
                            byte[] genericWheelLockByteArray = vehicleControlHelper.getGenericWheelLockControlByteArray(false);
                            mRevosBleManager.writeDataToOBDCharacteristic(genericWheelLockByteArray);
                        }
                    }, 1000);
                }

                //clear gps lock image if it is being shown
                if(mVehicleLiveParameters.getDisplayedImageCode() == GEO_FENCE_IMG_CODE) {
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmapOptions.inScaled = false;
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blank_48_x_32, bitmapOptions);
                    new Handler(getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(isColorSpeedometer()) {
                                sendCmdToClearImgOrMsgOnColorSpeedometer(true, true, true, true, true);
                            } else {
                                transferBitmap(bitmap, BLANK_IMG_METADATA_CODE);
                            }
                        }
                    }, 2000);
                }
            }
            return;
        }

        GeoFencing geoFencing = new Gson().fromJson(geoFencingDataStr, GeoFencing.class);
        boolean isInside = false;
        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        if(geoFencing.getGeoFenceType() == FenceType.RADIAL) {
            isInside = GeoFenceUtil.isInsideCircle(currentLatLng, geoFencing.getCircleGeoFenceCentre(), geoFencing.getCircleRadius());
            Timber.d("Geo fence : Circle " + isInside);
        } else if(geoFencing.getGeoFenceType() == FenceType.POLYGON) {
            isInside = GeoFenceUtil.isInsidePolygon(currentLatLng, geoFencing.getPolygonVerticesList());
            Timber.d("Geo fence : Polygon " + isInside);
        }

        //this means geo fence exists for this vehicle, set geo fence wheel lock for vehicle
        if(mDeviceFirmwareVersion != null && !mDeviceFirmwareVersion.isEmpty() && (new Version(mDeviceFirmwareVersion).compareTo(new Version(VER_1_1)) > 0)
                && mConnectionState == STATE_CONNECTED && mVehicleLiveParameters != null && !mVehicleLiveParameters.isGeoFencingLockStatus()) {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());
                    byte[] geoFenceLockByteArray = vehicleControlHelper.getGeoFencingWheelLockControlByteArray(true);
                    mRevosBleManager.writeDataToOBDCharacteristic(geoFenceLockByteArray);
                }
            });
        }

        //lock/unlock the generic wheel based on geo-fence breach condition
        if(mDeviceFirmwareVersion != null && !mDeviceFirmwareVersion.isEmpty() && (new Version(mDeviceFirmwareVersion).compareTo(new Version(VER_1_1)) > 0)
                && mConnectionState == STATE_CONNECTED && mVehicleLiveParameters != null) {

            boolean genericWheelLockStatus = mVehicleLiveParameters.isGenericWheelLockStatus();
            //vehicle is outside and wheel lock is not set, SET IT
            if(!isInside && !genericWheelLockStatus) {
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());

                        byte[] genericWheelLockByteArray = vehicleControlHelper.getGenericWheelLockControlByteArray(true);
                        mRevosBleManager.writeDataToOBDCharacteristic(genericWheelLockByteArray);
                    }
                }, 1000);
            } else if(isInside && genericWheelLockStatus) {
                //vehicle is inside and wheel lock is set, REMOVE IT
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());

                        byte[] genericWheelLockByteArray = vehicleControlHelper.getGenericWheelLockControlByteArray(false);
                        mRevosBleManager.writeDataToOBDCharacteristic(genericWheelLockByteArray);
                    }
                }, 1000);
            }

            //vehicle is outside, now either the generic wheel lock is engaged or not
            //if it is engaged then show lock icon, else show warning icon
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inScaled = false;
            int drawableId = -1;
            if(!isInside) {
                //wheel lock is engaged
                if(mVehicleLiveParameters.isGenericWheelLockEngaged()) {
                    drawableId = R.drawable.gps_lock;
                } else if(!mVehicleLiveParameters.isGenericWheelLockEngaged()) {
                    drawableId = R.drawable.gps_warning;
                }

            } else if(mVehicleLiveParameters.getDisplayedImageCode() == GEO_FENCE_IMG_CODE) {
                //vehicle is inside, remove the
                drawableId = R.drawable.blank_48_x_32;
            }

            //if an icon has been selected
            if(drawableId != -1) {
                int gpsInfoDrawableId = drawableId;
                new Handler(getMainLooper()).post(new Runnable() {
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), gpsInfoDrawableId, bitmapOptions);
                    @Override
                    public void run() {
                        if(gpsInfoDrawableId == R.drawable.blank_48_x_32) {
                            if(isColorSpeedometer()) {
                                sendCmdToClearImgOrMsgOnColorSpeedometer(true, true, true, true, true);
                            } else {
                                transferBitmap(bitmap, BLANK_IMG_METADATA_CODE);
                            }
                        } else {
                            if(isColorSpeedometer())  {
                                showColorSpeedometerGeofenceRelatedImage(gpsInfoDrawableId);
                            } else {
                                transferBitmap(bitmap, GEO_FENCE_IMG_CODE);
                            }
                        }
                    }
                });
            }
        }
    }



    private void sendShowPhoneIconCommandForColorSpeedometer(String displayString) {
        sendCmdToUpdateMsgOnColorSpeedometer(PHONE_MSG_FLAG, true, 2, displayString);
    }

    private void showColorSpeedometerGeofenceRelatedImage(int resourceId) {
        if(resourceId == R.drawable.gps_lock) {
            sendCmdToUpdateMsgOnColorSpeedometer(GPS_LOCK_MSG_FLAG, true, 2, getString(R.string.obd_error_vehicle_outside_bounds));
        } else if(resourceId == R.drawable.gps_warning) {
            sendCmdToUpdateMsgOnColorSpeedometer(GPS_WARN_MSG_FLAG, true, 2, getString(R.string.obd_error_vehicle_outside_bounds));
        }
    }

    private void sendShowSettingsChangedIconCommandForColorSpeedometer() {
        sendCmdToUpdateMsgOnColorSpeedometer(SETTING_CHANGED_MSG_FLAG, true, 2, getString(R.string.settings_applied));
    }

    private void sendShowVehicleLockedIconCommandForColorSpeedometer() {
        sendCmdToUpdateMsgOnColorSpeedometer(VEHICLE_LOCKED_MSG_FLAG, true, 2, getString(R.string.vehicle_locked_msg));
    }

    private void sendShowVehicleUnlockedIconCommandForColorSpeedometer() {
        sendCmdToUpdateMsgOnColorSpeedometer(VEHICLE_UNLOCKED_MSG_FLAG, true, 2, getString(R.string.vehicle_unlocked_msg));
    }

    private void sendShowHandleLockedIconCommandForColorSpeedometer() {
        sendCmdToUpdateMsgOnColorSpeedometer(HANDLE_LOCKED_MSG_FLAG, true, 2, getString(R.string.handle_locked));
    }

    private void sendShowHandleUnlockedIconCommandForColorSpeedometer() {
        sendCmdToUpdateMsgOnColorSpeedometer(HANDLE_UNLOCKED_MSG_FLAG, true, 2, getString(R.string.handle_unlocked));
    }

    private void sendShowSeatUnlockedIconCommandForColorSpeedometer() {
        sendCmdToUpdateMsgOnColorSpeedometer(SEAT_UNLOCKED_MSG_FLAG, true, 2, getString(R.string.seat_unlocked));
    }

    private void sendShowSpotifyIconCommandForColorSpeedometer(String tickerText) {
        if(mRevosBleManager.getConnectionState() != STATE_CONNECTED) {
            return;
        }
        mLastSentMusicText = tickerText;
        sendCmdToUpdateMsgOnColorSpeedometer(SPOTIFY_MSG_FLAG, true, 2, tickerText);
    }

    private void sendShowYoutubeMusicIconCommandForColorSpeedometer(String tickerText) {
        if(mRevosBleManager.getConnectionState() != STATE_CONNECTED) {
            return;
        }
        mLastSentMusicText = tickerText;
        sendCmdToUpdateMsgOnColorSpeedometer(YOUTUBE_MUSIC_MSG_FLAG, true, 2, tickerText);
    }

    private void sendCmdToUpdateMsgOnColorSpeedometer(byte flag, boolean loadImageWithMessage, int speedometerTextViewId, String message) {
        byte textView1OperationFlag = (0x01 << 0x00);
        byte textView2OperationFlag = (0x01 << 0x01);
        byte textView3OperationFlag = (0x01 << 0x02);
        byte textView4OperationFlag = (0x01 << 0x03);
        byte imageOperationFlag = (0x01 << 0x04);

        byte[] byteArrayToBeSent = new byte[1 + 1 + 1 + message.getBytes(Charset.forName("UTF-16LE")).length];
        byte[] messageBytes = message.getBytes(Charset.forName("UTF-16LE"));

        //generate operation flag
        byte overallOperationFlag = 0;
        if(loadImageWithMessage) {
            overallOperationFlag = (byte)(overallOperationFlag | imageOperationFlag);
        }

        if(speedometerTextViewId == 1) {
            overallOperationFlag = (byte)(overallOperationFlag | textView1OperationFlag);
        }

        if(speedometerTextViewId == 2) {
            overallOperationFlag = (byte)(overallOperationFlag | textView2OperationFlag);
        }

        if(speedometerTextViewId == 3) {
            overallOperationFlag = (byte)(overallOperationFlag | textView3OperationFlag);
        }

        if(speedometerTextViewId == 4) {
            overallOperationFlag = (byte)(overallOperationFlag | textView4OperationFlag);
        }

        byteArrayToBeSent[0] = flag;
        byteArrayToBeSent[1] = overallOperationFlag;
        byteArrayToBeSent[2] = (byte)messageBytes.length;
        System.arraycopy(messageBytes, 0, byteArrayToBeSent, 3, messageBytes.length);

        mRevosBleManager.writeDataToIMGCharacteristic(byteArrayToBeSent);
    }

    private void sendCmdToClearImgOrMsgOnColorSpeedometer(boolean clearImage,
                                                          boolean clearTextView1,
                                                          boolean clearTextView2,
                                                          boolean clearTextView3,
                                                          boolean clearTextView4) {
        if(!clearImage && !clearTextView1 && !clearTextView2) {
            return;
        }

        byte textView1OperationFlag = (0x01 << 0x00);
        byte textView2OperationFlag = (0x01 << 0x01);
        byte textView3OperationFlag = (0x01 << 0x02);
        byte textView4OperationFlag = (0x01 << 0x03);
        byte imageOperationFlag = (0x01 << 0x04);

        //generate operation flag
        byte overallOperationFlag = 0;
        if(clearTextView1) {
            overallOperationFlag = (byte)(overallOperationFlag | textView1OperationFlag);
        }

        if(clearTextView2) {
            overallOperationFlag = (byte)(overallOperationFlag | textView2OperationFlag);
        }

        if(clearTextView3) {
            overallOperationFlag = (byte)(overallOperationFlag | textView3OperationFlag);
        }

        if(clearTextView4) {
            overallOperationFlag = (byte)(overallOperationFlag | textView4OperationFlag);
        }

        if(clearImage) {
            overallOperationFlag = (byte)(overallOperationFlag | imageOperationFlag);
        }

        //this flag can be anything, I've just put NAV_MSG_FLAG
        byte[] byteArrayToBeSent = new byte[] {NAV_MSG_FLAG, overallOperationFlag, 0};

        mRevosBleManager.writeDataToIMGCharacteristic(byteArrayToBeSent);
    }

    private void startLocationUpdates() {

        // Create the location request to start receiving updates
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private void clearLastSentDistanceAndBitmap() {
        mLastSentDistanceFromTurn = -1;
        mLastSentBitmap = null;
    }

    private void initializeTimer() {
        if(mOneSecondTimer == null) {
            mOneSecondTimer = new Timer();
        }

        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putLong(BLUETOOTH_SERVICE_LAST_ACTIVE_TIMESTAMP, System.currentTimeMillis()).apply();
                //check if no google notification has been posted in last 3 seconds, if yes then google navigation has been turned off and we need to reset the last
                //distance and arrow sent to the speedometer
                //we also need to clear the current arrow being displayed
                long lastGoogleNotificationTimestamp = getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getLong(LAST_GOOGLE_NOTIFICATION_TIMESTAMP, 0);
                boolean previousGoogleNavActiveState = getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getBoolean(IS_GOOGLE_NAV_ACTIVE_KEY, false);
                if(Math.abs(System.currentTimeMillis() - lastGoogleNotificationTimestamp) > GOOGLE_NOTIFICATION_COOL_DOWN_TIME) {
                    if(!previousGoogleNavActiveState) {
                        if(PhoneStateReceiver.currentState == TelephonyManager.CALL_STATE_RINGING) {
                            return;
                        }
                        if(mVehicleLiveParameters != null && mVehicleLiveParameters.getDisplayedImageCode() == NAVIGATION_IMG_METADATA_CODE) {
                            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CLEAR_IMAGE));
                        }
                    }
                }

                sendEmergencySMSIfRequired();

                //TBIT devices get disconnected very often, hence, this logic runs every 10 seconds
                if(((System.currentTimeMillis() / 1000) % 10) == 0) {
                    connectToTbitDeviceIfRequired();
                }

                //update music info
                updateMusicInfoOnColorSpeedometerIfRequired();

                //clear call icon if required
                clearCallIconIfRequired();

            }
        }, 100, 1000);
    }

    private void clearTimer() {
        if(mOneSecondTimer == null) {
            return;
        }
        mOneSecondTimer.cancel();
        mOneSecondTimer.purge();
    }

    private void selfDestruct() {
        //cancel the timer
        clearTimer();

        stopForeground(true);
        stopSelf();
    }

    private void clearCallIconIfRequired() {
        TelephonyManager telephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyMgr != null) {
            Timber.d("call_state: %s", telephonyMgr.getCallState());
            PhoneStateReceiver.currentState = telephonyMgr.getCallState();
        }

        if(mVehicleLiveParameters == null) {
            return;
        }

        byte imageMetadataCode = (byte)mVehicleLiveParameters.getDisplayedImageCode();

        if(imageMetadataCode == PHONE_MSG_FLAG && telephonyMgr.getCallState() != TelephonyManager.CALL_STATE_RINGING) {
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CLEAR_IMAGE));
        }
    }

    private void updateMusicInfoOnColorSpeedometerIfRequired() {
        if(!isColorSpeedometer()) { // (|| mVehicleLiveParameters == null)
            return;
        }

        AudioManager audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);

        //long imageMetadataCode = mVehicleLiveParameters.getDisplayedImageCode();

        if(!audioManager.isMusicActive()) { //&& ((byte)imageMetadataCode == SPOTIFY_MSG_FLAG || (byte)imageMetadataCode == YOUTUBE_MUSIC_MSG_FLAG)
            //this condition means music is not playing and we're still showing SPOTIFY or YOUTUBE notification on the speedometer
            //we need to clear this notification

            mLastSentMusicText = null;
            clearSpeedometerImageAndTextHelper();
        } else if(audioManager.isMusicActive()) {
            //this means some music is playing
            //first lets check which app is active

            if(NotificationReaderService.mActiveMusicPlayerPackageName == null || NotificationReaderService.mActiveMusicPlayerPackageName.isEmpty()
                || (!NotificationReaderService.mActiveMusicPlayerPackageName.equals(SPOTIFY_PACKAGE_NAME) && !NotificationReaderService.mActiveMusicPlayerPackageName.equals(YOUTUBE_MUSIC_PACKAGE_NAME))) {
                //seems like music is playing but it's neither spotify nor youtube music
                //in this case we need to clear the notification
                mLastSentMusicText = null;
                clearSpeedometerImageAndTextHelper();
                return;
            }

            if(NotificationReaderService.mActiveMusicPlayerPackageName.equals(SPOTIFY_PACKAGE_NAME) || NotificationReaderService.mActiveMusicPlayerPackageName.equals(YOUTUBE_MUSIC_PACKAGE_NAME)) {
                //this means music is playing and it's either spotify or youtube music

                //now let's see if there's change in the text or not
                if(NotificationReaderService.mActiveMusicPlayerPackageName.equals(SPOTIFY_PACKAGE_NAME) && NotificationReaderService.mSpotifyNotification != null) {
                    String tickerText = NotificationReaderService.mSpotifyNotification.tickerText.toString();
                    if(mLastSentMusicText == null || mLastSentMusicText.isEmpty()) {
                        //nothing is being displayed, we need to show a notification
                        sendShowSpotifyIconCommandForColorSpeedometer(tickerText);
                    } else if(!mLastSentMusicText.equals(tickerText)) {
                        //song has been changed, we need to update the notification
                        sendShowSpotifyIconCommandForColorSpeedometer(tickerText);
                    }
                } else if(NotificationReaderService.mActiveMusicPlayerPackageName.equals(YOUTUBE_MUSIC_PACKAGE_NAME) && NotificationReaderService.mYoutubeMusicNotification != null) {
                    String tickerText = NotificationReaderService.mYoutubeMusicNotification.tickerText.toString();
                    if(mLastSentMusicText == null || mLastSentMusicText.isEmpty()) {
                        //nothing is being displayed, we need to show a notification
                        sendShowYoutubeMusicIconCommandForColorSpeedometer(tickerText);
                    } else if(!mLastSentMusicText.equals(tickerText)) {
                        //song has been changed, we need to update the notification
                        sendShowYoutubeMusicIconCommandForColorSpeedometer(tickerText);
                    }
                }
            }
        }
    }

    private Notification createConnectionStatusNotification() {

        // You only need to create the channel on API 26+ devices
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }

        Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(getPackageName());

        if(launchIntentForPackage == null) {
            return null;
        }

        Intent resumeAppIntent = launchIntentForPackage.setPackage(null).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

        PendingIntent resumeAppPendingIntent = PendingIntent.getActivity(getApplicationContext(), (int)System.currentTimeMillis(), resumeAppIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent stopSignalIntent = PendingIntent.getBroadcast(getApplicationContext(), (int)System.currentTimeMillis(), new Intent(getApplicationContext(), StopSignalReceiver.class), 0);

        //icon is added for the action just to support pre-nougat devices
        NotificationCompat.Builder notificationCompatBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                                                    .setContentTitle(getString(R.string.revos_running))
                                                    .setSmallIcon(R.drawable.ic_notification)
                                                    .setChannelId(CHANNEL_ID)
                                                    .setContentText(getConnectionStatusString())
                                                    .setPriority(Notification.PRIORITY_MAX)
                                                    .setStyle(new NotificationCompat.BigTextStyle())
                                                    .setContentIntent(resumeAppPendingIntent)
                                                    .addAction(R.drawable.ic_notification, getString(R.string.stop), stopSignalIntent)
                                                    .setColor(ContextCompat.getColor(getApplicationContext(), R.color.color_notification_accent_color));

        return notificationCompatBuilder.build();
    }

    private void updateNotification() {
        Timber.d("Device connection status : ");
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null) {
            Notification notification = createConnectionStatusNotification();
            if(notification != null) {
                notificationManager.notify(NOTIFICATION_ID, notification);
            }
        }
    }

    private String getConnectionStatusString() {
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        if(vehicle != null && vehicle.getModel() != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            return "";
        }

        String connectionStatus = getString(R.string.disconnected);
        if(mRevosBleManager == null) {
            return connectionStatus;
        }

        switch (mConnectionState) {
            case STATE_CONNECTED : return getString(R.string.connected);
            case STATE_CONNECTING : return  getString(R.string.connecting);
            case STATE_DISCONNECTED : return  getString(R.string.disconnected);
            default: return connectionStatus;
        }
    }

    private static final String CHANNEL_ID = "revos_connection_status_channel";

    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationManager
                mNotificationManager =
                (NotificationManager) getApplicationContext()
                        .getSystemService(Context.NOTIFICATION_SERVICE);
        // The id of the channel.
        String id = CHANNEL_ID;
        // The user-visible name of the channel.
        CharSequence name = getString(R.string.channel_name);
        // The user-visible description of the channel.
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        mNotificationManager.createNotificationChannel(mChannel);
    }


    private void initializeBleManagerAndScanner() {
        mRevosBleManager = new REVOSBleManager(this);
        mScanner = BluetoothLeScannerCompat.getScanner();
    }

    private void initializeImageSequenceHashMap() {
        if(mImageSequenceHashMap == null) {
            mImageSequenceHashMap = new HashMap<>();

            mImageSequenceHashMap.put(0, R.drawable.screen_1_48_x_32);
            mImageSequenceHashMap.put(1, R.drawable.screen_2_48_x_32);
            mImageSequenceHashMap.put(2, R.drawable.screen_3_48_x_32);
            mImageSequenceHashMap.put(3, R.drawable.screen_4_48_x_32);
            mImageSequenceHashMap.put(4, R.drawable.screen_5_48_x_32);
            mImageSequenceHashMap.put(5, R.drawable.screen_6_48_x_32);
            mImageSequenceHashMap.put(6, R.drawable.screen_7_48_x_32);
        }
    }

    private void generateLCDCharToDrawableHashMap() {
        if(mLCDCharToDrawableHashMap == null) {
            mLCDCharToDrawableHashMap = new HashMap<>();
        }

        mLCDCharToDrawableHashMap.put('A', R.drawable.letter_cap_a);
        mLCDCharToDrawableHashMap.put('B', R.drawable.letter_cap_b);
        mLCDCharToDrawableHashMap.put('C', R.drawable.letter_cap_c);
        mLCDCharToDrawableHashMap.put('D', R.drawable.letter_cap_d);
        mLCDCharToDrawableHashMap.put('E', R.drawable.letter_cap_e);
        mLCDCharToDrawableHashMap.put('F', R.drawable.letter_cap_f);
        mLCDCharToDrawableHashMap.put('G', R.drawable.letter_cap_g);
        mLCDCharToDrawableHashMap.put('H', R.drawable.letter_cap_h);
        mLCDCharToDrawableHashMap.put('I', R.drawable.letter_cap_i);
        mLCDCharToDrawableHashMap.put('J', R.drawable.letter_cap_j);
        mLCDCharToDrawableHashMap.put('K', R.drawable.letter_cap_k);
        mLCDCharToDrawableHashMap.put('L', R.drawable.letter_cap_l);
        mLCDCharToDrawableHashMap.put('M', R.drawable.letter_cap_m);
        mLCDCharToDrawableHashMap.put('N', R.drawable.letter_cap_n);
        mLCDCharToDrawableHashMap.put('O', R.drawable.letter_cap_o);
        mLCDCharToDrawableHashMap.put('P', R.drawable.letter_cap_p);
        mLCDCharToDrawableHashMap.put('Q', R.drawable.letter_cap_q);
        mLCDCharToDrawableHashMap.put('R', R.drawable.letter_cap_r);
        mLCDCharToDrawableHashMap.put('S', R.drawable.letter_cap_s);
        mLCDCharToDrawableHashMap.put('T', R.drawable.letter_cap_t);
        mLCDCharToDrawableHashMap.put('U', R.drawable.letter_cap_u);
        mLCDCharToDrawableHashMap.put('V', R.drawable.letter_cap_v);
        mLCDCharToDrawableHashMap.put('W', R.drawable.letter_cap_w);
        mLCDCharToDrawableHashMap.put('X', R.drawable.letter_cap_x);
        mLCDCharToDrawableHashMap.put('Y', R.drawable.letter_cap_y);
        mLCDCharToDrawableHashMap.put('Z', R.drawable.letter_cap_z);
        mLCDCharToDrawableHashMap.put(' ', R.drawable.letter_space);
        mLCDCharToDrawableHashMap.put('+', R.drawable.letter_plus_sign);
        mLCDCharToDrawableHashMap.put(':', R.drawable.letter_colon);

        mLCDCharToDrawableHashMap.put('k', R.drawable.small_k);
        mLCDCharToDrawableHashMap.put('m', R.drawable.small_m);

        mLCDCharToDrawableHashMap.put('.', R.drawable.num_decimal);
        mLCDCharToDrawableHashMap.put('0', R.drawable.num_0);
        mLCDCharToDrawableHashMap.put('1', R.drawable.num_1);
        mLCDCharToDrawableHashMap.put('2', R.drawable.num_2);
        mLCDCharToDrawableHashMap.put('3', R.drawable.num_3);
        mLCDCharToDrawableHashMap.put('4', R.drawable.num_4);
        mLCDCharToDrawableHashMap.put('5', R.drawable.num_5);
        mLCDCharToDrawableHashMap.put('6', R.drawable.num_6);
        mLCDCharToDrawableHashMap.put('7', R.drawable.num_7);
        mLCDCharToDrawableHashMap.put('8', R.drawable.num_8);
        mLCDCharToDrawableHashMap.put('9', R.drawable.num_9);
    }

    private void generateLCDCharToCompressedDrawableHashMap() {
        if(mLCDCharToCompressedDrawableHashMap == null) {
            mLCDCharToCompressedDrawableHashMap = new HashMap<>();
        }

        mLCDCharToCompressedDrawableHashMap.put('A', R.drawable.letter_cap_a);
        mLCDCharToCompressedDrawableHashMap.put('B', R.drawable.letter_cap_b);
        mLCDCharToCompressedDrawableHashMap.put('C', R.drawable.letter_cap_c);
        mLCDCharToCompressedDrawableHashMap.put('D', R.drawable.letter_cap_d);
        mLCDCharToCompressedDrawableHashMap.put('E', R.drawable.letter_cap_e);
        mLCDCharToCompressedDrawableHashMap.put('F', R.drawable.letter_cap_f);
        mLCDCharToCompressedDrawableHashMap.put('G', R.drawable.letter_cap_g);
        mLCDCharToCompressedDrawableHashMap.put('H', R.drawable.letter_cap_h);
        mLCDCharToCompressedDrawableHashMap.put('I', R.drawable.letter_cap_i);
        mLCDCharToCompressedDrawableHashMap.put('J', R.drawable.letter_cap_j);
        mLCDCharToCompressedDrawableHashMap.put('K', R.drawable.letter_cap_k);
        mLCDCharToCompressedDrawableHashMap.put('L', R.drawable.letter_cap_l);
        mLCDCharToCompressedDrawableHashMap.put('M', R.drawable.letter_cap_m);
        mLCDCharToCompressedDrawableHashMap.put('N', R.drawable.letter_cap_n);
        mLCDCharToCompressedDrawableHashMap.put('O', R.drawable.letter_cap_o);
        mLCDCharToCompressedDrawableHashMap.put('P', R.drawable.letter_cap_p);
        mLCDCharToCompressedDrawableHashMap.put('Q', R.drawable.letter_cap_q);
        mLCDCharToCompressedDrawableHashMap.put('R', R.drawable.letter_cap_r);
        mLCDCharToCompressedDrawableHashMap.put('S', R.drawable.letter_cap_s);
        mLCDCharToCompressedDrawableHashMap.put('T', R.drawable.letter_cap_t);
        mLCDCharToCompressedDrawableHashMap.put('U', R.drawable.letter_cap_u);
        mLCDCharToCompressedDrawableHashMap.put('V', R.drawable.letter_cap_v);
        mLCDCharToCompressedDrawableHashMap.put('W', R.drawable.letter_cap_w);
        mLCDCharToCompressedDrawableHashMap.put('X', R.drawable.letter_cap_x);
        mLCDCharToCompressedDrawableHashMap.put('Y', R.drawable.letter_cap_y);
        mLCDCharToCompressedDrawableHashMap.put('Z', R.drawable.letter_cap_z);
        mLCDCharToCompressedDrawableHashMap.put(' ', R.drawable.letter_space);
        mLCDCharToCompressedDrawableHashMap.put('+', R.drawable.letter_plus_sign);
        mLCDCharToCompressedDrawableHashMap.put(':', R.drawable.letter_colon);


        mLCDCharToCompressedDrawableHashMap.put('k', R.drawable.small_k);
        mLCDCharToCompressedDrawableHashMap.put('m', R.drawable.small_m);

        mLCDCharToCompressedDrawableHashMap.put('.', R.drawable.num_decimal_cmp);
        mLCDCharToCompressedDrawableHashMap.put('0', R.drawable.num_0_cmp);
        mLCDCharToCompressedDrawableHashMap.put('1', R.drawable.num_1_cmp);
        mLCDCharToCompressedDrawableHashMap.put('2', R.drawable.num_2_cmp);
        mLCDCharToCompressedDrawableHashMap.put('3', R.drawable.num_3_cmp);
        mLCDCharToCompressedDrawableHashMap.put('4', R.drawable.num_4_cmp);
        mLCDCharToCompressedDrawableHashMap.put('5', R.drawable.num_5_cmp);
        mLCDCharToCompressedDrawableHashMap.put('6', R.drawable.num_6_cmp);
        mLCDCharToCompressedDrawableHashMap.put('7', R.drawable.num_7_cmp);
        mLCDCharToCompressedDrawableHashMap.put('8', R.drawable.num_8_cmp);
        mLCDCharToCompressedDrawableHashMap.put('9', R.drawable.num_9_cmp);
    }


    private void clearStoredControlParams() {
        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putString(BLUETOOTH_EXPECTED_CONTROLLER_PARAMS_KEY, null).apply();
    }

    private void tryConnectingToLastSavedDevice(boolean forceReconnect) {
        if(mRevosBleManager.getConnectionState() == STATE_DISCONNECTED) {
            SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
            String lastConnectedDeviceMacAddress = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null);
            String lastConnectedDeviceName = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null);

            mBluetoothDeviceName = lastConnectedDeviceName;
            mBluetoothDeviceAddress = lastConnectedDeviceMacAddress;

            //check if user disconnected manually flag is set or not
            boolean userManuallyDisconnectedFlag = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, false);

            if(forceReconnect || !userManuallyDisconnectedFlag) {
                if (lastConnectedDeviceMacAddress != null && lastConnectedDeviceName != null) {
                    connectToDeviceHelper(lastConnectedDeviceMacAddress);
                }
            }
        }
    }

    private void connectToDeviceHelper(String deviceAddress) {
        if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON || deviceAddress == null) {
            return;
        }

        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

        if(cloudDevice != null) {
            if(cloudDevice.getStatus() == DeviceStatus.INITIALIZED) {
                mAesKey = null;
            } else if(cloudDevice.getStatus() == DeviceStatus.ACTIVATED || cloudDevice.getStatus() == DeviceStatus.RESET) {
                mAesKey = cloudDevice.getKey();
            } else {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_NOT_FOUND));
                return;
            }
            mBluetoothDeviceAddress = deviceAddress;

            startScan();
        }
    }

    /**
     * Scan for 5 seconds and then stop scanning when a BluetoothLE device is found then mLEScanCallback
     * is activated This will perform regular scan for custom BLE Service UUID and then filter out.
     * using class ScannerServiceParser
     */
    private synchronized void startScan() {

        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale
            Intent permissionActivity = new Intent(getApplicationContext(), RuntimePermissionActivity.class);
            startActivity(permissionActivity);
            return;
        }

        if(mIsScanning) {
            return;
        }

        mDeviceFound = false;
        mIsScanning = true;

        mScanner.startScan(scanCallback);

        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_SCAN_STARTED));

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mIsScanning) {
                    stopScanHelper(false);
                }
            }
        }, SCAN_DURATION);
    }

    /**
     * Stop scan if user tap Cancel button
     */
    private void stopScanHelper(boolean isForced) {
        mScanner.stopScan(scanCallback);

        mIsScanning = false;

        if(!mDeviceFound && !isForced) {
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_NOT_FOUND));
        }
    }

    public static void clearStaticDeviceVariables() {
        mBluetoothDeviceName = mBluetoothDeviceAddress = null;
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if(manager == null) {
            return false;
        }
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if(serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final ScanResult result) {
            if(result.getDevice().getAddress().trim().equals(mBluetoothDeviceAddress)) {
                mDeviceFound = true;
                if(!isServiceRunning(NordicBleService.class) || mConnectionState != STATE_DISCONNECTED) {
                    return;
                }
                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mRevosBleManager.connectToDevice(mBluetoothDeviceAddress, mAesKey);
                        Timber.d("Scan result connecting to device" + mBluetoothDeviceAddress);

                    }
                });
            }
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {
            for(ScanResult result : results) {
                if(result.getDevice().getAddress().trim().equals(mBluetoothDeviceAddress)) {
                    mDeviceFound = true;
                    if(!isServiceRunning(NordicBleService.class) || mConnectionState != STATE_DISCONNECTED) {
                        return;
                    }

                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            mRevosBleManager.connectToDevice(mBluetoothDeviceAddress, mAesKey);
                            Timber.d("Batch Scan result connecting to device" + mBluetoothDeviceAddress);
                        }
                    });
                }
            }
        }

        @Override
        public void onScanFailed(final int errorCode) {
        }
    };

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        Timber.d(event.getStatus());

        if(event.getStatus().contains(BT_EVENT_REQUEST_CONNECT_TO_DEVICE)) {

            String deviceAddress = event.getStatus().split(GEN_DELIMITER)[1];
            Timber.d("Connecting to device%s", deviceAddress);
            mBluetoothDeviceAddress = deviceAddress;
            connectToDeviceHelper(deviceAddress);

        } else if(event.getStatus().equals(START_STM_INTERNAL_OTA)) {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    stmOtaHelper(false);
                }
            });
        } else if(event.getStatus().equals(START_STM_EXTERNAL_OTA)) {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    stmOtaHelper(true);
                }
            });
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_SEND_LARGE_BITMAP)) {
            transferNavLargeBitmapInParts();
        } else if(event.getStatus().equals(BT_EVENT_IMG_CHAR_WRITE_SUCCESS)){
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if(mLargeBitmapFirstPartTransferInitiated) {

                        mLargeBitmapFirstPartTransferInitiated = false;
                        mLargeBitmapFirstPartTransferCompleted = true;

                        if(!mLargeBitmapSecondPartTransferInitiated) {
                            transferNavLargeBitmapInParts();
                        }
                    } else if(mLargeBitmapSecondPartTransferInitiated) {
                        //this effectively means everything has been sent
                        //clear all the state maintaining flags so that new image transfer can happen
                        mLargeBitmapFirstPartTransferInitiated = false;
                        mLargeBitmapFirstPartTransferCompleted = false;

                        mLargeBitmapSecondPartTransferInitiated = false;
                        mLargeBitmapSecondPartTransferCompleted = false;
                    }
                }
            });

        } else if(event.getStatus().equals(BT_EVENT_IMG_CHAR_WRITE_FAIL)){
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if(mLargeBitmapFirstPartTransferInitiated || mLargeBitmapSecondPartTransferInitiated) {
                        mLargeBitmapFirstPartTransferInitiated = false;
                        mLargeBitmapFirstPartTransferCompleted = false;

                        mLargeBitmapSecondPartTransferInitiated = false;
                        mLargeBitmapSecondPartTransferCompleted = false;
                    }
                }
            });
        } else if(event.getStatus().contains(BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS)) {
            SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);

            Timber.d("Gatt service not null");
            //there's only one characteristic to read
            String commandKey = event.getStatus().split(GEN_DELIMITER)[1];

            String byteStrArray[] = commandKey.split(" ");
            byte[] byteArray = new byte[byteStrArray.length];
            ArrayList<Byte> commandArrayList = new ArrayList<>();

            for(int byteStrIndex = 0; byteStrIndex < byteStrArray.length; ++ byteStrIndex) {
                byteArray[byteStrIndex] = (byte) Integer.parseInt(byteStrArray[byteStrIndex], 16);
                commandArrayList.add(byteStrIndex, byteArray[byteStrIndex]);
            }

            if(byteArray.length == WRITE_PARAMS_COMMAND_LENGTH) {
                sharedPreferences.edit().putLong(BLUETOOTH_CONTROL_PARAMS_WRITE_LAST_ATTEMPT_TIMESTAMP, System.currentTimeMillis()).apply();
                sharedPreferences.edit().putString(BLUETOOTH_EXPECTED_CONTROLLER_PARAMS_KEY, new Gson().toJson(commandArrayList)).apply();
            }

            mSettingsChangeInitiated = true;
            //try writing the changes to the bluetooth characteristic
            mRevosBleManager.writeDataToOBDCharacteristic(byteArray);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_ROTATE_TEST_IMAGE)) {
            rotateTestImageSequence();
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_SEND_UBER_SCENARIO_1_IMAGE)) {
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inScaled = false;
            int drawableId = R.drawable.screen_uber_1_48_x_32;
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableId, bitmapOptions);
            transferBitmap(bitmap, GENERIC_IMG_METADATA_CODE);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_SEND_UBER_SCENARIO_2_IMAGE)) {
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inScaled = false;
            int drawableId = R.drawable.screen_uber_2_48_x_32;
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableId, bitmapOptions);
            transferBitmap(bitmap, GENERIC_IMG_METADATA_CODE);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_SHOW_SETTINGS_CHANGED_ICON)) {
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inScaled = false;
            int drawableId = R.drawable.px_settings_changed_48_x_32;
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableId, bitmapOptions);
            if(isColorSpeedometer()) {
                sendShowSettingsChangedIconCommandForColorSpeedometer();
            } else {
                transferBitmap(bitmap, GENERIC_IMG_METADATA_CODE);
            }
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                   EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CLEAR_IMAGE));
                }
            }, 3000);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_CLEAR_IMAGE)) {
            clearSpeedometerImageAndTextHelper();
        } else if(event.getStatus().equals(BT_EVENT_DECREASE_CUSTOM_IMAGE_PRIORITY)) {
            /*if(mOBDDataGattService != null) {
                byte[] byteArray = {0x5A};
                writeCharacteristicGattDb(mOBDDataGattService.getCharacteristics().get(0), byteArray);
            }*/
            //showMessageOnSpeedometerHelper("CHETAN", "RIDE", "STARTED", 5000);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND)) {
            byte[] findVehicleByteArray = new byte[1];

            VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());

            findVehicleByteArray[0] = vehicleControlHelper.getFindMyVehicleByte();

            mSettingsChangeInitiated = false;

            mRevosBleManager.writeDataToOBDCharacteristic(findVehicleByteArray);

        } else if(event.getStatus().contains(BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS)) {
            String vehicleParamsString = event.getStatus().split(GEN_DELIMITER)[1];
            if(vehicleParamsString.isEmpty()) {
                return;
            }

            String vehicleParams[] = vehicleParamsString.split(" ");
            byte[] vehicleParamsByteArray = new byte[vehicleParams.length];
            for(int paramIndex = 0; paramIndex < vehicleParams.length; ++paramIndex) {
                vehicleParamsByteArray[paramIndex] = (byte)(Integer.parseInt(vehicleParams[paramIndex], 16));
            }
            mSettingsChangeInitiated = true;
            mRevosBleManager.writeDataToOBDCharacteristic(vehicleParamsByteArray);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_CANCEL_PAIRING)) {
            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, true).apply();
            mRevosBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
            stopScanHelper(true);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_RECONNECT)) {
            reconnect();
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_DISCONNECT)) {
            mDeviceFirmwareVersion = null;
            clearStaticDeviceVariables();
            mIsScanning = false;
            mRevosBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        } else if(event.getStatus().equals(BT_EVENT_STATE_CONNECTED)) {
            mConnectionState = STATE_CONNECTED;
            stopScanHelper(true);
            //clear previous data exchanger instance
            SpeedometerDataExchanger.clearInstance();
            mDeviceFirmwareVersion = mRevosBleManager.getDeviceFirmwareVersion();
            updateNotification();
            clearLastSentDistanceAndBitmap();
            SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
            //download model image if any
            downloadVehicleModelImage();
            //update device details from server if possible
            fetchDeviceDetailsFromServer();
            //update battery regression data from server if possible
            fetchBatteryRegressionDataFromServer();
            //update trip data if possible
            refreshTripData();

            //Update vehicel last known location
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                refreshVehicleLastKnownLocation();
            }

            //save the mac and name of the device we connected to
            if(mRevosBleManager.getBluetoothDevice() != null) {
                sharedPreferences.edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, mRevosBleManager.getBluetoothDevice().getAddress()).apply();
                sharedPreferences.edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, mRevosBleManager.getBluetoothDevice().getName()).apply();
            }
            //clear the retry count for connection attempts
            sharedPreferences.edit().putInt(BLUETOOTH_RETRY_ATTEMPT_COUNT_KEY, 0).apply();
            clearWorkingLogFiles();
            //clear expected image code
            clearImageStateFlags();
            //clear stored control params
            clearStoredControlParams();
            //update app notification
            updateNotification();
            //check deviceStatus state on the server and see if matches with actual device state
            syncDeviceMetadata();
            //clear user manually disconnected flag
            clearUserManuallyDisconnectedFlag();
            //clear the acceleration values
            mAccelerationValuesList = new ArrayList<>();
            //refreshControllerMetadataIfRequired
            refreshControllerMetadataIfRequired();
            //clear model default config applied flag
            mModelDefaultConfigApplied = false;
            mModelDefaultConfigChangeInitiated = false;
            //clear odo reset applied flag
            mResetOdometerApplied = false;
            mResetOdometerInitiated = false;
            mLastLiveLogSyncedTimestamp = -1;
            //update device firmware on server
            mainRepository.updateDeviceFirmwareOnServer(getApplicationContext(), getDeviceFirmwareVersion());
        } else if(event.getStatus().equals(BT_EVENT_STATE_DISCONNECTED)) {
            mConnectionState = STATE_DISCONNECTED;
            mVehicleLiveParameters = null;
            clearLastSentDistanceAndBitmap();
            SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
            sharedPreferences.edit().putInt(BLUETOOTH_CONNECTION_STATUS_KEY, STATE_DISCONNECTED).apply();
            sharedPreferences.edit().putBoolean(BLUETOOTH_SERVICES_DISCOVERED_FLAG_KEY, false).apply();
            clearWorkingLogFiles();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_RECONNECT));
                }
            }, 10000);
            //update app notification
            updateNotification();
        } else if(event.getStatus().equals(BT_EVENT_STATE_CONNECTING)) {
              mConnectionState = STATE_CONNECTING;
            //update app notification
            updateNotification();
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE)) {
            tryConnectingToLastSavedDevice(true);
        } else if(event.getStatus().contains(BT_EVENT_DEVICE_LOCKED)){
            mVehicleLiveParameters = null;
        } else if(event.getStatus().contains(OBD_EVENT_LIVE_DATA)) {
            mDeviceFirmwareVersion = mRevosBleManager.getDeviceFirmwareVersion();
            final String dataString = event.getStatus().split(GEN_DELIMITER)[1];
            processLiveOBDData(dataString);
            clearSpeedometerMessageIfRequired();
            makeLogSyncRequest();
            applyDefaultVehicleConfigIfRequired();

            applyMediaChangesIfRequired();

            boolean isOdoResetRequired = getApplicationContext().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY, false);
            if(isOdoResetRequired) {
                Timber.d("odo reset required | call from obd event live data received");
                initiateResetOdometerIfRequired();
            } else {
                boolean isFirmwareUpdateInProgress = getApplicationContext().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getBoolean(DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS, false);

                if(!isFirmwareUpdateInProgress) {

                    Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

                    if(cloudDevice.getDeviceUpdateStatus() == null || cloudDevice.getDeviceUpdateStatus() != DeviceUpdateStatus.UPDATE_REQUIRED) {
                        mainRepository.updateDeviceFirmwareOnServer(getApplicationContext(), getDeviceFirmwareVersion());
                    }
                }
            }
        } else if(event.getStatus().contains(OBD_EVENT_OFFLINE_DATA)) {
            final String dataString = event.getStatus().split(GEN_DELIMITER)[1];
            processOfflineOBDData(dataString);
        } else if(event.getStatus().contains(OBD_EVENT_BATTERY_ADC_OFFLINE_DATA)){
            final String dataString = event.getStatus().split(GEN_DELIMITER)[1];
            processOfflineBatteryAdcData(dataString);
        } else if(event.getStatus().contains(BT_EVENT_REQUEST_SEND_CALL_BITMAP)) {
            String displayString = event.getStatus().split(GEN_DELIMITER)[1];
            if(isColorSpeedometer()) {
                sendShowPhoneIconCommandForColorSpeedometer(displayString);
            } else {
                generateAndSendIncomingCallBitmap(displayString);
            }
        } else if(event.getStatus().equals(NAV_EVENT_TRIP_ADVICE_DATA_UPDATED)) {
            //if version number is less than 5.0.0 then it's a monochrome speedometer
            if(isColorSpeedometer()) {
                decideAndSendNavigationDataForColor();
            } else {
                decideAndSendNavigationArrowForMonochrome();
            }
        } else if(event.getStatus().equals(BT_EVENT_OBD_CHAR_WRITE_SUCCESS)) {
            if(mSettingsChangeInitiated) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_SHOW_SETTINGS_CHANGED_ICON));
            }
            mSettingsChangeInitiated = false;
            if(!mModelDefaultConfigApplied && mModelDefaultConfigChangeInitiated) {
                mModelDefaultConfigApplied = true;
                mModelDefaultConfigChangeInitiated = false;
            }

            if(!mResetOdometerApplied && mResetOdometerInitiated) {
                mResetOdometerApplied = true;
                mResetOdometerInitiated = false;
                Timber.d("odo reset odometer reset write characteristics success");
                mainRepository.markOdometerResetFlag(getApplicationContext(), !mResetOdometerApplied);
            }
        } else if(event.getStatus().equals(BT_EVENT_OBD_CHAR_WRITE_FAIL)) {
            mSettingsChangeInitiated = false;
        } else if(event.getStatus().equals(KILL_SWITCH)) {
            if(mConnectionState == STATE_DISCONNECTED) {
                clearWorkingLogFiles();
                selfDestruct();
            } else if(mRevosBleManager != null) {
                //disconnect first
                mRevosBleManager.disconnectFromDevice(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {
                        Timber.d("disconnect_success");
                        clearWorkingLogFiles();
                        selfDestruct();
                    }
                }, new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                        Timber.d("disconnect_fail");
                    }
                });
            }
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_LOCK_VEHICLE)) {
            VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());
            byte[] commandArray = new byte[1];
            commandArray[0] = vehicleControlHelper.getVehicleLockControlByte(true);
            mRevosBleManager.writeDataToOBDCharacteristic(commandArray);
            showMessageOnSpeedometerHelper("VEHICLE", "LOCKED", "", 3000);
        }  else if(event.getStatus().equals(BT_EVENT_REQUEST_UNLOCK_VEHICLE)) {
            VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());
            byte[] commandArray = new byte[1];
            commandArray[0] = vehicleControlHelper.getVehicleLockControlByte(false);
            mRevosBleManager.writeDataToOBDCharacteristic(commandArray);
            showMessageOnSpeedometerHelper("VEHICLE", "UNLOCK", "", 3000);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_LOCK_HANDLE)) {
            parseAndSendStringToOBDChar("E1");
            showMessageOnSpeedometerHelper("HANDLE", "LOCKED", "", 3000);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_UNLOCK_HANDLE)) {
            parseAndSendStringToOBDChar("E0");
            showMessageOnSpeedometerHelper("HANDLE", "UNLOCK", "", 3000);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_UNLOCK_SEAT)) {
            parseAndSendStringToOBDChar("C9");
            showMessageOnSpeedometerHelper("SEAT", "UNLOCK", "", 3000);
        } else if(event.getStatus().contains(BT_EVENT_REQUEST_SEND_FILLER_BYTES_OBD)) {
            sendFillerBytesOBD(event.getStatus().split(GEN_DELIMITER)[1], event.getStatus().split(GEN_DELIMITER)[2]);
        } else if(event.getStatus().contains(BT_EVENT_REQUEST_SEND_FILLER_BYTES_IMG)) {
            sendFillerBytesIMG(event.getStatus().split(GEN_DELIMITER)[1], event.getStatus().split(GEN_DELIMITER)[2]);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_DEVICE_SYNC) || event.getStatus().equals(NETWORK_EVENT_DEVICE_DATA_REFRESHED)) {
            syncDeviceMetadata();
            mModelDefaultConfigApplied = false;
            applyDefaultVehicleConfigIfRequired();

            boolean isOdoResetRequired = getApplicationContext().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getBoolean(DEVICE_IS_ODO_RESET_REQUIRED_KEY, false);
            if(isOdoResetRequired) {
                Timber.d("odo reset required | call from bt_event_request_device_sync/netwokr_event_device_data_refreshed");
                initiateResetOdometerIfRequired();
            }
        } else if(event.getStatus().equals(BT_EVENT_DEVICE_SYNC_SUCCESS)) {
            Timber.d("device sync success");
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    syncBatteryRegressionDataIfRequired();
                }
            }, 500);
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_CONNECT_TBIT)) {
            connectToTbitDeviceIfRequired();
        } else if(event.getStatus().equals(BT_EVENT_REQUEST_CONNECT_ICONCOX)) {
            connectToIConCoxDeviceIfRequired();
        }
    }

    private void refreshVehicleLastKnownLocation() {
        PeriodicWorkRequest workRequest = new PeriodicWorkRequest
                .Builder(VehicleLastKnownLocationWorker.class, 1, TimeUnit.MINUTES  ).build();

        WorkManager.getInstance(getApplicationContext()).enqueue(workRequest);
    }

    private void applyMediaChangesIfRequired() {

        if(mVehicleLiveParameters == null) {
            return;
        }

        if(mHighBeamStateChangeTimestampList == null) {
            mHighBeamStateChangeTimestampList = new ArrayList<>();
        }

        if(mLastKnownHighBeamState != mVehicleLiveParameters.isHeadLampOn()) {
            Timber.d("media : change in high beam detected");

            mHighBeamStateChangeTimestampList.add(System.currentTimeMillis());
            mHighBeamStateHandler.removeMessages(PROCESS_HIGH_BEAM_STATE_CHANGES);
            mHighBeamStateHandler.sendEmptyMessageDelayed(PROCESS_HIGH_BEAM_STATE_CHANGES, HIGH_BEAM_STATE_CHANGE_COOL_OFF_TIME);

        }

        mLastKnownHighBeamState = mVehicleLiveParameters.isHeadLampOn();
    }

    private void stmOtaHelper(boolean isExternalOta) {
        File otaFile = new File(getExternalFilesDir(OTA_DIR_PATH), "internal.bin");

        if(isExternalOta) {
            otaFile = new File(getExternalFilesDir(OTA_DIR_PATH), "external.bin");
        }

        if(otaFile.exists()) {
            mRevosBleManager.startStmOta(otaFile.getAbsolutePath(), isExternalOta);
        }
    }

    private boolean isColorSpeedometer() {

        if(NordicBleService.getDeviceFirmwareVersion() != null
                && !NordicBleService.getDeviceFirmwareVersion().isEmpty()
                && new Version(NordicBleService.getDeviceFirmwareVersion()).compareTo(new Version(VER_5_0)) < 0) {
            return false;
        } else if(NordicBleService.getDeviceFirmwareVersion() != null
                && !NordicBleService.getDeviceFirmwareVersion().isEmpty()
                && new Version(NordicBleService.getDeviceFirmwareVersion()).compareTo(new Version(VER_5_0)) >= 0){
            return true;
        } else {
            return false;
        }
    }

    private void syncBatteryRegressionDataIfRequired () {
        if(mRevosBleManager == null || mConnectionState != STATE_CONNECTED) {
            return;
        }

        if(new Version(mRevosBleManager.getDeviceFirmwareVersion()).compareTo(new Version(VER_1_0)) > 0) {

            String regressionDataStr = getSharedPreferences(BATTERY_PREFS, MODE_PRIVATE).getString(BATTERY_REGRESSION_DATA_KEY, null);

            if(regressionDataStr == null) {
                return;
            }

            BatteryRegressionData batteryRegressionData = new Gson().fromJson(regressionDataStr, BatteryRegressionData.class);

            if(batteryRegressionData.getVersion() != mRevosBleManager.getBatteryRegressionDataSetVersion()) {
                mRevosBleManager.syncBatteryRegressionData();
            }
        }
    }

    private void refreshControllerMetadataIfRequired() {
        if(mConnectionState != STATE_CONNECTED) {
            return;
        }

        MetadataDecryptionHelper metadataDecryptionHelper = MetadataDecryptionHelper.getInstance(getApplicationContext());
        if(metadataDecryptionHelper == null) {
            return;
        }

        String metadataVersionNo = metadataDecryptionHelper.getMetadataFirmwareVersion();

        if(metadataVersionNo == null) {
            return;
        }

        if(mRevosBleManager.getDeviceFirmwareVersion() != null && !mRevosBleManager.getDeviceFirmwareVersion().equals(metadataVersionNo)) {
            metadataDecryptionHelper.clearLocalCache();
            mainRepository.refreshControllerMetadata(getApplicationContext(), mRevosBleManager.getControllerFirmwareVersion());
        }
    }

    private void makeLogSyncRequest() {
        VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());
        byte[] commandArray = new byte[1];
        commandArray[0] = vehicleControlHelper.getStartLogSyncByte();
        mRevosBleManager.writeDataToOBDCharacteristic(commandArray);
    }

    private void downloadVehicleModelImage() {
        //check if we have device details present
        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(cloudDevice != null) {
            mainRepository.downloadVehicleModelImageIfRequired(getApplicationContext(), cloudDevice.getVehicle().getModel().getId());
        }
    }

    private void fetchDeviceDetailsFromServer() {
        //check if we have device details present
        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(cloudDevice != null) {
            mainRepository.refreshDeviceData(getApplicationContext());
        }
    }

    private void fetchBatteryRegressionDataFromServer() {
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        if(vehicle.getVin() == null || vehicle.getVin().isEmpty()) {
            return;
        }

        mainRepository.refreshBatteryRegressionData(getApplicationContext());
    }

    private void refreshTripData() {
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        if(vehicle != null && vehicle.getVin() != null){
            mainRepository.refreshTripData(getApplicationContext(), vehicle.getVin());
        }
    }

    private void syncDeviceMetadata() {
        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(cloudDevice == null) {
            return;
        }

        mDeviceMetadataString = mRevosBleManager.getDeviceMetadataString();

        if(cloudDevice.getStatus() == DeviceStatus.ACTIVATED && cloudDevice.isPinResetRequired()) {
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_CHANGE_PIN_REQUIRED + GEN_DELIMITER + cloudDevice.getVehicle().getVin() + GEN_DELIMITER + cloudDevice.getVehicle().getBuyerPhoneNo()));
        } else if(cloudDevice.getStatus() == DeviceStatus.INITIALIZED || cloudDevice.getStatus() == DeviceStatus.RESET) {
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_START_ACTIVATION_PROCEDURE));
        } else {
            mRevosBleManager.syncDeviceConstants();
        }
    }

    private void applyDefaultVehicleConfigIfRequired() {
        if(mVehicleLiveParameters == null || mModelDefaultConfigApplied || mModelDefaultConfigChangeInitiated) {
            return;
        }

        if(mVehicleLiveParameters.getSpeed() > 0) {
            return;
        }

        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(cloudDevice == null) {
            return;
        }

        Model model = cloudDevice.getVehicle().getModel();

        if(model == null || model.getId() == null || model.getName() == null || model.getModelAccessType() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }


        ControllerWriteCommandHelper controllerWriteCommandHelper = new ControllerWriteCommandHelper(mVehicleLiveParameters, getApplicationContext());
        String firmwareVersion = getDeviceFirmwareVersion();
        //set speed limit
        if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
            if(vehicle.getChildSpeedLock() == -1) {     //not set
                controllerWriteCommandHelper.setSpeedPercent(model.getSpeedLimit());
            } else {
                controllerWriteCommandHelper.setSpeedPercent(vehicle.getChildSpeedLock());
            }
        } else {
            controllerWriteCommandHelper.setSpeedPercent(model.getSpeedLimit());
        }

        controllerWriteCommandHelper.setPickupControlLimit(model.getPickupLimit());
        controllerWriteCommandHelper.setBrakeRegenLimit(model.getBrakeRegenLimit());
        controllerWriteCommandHelper.setThrottleZeroRegenLimit(model.getZeroThrottleRegenLimit());
        controllerWriteCommandHelper.setOverCurrentLimit(model.getCurrentLimit());
        controllerWriteCommandHelper.setOverVoltageLimit(model.getOverVoltageLimit());
        controllerWriteCommandHelper.setUnderVoltageLimit(model.getUnderVoltageLimit());

        controllerWriteCommandHelper.setHillAssist(model.isHillAssist());
        controllerWriteCommandHelper.setParking(model.isParking());
        controllerWriteCommandHelper.setRegenBraking(model.isRegenBraking());
        controllerWriteCommandHelper.setEAbs(model.getEAbs());

        byte[] commandArray = controllerWriteCommandHelper.constructWriteCommandByteArray();

        mModelDefaultConfigChangeInitiated = true;

        mRevosBleManager.writeDataToOBDCharacteristic(commandArray);
    }

    private void initiateResetOdometerIfRequired() {
        if(mVehicleLiveParameters == null || mVehicleLiveParameters.getSpeed() > 0) {
            Timber.d("odo reset required | vehicle live parameters null");
            return;
        }

        if(mResetOdometerApplied || mResetOdometerInitiated) {
            Timber.d("odo reset required | odo reset applied or initiated true, hence return");
            return;
        }

        Timber.d("odo reset initiated");

        int resetTripIdValue = (int)mVehicleLiveParameters.getTripId();

        mResetOdometerInitiated = true;
        mResetOdometerApplied = false;

        mRevosBleManager.performResetOdometer(resetTripIdValue);
    }

    private void sendFillerBytesOBD(String byteStr, String countStr) {
        if(byteStr.isEmpty() || countStr.isEmpty()) {
            return;
        }

        String byteTokens[] = byteStr.split(" ");
        byte[] vehicleParamsByteArray = new byte[byteTokens.length * Integer.parseInt(countStr)];
        for(int paramIndex = 0; paramIndex < vehicleParamsByteArray.length; ++paramIndex) {
            vehicleParamsByteArray[paramIndex] = (byte)(Integer.parseInt(byteTokens[paramIndex % byteTokens.length], 16));
        }

        mRevosBleManager.writeDataToOBDCharacteristic(vehicleParamsByteArray);
    }

    private void sendFillerBytesIMG(String byteStr, String countStr) {
        if(byteStr.isEmpty() || countStr.isEmpty()) {
            return;
        }

        String byteTokens[] = byteStr.split(" ");
        byte[] vehicleParamsByteArray = new byte[byteTokens.length * Integer.parseInt(countStr)];
        for(int paramIndex = 0; paramIndex < vehicleParamsByteArray.length; ++paramIndex) {
            vehicleParamsByteArray[paramIndex] = (byte)(Integer.parseInt(byteTokens[paramIndex % byteTokens.length], 16));
        }

        mRevosBleManager.writeDataToIMGCharacteristic(vehicleParamsByteArray);
    }

    private void parseAndSendStringToOBDChar(String hexString) {
        if(hexString.isEmpty()) {
            return;
        }

        String vehicleParams[] = hexString.split(" ");
        byte[] vehicleParamsByteArray = new byte[vehicleParams.length];
        for(int paramIndex = 0; paramIndex < vehicleParams.length; ++paramIndex) {
            vehicleParamsByteArray[paramIndex] = (byte)(Integer.parseInt(vehicleParams[paramIndex], 16));
        }
        mRevosBleManager.writeDataToOBDCharacteristic(vehicleParamsByteArray);
    }

    public static String getDeviceName() {
        return mBluetoothDeviceName;
    }

    public static String getDeviceAddress() {
        return mBluetoothDeviceAddress;
    }

    public static void setDeviceName(String deviceName) {
        mBluetoothDeviceName = deviceName;
    }

    public static int getConnectionState() {
        return mConnectionState;
    }

    public static String getDeviceFirmwareVersion() {
        return mDeviceFirmwareVersion;
    }

    private void generateAndSendIncomingCallBitmap(String displayString) {
        displayString = displayString.toUpperCase();

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;

        Bitmap phoneIconBitmap;
        phoneIconBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test_bmp_phone_cmp, bitmapOptions);

        Bitmap mutableBitmap = phoneIconBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(mutableBitmap);

        int PHONE_ICON_DRAW_HEIGHT = 14;

        int startXPosition;

        int currentPlaceXPosition;

        //calculate the total width for the first line that will be required for the text and how many characters we can fit
        int firstLineTotalMessageWidth = 0;
        int firstLineCharIndexLimit = 0;
        for(int charIndex = 0; charIndex < displayString.length(); ++charIndex) {
            if(mLCDCharToCompressedDrawableHashMap.containsKey(displayString.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToCompressedDrawableHashMap.get(displayString.charAt(charIndex)), bitmapOptions);

                //only add char width to calculation if it fits entirely else skip
                if(firstLineTotalMessageWidth + charBitmap.getWidth() < DYNAMIC_AREA_WIDTH) {
                    firstLineTotalMessageWidth += charBitmap.getWidth();
                    firstLineCharIndexLimit = charIndex;
                } else {
                    break;
                }
                charBitmap.recycle();
            }
        }

        //calculate the total width for the first line that will be required for the text and how many characters we can fit
        int secondLineTotalMessageWidth = 0;
        int secondLineCharIndexLimit = 0;
        for(int charIndex = firstLineCharIndexLimit + 1; charIndex < displayString.length(); ++charIndex) {
            if(mLCDCharToCompressedDrawableHashMap.containsKey(displayString.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToCompressedDrawableHashMap.get(displayString.charAt(charIndex)), bitmapOptions);

                //only add char width to calculation if it fits entirely else skip
                if(secondLineTotalMessageWidth + charBitmap.getWidth() < DYNAMIC_AREA_WIDTH) {
                    secondLineTotalMessageWidth += charBitmap.getWidth();
                    secondLineCharIndexLimit = charIndex;
                } else {
                    break;
                }
                charBitmap.recycle();
            }
        }


        //draw the first line
        startXPosition = 0;
        currentPlaceXPosition = startXPosition;

        for(int charIndex = 0; charIndex <= firstLineCharIndexLimit; ++charIndex) {
            if(mLCDCharToCompressedDrawableHashMap.containsKey(displayString.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToCompressedDrawableHashMap.get(displayString.charAt(charIndex)), bitmapOptions);
                canvas.drawBitmap(charBitmap, currentPlaceXPosition, PHONE_ICON_DRAW_HEIGHT + 1, null);
                currentPlaceXPosition += charBitmap.getWidth();
                charBitmap.recycle();
            }
        }

        //draw the second line
        startXPosition = 0;
        currentPlaceXPosition = startXPosition;

        for(int charIndex = firstLineCharIndexLimit + 1; charIndex <= secondLineCharIndexLimit; ++charIndex) {
            if(mLCDCharToCompressedDrawableHashMap.containsKey(displayString.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToCompressedDrawableHashMap.get(displayString.charAt(charIndex)), bitmapOptions);
                canvas.drawBitmap(charBitmap, currentPlaceXPosition, PHONE_ICON_DRAW_HEIGHT + 2 + charBitmap.getHeight(), null);
                currentPlaceXPosition += charBitmap.getWidth();
                charBitmap.recycle();
            }
        }

        transferBitmap(mutableBitmap, PHONE_MSG_FLAG);
    }

    private void generateAndSendDestinationETABitmap(HashMap<String, Object> tripAdviceHashMap) {

        String etaDisplayString = null;

        if(tripAdviceHashMap.containsKey(TRIP_ADVICE_ETA_KEY)) {
            etaDisplayString = (String) tripAdviceHashMap.get(TRIP_ADVICE_ETA_KEY);
            etaDisplayString = etaDisplayString.toUpperCase();
        }

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;

        Bitmap phoneIconBitmap;
        phoneIconBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.flag_48_x_32, bitmapOptions);

        Bitmap mutableBitmap = phoneIconBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(mutableBitmap);

        int PHONE_ICON_DRAW_HEIGHT = 14;

        int startXPosition;

        int currentPlaceXPosition;

        //calculate the total width for the first line that will be required for the text and how many characters we can fit
        int firstLineTotalMessageWidth = 0;
        int firstLineCharIndexLimit = 0;
        for(int charIndex = 0; charIndex < etaDisplayString.length(); ++charIndex) {
            if(mLCDCharToCompressedDrawableHashMap.containsKey(etaDisplayString.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToCompressedDrawableHashMap.get(etaDisplayString.charAt(charIndex)), bitmapOptions);

                //only add char width to calculation if it fits entirely else skip
                if(firstLineTotalMessageWidth + charBitmap.getWidth() < DYNAMIC_AREA_WIDTH) {
                    firstLineTotalMessageWidth += charBitmap.getWidth();
                    firstLineCharIndexLimit = charIndex;
                } else {
                    break;
                }
                charBitmap.recycle();
            }
        }

        //calculate the total width for the first line that will be required for the text and how many characters we can fit
        int secondLineTotalMessageWidth = 0;
        int secondLineCharIndexLimit = 0;
        for(int charIndex = firstLineCharIndexLimit + 1; charIndex < etaDisplayString.length(); ++charIndex) {
            if(mLCDCharToCompressedDrawableHashMap.containsKey(etaDisplayString.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToCompressedDrawableHashMap.get(etaDisplayString.charAt(charIndex)), bitmapOptions);

                //only add char width to calculation if it fits entirely else skip
                if(secondLineTotalMessageWidth + charBitmap.getWidth() < DYNAMIC_AREA_WIDTH) {
                    secondLineTotalMessageWidth += charBitmap.getWidth();
                    secondLineCharIndexLimit = charIndex;
                } else {
                    break;
                }
                charBitmap.recycle();
            }
        }


        //draw the first line
        startXPosition = 0;
        currentPlaceXPosition = startXPosition;

        for(int charIndex = 0; charIndex <= firstLineCharIndexLimit; ++charIndex) {
            if(mLCDCharToCompressedDrawableHashMap.containsKey(etaDisplayString.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToCompressedDrawableHashMap.get(etaDisplayString.charAt(charIndex)), bitmapOptions);
                canvas.drawBitmap(charBitmap, currentPlaceXPosition, PHONE_ICON_DRAW_HEIGHT + 1  + charBitmap.getHeight(), null);
                currentPlaceXPosition += charBitmap.getWidth();
                charBitmap.recycle();
            }
        }


        transferBitmap(mutableBitmap, NAVIGATION_IMG_METADATA_CODE);
        mLastSentBitmap = null;
    }

    private void decideAndSendNavigationDataForColor() {
        if(PhoneStateReceiver.currentState == TelephonyManager.CALL_STATE_RINGING) {
            return;
        }

        boolean wasNavBitmapSent = false;
        //first check if we need to send a new bitmap or not
        if(mLastSentBitmap == null
                || ImageUtils.findBitmapPercentageDiff(mLastSentBitmap, NotificationReaderService.mCachedNavigationBitmap) > 5
                || (mVehicleLiveParameters != null && mVehicleLiveParameters.getDisplayedImageCode() == BLANK_IMG_METADATA_CODE)) {
            transferNavLargeBitmapInParts();
            wasNavBitmapSent = true;
        }


        //now we need to check if we need to send text based data or not
        HashMap<String, Object> tripAdviceDataHashMap = null;
        String tripDataStr = getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(TRIP_ADVICE_DATA_KEY, null);
        if(tripDataStr != null) {
            Type hashMapType = new TypeToken<HashMap<String, Object>>(){}.getType();
            tripAdviceDataHashMap = new Gson().fromJson(tripDataStr, hashMapType);
        } else {
            return;
        }

        String navAdviceText = (String)tripAdviceDataHashMap.get(TRIP_ADVICE_TEXT_KEY);
        String navAdviceTitle = (String)tripAdviceDataHashMap.get(TRIP_ADVICE_TITLE_KEY);

        if(navAdviceText == null) {
            navAdviceText = "";
        }

        if(navAdviceTitle == null) {
            navAdviceTitle = "";
        }


        boolean sendTitle = false;
        boolean sendText = false;
        //check if we need to send text data or not
        if(mLastSentNavAdviceTitle == null || !mLastSentNavAdviceTitle.equals(navAdviceTitle)) {
            sendTitle = true;
        }

        if(mLastSentNavAdviceText == null || !mLastSentNavAdviceText.equals(navAdviceText)) {
            sendText = true;
        }

        if(sendTitle) {
            int sizeOfByteArrayToBeSent;
            byte[] messageBytes = new byte[1];
            if(navAdviceTitle.isEmpty()) {
                sizeOfByteArrayToBeSent = 1;
            } else {
                messageBytes = navAdviceTitle.getBytes(Charset.forName("UTF-16LE"));
                sizeOfByteArrayToBeSent = 1 + 1 + messageBytes.length;
            }

            byte[] byteArrayToBeSent = new byte[sizeOfByteArrayToBeSent];

            if(sizeOfByteArrayToBeSent == 1) {
                //send command to clear the textview
                byteArrayToBeSent[0] = (byte)0xA3;
            } else {
                byteArrayToBeSent[0] = (byte)0xA1;
                byteArrayToBeSent[1] = (byte)messageBytes.length;
                System.arraycopy(messageBytes, 0, byteArrayToBeSent, 2, messageBytes.length);
            }

            if(wasNavBitmapSent) {
                //add a bit of delay for bitmap to be transferred
                String finalNavAdviceTitle = navAdviceTitle;
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 1, finalNavAdviceTitle);
                    }
                }, 1000);
            } else {
                //send text data immediately
                String finalNavAdviceTitle1 = navAdviceTitle;
                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 1, finalNavAdviceTitle1);
                    }
                });
            }
        }

        if(sendText) {
            int sizeOfByteArrayToBeSent;
            byte[] messageBytes = new byte[1];
            if(navAdviceText.isEmpty()) {
                sizeOfByteArrayToBeSent = 1;
            } else {
                messageBytes = navAdviceText.getBytes(Charset.forName("UTF-16LE"));
                sizeOfByteArrayToBeSent = 1 + 1 + messageBytes.length;
            }

            byte[] byteArrayToBeSent = new byte[sizeOfByteArrayToBeSent];

            if(sizeOfByteArrayToBeSent == 1) {
                //send command to clear the textview
                byteArrayToBeSent[0] = (byte)0xA4;
            } else {
                byteArrayToBeSent[0] = (byte)0xA2;
                byteArrayToBeSent[1] = (byte)messageBytes.length;
                System.arraycopy(messageBytes, 0, byteArrayToBeSent, 2, messageBytes.length);
            }

            if(wasNavBitmapSent) {
                //add a bit of delay for bitmap to be transferred
                String finalNavAdviceText = navAdviceText;
                String[] tokens = finalNavAdviceText.split("-");

                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(tokens.length == 1) {
                            sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 2, finalNavAdviceText);
                        } else {
                            sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 2, tokens[0]);
                            sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 3, finalNavAdviceText.substring(finalNavAdviceText.indexOf("-") + 1));
                        }
                    }
                }, 1000);
            } else {
                //send text data immediately
                String finalNavAdviceText1 = navAdviceText;
                String[] tokens1 = finalNavAdviceText1.split("-");
                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(tokens1.length == 1) {
                            sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 2, finalNavAdviceText1);
                        } else {
                            sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 2, tokens1[0]);
                            sendCmdToUpdateMsgOnColorSpeedometer(NAV_MSG_FLAG, false, 3, finalNavAdviceText1.substring(finalNavAdviceText1.indexOf("-") + 1));
                        }
                    }
                });
            }
        }
    }

    private void decideAndSendNavigationArrowForMonochrome() {

        if(PhoneStateReceiver.currentState == TelephonyManager.CALL_STATE_RINGING) {
            return;
        }

        String adviceText = "";

        HashMap<String, Object> tripAdviceDataHashMap = null;

        String tripDataStr = getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(TRIP_ADVICE_DATA_KEY, null);
        if(tripDataStr != null) {
            Type hashMapType = new TypeToken<HashMap<String, Object>>(){}.getType();
            tripAdviceDataHashMap = new Gson().fromJson(tripDataStr, hashMapType);
        } else {
            return;
        }

        //check if rerouting. If yes, send screen 3(for now) 48x32 png image and return.
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_IS_REROUTING_KEY)) {

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inScaled = false;

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_reroute, bitmapOptions);
            transferBitmap(ImageUtils.createScaledAndProcessedBitmap(bitmap, 48, 32), NAVIGATION_IMG_METADATA_CODE);
            return;
        }

        double distanceFromTurnInMeters = -1;
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_DISTANCE_FROM_TURN_KEY)) {
            distanceFromTurnInMeters = (double) tripAdviceDataHashMap.get(TRIP_ADVICE_DISTANCE_FROM_TURN_KEY);
            String distanceFromTurnStr;
            String unitStr;
            if(distanceFromTurnInMeters >= 1000) {
                double distanceFromTurnInKilos = distanceFromTurnInMeters / 1000;
                distanceFromTurnStr = String.format("%.1f", distanceFromTurnInKilos);
                unitStr = "km";
            } else {
                distanceFromTurnStr = String.valueOf(Math.round(distanceFromTurnInMeters));
                unitStr = "m";
            }

            adviceText = distanceFromTurnStr + unitStr;
        }

        if(NotificationReaderService.mCachedCroppedAndScaledNavigationBitmap == null) {
            return;
        }

        mETACircularCount = ++mETACircularCount % 10;

        if(mETACircularCount > 7) {
            generateAndSendDestinationETABitmap(tripAdviceDataHashMap);
            return;
        }

        //direction arrow matches the last one that we sent
        Timber.d("checking for : " + String.valueOf(distanceFromTurnInMeters) + ", " + String.valueOf(mLastSentDistanceFromTurn));
        if(mLastSentBitmap == null ||
            ImageUtils.findBitmapPercentageDiff(mLastSentBitmap, NotificationReaderService.mCachedCroppedAndScaledNavigationBitmap) < 5) {
            if(mLastSentDistanceFromTurn == -1 || mLastSentDistanceFromTurn != distanceFromTurnInMeters) {
                Timber.d("sent for : %s", adviceText);
                //no distance advice was sent last time, send the bitmap
                generateAndSendDistanceAdviceBitmap(adviceText);
                mLastSentDistanceFromTurn = distanceFromTurnInMeters;
                mLastSentBitmap = NotificationReaderService.mCachedCroppedAndScaledNavigationBitmap;
            }
        } else {
            Timber.d("sent for : %s", adviceText);
            //direction arrow change, send the bitmap
            generateAndSendDistanceAdviceBitmap(adviceText);
            mLastSentDistanceFromTurn = distanceFromTurnInMeters;
            mLastSentBitmap = NotificationReaderService.mCachedCroppedAndScaledNavigationBitmap;
        }
    }

    private void generateAndSendDistanceAdviceBitmap(String adviceText) {
        //adviceText = adviceText.toUpperCase();

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;
        bitmapOptions.inMutable = true;


        Bitmap directionArrowBitmap = NotificationReaderService.mCachedCroppedAndScaledNavigationBitmap;

        Bitmap blank48x32Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blank_48_x_32, bitmapOptions);

        //now copy the directionArrowBitmap
        int dirStartXPosition = 0;
        if(!NotificationReaderService.mAlignIconLeft) {
            dirStartXPosition = 48 - directionArrowBitmap.getWidth();
        }
        Canvas canvas = new Canvas(blank48x32Bitmap);

        //set the density right
        directionArrowBitmap.setDensity(canvas.getDensity());

        //calculate the total width that will be required for the text and how many characters we can fit
        ArrayList<Integer> msgParamsArrayList = findMessagePixelParams(adviceText, 48);
        int totalMessageWidth = msgParamsArrayList.get(0);
        final int charIndexLimit = msgParamsArrayList.get(1);
        int msgHeight = msgParamsArrayList.get(2);

        int msgStartXPosition = 0;
        if(NotificationReaderService.mAlignIconLeft) {
            msgStartXPosition = 48 - totalMessageWidth;
        } else {
            msgStartXPosition = 0;
        }

        Bitmap processedDirectionBitmap = checkAndRescaleDirectionBitmapIfRequired(blank48x32Bitmap, directionArrowBitmap, msgStartXPosition, msgHeight, totalMessageWidth, charIndexLimit, adviceText);

        //set density for the bitmap
        processedDirectionBitmap.setDensity(canvas.getDensity());
        processedDirectionBitmap.setHasAlpha(true);

        //draw the bitmap
        canvas.drawBitmap(processedDirectionBitmap, dirStartXPosition, 0, null);


        if(!adviceText.isEmpty()) {
            transferBitmap(writeLineOnBitmap(blank48x32Bitmap, adviceText.substring(0, charIndexLimit + 1), msgStartXPosition, 32), NAVIGATION_IMG_METADATA_CODE);
        } else {
            transferBitmap(writeLineOnBitmap(blank48x32Bitmap, "", msgStartXPosition, 32), NAVIGATION_IMG_METADATA_CODE);

        }

    }

    private Bitmap checkAndRescaleDirectionBitmapIfRequired(Bitmap entire48x32Bitmap,
                                                            Bitmap directionBitmap,
                                                            int msgStartXPosition,
                                                            int msgHeight,
                                                            int totalMessageWidth,
                                                            int charIndexLimit,
                                                            String adviceText) {
        int directionArrowStartXPosition = NotificationReaderService.mAlignIconLeft ? 0 : 48 - directionBitmap.getWidth();

        //find out final coordinates of the message box
        int xLeftMsg, yTopMsg, xRightMsg, yBottomMsg;

        xLeftMsg = msgStartXPosition;
        yTopMsg = 32 - msgHeight;
        xRightMsg = msgStartXPosition + totalMessageWidth - 1;
        yBottomMsg = 31;

        //find out final coordinates of the direction arrow
        int xLeftDir, yTopDir, xRightDir, yBottomDir;

        xLeftDir = directionArrowStartXPosition;
        yTopDir = 0;
        xRightDir = directionArrowStartXPosition + directionBitmap.getWidth() - 1;
        yBottomDir = directionBitmap.getHeight() - 1;

        //check if the bounding box of these two bitmaps intersect at all to begin with
        if(!doRectangleOverLap(new Point(xLeftMsg, yTopMsg), new Point(xRightMsg, yBottomMsg)
                ,new Point(xLeftDir, yTopDir), new Point(xRightDir, yBottomDir))) {
            //no need not do anything, just return
            return directionBitmap;
        }

        //the rectangle intersect
        //calculate the bounds of the intersecting rectangles
        //these would always be the 2nd and 3rd x and y values
        ArrayList<Integer> xCordList = new ArrayList<>();
        ArrayList<Integer> yCordList = new ArrayList<>();

        xCordList.add(xLeftMsg);
        xCordList.add(xRightMsg);
        xCordList.add(xLeftDir);
        xCordList.add(xRightDir);

        yCordList.add(yTopMsg);
        yCordList.add(yBottomMsg);
        yCordList.add(yTopDir);
        yCordList.add(yBottomDir);

        Collections.sort(xCordList);
        Collections.sort(yCordList);

        //use the second and third, x and y values
        int intersectionXLeft = Math.min(xCordList.get(1), xCordList.get(2));
        int intersectionXRight = Math.max(xCordList.get(1), xCordList.get(2));

        int intersectionYTop = Math.min(yCordList.get(1), yCordList.get(2));
        int intersectionYBottom = Math.max(yCordList.get(1), yCordList.get(2));

        //write the line on the blank bitmap first so that we have a reference of the pixel state
        entire48x32Bitmap = writeLineOnBitmap(entire48x32Bitmap, adviceText.substring(0, charIndexLimit + 1), msgStartXPosition, 32);

        boolean pixelOverlapDetected = false;

        for(int xIndex = intersectionXLeft; xIndex <= intersectionXRight && !pixelOverlapDetected; ++xIndex) {
            for(int yIndex = intersectionYTop; yIndex <= intersectionYBottom && !pixelOverlapDetected; ++yIndex) {
                //transform x and y position to corresponding bitmap locations and check if the alpha at that position is non-zero
                //no need to transform for msg as it is already laid out on the bigger bitmap
                int alphaMsg = Color.alpha(entire48x32Bitmap.getPixel(xIndex, yIndex));
                int alphaDir = Color.alpha(directionBitmap.getPixel(xIndex - directionArrowStartXPosition, yIndex));
                if(alphaMsg != 0 && alphaDir != 0) {
                    pixelOverlapDetected = true;
                }
            }
        }

        Bitmap rescaledBitmap;

        if(pixelOverlapDetected) {
            //an actual pixel overlap was detected, we need to rescale the bitmap
            rescaledBitmap = Bitmap.createScaledBitmap(directionBitmap, directionBitmap.getWidth(), 32 - msgHeight, false);
            //set alpha true
            rescaledBitmap.setHasAlpha(true);

            return rescaledBitmap;
        } else {
            return directionBitmap;
        }
    }

    private boolean doRectangleOverLap(Point l1, Point r1, Point l2, Point r2) {
        // If one rectangle is on left side of other
        if (l1.x > r2.x || l2.x > r1.x) {
            return false;
        }

        // If one rectangle is above other
        //inverted signs compared to x in our case as y axis is inverted for bitmaps
        if (l1.y > r2.y || l2.y > r1.y) {
            return false;
        }

        return true;
    }


    private Bitmap writeLineOnBitmap(Bitmap mutableBitmap, String message, int left, int baselineY) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;

        int currentXPosition = left;

        Canvas canvas = new Canvas(mutableBitmap);

        for(int charIndex = 0; charIndex < message.length(); ++charIndex) {
            if(mLCDCharToDrawableHashMap.containsKey(message.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToDrawableHashMap.get(message.charAt(charIndex)), bitmapOptions);
                canvas.drawBitmap(charBitmap, currentXPosition, baselineY - charBitmap.getHeight(), null);
                currentXPosition += charBitmap.getWidth();
                charBitmap.recycle();
            }
        }

        return mutableBitmap;
    }

    private ArrayList<Integer> findMessagePixelParams(String message, int widthInPixels) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;

        int totalMessageWidth = 0;
        int charIndexLimit = 0;
        int maxMessageHeight = 0;
        for(int charIndex = 0; charIndex < message.length(); ++charIndex) {
            if(mLCDCharToDrawableHashMap.containsKey(message.charAt(charIndex))) {
                Bitmap charBitmap = BitmapFactory.decodeResource(getResources(), mLCDCharToDrawableHashMap.get(message.charAt(charIndex)), bitmapOptions);

                //only add char width to calculation if it fits entirely else skip
                if(totalMessageWidth + charBitmap.getWidth() < widthInPixels) {
                    totalMessageWidth += charBitmap.getWidth();
                    charIndexLimit = charIndex;
                    if(charBitmap.getHeight() >= maxMessageHeight) {
                        maxMessageHeight = charBitmap.getHeight();
                    }
                } else {
                    break;
                }
                charBitmap.recycle();
            }
        }
        ArrayList<Integer> msgParamsArrayList = new ArrayList<>();
        msgParamsArrayList.add(totalMessageWidth);
        msgParamsArrayList.add(charIndexLimit);
        msgParamsArrayList.add(maxMessageHeight);

        return msgParamsArrayList;
    }

    private void processOfflineBatteryAdcData(String dataString) {
        String[] dataArray = dataString.split(" ");

        boolean allZero = true;
        //skip the last byte
        for(int index = 0; index < dataArray.length - 2 && allZero; ++index) {
            if(!dataArray[index].equals("00"))
            {
                allZero = false;
                break;
            }
        }

        if(allZero) {
            Timber.d("ALL_ZERO");
            return;
        }

        ArrayList<BatteryAdcParameters> batteryAdcParametersArrayList = new BatteryAdcLogPacketParser(getApplicationContext()).parseLogPacket(dataString);

        logOfflineBatteryAdcData(batteryAdcParametersArrayList);
    }

    private void logOfflineBatteryAdcData(ArrayList<BatteryAdcParameters> batteryAdcParametersList) {
        if(batteryAdcParametersList == null || batteryAdcParametersList.isEmpty()) {
            return;
        }


        if(mWorkingOfflineBatteryAdcLogFile == null) {
            String fileName = String.valueOf(System.currentTimeMillis());
            mWorkingOfflineBatteryAdcLogFile = new File(getExternalFilesDir(VEHICLE_DATA_OFFLINE_BATTERY_ADC_LOG_DIR_PATH), fileName + VEHICLE_DATA_OFFLINE_BATTERY_ADC_LOG_EXTENSION);
            File parentDirectory = mWorkingOfflineBatteryAdcLogFile.getParentFile();
            if(parentDirectory == null) {
                return;
            }
            getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY, parentDirectory.getAbsolutePath()).apply();
        }


        if(mWorkingOfflineBatteryAdcLogFile != null) {
            getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY, mWorkingOfflineBatteryAdcLogFile.getName()).apply();
        } else {
            return;
        }

        try {
            for (BatteryAdcParameters batteryAdcParameters : batteryAdcParametersList) {
                String offlineBatteryAdcLogJSONStr = createOfflineBatteryAdcLogJsonObjectStr(batteryAdcParameters);
                byte[] encryptedBytes = EncryptionHelper.encryptBytes(offlineBatteryAdcLogJSONStr.getBytes(), constructEncryptionKey());

                String encryptedString = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP | Base64.URL_SAFE);

                try (FileWriter fw = new FileWriter(mWorkingOfflineBatteryAdcLogFile, true);
                     BufferedWriter bw = new BufferedWriter(fw);
                     PrintWriter out = new PrintWriter(bw)) {
                    out.println(encryptedString);
                } catch (Exception e) {
                    //exception handling left as an exercise for the reader
                }
            }
        } catch (Exception e) {

        }

        clearWorkingOfflineAdcLogFile();

    }

    private String createOfflineBatteryAdcLogJsonObjectStr(BatteryAdcParameters batteryAdcParameters) {
        String logJsonObjectStr = "";
        JsonObject logJsonObject = new JsonObject();

        String vin = "n/a";

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        if(vehicle != null) {
            vin = vehicle.getVin();
        }

        logJsonObject.addProperty(LOG_TYPE, LOG_TYPE_OFFLINE_BATTERY_ADC);

        logJsonObject.addProperty(LOG_VIN, vin);
        logJsonObject.addProperty(LOG_FIRMWARE_VERSION, mRevosBleManager.getDeviceFirmwareVersion());
        logJsonObject.addProperty(LOG_TIMESTAMP, batteryAdcParameters.getTimestamp() * 1000);
        logJsonObject.addProperty(LOG_IS_TIMESTAMP_VERIFIED, batteryAdcParameters.isTimeStampTrue() == 1);
        logJsonObject.addProperty(LOG_BATTERY_VOLTAGE_ADC, batteryAdcParameters.getBatteryAdcVoltage());

        logJsonObjectStr = new Gson().toJson(logJsonObject);

        return logJsonObjectStr;
    }

    private void processOfflineOBDData(String dataString) {
        String[] dataArray = dataString.split(" ");

        boolean allZero = true;
        //skip the last byte
        for(int index = 0; index < dataArray.length - 2 && allZero; ++index) {
            if(!dataArray[index].equals("00"))
            {
                allZero = false;
                break;
            }
        }

        if(allZero) {
            Timber.d("ALL_ZERO");
            return;
        }

        Pair <Boolean, ArrayList<VehicleParameters>>  vehicleParameterListPair = new OfflineLogPacketParser(getApplicationContext()).parseLogPacket(dataString);

        if(vehicleParameterListPair == null || vehicleParameterListPair.second == null) {
            return;
        }

        logOfflineData(vehicleParameterListPair);

    }

    private void logOfflineData(Pair<Boolean, ArrayList<VehicleParameters>> vehicleParameterListPair) {
        if(vehicleParameterListPair.first == null || vehicleParameterListPair.second == null) {
            return;
        }

        boolean isTimeStampVerified = vehicleParameterListPair.first;

        if(mWorkingOfflineLogFile == null) {
            String fileName = String.valueOf(System.currentTimeMillis());
            mWorkingOfflineLogFile = new File(getExternalFilesDir(VEHICLE_DATA_OFFLINE_LOG_DIR_PATH), fileName + VEHICLE_DATA_OFFLINE_LOG_EXTENSION);
            File parentDirectory = mWorkingOfflineLogFile.getParentFile();
            if(parentDirectory == null) {
                return;
            }
            getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_OFFLINE_LOG_DIR_KEY, parentDirectory.getAbsolutePath()).apply();
        }


        if(mWorkingOfflineLogFile != null) {
            getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(WORKING_OFFLINE_LOG_FILE_NAME_KEY, mWorkingOfflineLogFile.getName()).apply();
        } else {
            return;
        }

        try {
            for (VehicleParameters vehicleParameters : vehicleParameterListPair.second) {
                String offlineLogJSONStr = createOfflineLogJsonObjectStr(isTimeStampVerified, vehicleParameters);
                byte[] encryptedBytes = EncryptionHelper.encryptBytes(offlineLogJSONStr.getBytes(), constructEncryptionKey());

                String encryptedString = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP | Base64.URL_SAFE);

                try (FileWriter fw = new FileWriter(mWorkingOfflineLogFile, true);
                     BufferedWriter bw = new BufferedWriter(fw);
                     PrintWriter out = new PrintWriter(bw)) {
                    out.println(encryptedString);
                } catch (Exception e) {
                    //exception handling left as an exercise for the reader
                }
            }
        } catch (Exception e) {

        }

        clearWorkingOfflineLogFile();
    }

    private String createOfflineLogJsonObjectStr(boolean isTimeStampVerified, VehicleParameters vehicleParameters) {
        String logJsonObjectStr = "";
        JsonObject logJsonObject = new JsonObject();

        String vehicleId = "n/a";
        String vin = "n/a";
        String regen = "off";

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        if(vehicle != null) {
            vehicleId = vehicle.getVehicleId();
            vin = vehicle.getVin();
        }

        logJsonObject.addProperty(LOG_TYPE, LOG_TYPE_OFFLINE);

        logJsonObject.addProperty(LOG_VIN, vin);
        logJsonObject.addProperty(LOG_FIRMWARE_VERSION, mRevosBleManager.getDeviceFirmwareVersion());
        logJsonObject.addProperty(LOG_TRIP_ID, String.valueOf(vehicleParameters.getTripId()));
        logJsonObject.addProperty(LOG_TIMESTAMP, vehicleParameters.getTimeStamp() * 1000);
        logJsonObject.addProperty(LOG_IS_TIMESTAMP_VERIFIED, isTimeStampVerified);
        logJsonObject.addProperty(LOG_WHEEL_RPM, vehicleParameters.getRpm());
        logJsonObject.addProperty(LOG_THROTTLE, vehicleParameters.getThrottle());
        logJsonObject.addProperty(LOG_BATTERY_VOLTAGE, vehicleParameters.getVoltage());
        logJsonObject.addProperty(LOG_BATTERY_CURRENT, vehicleParameters.getCurrent());
        logJsonObject.addProperty(LOG_ODO, vehicleParameters.getOdo());
        logJsonObject.addProperty(LOG_CONTROLLER_TEMP, vehicleParameters.getTemperature());
        logJsonObject.addProperty(LOG_MODE, vehicleParameters.getMode());
        logJsonObject.addProperty(LOG_SOFT_LOCK_STATUS, vehicleParameters.isLocked());
        logJsonObject.addProperty(LOG_OVERLOAD_STATUS, vehicleParameters.isOverLoad());
        logJsonObject.addProperty(LOG_OVERCURRENT_STATUS, vehicleParameters.isOverCurrent());
        logJsonObject.addProperty(LOG_REVERSE_STATUS, vehicleParameters.isReverse());
        logJsonObject.addProperty(LOG_PARKING_STATUS, vehicleParameters.isParking());
        logJsonObject.addProperty(LOG_ANTI_THEFT_STATUS, vehicleParameters.isAntiTheftEngaged());
        logJsonObject.addProperty(LOG_EABS_STATUS, vehicleParameters.isEABSEngaged());
        logJsonObject.addProperty(LOG_CHARGING_STATUS, vehicleParameters.isCharging());
        logJsonObject.addProperty(LOG_REGEN_BRAKING_STATUS, vehicleParameters.isRegenBraking());
        logJsonObject.addProperty(LOG_BRAKE_STATUS, vehicleParameters.isBraking());
        logJsonObject.addProperty(LOG_THROTTLE_STATUS, vehicleParameters.isThrottleError());
        logJsonObject.addProperty(LOG_CONTROLLER_STATUS, vehicleParameters.isControllerError());
        logJsonObject.addProperty(LOG_MOTOR_STATUS, vehicleParameters.isMotorError());
        logJsonObject.addProperty(LOG_HILL_ASSIST_STATUS, vehicleParameters.isHillAssistEngaged());

        logJsonObjectStr = new Gson().toJson(logJsonObject);

        return logJsonObjectStr;
    }

    public static String getDeviceMetadataString() {
        if(mDeviceMetadataString == null) {
            return "n/a";
        } else {
            return mDeviceMetadataString;
        }
    }

    private void checkAndMarkVehicleCrash() {
        long crashTimestamp = getSharedPreferences(APP_PREFS, MODE_PRIVATE).getLong(CRASH_TIMESTAMP_KEY, 0);
        long currentTimestamp = System.currentTimeMillis();

        final float ACC_THRESHOLD = 6.5f;
        final int BUFFER_SIZE = 20;

        if(mAccelerationValuesList == null) {
            mAccelerationValuesList = new ArrayList<>();
        }

        mAccelerationValuesList.add(mVehicleLiveParameters.getYAcc());


        if(mAccelerationValuesList.size() < BUFFER_SIZE) {
            return;
        }

        if(mAccelerationValuesList.size() == BUFFER_SIZE) {
            mAccelerationValuesList.remove(0);
        }

        float sum = 0;
        float average = 0;

        for(float accValue : mAccelerationValuesList) {
            sum += accValue;
        }

        average = sum / BUFFER_SIZE;

        if(Math.abs(average) < ACC_THRESHOLD) {
            return;
        }

        if(crashTimestamp != 0 && currentTimestamp - crashTimestamp < SOS_COOL_OFF_IN_MILLIS) {
            return;
        }

        long smsSentTimestamp = getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).getLong(SMS_SENT_TIMESTAMP_KEY, 0);

        if(smsSentTimestamp != 0 && currentTimestamp - smsSentTimestamp < SOS_COOL_OFF_IN_MILLIS) {
            return;
        }

        getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putLong(CRASH_TIMESTAMP_KEY, System.currentTimeMillis()).apply();
        getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).edit().putBoolean(IS_SOS_SMS_SENT_KEY, false).apply();

        EventBus.getDefault().post(new EventBusMessage(GENERAL_EVENT_START_SOS));
    }

    private void processLiveOBDData(String dataString) {

        String[] dataArray = dataString.split(" ");

        boolean allZero = true;
        //skip the last byte
        for(int index = 0; index < dataArray.length - 2 && allZero; ++index) {
            if(!dataArray[index].equals("00"))
            {
                allZero = false;
                break;
            }
        }

        if(allZero) {
            Timber.d("ALL_ZERO");
            return;
        }

        mVehicleLiveParameters = new VehicleParameterParser(getApplicationContext(), dataString).parseAndCreateVehicleParametersObject(PACKET_TYPE_LIVE);

        if(mVehicleLiveParameters == null) {
            return;
        }

        long imageMetadataCode = mVehicleLiveParameters.getDisplayedImageCode();

        if(imageMetadataCode == 0) {
            clearLastSentDistanceAndBitmap();
        }
        checkAndMarkVehicleCrash();
        clearSpeedometerMessageIfRequired();
        logLiveOBDData();

        if(mCurrentLocation == null) {
            return;
        }

        //store lat long locally in vehicle prefs
        SharedPreferences sharedPreferences = getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putString(VEHICLE_LAST_KNOWN_LATITUDE, String.valueOf(mCurrentLocation.getLatitude())).apply();
        sharedPreferences.edit().putString(VEHICLE_LAST_KNOWN_LONGITUDE, String.valueOf(mCurrentLocation.getLongitude())).apply();
    }

    private String createLiveLogJsonObjectStr() {
        String logJsonObjectStr = "";
        JsonObject logJsonObject = new JsonObject();

        String vehicleId = "n/a";
        String vin = "n/a";
        String regen = "off";

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        if(vehicle != null) {
            vehicleId = vehicle.getVehicleId();
            vin = vehicle.getVin();
        }

        logJsonObject.addProperty(LOG_TYPE, LOG_TYPE_LIVE);

        logJsonObject.addProperty(LOG_VIN, vin);
        logJsonObject.addProperty(LOG_FIRMWARE_VERSION, mRevosBleManager.getDeviceFirmwareVersion());
        logJsonObject.addProperty(LOG_TRIP_ID, String.valueOf(mVehicleLiveParameters.getTripId()));
        logJsonObject.addProperty(LOG_TIMESTAMP, System.currentTimeMillis());
        logJsonObject.addProperty(LOG_WHEEL_RPM, mVehicleLiveParameters.getRpm());
        logJsonObject.addProperty(LOG_THROTTLE, mVehicleLiveParameters.getThrottle());
        logJsonObject.addProperty(LOG_BATTERY_VOLTAGE, mVehicleLiveParameters.getVoltage());
        logJsonObject.addProperty(LOG_BATTERY_CURRENT, mVehicleLiveParameters.getCurrent());
        logJsonObject.addProperty(LOG_ODO, mVehicleLiveParameters.getOdo());
        logJsonObject.addProperty(LOG_CONTROLLER_TEMP, mVehicleLiveParameters.getTemperature());
        logJsonObject.addProperty(LOG_MODE, mVehicleLiveParameters.getMode());
        logJsonObject.addProperty(LOG_SOFT_LOCK_STATUS, mVehicleLiveParameters.isLocked());
        logJsonObject.addProperty(LOG_OVERLOAD_STATUS, mVehicleLiveParameters.isOverLoad());
        logJsonObject.addProperty(LOG_OVERCURRENT_STATUS, mVehicleLiveParameters.isOverCurrent());
        logJsonObject.addProperty(LOG_REVERSE_STATUS, mVehicleLiveParameters.isReverse());
        logJsonObject.addProperty(LOG_PARKING_STATUS, mVehicleLiveParameters.isParking());
        logJsonObject.addProperty(LOG_ANTI_THEFT_STATUS, mVehicleLiveParameters.isAntiTheftEngaged());
        logJsonObject.addProperty(LOG_EABS_STATUS, mVehicleLiveParameters.isEABSEngaged());
        logJsonObject.addProperty(LOG_CHARGING_STATUS, mVehicleLiveParameters.isCharging());
        logJsonObject.addProperty(LOG_REGEN_BRAKING_STATUS, mVehicleLiveParameters.isRegenBraking());
        logJsonObject.addProperty(LOG_BRAKE_STATUS, mVehicleLiveParameters.isBraking());
        logJsonObject.addProperty(LOG_THROTTLE_STATUS, mVehicleLiveParameters.isThrottleError());
        logJsonObject.addProperty(LOG_CONTROLLER_STATUS, mVehicleLiveParameters.isControllerError());
        logJsonObject.addProperty(LOG_MOTOR_STATUS, mVehicleLiveParameters.isMotorError());
        logJsonObject.addProperty(LOG_HILL_ASSIST_STATUS, mVehicleLiveParameters.isHillAssistEngaged());

        logJsonObject.addProperty(LOG_HEADLIGHT, mVehicleLiveParameters.isHeadLampOn());
        logJsonObject.addProperty(LOG_LEFT_INDICATOR, mVehicleLiveParameters.isLeftIndicatorOn());
        logJsonObject.addProperty(LOG_RIGHT_INDICATOR, mVehicleLiveParameters.isRightIndicatorOn());
        logJsonObject.addProperty(LOG_X_ACC, mVehicleLiveParameters.getXAcc());
        logJsonObject.addProperty(LOG_Y_ACC, mVehicleLiveParameters.getYAcc());
        logJsonObject.addProperty(LOG_Z_ACC, mVehicleLiveParameters.getZAcc());

        logJsonObject.addProperty(LOG_SPEED_LIMIT, mVehicleLiveParameters.getSpeedCheckByte());
        logJsonObject.addProperty(LOG_CURRENT_LIMIT, mVehicleLiveParameters.getCurrentCheckByte());
        logJsonObject.addProperty(LOG_UNDER_VOLTAGE_LIMIT, mVehicleLiveParameters.getUnderVoltageCheckByte());
        logJsonObject.addProperty(LOG_OVER_VOLTAGE_LIMIT, mVehicleLiveParameters.getOverVoltageCheckByte());
        logJsonObject.addProperty(LOG_ZERO_THROTTLE_REGEN_LIMIT, mVehicleLiveParameters.getThrottleZeroRegenCheckByte());
        logJsonObject.addProperty(LOG_BRAKE_REGEN_LIMIT, mVehicleLiveParameters.getBrakeRegenCheckByte());
        logJsonObject.addProperty(LOG_PICKUP_CONTROL_LIMIT, mVehicleLiveParameters.getPickupPercentageCheckByte());

        if(mDeviceFirmwareVersion != null
            && !mDeviceFirmwareVersion.isEmpty()
            && new Version(mDeviceFirmwareVersion).compareTo(new Version(VER_1_0_0)) > 0) {
            logJsonObject.addProperty(LOG_BATTERY_VOLTAGE_ADC, mVehicleLiveParameters.getBatteryAdcVoltage());
        }


        if(mCurrentLocation != null) {
            logJsonObject.addProperty(LOG_LAT, mCurrentLocation.getLatitude());
            logJsonObject.addProperty(LOG_LNG, mCurrentLocation.getLongitude());

            if(mCurrentLocation.hasAltitude()) {
                logJsonObject.addProperty(LOG_ELEVATION, mCurrentLocation.getAltitude());
            }

            if(mCurrentLocation.hasSpeed()) {
                logJsonObject.addProperty(LOG_GPS_SPEED, mCurrentLocation.getSpeed() * 18 / 5);
            }

            if(mCurrentLocation.hasBearing()) {
                logJsonObject.addProperty(LOG_HEADING, mCurrentLocation.getBearing());
            }
        } else {
            //request for last known location
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                logJsonObjectStr = new Gson().toJson(logJsonObject);

                return logJsonObjectStr;
            }

            LocationServices.getFusedLocationProviderClient(getApplicationContext()).getLastLocation().addOnSuccessListener((Executor) this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mCurrentLocation = location;
                    //write last location to prefs
                    SharedPreferences sharedpreferences = getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(VEHICLE_LAST_KNOWN_LATITUDE, String.valueOf(mCurrentLocation.getLatitude())).apply();
                    editor.putString(VEHICLE_LAST_KNOWN_LONGITUDE, String.valueOf(mCurrentLocation.getLongitude())).apply();

                    EventBus.getDefault().post(new EventBusMessage(GEN_EVENT_PHONE_LOCATION_UPDATED));
                }
            });
        }

        logJsonObjectStr = new Gson().toJson(logJsonObject);

        return logJsonObjectStr;
    }


    private void logLiveOBDData() {
        //check if data string is not entirely zero, except for the image byte being sent

        if(mWorkingLiveLogFile == null) {
            String fileName = String.valueOf(System.currentTimeMillis());
            mWorkingLiveLogFile = new File(getExternalFilesDir(VEHICLE_DATA_LIVE_LOG_DIR_PATH), fileName + VEHICLE_DATA_LIVE_LOG_EXTENSION);
            File parentDirectory = mWorkingLiveLogFile.getParentFile();
            getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_LIVE_LOG_DIR_KEY, parentDirectory.getAbsolutePath()).apply();
        } else {
            try {
                if(!mWorkingLiveLogFile.exists() || countLinesInFile(mWorkingLiveLogFile.getAbsolutePath()) >= LIVE_LOG_FILE_SIZE_LIMIT) {
                    //create a new log part file

                    String fileName = String.valueOf(System.currentTimeMillis());
                    mWorkingLiveLogFile = new File(getExternalFilesDir(VEHICLE_DATA_LIVE_LOG_DIR_PATH), fileName + VEHICLE_DATA_LIVE_LOG_EXTENSION);
                    File parentDirectory = mWorkingLiveLogFile.getParentFile();
                    if(parentDirectory == null) {
                        return;
                    }
                    getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_LIVE_LOG_DIR_KEY, parentDirectory.getAbsolutePath()).apply();
                }
            } catch (Exception e) {
                return;
            }
        }


        if(mWorkingLiveLogFile != null) {
            getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(WORKING_LIVE_LOG_FILE_NAME_KEY, mWorkingLiveLogFile.getName()).apply();
        } else {
            return;
        }

        try {
            String logJsonObjectStr = createLiveLogJsonObjectStr();

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(logJsonObjectStr.getBytes(), constructEncryptionKey());

            String encryptedString = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP | Base64.URL_SAFE);

            //check if the timestamp is larger than the previous one
            if (mVehicleLiveParameters.getTimeStamp() > mLastLiveLogSyncedTimestamp) {
                mLastLiveLogSyncedTimestamp = mVehicleLiveParameters.getTimeStamp();
                try (FileWriter fw = new FileWriter(mWorkingLiveLogFile, true);
                     BufferedWriter bw = new BufferedWriter(fw);
                     PrintWriter out = new PrintWriter(bw)) {
                    out.println(encryptedString);
                } catch (Exception e) {
                    //exception handling left as an exercise for the reader
                    mLastLiveLogSyncedTimestamp = -1;
                }
            }
        } catch (Exception e) {

        }

    }

    private String constructEncryptionKey() {

        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        String devicePin = getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        if(cloudDevice == null || devicePin == null) {
            return null;
        }

        byte[] aesKey = Base64.decode(cloudDevice.getKey(), Base64.NO_WRAP | Base64.URL_SAFE);

        for(int byteIndex = 0; byteIndex < devicePin.length(); ++byteIndex) {
            aesKey[aesKey.length - devicePin.length() + byteIndex] = (byte)devicePin.charAt(byteIndex);
        }

        return Base64.encodeToString(aesKey, Base64.NO_WRAP|Base64.URL_SAFE);
    }

    private void showMessageOnSpeedometerHelper(String line1, String line2, String line3, int timeInMillis) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;

        boolean alignTextLeft = true;

        Bitmap blankBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blank_48_x_32, bitmapOptions);

        Bitmap mutableBitmap = blankBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(mutableBitmap);

        final int LINE_1_X_POSITION = 0, LINE_2_X_POSITION = 0, LINE_3_X_POSITION = 0;
        final int LINE_1_Y_POSITION = 10;
        final int LINE_2_Y_POSITION = LINE_1_Y_POSITION + 10;
        final int LINE_3_Y_POSITION = LINE_2_Y_POSITION + 10;

        mutableBitmap = writeLineOnBitmap(mutableBitmap, line1, LINE_1_X_POSITION, LINE_1_Y_POSITION);
        mutableBitmap = writeLineOnBitmap(mutableBitmap, line2, LINE_2_X_POSITION, LINE_2_Y_POSITION);
        mutableBitmap = writeLineOnBitmap(mutableBitmap, line3, LINE_3_X_POSITION, LINE_3_Y_POSITION);

        if(isColorSpeedometer()) {
            if(line1.equals("VEHICLE") && line2.equals("LOCKED")) {
                sendShowVehicleLockedIconCommandForColorSpeedometer();
            } else if(line1.equals("VEHICLE") && line2.equals("UNLOCK")) {
                sendShowVehicleUnlockedIconCommandForColorSpeedometer();
            } else if(line1.equals("HANDLE") && line2.equals("LOCKED")) {
                sendShowHandleLockedIconCommandForColorSpeedometer();
            } else if(line1.equals("HANDLE") && line2.equals("UNLOCK")) {
                sendShowHandleUnlockedIconCommandForColorSpeedometer();
            } else if(line1.equals("SEAT") && line2.equals("UNLOCK")) {
                sendShowSeatUnlockedIconCommandForColorSpeedometer();
            }
        } else {
            transferBitmap(mutableBitmap, GENERIC_IMG_METADATA_CODE);
        }

        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit()
                .putLong(BLUETOOTH_SPEEDOMETER_MESSAGE_EXPIRY_TIME, System.currentTimeMillis() + timeInMillis).apply();

        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit()
                .putBoolean(BLUETOOTH_SPEEDOMETER_MESSAGE_ERASED, false).apply();
    }


    private int countLinesInFile(String filename) throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(filename));

        try {
            byte[] c = new byte[1024];

            int readChars = is.read(c);
            if(readChars == -1) {
                // bail out if nothing to read
                return 0;
            }

            // make it easy for the optimizer to tune this loop
            int count = 0;
            while (readChars == 1024) {
                for (int i=0; i<1024;) {
                    if(c[i++] == '\n') {
                        ++count;
                    }
                }
                readChars = is.read(c);
            }

            // count remaining characters
            while (readChars != -1) {
                System.out.println(readChars);
                for (int i=0; i<readChars; ++i) {
                    if(c[i] == '\n') {
                        ++count;
                    }
                }
                readChars = is.read(c);
            }

            return count == 0 ? 1 : count;
        } finally {
            is.close();
        }
    }


    /*
    private void storeVehicleParamsStateAndRewriteControlParamsIfRequired(String dataString) {
        String dataArray[] = dataString.split(" ");

        byte commandStringHeader = (byte)Integer.parseInt(COMMAND_STRING_HEADER, 16);

        byte softLockHeader = (byte)Integer.parseInt(SOFT_LOCK_HEADER, 16);
        byte softLockStatus = (byte)Integer.parseInt(dataArray[SOFT_LOCK], 16);

        byte speedLimitHeader = (byte)Integer.parseInt(SPEED_LIMIT_HEADER, 16);
        byte speedLimit = (byte)Integer.parseInt(dataArray[SPEED_LIMIT], 16);

        byte currentLimitHeader = (byte)Integer.parseInt(CURRENT_LIMIT_HEADER, 16);
        byte currentLimit = (byte)Integer.parseInt(dataArray[CURRENT_LIMIT], 16);

        byte voltageLimitHeader = (byte)Integer.parseInt(VOLTAGE_LIMIT_HEADER, 16);
        byte voltageLimit = (byte)Integer.parseInt(dataArray[VOLTAGE_LIMIT], 16);

        byte regenLimit = (byte)Integer.parseInt(dataArray[RESERVED_BYTE], 16);

        byte delimiter = (byte)Integer.parseInt(CONTROL_PARAMS_DELIMITER, 16);

        ArrayList<Byte> commandArrayList = new ArrayList<>();
        commandArrayList.add(commandStringHeader);
        commandArrayList.add(softLockHeader);
        commandArrayList.add(softLockStatus);
        commandArrayList.add(speedLimitHeader);
        commandArrayList.add(speedLimit);
        commandArrayList.add(currentLimitHeader);
        commandArrayList.add(currentLimit);
        commandArrayList.add(voltageLimitHeader);
        commandArrayList.add(voltageLimit);
        commandArrayList.add(regenLimit);
        commandArrayList.add(delimiter);

        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putString(BLUETOOTH_LAST_READ_VEHICLE_CONTROL_PARAMS_KEY, new Gson().toJson(commandArrayList)).apply();

        String expectedCommandArrayListString = sharedPreferences.getString(BLUETOOTH_EXPECTED_CONTROLLER_PARAMS_KEY, null);
        if(expectedCommandArrayListString == null) {
            return;
        }

        ArrayList<Byte> expectedCommandArrayList = new Gson().fromJson(expectedCommandArrayListString, new TypeToken<ArrayList<Byte>>(){}.getType());

        boolean commandStringsEqual = true;

        byte[] commandByteArray = new byte[WRITE_PARAMS_COMMAND_LENGTH];

        //compare the the command strings
        for(int byteIndex = 0; byteIndex < commandArrayList.size(); ++byteIndex) {
            if((byte)commandArrayList.get(byteIndex) != expectedCommandArrayList.get(byteIndex)) {
                commandStringsEqual = false;
            }
            commandByteArray[byteIndex] = expectedCommandArrayList.get(byteIndex);
        }

        long lastAttemptedWriteTimeStamp = sharedPreferences.getLong(BLUETOOTH_CONTROL_PARAMS_WRITE_LAST_ATTEMPT_TIMESTAMP, 0);

        if(!commandStringsEqual && (lastAttemptedWriteTimeStamp !=0) && ((System.currentTimeMillis() - lastAttemptedWriteTimeStamp) > 4000)) {
            sharedPreferences.edit().putLong(BLUETOOTH_CONTROL_PARAMS_WRITE_LAST_ATTEMPT_TIMESTAMP, System.currentTimeMillis()).apply();
            mRevosBleManager.writeDataToOBDCharacteristic(commandByteArray);
        } else if(commandStringsEqual) {
            //command parameters have synced, clear our internal state
            sharedPreferences.edit().putString(BLUETOOTH_EXPECTED_CONTROLLER_PARAMS_KEY, null).apply();
        }
    }*/

    private void clearSpeedometerMessageIfRequired() {
        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
        boolean hasSpeedometerMessageBeenErased = sharedPreferences.getBoolean(BLUETOOTH_SPEEDOMETER_MESSAGE_ERASED, true);
        if(hasSpeedometerMessageBeenErased) {
            return;
        }

        long messageExpiryTime = sharedPreferences.getLong(BLUETOOTH_SPEEDOMETER_MESSAGE_EXPIRY_TIME, 0);
        if(System.currentTimeMillis() > messageExpiryTime ) {
            clearSpeedometerImageAndTextHelper();
            sharedPreferences.edit().putBoolean(BLUETOOTH_SPEEDOMETER_MESSAGE_ERASED, true).apply();
        }
    }

    private void sendEmergencySMSIfRequired() {
        long crashTimestamp = getSharedPreferences(APP_PREFS, MODE_PRIVATE).getLong(CRASH_TIMESTAMP_KEY, 0);

        if(crashTimestamp == 0) {
            return;
        }

        if(NetworkUtils.getInstance(getApplicationContext()).isSosSMSInProgress()) {
            return;
        }

        if(System.currentTimeMillis() - crashTimestamp > SOS_COOL_OFF_IN_MILLIS) {
            SharedPreferences sharedPreferences = getSharedPreferences(USER_PREFS, MODE_PRIVATE);
            String lat = sharedPreferences.getString(VEHICLE_LAST_KNOWN_LATITUDE, "");
            String lng = sharedPreferences.getString(VEHICLE_LAST_KNOWN_LONGITUDE, "");

            User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

            if(user == null) {
                return;
            }

            String mapString = String.format(getResources().getString(R.string.map_url_dynamic), lat, lng);
            String alertMessage = String.format(getResources().getString(R.string.alert_message_dynamic), user.getFirstName() + " " + user.getLastName(), user.getPhone(), mapString);

            mainRepository.sendEmergencySMS(getApplicationContext(), alertMessage);
        }
    }


    private void clearImageStateFlags() {
        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putInt(BLUETOOTH_EXPECTED_IMAGE_CODE_KEY, -1).apply();
        sharedPreferences.edit().putLong(BLUETOOTH_LAST_IMAGE_TX_ATTEMPT_TIMESTAMP, -1).apply();
    }

    private void clearUserManuallyDisconnectedFlag() {
        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, false).apply();
    }

    private void reconnect() {
        //check if we have a saved device else bail
        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
        if(sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null) == null ||
                sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null) == null ||
                sharedPreferences.getBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, false)) {
            return;
        }

        //check if we have a saved device else bail
        //check if we are within retry limit
        int currentRetryCount = sharedPreferences.getInt(BLUETOOTH_RETRY_ATTEMPT_COUNT_KEY, 0);
        if(currentRetryCount > MAX_RETRY_ATTEMPT_COUNT) {
            return;
        }

        connectToDeviceHelper(sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null));

        //increment the retry count in shared prefs
        sharedPreferences.edit().putInt(BLUETOOTH_RETRY_ATTEMPT_COUNT_KEY, ++currentRetryCount).apply();
    }

    private void clearWorkingLogFiles() {
        clearWorkingLiveLogFile();
        clearWorkingOfflineLogFile();
        clearWorkingOfflineAdcLogFile();
    }

    private void clearWorkingOfflineAdcLogFile() {
        if(mWorkingOfflineBatteryAdcLogFile != null) {
            mWorkingOfflineBatteryAdcLogFile = null;
        }
        getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY, null).apply();
    }

    private void clearWorkingLiveLogFile() {
        if(mWorkingLiveLogFile != null) {
            mWorkingLiveLogFile = null;
        }
        getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(WORKING_LIVE_LOG_FILE_NAME_KEY, null).apply();
    }

    private void clearWorkingOfflineLogFile() {
        if(mWorkingOfflineLogFile != null) {
            mWorkingOfflineLogFile = null;
        }
        getSharedPreferences(LOG_PREFS, MODE_PRIVATE).edit().putString(WORKING_OFFLINE_LOG_FILE_NAME_KEY, null).apply();
    }

    private void clearSpeedometerImageAndTextHelper() {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blank_48_x_32, bitmapOptions);

        if(isColorSpeedometer()) {
            sendCmdToClearImgOrMsgOnColorSpeedometer(true, true, true, true, true);
        } else {
            transferBitmap(bitmap, BLANK_IMG_METADATA_CODE);
        }
    }

    private void rotateTestImageSequence() {

        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);

        int imageSequence = sharedPreferences.getInt(BLUETOOTH_IMAGE_SEQUENCE_KEY, 0);

        ++imageSequence;

        sharedPreferences.edit().putInt(BLUETOOTH_IMAGE_SEQUENCE_KEY, imageSequence).apply();

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;
        int drawableId = mImageSequenceHashMap.get(imageSequence % mImageSequenceHashMap.size());
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableId, bitmapOptions);

        transferBitmap(bitmap, GENERIC_IMG_METADATA_CODE);
    }

    private void transferNavLargeBitmapInParts() {
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(NotificationReaderService.mCachedNavigationBitmap == null) {
                    return;
                }

                int sizeOfByteArrayToBeSent;
                if(!mLargeBitmapFirstPartTransferInitiated && !mLargeBitmapFirstPartTransferCompleted) {
                    sizeOfByteArrayToBeSent = 4 + (505);
                } else {
                    sizeOfByteArrayToBeSent = 4 + (968 - 505);
                }

                byte[] byteArrayToBeSent = new byte[sizeOfByteArrayToBeSent];

                byte[] imageByteArray = new byte[BIG_BITMAP_BUFFER_LENGTH];

                if(!mLargeBitmapFirstPartTransferInitiated && !mLargeBitmapFirstPartTransferCompleted) {
                    LARGE_BITMAP_TRANSFER_CODE = new Random().nextInt(128);
                }

                Bitmap scaledAndBinaryAlphaBitmap = Bitmap.createScaledBitmap(NotificationReaderService.mCachedNavigationBitmap, 88, 88, false);
                scaledAndBinaryAlphaBitmap.setHasAlpha(true);

                //process the bitmap first
                for(int x = 0; x < scaledAndBinaryAlphaBitmap.getWidth(); ++x) {
                    for(int y = 0; y < scaledAndBinaryAlphaBitmap.getHeight(); ++y) {
                        int pixel = scaledAndBinaryAlphaBitmap.getPixel(x,y);

                        //if the alpha is greater than 0 alpha to 256
                        if(Color.alpha(pixel) > 127) {
                            scaledAndBinaryAlphaBitmap.setPixel(x,y, 0xFF000000);
                        } else {
                            scaledAndBinaryAlphaBitmap.setPixel(x,y, 0x00000000);
                        }
                    }
                }

                //generate image encoding
                byte tempByte = 0;
                int pixelCount = 0;
                for(int y = 0; y < scaledAndBinaryAlphaBitmap.getHeight(); y = y + 8) {
                    for(int x = 0; x < scaledAndBinaryAlphaBitmap.getWidth(); ++x) {

                        if(pixelCount > 0 && pixelCount % 8 == 0) {
                            imageByteArray[Math.round((pixelCount - 1) / 8)] = tempByte;
                            tempByte = 0;
                        }

                        for(int bitIndex = 7; bitIndex >= 0; --bitIndex) {
                            int pixel = scaledAndBinaryAlphaBitmap.getPixel(x,y + bitIndex);
                            //Timber.d("pixel value : " + x + ", " + (y+bitIndex));
                            int alpha = Color.alpha(pixel);
                            if(alpha == 0 || pixel == -1) {
                                tempByte = (byte)(tempByte << 1);
                                Timber.d("transparent");
                            } else {
                                tempByte = (byte)((tempByte << 1) | 0x01);
                                Timber.d("black, %s", alpha);
                            }

                            ++pixelCount;
                        }
                    }
                }

                imageByteArray[BIG_BITMAP_BUFFER_LENGTH - 1] = tempByte;

                if(!mLargeBitmapFirstPartTransferInitiated && !mLargeBitmapFirstPartTransferCompleted) {
                    byteArrayToBeSent[0] = (byte)0x31;
                } else {
                    byteArrayToBeSent[0] = (byte)0x32;
                }

                byteArrayToBeSent[1] = (byte)(LARGE_BITMAP_TRANSFER_CODE);
                byteArrayToBeSent[2] = 88;
                byteArrayToBeSent[3] = 88;

                //now copy contents accordingly
                if(!mLargeBitmapFirstPartTransferInitiated && !mLargeBitmapFirstPartTransferCompleted) {
                    //copy first 505 bytes
                    System.arraycopy(imageByteArray, 0, byteArrayToBeSent, 4, 505);
                } else {
                    //copy the remaining bytes
                    System.arraycopy(imageByteArray, 505, byteArrayToBeSent, 4, 968 - 505);
                }

                if(!mLargeBitmapFirstPartTransferInitiated && !mLargeBitmapFirstPartTransferCompleted) {
                    mLargeBitmapFirstPartTransferInitiated = true;
                    mLargeBitmapFirstPartTransferCompleted = false;

                    mLargeBitmapSecondPartTransferInitiated = false;
                    mLargeBitmapSecondPartTransferCompleted = false;
                } else {
                    mLargeBitmapSecondPartTransferInitiated = true;
                    mLargeBitmapSecondPartTransferCompleted = false;
                }

                mLastSentBitmap = NotificationReaderService.mCachedNavigationBitmap;

                mRevosBleManager.writeDataToIMGCharacteristic(byteArrayToBeSent);

                scaledAndBinaryAlphaBitmap.recycle();
            }
        });
    }

    /*private void transferLargeBitmap() {
        if(NotificationReaderService.mCachedBitmap == null) {
            return;
        }

        byte[] byteArrayToBeSent = new byte[BIG_BITMAP_BUFFER_LENGTH];


        Bitmap scaledAndBinaryAlphaBitmap = Bitmap.createScaledBitmap(NotificationReaderService.mCachedBitmap, 88, 88, false);
        scaledAndBinaryAlphaBitmap.setHasAlpha(true);

        //process the bitmap first
        for(int x = 0; x < scaledAndBinaryAlphaBitmap.getWidth(); ++x) {
            for(int y = 0; y < scaledAndBinaryAlphaBitmap.getHeight(); ++y) {
                int pixel = scaledAndBinaryAlphaBitmap.getPixel(x,y);

                //if the alpha is not 255 then set the alpha to zero
                if(Color.alpha(pixel) != 0xFF) {
                    scaledAndBinaryAlphaBitmap.setPixel(x,y, 0x00000000);
                } else {
                    scaledAndBinaryAlphaBitmap.setPixel(x,y, 0xFF000000);
                }
            }
        }

        //flag packet
        byteArrayToBeSent[0] = (byte)0x33;
        //image unique code
        byteArrayToBeSent[1] = (byte)0x64;
        //image width
        byteArrayToBeSent[2] = 88;
        //image height
        byteArrayToBeSent[3] = 88;


        byte[] imageByteArray = new byte[968];

        byte tempByte = 0;

        int pixelCount = 0;

        for(int y = 0; y < scaledAndBinaryAlphaBitmap.getHeight(); y = y + 8) {
            for(int x = 0; x < scaledAndBinaryAlphaBitmap.getWidth(); ++x) {

                if(pixelCount > 0 && pixelCount % 8 == 0) {
                    imageByteArray[Math.round((pixelCount - 1) / 8)] = tempByte;
                    tempByte = 0;
                }

                for(int bitIndex = 7; bitIndex >= 0; --bitIndex) {
                    int pixel = scaledAndBinaryAlphaBitmap.getPixel(x,y + bitIndex);
                    //Timber.d("pixel value : " + x + ", " + (y+bitIndex));
                    int alpha = Color.alpha(pixel);
                    if(alpha == 0 || pixel == -1) {
                        tempByte = (byte)(tempByte << 1);
                        Timber.d("transparent");
                    } else {
                        tempByte = (byte)((tempByte << 1) | 0x01);
                        Timber.d("black, %s", alpha);
                    }

                    ++pixelCount;
                }
            }
        }

        imageByteArray[967] = tempByte;

        System.arraycopy(imageByteArray, 0, byteArrayToBeSent, 4, imageByteArray.length);

        scaledAndBinaryAlphaBitmap.recycle();

        mRevosBleManager.writeLongDataToIMGCharacteristic(byteArrayToBeSent);

    }*/

    private void transferBitmap(Bitmap bitmap, int metadataCode) {

        if(NordicBleService.getDeviceFirmwareVersion() == null
                || NordicBleService.getDeviceFirmwareVersion().isEmpty()
                || new Version(NordicBleService.getDeviceFirmwareVersion()).compareTo(new Version(VER_5_0)) >= 0) {
            return;
        }

        byte[] hexValByteArray = new byte[BUFFER_LENGTH];

        byte tempByte = 0;

        int pixelCount = 0;

        for(int y = 0; y < bitmap.getHeight(); y = y + 8) {
            for(int x = 0; x < bitmap.getWidth(); ++x) {

                if(pixelCount > 0 && pixelCount % 8 == 0) {
                    hexValByteArray[Math.round((pixelCount - 1) / 8)] = tempByte;
                    tempByte = 0;
                }

                for(int bitIndex = 7; bitIndex >= 0; --bitIndex) {
                    int pixel = bitmap.getPixel(x,y + bitIndex);
                    //Timber.d("pixel value : " + x + ", " + (y+bitIndex));
                    int alpha = Color.alpha(pixel);
                    if(alpha == 0 || pixel == -1) {
                        tempByte = (byte)(tempByte << 1);
                        Timber.d("transparent");
                    } else {
                        tempByte = (byte)((tempByte << 1) | 0x01);
                        Timber.d("black, %s", alpha);
                    }

                    ++pixelCount;
                }

            }
        }

        hexValByteArray[BUFFER_LENGTH - 1] = tempByte;

        Timber.d("pixel_count : %s", pixelCount);
        Timber.d("arrayData : %s", Arrays.toString(hexValByteArray));

        String hexString = BleUtils.byteArrayToHexString(hexValByteArray);

        String formattedHexString = "";

        for(int index = 0; index < hexString.length(); index = index + 2) {
            formattedHexString = formattedHexString + "0x" + hexString.charAt(index) + hexString.charAt(index + 1) + ", ";
        }

        Timber.d("hex_string : %s", formattedHexString);

        ArrayList<Byte> imageBufferArrayList = new ArrayList<>();

        for(int byteIndex = 0; byteIndex < BUFFER_LENGTH; ++byteIndex) {
            imageBufferArrayList.add(hexValByteArray[byteIndex]);
        }

        byte[] byteArray = new byte[BUFFER_LENGTH + 1];
        for(int byteIndex = 0; byteIndex < BUFFER_LENGTH; ++byteIndex) {
            byteArray[byteIndex] = imageBufferArrayList.get(byteIndex);
        }
        byteArray[BUFFER_LENGTH] = (byte)metadataCode;

        mRevosBleManager.writeDataToImgCharacteristic(byteArray, new SuccessCallback() {
            @Override
            public void onRequestCompleted(@NonNull BluetoothDevice device) {
                Timber.d("image code sent successfully");
            }
        }, new FailCallback() {
            @Override
            public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                Timber.d("image code send failed");
            }
        });

        bitmap.recycle();
    }

    private final BroadcastReceiver mBluetoothAdapterStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if(action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        break;
                    case BluetoothAdapter.STATE_ON:
                        tryConnectingToLastSavedDevice(false);
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        try {
            unregisterReceiver(mBluetoothAdapterStateChangeReceiver);
        } catch (Exception e){
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        //unregister with the EventBus, if registered
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

        //stop location updates
        try {
            LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(mLocationCallback);
        } catch (Exception e) {

        }

        if(TbitBle.hasInitialized()) {
            TbitBle.destroy();
        }

        super.onDestroy();
    }

    @Override
    public void bluetoothOn() {
        Timber.d("iConcox Ble callback: bluetoothOn");
    }

    @Override
    public void bluetoothOff() {
        Timber.d("iConcox Ble callback: bluetoothOff");

        updateIsIconcoxConnectedState(false);
    }

    @Override
    public void startScanDevice() {
        Timber.d("iConcox Ble callback: startScanDevice");

    }

    @Override
    public void endScanDevice() {
        Timber.d("iConcox Ble callback: endScanDevice");
    }

    @Override
    public void disconnect(boolean handDisconnect) {
        Timber.d("iConcox Ble callback: disconnect");

        updateIsIconcoxConnectedState(false);

    }

    @Override
    public void connectionSuccess() {
        Timber.d("iConcox Ble callback: connectionSuccess");

        updateIsIconcoxConnectedState(true);

        //After the connection is successful, send the mac address
        handler.removeMessages(MSG_SEND_DELAY_SEND_MAC);
        Message msg1 = new Message();
        msg1.what = MSG_SEND_DELAY_SEND_MAC;
        handler.sendMessageDelayed(msg1, MSG_SEND_DELAY_SEND_MAC_TIME);
    }

    @Override
    public void getData(String hexString) {
        Timber.d("iConcox Ble callback: getData");
        dealResponse(hexString);
    }

    @Override
    public void scanTimeOut() {
        Timber.d("iConcox Ble callback: scanTimeOut");
    }

    @Override
    public void searchBleDevice(BluetoothDevice device) {
        Timber.d("iConcox Ble callback: searchBleDevice %s", device.getAddress());
    }

    @Override
    public void connectionFailed(boolean isTimeOut) {
        Timber.d("iConcox Ble callback: connectionFailed");

        //TODO:: connection failed. Clear prefs and update views
        updateIsIconcoxConnectedState(false);
    }

    @Override
    public void cmdTimeTimeOut(int cmd) {
        Timber.d("iConcox Ble callback: cmdTimeTimeOut");

    }

    private void dealResponse(String hexString) {
        if (hexString.contains("23240200010D0A")) {
            //Address verification succeeded
            handler.removeMessages(MSG_SEND_MAC_TIME_OUT);

        } else if ("23240201010D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            Timber.d("iConcox locked successfully via BLE");

        } else if ("23240201000D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            Timber.d("iConcox lock failed via BLE");

        } else if ("23240202010D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            Timber.d("iConcox unlocked successfully via BLE");

        } else if ("23240202000D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            Timber.d("iConcox unlock failed via BLE");

        } else if ("23240203010D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            EventBus.getDefault().post(new EventBusMessage(BT_GPS_ICONCOX_CMD_RESPONSE + GEN_DELIMITER + CMD_SEND_REMOTE_IGNITION + GEN_DELIMITER + true));
            Timber.d("iConcox remote ignition successful via BLE");

        } else if ("23240203000D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            Timber.d("iConcox remote ignition failed via BLE");
            EventBus.getDefault().post(new EventBusMessage(BT_GPS_ICONCOX_CMD_RESPONSE + GEN_DELIMITER + CMD_SEND_REMOTE_IGNITION + GEN_DELIMITER + false));

        } else if ("23240204010D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            EventBus.getDefault().post(new EventBusMessage(BT_GPS_ICONCOX_CMD_RESPONSE + GEN_DELIMITER + CMD_SEND_REMOTE_LOCK + GEN_DELIMITER + true));
            Timber.d("iConcox remote lock successful via BLE");

        } else if ("23240204000D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            EventBus.getDefault().post(new EventBusMessage(BT_GPS_ICONCOX_CMD_RESPONSE + GEN_DELIMITER + CMD_SEND_REMOTE_LOCK + GEN_DELIMITER + false));
            Timber.d("iConcox remote lock failed via BLE");

        } else if ("23240205010D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            EventBus.getDefault().post(new EventBusMessage(BT_GPS_ICONCOX_CMD_RESPONSE + GEN_DELIMITER + CMD_SEND_FIND_VEHICLE + GEN_DELIMITER + true));
            Timber.d("iConcox vehicle search successful via BLE");

        } else if ("23240205000D0A".equals(hexString)) {

            handler.removeMessages(MSG_SEND_TIME_OUT);
            EventBus.getDefault().post(new EventBusMessage(BT_GPS_ICONCOX_CMD_RESPONSE + GEN_DELIMITER  + CMD_SEND_FIND_VEHICLE + GEN_DELIMITER + false));
            Timber.d("iConcox vehicle search failed via BLE");

        }
    }

    private void postEventDeviceConnectionState(String state){
        switch (state){
            case PNP_BT_EVENT_STATE_CONNECTING :
                EventBus.getDefault().post(new EventBusMessage(PNP_BT_EVENT_STATE_CONNECTING));
                break;
            case PNP_BT_EVENT_STATE_CONNECTED :
                //Update vehicel last known location
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    refreshVehicleLastKnownLocation();
                }
                Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
                if(device != null && device.getVehicle() != null) {
                    getSharedPreferences(LAST_VEHICLE_PREFS, MODE_PRIVATE).edit().putString(LAST_CONNECTED_DEVICE_VIN, device.getVehicle().getVin()).apply();
                }
                EventBus.getDefault().post(new EventBusMessage(PNP_BT_EVENT_STATE_CONNECTED));
                break;
            case PNP_BT_EVENT_STATE_DISCONNECTED :
                EventBus.getDefault().post(new EventBusMessage(PNP_BT_EVENT_STATE_DISCONNECTED));
                break;
        }
    }
}
