package com.revos.android.fleet.api.v2.data


import com.google.gson.annotations.SerializedName

internal data class GetAllVehiclesModel(
    @SerializedName("data")
    val data: Data,
    @SerializedName("status")
    val status: Int,
    @SerializedName("message")
    val message: String) {

    data class Data(
        @SerializedName("count")
        val count: Int,
        @SerializedName("vehicles")
        val vehicles: List<Vehicle>) {

        data class Vehicle(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("isDeliverable")
            val isDeliverable: Boolean,
            @SerializedName("isHealthy")
            val isHealthy: Boolean,
            @SerializedName("lastMarkedLatitude")
            val lastMarkedLatitude: Double,
            @SerializedName("lastMarkedLongitude")
            val lastMarkedLongitude: Double,
            @SerializedName("modelTag")
            val modelTag: String,
            @SerializedName("rentalStatus")
            val rentalStatus: String,
            @SerializedName("status")
            val status: String,
            @SerializedName("updatedAt")
            val updatedAt: String,
            @SerializedName("vin")
            val vin: String
        )
    }
}