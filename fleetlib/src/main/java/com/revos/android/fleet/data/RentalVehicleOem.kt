package com.revos.android.fleet.data

data class RentalVehicleOem (
    var id: String? = null,
    var name: String? = null,
    var phone: String? = null,
)