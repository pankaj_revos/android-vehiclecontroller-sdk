package com.revos.android.fleet.payment

import android.app.Activity
import android.content.Intent
import androidx.core.os.bundleOf
import com.revos.android.fleet.ui.activities.CashFreePaymentActivity
import com.cashfree.pg.CFPaymentService
import com.revos.android.fleet.R
import com.revos.android.fleet.data.Booking
import com.revos.android.fleet.data.User
import java.util.HashMap

internal class CashFreePaymentStrategy (
                val activity: Activity,
                val booking: Booking,
                val cftoken: String,
                val user: User,
                val vendorSplit : String) : PaymentStrategy {


    override fun pay(amount: Float) {
        if(user.phone.isNullOrEmpty() || user.email.isNullOrEmpty()){
            return
        }
        val params: MutableMap<String, String> = HashMap()

        val phone = user.phone!!.replaceFirst("\\+91".toRegex(), "")

        params[CFPaymentService.PARAM_APP_ID] = activity.getString(R.string.cf_app_id)
        params[CFPaymentService.PARAM_ORDER_ID] = booking.id!!
        params[CFPaymentService.PARAM_ORDER_CURRENCY] = "INR"
        params[CFPaymentService.PARAM_ORDER_AMOUNT] = (amount * 1.0f).toString()
        params[CFPaymentService.PARAM_CUSTOMER_PHONE] = phone
        params[CFPaymentService.PARAM_CUSTOMER_EMAIL] = user.email.toString()
        params[CFPaymentService.PARAM_VENDOR_SPLIT] = vendorSplit

        val intent = Intent(activity, CashFreePaymentActivity::class.java)

        val bundle = bundleOf(
            "cf_params" to params,
            "cf_token" to cftoken,
        )
        intent.putExtras(bundle)
        activity.startActivity(intent)
    }

}