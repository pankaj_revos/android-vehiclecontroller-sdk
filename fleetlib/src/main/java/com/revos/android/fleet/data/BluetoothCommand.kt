package com.revos.android.fleet.data

data class BluetoothCommand (
    var isCommandBinary : Boolean = false,
    var commandValue: String? = null,
    var commandRetryCount : Int = 0,
)