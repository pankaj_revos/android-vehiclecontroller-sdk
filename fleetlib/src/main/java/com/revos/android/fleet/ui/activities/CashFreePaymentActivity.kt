package com.revos.android.fleet.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.revos.android.fleet.R
import com.revos.android.fleet.eventbus.EventBusMessage
import com.revos.android.fleet.utils.EVENT_MESSAGE_TRANSACTION_FAILED
import com.revos.android.fleet.utils.EVENT_MESSAGE_TRANSACTION_SUCCESSFUL
import com.cashfree.pg.CFPaymentService
import org.greenrobot.eventbus.EventBus
import timber.log.Timber

class CashFreePaymentActivity : AppCompatActivity() {

    private val TXN_STATUS_KEY = "txStatus"
    private val SUCCESS = "SUCCESS"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(intent != null && intent.extras != null){
            var bundle = intent.extras

            if(bundle == null) return

            if(bundle.containsKey("cf_token") && bundle.containsKey("cf_params")){
                var params = bundle.get("cf_params") as HashMap<String, String>
                var cf_token = bundle.getString("cf_token")
                CFPaymentService.getCFPaymentServiceInstance().doPayment(this, params, cf_token, getString(R.string.cf_stage))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == CFPaymentService.REQ_CODE){
            if (data != null) {
                val bundle = data.extras
                if (bundle != null && bundle.containsKey(TXN_STATUS_KEY)) {
                    val status = bundle.getString(TXN_STATUS_KEY)
                    var map = hashMapOf<String, String>()
                    if (status == SUCCESS) {
                        var keys = bundle.keySet()
                        for(k in keys){
                            Timber.d(" key $k and value ${bundle.getString(k) }")
                            if(k != null && bundle.getString(k) != null){
                                map[k] = bundle.getString(k)!!
                            }
                        }
                        var referenceId = bundle.getString("referenceId")
                        EventBus.getDefault().post(EventBusMessage(EVENT_MESSAGE_TRANSACTION_SUCCESSFUL, referenceId!!))
                    }else{
                        EventBus.getDefault().post(EventBusMessage(EVENT_MESSAGE_TRANSACTION_FAILED, "Failed to pay"))
                    }
                } else {
                    EventBus.getDefault().post(EventBusMessage(EVENT_MESSAGE_TRANSACTION_FAILED, "Failed to pay"))
                }
            }
            finish()
        }
    }

}