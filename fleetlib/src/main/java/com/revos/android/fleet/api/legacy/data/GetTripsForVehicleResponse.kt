package com.revos.android.fleet.api.legacy.data

import com.google.gson.annotations.SerializedName

data class GetTripsForVehicleResponse(
    @SerializedName("tripCount")
    val tripCount: Int,
    @SerializedName("trips")
    val trips: List<Trip>) {

    data class Trip(
        @SerializedName("avgGpsSpeed")
        val avgGpsSpeed: Double,
        @SerializedName("batteryVoltageAdc")
        val batteryVoltageAdc: List<BatteryVoltageAdc>,
        @SerializedName("distance")
        val distance: Int,
        @SerializedName("endTime")
        val endTime: String,
        @SerializedName("energy")
        val energy: Double,
        @SerializedName("location")
        val location: List<Location>,
        @SerializedName("maxGpsSpeed")
        val maxGpsSpeed: Double,
        @SerializedName("maxWheelRPM")
        val maxWheelRPM: Int,
        @SerializedName("minGpsSpeed")
        val minGpsSpeed: Double,
        @SerializedName("minWheelRPM")
        val minWheelRPM: Int,
        @SerializedName("mode")
        val mode: Mode,
        @SerializedName("startTime")
        val startTime: String,
        @SerializedName("tripId")
        val tripId: String,
        @SerializedName("vin")
        val vin: String) {

        data class BatteryVoltageAdc(
            @SerializedName("timestamp")
            val timestamp: Long,
            @SerializedName("voltage")
            val voltage: Double
        )

        data class Location(
            @SerializedName("lat")
            val lat: Double,
            @SerializedName("lng")
            val lng: Double,
            @SerializedName("timestamp")
            val timestamp: Long
        )

        data class Mode(
            @SerializedName("ECONOMY")
            val eCONOMY: Double,
            @SerializedName("RIDE")
            val rIDE: Double,
            @SerializedName("SPORT")
            val sPORT: Int
        )
    }
}