package com.revos.android.fleet.graphql

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class GraphQLInterceptor constructor(val token : String): Interceptor {

    private val AUTHORIZATION_HEADER_NAME = "Authorization"
    private val AUTHORIZATION_HEADER_VALUE_PREFIX = "Bearer "

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val newRequest: Request = request.newBuilder()
            .addHeader(AUTHORIZATION_HEADER_NAME, AUTHORIZATION_HEADER_VALUE_PREFIX + token)
            .build()
        return chain.proceed(newRequest)
    }
}