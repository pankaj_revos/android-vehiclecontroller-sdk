package com.revos.android.fleet.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DataFeedBatteryVoltageAdc (
    var voltage : Double = 0.0,
    var timestamp: String? = null,
) : Parcelable