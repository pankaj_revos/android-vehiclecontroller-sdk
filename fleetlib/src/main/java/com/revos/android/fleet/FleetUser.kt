package com.revos.android.fleet

import android.content.Context

class FleetUser private constructor(builder : Builder) {

    internal var phoneNumber: String? = null
    internal var email: String? = null
    internal var firstName: String? = null
    internal var lastName: String? = null
    internal var firebaseToken: String? = null
    internal var buyChargerUrl: String? = null
    internal var context: Context
    internal var appToken: String
    internal var userId: String
    internal var address : String? = null

    init {
         this.phoneNumber = builder.phoneNumber
         this.email = builder.email
         this.firstName = builder.firstName
         this.lastName = builder.lastName
         this.firebaseToken = builder.firebaseToken
         this.buyChargerUrl = builder.buyChargerUrl
         this.context = builder.context
         this.appToken = builder.appToken
         this.userId = builder.userId
    }
    class Builder(
        var context: Context,
        var appToken: String,
        var userId: String) {

        internal var phoneNumber: String? = null
        internal var email: String? = null
        internal var firstName: String? = null
        internal var lastName: String? = null
        internal var firebaseToken: String? = null
        internal var buyChargerUrl: String? = null
        internal var address : String? = null

        fun setPhoneNumber(phoneNumber: String?) = apply {
            this.phoneNumber = phoneNumber
        }
        fun setEmail(email: String?) = apply {
            this.email = email
        }
        fun setFirstName(firstName: String?) = apply {
            this.firstName = firstName
        }
        fun setLastName(lastName: String?) = apply {
            this.lastName = lastName
        }
        fun setIdToken(idToken: String?) = apply {
            this.firebaseToken = idToken
        }
        fun setBuyChargerUrl(buyChargerUrl: String?) = apply {
            this.buyChargerUrl = buyChargerUrl
        }
        fun setAddress(address : String?) = apply {
            this.address = address
        }
        fun build() = FleetUser( this )
    }
}