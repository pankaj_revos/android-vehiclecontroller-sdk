package com.revos.android.fleet.utils.vehicleDataTransfer;

import static com.revos.android.fleet.utils.vehicleDataTransfer.SpeedometerDataExchanger.PACKET_TYPE_OFFLINE;

import android.content.Context;

import androidx.core.util.Pair;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class OfflineLogPacketParser {
    private Context mContext;
    private final String LOG_PACKET_HEADER_FORMAT_KEY = "logPacketHeaderFormat";
    private final String TIMESTAMP_KEY = "timeStamp";
    private final String TIMESTAMP_TRUE_KEY = "timeStampTrueFlag";
    private final String TRIP_ID_KEY = "tripId";
    private final String PARAMETERS_KEY = "parameters";
    private final String ID_KEY = "id";
    private final String LENGTH_KEY = "length";
    private final String START_POSITION_KEY = "startPosition";

    public OfflineLogPacketParser(Context context) {
        mContext = context;
    }

    public Pair<Boolean, ArrayList<VehicleParameters>> parseLogPacket(String dataString) {
        String[] dataArray = dataString.split(" ");

        //check if this data is valid or not
        //first check if the length is valid
        SpeedometerDataExchanger speedometerDataExchanger = SpeedometerDataExchanger.getInstance(mContext);
        if(speedometerDataExchanger == null) {
            return null;
        }

        if(speedometerDataExchanger.getMetadataFirmwareVersion() != null) {
            return parseOfflineLogPackets(dataArray, speedometerDataExchanger);
        } else {
            return null;
        }
    }

    private Pair<Boolean, ArrayList<VehicleParameters>> parseOfflineLogPackets(String[] dataArray, SpeedometerDataExchanger speedometerDataExchanger) {
        int logPacketHeaderLength = speedometerDataExchanger.getOfflineLogPacketHeaderLength();
        int parameterFormatLength = speedometerDataExchanger.getParameterFormatLength();

        if((dataArray.length - logPacketHeaderLength) % parameterFormatLength != 0) {
            return null;
        }

        //data length is valid
        int noOfPackets = (dataArray.length - logPacketHeaderLength) / parameterFormatLength;

        ArrayList<VehicleParameters> vehicleParametersArrayList = new ArrayList<>();

        JsonObject parameterLogJsonObject = speedometerDataExchanger.getParameterLogFormatJsonObject();

        long initialTimeStamp = -1;
        long tripId = -1;
        boolean isTimeStampTrue = false;

        //find out initial timestamp, whether it was true or not and the trip id for these log packets
        if(parameterLogJsonObject.has(LOG_PACKET_HEADER_FORMAT_KEY)) {
            JsonObject logPacketInfoJsonObject = parameterLogJsonObject.getAsJsonObject(LOG_PACKET_HEADER_FORMAT_KEY);
            if(logPacketInfoJsonObject.has(PARAMETERS_KEY)) {
                JsonArray parametersJsonArray = logPacketInfoJsonObject.get(PARAMETERS_KEY).getAsJsonArray();

                //iterate over this array
                for(int paramIndex = 0; paramIndex < parametersJsonArray.size(); ++paramIndex) {
                    JsonObject paramJsonObject = parametersJsonArray.get(paramIndex).getAsJsonObject();
                    if(paramJsonObject.has(ID_KEY)) {
                        String id = paramJsonObject.get(ID_KEY).getAsString();
                        int length = paramJsonObject.get(LENGTH_KEY).getAsInt();
                        int startPosition = paramJsonObject.get(START_POSITION_KEY).getAsInt();

                        //construct byteString list
                        ArrayList<String> byteStringArrayList = new ArrayList<>();
                        for(int arrayIndex = startPosition; arrayIndex < startPosition + length; ++arrayIndex) {
                            byteStringArrayList.add(dataArray[arrayIndex]);
                        }

                        //construct hex string to be parsed from the list
                        String hexString = "";
                        for(String dataString : byteStringArrayList) {
                            hexString += dataString;
                        }

                        //parse this value
                        long paramValue = Long.parseLong(hexString, 16);

                        if(id.equals(TIMESTAMP_KEY)) {
                            initialTimeStamp = paramValue;
                        } else if(id.equals(TRIP_ID_KEY)) {
                            tripId = paramValue;
                        } else if(id.equals(TIMESTAMP_TRUE_KEY)) {
                            isTimeStampTrue = paramValue == 1;
                        }
                    }
                }
            }
        }

        //now process the list of concatenated packets
        for(int packetIndex = 0; packetIndex < noOfPackets; ++packetIndex) {

            int startIndex = logPacketHeaderLength + packetIndex * parameterFormatLength;
            String partDataString = "";
            StringBuilder stringBuilder = new StringBuilder(partDataString);

            for(int byteIndex = startIndex; byteIndex < startIndex + parameterFormatLength; ++byteIndex) {
                stringBuilder.append(dataArray[byteIndex]);
                stringBuilder.append(" ");
            }

            //string has been built, try to parse it
            VehicleParameters vehicleParameters = new VehicleParameterParser(mContext, stringBuilder.toString().trim()).parseAndCreateVehicleParametersObject(PACKET_TYPE_OFFLINE);

            if(vehicleParameters != null) {
                vehicleParameters.setTripId(tripId);
                vehicleParameters.setTimeStamp(initialTimeStamp + packetIndex);
                vehicleParametersArrayList.add(vehicleParameters);
            }
        }

        return new Pair<>(isTimeStampTrue, vehicleParametersArrayList);
    }
}
