package com.revos.android.fleet.pnp

import androidx.lifecycle.LiveData
import com.revos.android.fleet.data.DataState

interface PnpDevice  {
    fun lock()
    fun unlock()
    fun findVehicle()
    fun disconnect()
    fun get() : LiveData<DataState<String>>
    fun isConnected() : Boolean

    enum class COMMAND { FIND, IGNITION_ON, IGNITION_OFF }
}

