package com.revos.android.fleet.eventbus

data class EventBusMessage ( var status : String, var message: String) {
    constructor( status: String) : this(status, "")
    var type : Int = 0
    var data : Any? = null
}