package com.csy.encrypt;

import android.content.Context;


import com.csy.bl.ble.common.utils.Dbug;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by mtl on 17-9-18.
 */

public class HttpCrypto {

    private Context mContext;

    private static HttpCrypto mInstance;

    private HttpCrypto(Context context) {
        this.mContext = context;
    }

    public static HttpCrypto getInstance(Context context) {
        if (null == mInstance) {
            mInstance = new HttpCrypto(context);
        }
        return mInstance;
    }


    public String signTopRequest2(Map<String, Object> params) {
        try {
            String secret = NativeControl.getNetKey();
            // 第一步：检查参数是否已经排序
            String[] keys = params.keySet().toArray(new String[0]);
            Arrays.sort(keys);

            // 第二步：把所有参数名和参数值串在一起
            StringBuilder query = new StringBuilder();
            query.append(secret);

            for (String key : keys) {
                String value = ""+params.get(key);
                if (areNotEmpty(key, value)) {
                    query.append(key).append(value);
                }
            }
            // 第三步：使用MD5
            byte[] bytes;
            query.append(secret);
            bytes = encryptMD5(query.toString());
            Dbug.d(Dbug.getHeadStr(),"拼接参数:"+query.toString());


            // 第四步：把二进制转化为大写的十六进制
            return byte2hex(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static byte[] encryptHMAC(String data, String secret) throws IOException {
        byte[] bytes = null;
        try {
            SecretKey secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacMD5");
            Mac mac = Mac.getInstance(secretKey.getAlgorithm());
            mac.init(secretKey);
            bytes = mac.doFinal(data.getBytes("UTF-8"));
        } catch (GeneralSecurityException gse) {
            throw new IOException(gse.toString());
        }
        return bytes;
    }

    public static byte[] encryptMD5(String data) throws IOException {
        return encryptMD5(data.getBytes("UTF-8"));
    }

    private static byte[] encryptMD5(byte[] bytes) throws IOException {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IOException(e.toString());
        }
        return md.digest(bytes);
    }

    public static String byte2hex(byte[] bytes) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() == 1) {
                sign.append("0");
            }
            sign.append(hex.toUpperCase());
        }
        return sign.toString();
    }

//    ////>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> c/c++  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//
//    public String signRequest(Map<String, String> params, String secret, String signMethod) {
//        String[] keys = params.keySet().toArray(new String[0]);
//        Arrays.sort(keys);
//        StringBuilder query = new StringBuilder();
//        for (String key : keys) {
//            String value = params.get(key);
//            if (areNotEmpty(key, value)) {
//                query.append(key).append(value);
//            }
//        }
//        Dbug.e("","[signRequest]:" + query.toString());
//        return signTopRequest(mContext, query.toString(), secret, signMethod);
//    }

    public boolean areNotEmpty(String... values) {
        boolean result = true;
        if ((values == null) || (values.length == 0)){
            result = false;

        } else {
            for (String value : values) {
                result &= !isEmpty(value);
            }
        }
        return result;
    }

    public boolean isEmpty(String value) {
        int strLen;
        if ((value == null) || ((strLen = value.length()) == 0)){
            return true;

        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(value.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static native String getKey();

    static {
        try {
            System.loadLibrary("encryp-lib");
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}
