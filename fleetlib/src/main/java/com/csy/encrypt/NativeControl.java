package com.csy.encrypt;

/**
 * Created by chenshouyin on 2017/11/22.
 * Email:shouyinchen@gmail.com
 */

public class NativeControl {

    static {
        System.loadLibrary("encryp-lib");
    }


    public static native String getNetKey();

    public static native String getDataEncodeKey();

    public static native byte[] EncodeAes();
    public static native String DecodeAes();

}
