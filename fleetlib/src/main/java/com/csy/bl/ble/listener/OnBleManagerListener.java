package com.csy.bl.ble.listener;

import android.bluetooth.BluetoothDevice;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */
public interface OnBleManagerListener {

	/**
	 * 蓝牙打开
	 */
	void bluetoothOn();

	/**
	 * 蓝牙关闭
	 */
	void bluetoothOff();

	/**
	 * 开始扫描
	 */
	void startScanDevice();

	/**
	 * 结束扫描
	 */
	void endScanDevice();

	/**
	 * @param handDisconnect true主动断开 false被动断开
	 */
	void disconnect(boolean handDisconnect);


	/**
	 * 连接成功
	 */
	void connectionSuccess();

	/**
	 * @param hexString  蓝牙传过来解密之后的数据
	 */
	void getData(String hexString);

	/**
	 * 扫描超时
	 */
	void scanTimeOut();

	/**
	 * 扫描到设备
	 * @param device
	 */
	void searchBleDevice(BluetoothDevice device);

	/**
	 * 连接失败
	 * @param isTimeOut
	 * true 超时失败的回调
	 * false 其它原因导致的连接失败回调
	 */
	void connectionFailed(boolean isTimeOut);


	/**
	 * 命令发送超时
	 * @param cmd  列如获取Token:MGDataUtils.MSG_GET_TOKEN
	 */
	void cmdTimeTimeOut(int cmd);
}
