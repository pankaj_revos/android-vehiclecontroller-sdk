package com.csy.bl.ble.menggou.utils;

import java.util.UUID;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */

public class Constants {
    //梦购车锁相关UUID
//    public static final UUID SERVICE_UUID = UUID
//        .fromString("0000FEE7-0000-1000-8000-00805f9b34fb");
//    public static final UUID UUID_READ = UUID
//            .fromString("000036F6-0000-1000-8000-00805f9b34fb");
//    public static final UUID UUID_WRITE = UUID
//            .fromString("000036F5-0000-1000-8000-00805f9b34fb");


//    //开发板  好的
//    public static final UUID SERVICE_UUID = UUID
//            .fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
////    public static final UUID UUID_READ = UUID
////            .fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
////    public static final UUID UUID_WRITE = UUID
////            .fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");
//
//public static final UUID UUID_WRITE = UUID
//        .fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
//    public static final UUID UUID_READ = UUID
//            .fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");

    //网络请求用到key 和 sec
    public static final String APP_KEY = "100020";

    public static  String BASE_URL = "http://mgcart.jimicloud.com/cart/sync/app/";
    //public static  String BASE_URL = "http://172.16.0.109:9070/cart/sync/app";
    public static  String TEST_URL = "http://mgcart.jimicloud.com/cart/sync/app/";


}
