package com.csy.bl.ble.common.utils;

import android.util.Log;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */
public class Dbug {
	private static boolean IS_DEBUG = true;

	public static String getHeadStr() {
		return HeadStr;
	}

	private final static String HeadStr = "BL====LOG>>>";
	public static void openDebug(boolean isOpen) {
		IS_DEBUG = isOpen;
	}

	public static void v(String tag, String msg) {
		if(IS_DEBUG){
			Log.v(tag, HeadStr+msg);
		}
	}
	public static void d(String tag, String msg) {
		if(IS_DEBUG){
//			Log.d(tag, HeadStr+msg);
			System.out.println(tag+HeadStr+msg);
		}
	}
	public static void i(String tag, String msg) {
		if(IS_DEBUG){
			Log.i(tag, HeadStr+msg);
		}
	}
	public static void w(String tag, String msg) {
		if(IS_DEBUG){
			Log.w(tag, HeadStr+msg);
		}
	}
	public static void e(String tag, String msg) {
		if(IS_DEBUG){
			Log.e(tag, HeadStr+msg);
		}
	}
}
