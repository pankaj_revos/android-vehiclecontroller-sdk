package com.csy.bl.ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.csy.bl.ble.common.utils.BleConfig;
import com.csy.bl.ble.common.utils.Dbug;
import com.csy.bl.ble.common.utils.TypeConvert;
import com.csy.bl.ble.common.utils.VersionTool;
import com.csy.bl.ble.listener.OnBleManagerListener;
import com.csy.bl.ble.menggou.utils.CmdConstants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.csy.bl.ble.BleManagerControlService.ACTION_GATT_CONNECT_FAILURE_TIME_OUT;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_BATTERY;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_CAR_WORK_MODE;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_CHARGE_DIANABLE;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_CHARGE_ENABLE;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_CHARGE_END;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_CHARGE_START;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_CLOSE_LOCK;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_IMEI;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_LOCK_STATUS;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_MAC_ADDRESS;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_OPEN_LOCK;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_RETURN_CAR_STATUS;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_SET_CAR_WORK_MODE;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_TAKE_CAR_STATUS;
import static com.csy.bl.ble.menggou.utils.MGDataUtils.MSG_GET_TOKEN;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */
public class IconCoxBleManager {

    private BleManagerControlService mBluetoothLeService = null;
    private static IconCoxBleManager uniqueInstance = null;
    protected Intent mBtServiceIntent = null;
    private static final String TAG = Dbug.getHeadStr();
    private Context mActivity;
    protected BluetoothAdapter mBtAdapter = null;
    protected BluetoothDevice mCurrentDevice;
    private ArrayList<BluetoothDevice> mDevices = null;
    private boolean isScanning = false;
    private Runnable temRunnable;
    private Runnable tempCanselScaneRunnable;

    // 是否扫描到设备；
    private boolean ishavedevice = false;
    //监听
    private static List<OnBleManagerListener> mListOnBleManagerListener;



    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                mListOnBleManagerListener.get(i).cmdTimeTimeOut(msg.what);
            }
        }

    };


    private IconCoxBleManager(Context context) {
        if (VersionTool.isUnder4_3()) {

        } else {

            mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                    if (!checkAddressExist(device)) {
                        if (mDevices == null) {
                            mDevices = new ArrayList<BluetoothDevice>();
                        }
                        mDevices.add(device);
                        getDevice(device);
                    }
                }
            };


            Log.i(TAG, "BTManager new");
            mActivity = context.getApplicationContext();
            mBtAdapter = getBluetoothAdapter();

            temRunnable = new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    if (!ishavedevice) {
                        for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                            mListOnBleManagerListener.get(i).scanTimeOut();
                        }
                    }
                }
            };
            tempCanselScaneRunnable = new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    if (mDevices == null || mDevices.size() == 0) {
                        for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                            mListOnBleManagerListener.get(i).scanTimeOut();
                        }
                    }
                    cancelDiscovery();
                }
            };

            mBtServiceIntent = new Intent(mActivity, BleManagerControlService.class);
            startConnectBtService();

            registerBluetooth(new IntentFilter());
            isScanning = false;
            Log.d(TAG, "acsUtility 1");
            if (mBtAdapter == null) {
                Log.d(TAG, "error,mBtAdapter == null");
                return;
            }
        }


    }

    public static IconCoxBleManager getInstance(Context context) {
        if (uniqueInstance == null) {
            uniqueInstance = new IconCoxBleManager(context);
            mListOnBleManagerListener = new ArrayList<>();
        }
        return uniqueInstance;
    }

    private synchronized int startConnectBtService() {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            // Log.i(TAG, "startService");
            mActivity.startService(mBtServiceIntent);
            mActivity.bindService(mBtServiceIntent, mConn, Service.BIND_AUTO_CREATE);

            return 1;
        }

    }

    private int stopConnectBtService() {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            try {
                mActivity.unbindService(mConn);
                mActivity.stopService(mBtServiceIntent);
            }catch (Exception e){
                Dbug.d("BleManager", "Exception:"+e.toString());
            }
            return 1;
        }
    }

    public int connect(String address) {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            try {
                if (mBluetoothLeService != null) {
                    Log.e("fadfa","fuck");
                    mBluetoothLeService.connect(address);
                } else {
                    Dbug.d("BleManager", "mBluetoothLeService.connect  else mBluetoothLeService null");
                    for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                        mListOnBleManagerListener.get(i).connectionFailed(false);
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception
                Dbug.d(getClass().getSimpleName(), "==connect==false==Exception e==" + e);
            }
            return 1;
        }


    }

    public int disconnect() {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            try {
                if (mBluetoothLeService != null) {
                    mBluetoothLeService.disconnect();//为了防止外部调用没有断开连接,手动调用一次,不然没断开扫描不到设备
                }
            } catch (Exception e) {
                // TODO: handle exception
                Dbug.d("cancelConnectBt", "=此异常可忽略,用于扫描前断开蓝牙色设备,以防外部调用没有断开蓝牙连接==disconnect==Exception==" + e);
            }
//			if (mBluetoothLeService != null) {
//				isHandDisconnect = true;
//				mBluetoothLeService.disconnect();
//			}

            return 1;
        }
    }

    private int write(byte[] data) {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            if (mBluetoothLeService != null) {
                mBluetoothLeService.writeStringToGatt(data);
                return 1;
            }
            return -1;
        }
    }


    public int close() {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            Dbug.d("BleManager", "close");

            //unregisterBluetooth(); 外部在调用 需要关闭的地方取消注册
            stopConnectBtService();
            return 1;
        }


    }

    private final ServiceConnection mConn = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder service) {
            mBluetoothLeService = ((BleManagerControlService.LocalBinder) service)
                    .getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");

            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            mBluetoothLeService = null;

        }


    };

    public boolean isEnabled() {
        return mBtAdapter.isEnabled();
    }

    public boolean isSearch() {
        if (!mBtAdapter.isEnabled()) {
            return false;
        }
        return true;
    }

    public boolean isDiscovery() {
        if (!mBtAdapter.isEnabled()) {
            return false;
        }

        if (mBtAdapter.getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE) {
            return true;
        }

        if (mBtAdapter.getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            return false;
        }
        return true;
    }

    public boolean enable() {
        return mBtAdapter.enable();
    }

    public boolean disable() {
        return mBtAdapter.disable();
    }

    public Context getContext() {
        return mActivity;
    }

    public Handler getHandler() {
        return mHandler;
    }

    public BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)) {
                    case BluetoothAdapter.STATE_ON:
                        for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                            mListOnBleManagerListener.get(i).bluetoothOn();
                        }
                        break;
                    case BluetoothAdapter.STATE_OFF:
                        for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                            mListOnBleManagerListener.get(i).bluetoothOff();
                        }
                        break;
                }
            } else if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                getDevice(device);
               // mListOnBleManagerListener.get(0).searchBleDevice(device);
            }

            else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)) {
                for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                    mListOnBleManagerListener.get(i).startScanDevice();
                }
            } else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                    mListOnBleManagerListener.get(i).endScanDevice();
                }
            } else if (action.equals(BleManagerControlService.ACTION_GATT_CONNECTED)) {
                for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                    mListOnBleManagerListener.get(i).connectionSuccess();
                    Log.e("ffff","fuck");
                }

                Dbug.d("BleManager", "====mReceiver=ACTION_GATT_CONNECTED=");
            } else if (action.equals(BleManagerControlService.ACTION_GATT_DISCONNECTED)) {
                Dbug.d("BleManager", "====mReceiver=ACTION_GATT_DISCONNECTED=");
                if (mBluetoothLeService!=null){
                    for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                        mListOnBleManagerListener.get(i).disconnect(mBluetoothLeService.isHandDisconnect());
                    }
                    Dbug.d("==BleManager==", "是否手动断开:" + mBluetoothLeService.isHandDisconnect());
                }else{
                    Dbug.d("BleManager", "====mReceiver=ACTION_GATT_DISCONNECTED=mBluetoothLeService"+null);
                }
            }else if (action.equals(BleManagerControlService.ACTION_GATT_CONNECT_FAILURE)) {
                Dbug.d("BleManager", "====mReceiver=ACTION_GATT_CONNECT_FAILURE=");
                for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                    mListOnBleManagerListener.get(i).connectionFailed(false);
                }
                Dbug.d("==BleManager==", "连接失败==");
            } else if (action.equals(ACTION_GATT_CONNECT_FAILURE_TIME_OUT)) {
                Dbug.d("BleManager", "====mReceiver=ACTION_GATT_CONNECT_FAILURE_TIME_OUT=");
                for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                    mListOnBleManagerListener.get(i).connectionFailed(true);
                }
                Dbug.d("==BleManager==", "连接超时==");
            } else if (action.equals(BleManagerControlService.ACTION_DATA_AVAILABLE)) {
                Dbug.d("==BleManager==", "收到数据==");
                byte[] mData = intent.getByteArrayExtra(BleManagerControlService.EXTRA_DATA);
                dealWithReturnData(mData);
            }else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Dbug.d("==BleManager==", "蓝牙连接==");
            }else if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
                Dbug.d("==BleManager==", "蓝牙被动断开连接=比如距离走远=");
                if (mBluetoothLeService!=null){
                    for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                        mListOnBleManagerListener.get(i).disconnect(mBluetoothLeService.isHandDisconnect());
                    }
                    Dbug.d("==BleManager==", "是否手动断开:" + mBluetoothLeService.isHandDisconnect());
                }else{
                    Dbug.d("BleManager", "====mReceiver=ACTION_GATT_DISCONNECTED=mBluetoothLeService"+null);
                }
            }
        }
    };


    /**
     * 解密蓝牙发回的数据包
     * 同时根据返回的数据头区分是哪个命令的返回,用于取消命令超时检测
     *
     * @param mData
     */
    private void dealWithReturnData(byte[] mData) {
        if (mData != null && mData.length > 0) {
            //解密数据
            String result = TypeConvert.bytesToHexString(mData);
            for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                mListOnBleManagerListener.get(i).getData(result);
            }
            //判断返回的是何种数据,并取消相应的超时检测
            judgeWetherCancelCmdTimeOut(result);
        }else{
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("data1", "null");
            intent.putExtra("data2","null");
            intent.setAction("com.scott.sayhi2");
            mActivity.sendBroadcast(intent);
        }
    }

    public void registerBluetooth(IntentFilter filter) {
        Log.i(TAG, "registerBluetooth()");
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BleManagerControlService.ACTION_GATT_CONNECTED);
        filter.addAction(BleManagerControlService.ACTION_GATT_DISCONNECTED);
        filter.addAction(BleManagerControlService.ACTION_GATT_CONNECT_FAILURE);
        filter.addAction(BleManagerControlService.ACTION_GATT_CONNECT_FAILURE_TIME_OUT);
        filter.addAction(BleManagerControlService.ACTION_GATT_SERVICES_DISCOVERED);
        filter.addAction(BleManagerControlService.ACTION_DATA_AVAILABLE);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);


        //蓝牙被动断开连接，比距距离远
        filter.addAction("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED");
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction("android.bluetooth.BluetoothAdapter.STATE_OFF");
        filter.addAction("android.bluetooth.BluetoothAdapter.STATE_ON");

        mActivity.registerReceiver(mReceiver, filter);
    }

    public void unregisterBluetooth() {
        //TODO 注册registerBluetooth只,而unregisterBluetooth可能多次
        try {
            mActivity.unregisterReceiver(mReceiver);
        }catch (Exception e){
            //没注册
            Dbug.d("BleManager", "Exception:"+e.toString());
        }
    }

    public BluetoothAdapter.LeScanCallback mLeScanCallback;


    public boolean searchDevice(boolean shouldReSearch) {
        if (shouldReSearch){
            searchDevice();
        }else{
            cancelDiscovery();
        }
        return true;
    }

    public boolean searchDevice() {
        mDevices = null;
        if (isScanning) {
            return false;
        }
        for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
            mListOnBleManagerListener.get(i).startScanDevice();
        }
        ishavedevice = false;

        //已配对设备
//        if (getBondDevices() != null) {
//            if (getBondDevices().size() > 0) {
//                List<BluetoothDevice> mlist = getBondDevices();
//                for (int i = 0; i < mlist.size(); i++) {
//                    BluetoothDevice mdevice = mlist.get(i);
//                    if (!checkAddressExist(mdevice)) {
//                        if (mDevices == null) {
//                            mDevices = new ArrayList<BluetoothDevice>();
//                        }
//                        mDevices.add(mdevice);
//
//                        getDevice(mdevice);
//                    }
//                }
//            }
//        }

        if (BleConfig.getInstance().getServiceUuids() != null) {
            mBtAdapter.startLeScan(BleConfig.getInstance().getServiceUuids(), mLeScanCallback);
        } else {
            mBtAdapter.startLeScan(mLeScanCallback);
        }

        getHandler().postDelayed(temRunnable, BleConfig.getInstance().getScanTimeout());
        isScanning = true;
        return true;
    }

    public int scanLeDevice(final boolean enable) {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            if (enable) {
                // Stops scanning after a pre-defined scan period.
                disconnect();//为了防止外部调用没有断开连接,手动调用一次,不然没断开扫描不到设备
                getHandler().postDelayed(tempCanselScaneRunnable, BleConfig.getInstance().getScanTimeout());
                searchDevice();
            } else {
                cancelDiscovery();
            }
            return 1;
        }
    }

    public void startDiscovery(){
        if(mBtAdapter!=null){
            mBtAdapter.startDiscovery();
        }
    }

    public boolean cancelDiscovery() {
        mHandler.removeCallbacks(tempCanselScaneRunnable);
        mHandler.removeCallbacks(temRunnable);
        isScanning = false;
        mBtAdapter.stopLeScan(mLeScanCallback);
        for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
            mListOnBleManagerListener.get(i).endScanDevice();
        }
        return true;
    }


    public BluetoothAdapter getBluetoothAdapter() {
        return ((BluetoothManager) getContext().getSystemService(
                Context.BLUETOOTH_SERVICE)).getAdapter();
    }


    public boolean checkAddressExist(BluetoothDevice paramDevice) {
        if (mDevices == null) {
            return false;
        }

        for (BluetoothDevice device : mDevices) {
            if (device.getAddress().equals(paramDevice.getAddress())) {
                return true;
            }
        }
        return false;
    }

    public List<BluetoothDevice> getBondDevices() {
        Set<BluetoothDevice> set = mBtAdapter.getBondedDevices();
        List<BluetoothDevice> bondDevices = new ArrayList<BluetoothDevice>();

        if (set.size() == 0) {
            return null;
        }

        Iterator<BluetoothDevice> i = set.iterator();
        while (i.hasNext()) {
            bondDevices.add(i.next());
        }

        return bondDevices;
    }

    public int getDevice(BluetoothDevice mDevice) {
        if (VersionTool.isUnder4_3()) {
            return -1;
        } else {
            if (mDevice != null) {
                Dbug.d("BleManager==getDevice", "搜索到绑定的设备,开始过滤");
                if (TextUtils.isEmpty(BleConfig.getInstance().getmDeviceMac()) && (null == BleConfig.getInstance().getmDeviceNames() || BleConfig.getInstance().getmDeviceNames().length < 1)) {
                    ishavedevice = true;
                    for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                        mListOnBleManagerListener.get(i).searchBleDevice(mDevice);
                    }
                    return 1;
                }
                if (!TextUtils.isEmpty(BleConfig.getInstance().getmDeviceMac())) {
                    //根据指定的mac地址过滤
                    if (BleConfig.getInstance().getmDeviceMac().equals(mDevice.getAddress())) {
                        ishavedevice = true;
                        for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                            mListOnBleManagerListener.get(i).searchBleDevice(mDevice);
                        }
                    }
                    return 1;
                }

                if (BleConfig.getInstance().getmDeviceNames() != null && BleConfig.getInstance().getmDeviceNames().length > 0) {
                    //根据指定的名字地址过滤

                    //模糊匹配
                    for (String name : BleConfig.getInstance().getmDeviceNames()) {
                        if (BleConfig.getInstance().ismFuzzy()){
                            if (name.contains(mDevice.getName())) {
                                ishavedevice = true;
                                for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                                    mListOnBleManagerListener.get(i).searchBleDevice(mDevice);
                                }
                            }
                        }else{
                            if (name.equalsIgnoreCase(mDevice.getName())) {
                                ishavedevice = true;
                                for (int i = 0; i < mListOnBleManagerListener.size(); i++) {
                                    mListOnBleManagerListener.get(i).searchBleDevice(mDevice);
                                }
                            }
                        }
                        return 1;
                    }
                }
            }
            return 1;
        }
    }


    public void addOnBleManagerListener(OnBleManagerListener mOnBleManagerListener) {
        if (mListOnBleManagerListener != null && !mListOnBleManagerListener.contains(mOnBleManagerListener)) {
            mListOnBleManagerListener.add(mOnBleManagerListener);
        }
    }

    public void removeOnBleManagerListener(OnBleManagerListener mOnBleManagerListener) {
        if (mListOnBleManagerListener != null && mListOnBleManagerListener.contains(mOnBleManagerListener)) {
            mListOnBleManagerListener.remove(mOnBleManagerListener);
        }
    }

    public boolean cheakBluetoothOpen() {
        return (null != mBtAdapter && mBtAdapter.isEnabled() && !mBtAdapter.isDiscovering());
    }


    /**
     * 通用指令发送
     * @param hexCmd
     */
    public void sendHexStringCmd(String hexCmd) {
        write(TypeConvert.hexStringToBytes(hexCmd));
        mHandler.sendEmptyMessageDelayed(hexCmd.hashCode(), BleConfig.getInstance().getSendCmdTimeOut());
    }

    /**
     * 获取Token
     */

    public void getToken() {
        write(TypeConvert.hexStringToBytes(CmdConstants.CMD_GET_TOKEN));
        mHandler.sendEmptyMessageDelayed(MSG_GET_TOKEN, BleConfig.getInstance().getSendCmdTimeOut());
    }



    /**
     * 判断是何种命令的返回数据
     * 用于取消对应的超时检测
     *
     * @param hexString
     */
    private void judgeWetherCancelCmdTimeOut(String hexString) {
        if (hexString.startsWith("0602")) {
            //解析获取Token返回的通信帧
            mHandler.removeMessages(MSG_GET_TOKEN);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_TOKEN");
        } else if (hexString.startsWith("080101")) {
            //解析开锁返回通信帧
            mHandler.removeMessages(MSG_GET_OPEN_LOCK);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_OPEN_LOCK");
        } else if (hexString.startsWith("080102")) {
            //解析关锁返回通信帧
            mHandler.removeMessages(MSG_GET_CLOSE_LOCK);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_CLOSE_LOCK");
        } else if (hexString.startsWith("080201")) {
            //解析查询锁状态返回通信帧
            mHandler.removeMessages(MSG_GET_LOCK_STATUS);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_LOCK_STATUS");
        } else if (hexString.startsWith("080301")) {
            //解析设置借车状态通信帧
            mHandler.removeMessages(MSG_GET_TAKE_CAR_STATUS);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_TAKE_CAR_STATUS");
        } else if (hexString.startsWith("080302")) {
            //解析设置还车状态通信帧
            mHandler.removeMessages(MSG_GET_RETURN_CAR_STATUS);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_RETURN_CAR_STATUS");
        } else if (hexString.startsWith("080401")) {
            //解析查询工作模式通信帧解析
            mHandler.removeMessages(MSG_GET_CAR_WORK_MODE);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_CAR_WORK_MODE");
        } else if (hexString.startsWith("080402")) {
            //解析设置工作模式通信帧解析
            mHandler.removeMessages(MSG_GET_SET_CAR_WORK_MODE);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_SET_CAR_WORK_MODE");
        } else if (hexString.startsWith("080501")) {
            //电量通信帧解析
            mHandler.removeMessages(MSG_GET_BATTERY);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_BATTERY");
        } else if (hexString.startsWith("080601")) {
            //充电宝功能激活
            mHandler.removeMessages(MSG_GET_CHARGE_ENABLE);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_CHARGE_ENABLE");
        } else if (hexString.startsWith("080602")) {
            //充电宝功能激活关闭
            mHandler.removeMessages(MSG_GET_CHARGE_DIANABLE);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_CHARGE_DIANABLE");
        } else if (hexString.startsWith("080603")) {
            //充电宝充电开始
            mHandler.removeMessages(MSG_GET_CHARGE_START);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_CHARGE_START");
        } else if (hexString.startsWith("080604")) {
            //充电宝充电结束
            mHandler.removeMessages(MSG_GET_CHARGE_END);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_CHARGE_END");
        } else if (hexString.startsWith("080701")) {
            //获取Mac地址数据解析
            mHandler.removeMessages(MSG_GET_MAC_ADDRESS);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_MAC_ADDRESS");
        } else if (hexString.startsWith("080801")) {
            //获取imei数据解析
            mHandler.removeMessages(MSG_GET_IMEI);
            Dbug.d("", "judgeWetherCancelCmdTimeOut==取消CMD超时检测==MSG_GET_IMEI");
        }
    }

}

