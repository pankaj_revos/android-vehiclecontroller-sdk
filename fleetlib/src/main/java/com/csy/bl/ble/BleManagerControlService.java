package com.csy.bl.ble;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.csy.bl.ble.common.utils.BleConfig;
import com.csy.bl.ble.common.utils.ConnectState;
import com.csy.bl.ble.common.utils.Dbug;
import com.csy.bl.ble.common.utils.TypeConvert;
import com.csy.bl.ble.menggou.utils.CmdUtls;
import com.csy.bl.ble.menggou.utils.Constants;

import java.util.List;
import java.util.UUID;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */
@SuppressLint("NewApi")
public class BleManagerControlService extends Service {

	private final static String TAG = Dbug.getHeadStr();
	private UUID uuid;
	private UUID UUID_READ ;
	private UUID UUID_WRITE ;
	private BluetoothGattService myGattService;
	private BluetoothManager mBluetoothManager;
	private BluetoothAdapter mBluetoothAdapter;
	private String mBluetoothDeviceAddress;
	private BluetoothGatt mBluetoothGatt;
	private BluetoothGattCharacteristic readGattCharacteristic;
	private BluetoothGattCharacteristic writeGattCharacteristic;
	public final static String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_CONNECTING = "com.example.bluetooth.le.ACTION_GATT_CONNECTING";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
	public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
	public final static String ACTION_GATT_CONNECT_FAILURE = "ACTION_GATT_CONNECT_FAILURE";
	public final static String ACTION_GATT_CONNECT_FAILURE_TIME_OUT = "ACTION_GATT_CONNECT_FAILURE_TIME_OUT";

	//是否主动断开连接 在BleManagerControlService中设置
	public boolean isHandDisconnect = false;
	public boolean isHandDisconnect() {
		return isHandDisconnect;
	}
	public void setHandDisconnect(boolean handDisconnect) {
		isHandDisconnect = handDisconnect;
	}
	//当前连接设备
	private BluetoothDevice device;
	//当前重试次数
	private int connectRetryCount = 0;
	private final static int MSG_CONNECT_TIME_OUT = -11;
	private final static int MSG_CONNECT_RETRY = -12;
	private ConnectState connectState = ConnectState.CONNECT_INIT;//蓝牙当前连接状态
	private Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == MSG_CONNECT_TIME_OUT){
				Dbug.d("BleManagerControlService:","连接超时啦");
				close();
				connecTionFail(true);
			}else if (msg.what == MSG_CONNECT_RETRY){
				connectRetryCount ++;
				Dbug.d("BleManagerControlService:","重连:"+connectRetryCount);
				reConnect();
			}
		}
	};
	/*
	 * public static final UUID SERVIE_UUID = UUID
	 * .fromString("0000FFF0-0000-1000-8000-00805f9b34fb");
	 */
	/*
	 * public static final UUID SERVIE_UUID = UUID
	 * .fromString("0000fc00-0000-1000-8000-00805f9b34fb"); public static final
	 * UUID RED_LIGHT_CONTROL_UUID = UUID
	 * .fromString("0000FFF4-0000-1000-8000-00805f9b34fb"); public static final
	 * UUID RED_LIGHT_CONTROL_UUID_TWO = UUID
	 * .fromString("0000FFF1-0000-1000-8000-00805f9b34fb");
	 */
	public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
			.fromString(BleSampleGattAttributes.HEART_RATE_MEASUREMENT);

	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			Dbug.d("onConnectionStateChange", "====status:"+status+"==newState:"+newState);
			String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				connectState = ConnectState.CONNECT_SUCCESS;
				intentAction = ACTION_GATT_CONNECTED;
				broadcastUpdate(intentAction);
				//发现服务之后会取消连接超时检测
				mBluetoothGatt.discoverServices();
				Dbug.d("BleManagerControlService", "====mGattCallback=STATE_CONNECTED==start discoverServices");
			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				//关闭GATT
				close();
				if (mHandler!=null){
					mHandler.removeCallbacksAndMessages(null);
					Dbug.d("onConnectionStateChange", "===移除全部消息===");
				}
				if (status == BluetoothGatt.GATT_SUCCESS) {
					connectState = ConnectState.CONNECT_DISCONNECT;
					intentAction = ACTION_GATT_DISCONNECTED;
					broadcastUpdate(intentAction);
					Dbug.d("BleManagerControlService", "====mGattCallback=ACTION_GATT_DISCONNECTED==status:"+status);
				} else {
					connectState = ConnectState.CONNECT_FAILURE;
					Dbug.d("BleManagerControlService", "====mGattCallback=ACTION_GATT_CONNECT_FAILURE==status:"+status);
					connecTionFail(false);
				}
			}else if (newState == BluetoothGatt.STATE_CONNECTING) {
				connectState = ConnectState.CONNECT_ING;
				intentAction = ACTION_GATT_CONNECTING;
				broadcastUpdate(intentAction);
				Dbug.d("BleManagerControlService", "====mGattCallback=STATE_CONNECTING==status:"+status);
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {

			Dbug.d("onServicesDiscovered", "======status:"+status);

			if (mHandler!=null){
				mHandler.removeMessages(MSG_CONNECT_TIME_OUT);
				Dbug.d("onServicesDiscovered", "===发现服务===移除连接超时检测==");
			}
			if (status == 0) {
				connectState = ConnectState.CONNECT_SUCCESS;
				isHandDisconnect = false;
				if (mBluetoothGatt != null) {
					myGattService = mBluetoothGatt.getService(uuid);
					Dbug.d("BleManagerControlService", "====mBluetoothGatt.getService(uuid)="+uuid.toString());
				}
				if (myGattService != null) {
					readGattCharacteristic = myGattService
							.getCharacteristic(UUID_READ);

					writeGattCharacteristic = myGattService
							.getCharacteristic(UUID_WRITE);
					Dbug.e("BleManagerControlService", "====getCharacteristic(UUID_READ)="+UUID_READ.toString());
					Dbug.e("BleManagerControlService", "====getCharacteristic(UUID_WRITE)="+UUID_WRITE.toString());

				}else{
					Dbug.d("BleManagerControlService", "====服务为空");
				}
				if (readGattCharacteristic != null
						&& writeGattCharacteristic != null) {
				/*
				 * setCharacteristicNotification( writeGattCharacteristic,
				 * true);
				 */
					setCharacteristicNotification(readGattCharacteristic, true);
					Dbug.d("BleManagerControlService", "====setCharacteristicNotification=");
				}
			}else{
				connectState = ConnectState.CONNECT_FAILURE;
				//连接失败
				connecTionFail(false);
			}

		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			Log.i(TAG, "onCharacteristicRead");
			Dbug.e("onServicesDiscovered", "======onCharacteristicRead 特征值变化:"+status);
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
				Dbug.d("BleControlerActivity==onCharacteristicRead", "==读取成功");
			}else{
				Dbug.d("BleControlerActivity==onCharacteristicRead", "==读取失败");
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			Dbug.e("onServicesDiscovered", "======onCharacteristicChanged 特征值变化:"
					+TypeConvert.bytesToHexString(characteristic.getValue()));
			broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
			Dbug.d("BleControlerActivity==onCharacteristicChanged", "特征值发生变化");
		}
		
		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
			Dbug.e("onServicesDiscovered", "======onCharacteristicWrite 特征值变化:"
			+characteristic.getUuid()+"=="+TypeConvert.bytesToHexString(characteristic.getValue()));

			if (status == BluetoothGatt.GATT_SUCCESS){
				Dbug.d("BleControlerActivity==onCharacteristicWrite", "==写入成功");
			}else{
				Dbug.d("BleControlerActivity==onCharacteristicWrite", "==写入失败");
			}
		}
	};

	/**
	 * 处理连接失败
	 * @param timeOut
	 */
	private void connecTionFail(boolean timeOut) {
		if (connectRetryCount < BleConfig.getInstance().getConnectRetryCount() || BleConfig.getInstance().isConnectRetryAllTime()) {
			//重连
			mHandler.removeMessages(MSG_CONNECT_TIME_OUT);
			mHandler.sendEmptyMessageDelayed(MSG_CONNECT_RETRY,BleConfig.getInstance().getConnectRetryInterval());
		}else{
			//重连达到最大次数,重置重连
			connectRetryCount = 0;
			mHandler.removeMessages(MSG_CONNECT_TIME_OUT);
			mHandler.removeMessages(MSG_CONNECT_RETRY);
			if (timeOut){
				String intentAction = ACTION_GATT_CONNECT_FAILURE_TIME_OUT;
				broadcastUpdate(intentAction);
			}else{
				String intentAction = ACTION_GATT_CONNECT_FAILURE;
				broadcastUpdate(intentAction);
			}
		}

	}

	private void broadcastUpdate(final String action) {
		final Intent intent = new Intent(action);
		sendBroadcast(intent);
	}

	private void broadcastUpdate(final String action,
			final BluetoothGattCharacteristic characteristic) {
		final Intent intent = new Intent(action);
		if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
			int flag = characteristic.getProperties();
			int format = -1;
			if ((flag & 0x01) != 0) {
				format = BluetoothGattCharacteristic.FORMAT_UINT16;
				Log.d(TAG, "Heart rate format UINT16.");
			} else {
				format = BluetoothGattCharacteristic.FORMAT_UINT8;
				Log.d(TAG, "Heart rate format UINT8.");
			}
			final int heartRate = characteristic.getIntValue(format, 1);
			Log.d(TAG, String.format("Received heart rate: %d", heartRate));
			intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
		} else if (action.equals(ACTION_DATA_AVAILABLE)) {
			// For all other profiles, writes the data formatted in HEX.
			final byte[] data = characteristic.getValue();
			if (data != null && data.length > 0) {
				/*
				 * final StringBuilder stringBuilder = new
				 * StringBuilder(data.length); for(byte byteChar : data)
				 * stringBuilder.append(String.format("%02X ", byteChar));
				 * intent.putExtra(EXTRA_DATA, new String(data) + "\n" +
				 * stringBuilder.toString());
				 */
				intent.putExtra(EXTRA_DATA, data);
				Dbug.d("BleManagerControlService", "====ACTION_DATA_AVAILABLE==收到数据广播出去=="+ TypeConvert.bytesToHexString(data));
				//先判断有没有错误代码
			}
			Dbug.d("BleManagerControlService", "====ACTION_DATA_AVAILABLE=data="+data);
		}
		sendBroadcast(intent);
	}

	public class LocalBinder extends Binder {
		public BleManagerControlService getService() {
			return BleManagerControlService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		// After using a given device, you should make sure that
		// BluetoothGatt.close() is called
		// such that resources are cleaned up properly. In this particular
		// example, close() is
		// invoked when the UI is disconnected from the Service.
		close();
		return super.onUnbind(intent);
	}

	private final IBinder mBinder = new LocalBinder();

	/**
	 * Initializes a reference to the local Bluetooth adapter.
	 *
	 * @return Return true if the initialization is successful.
	 */
	@SuppressLint("NewApi")
	public boolean initialize() {
		// For API level 18 and above, get a reference to BluetoothAdapter
		// through
		// BluetoothManager.
		if (mBluetoothManager == null) {
			mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null) {
				Log.e(TAG, "Unable to initialize BluetoothManager.");
				return false;
			}
		}

		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null) {
			Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
			return false;
		}

		return true;
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 *
	 * @param address
	 *            The device address of the destination device.
	 *
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */

	public boolean connect(final String address) {
		uuid = BleConfig.getInstance().getSERVICE_UUID();
		UUID_READ = BleConfig.getInstance().getUUID_READ();
		UUID_WRITE = BleConfig.getInstance().getUUID_WRITE();

		connectState = ConnectState.CONNECT_ING;
		if (mHandler != null) {
			mHandler.removeCallbacksAndMessages(null);
			//启动连接超时检测
			mHandler.sendEmptyMessageDelayed(MSG_CONNECT_TIME_OUT, BleConfig.getInstance().getConnectTimeOut());
		}

		if (mBluetoothAdapter == null || address == null) {
			Log.w(TAG,
					"BluetoothAdapter not initialized or unspecified address.");
			Dbug.d("BleManager", "BluetoothAdapter not initialized or unspecified address.");

			return false;
		}

		// Previously connected device. Try to reconnect.
		if (mBluetoothDeviceAddress != null
				&& address.equals(mBluetoothDeviceAddress)
				&& mBluetoothGatt != null) {
			if (mBluetoothGatt.connect()) {
				Dbug.d("BleManager", "Trying to use an existing mBluetoothGatt for connection.==true==");
				return true;
			} else {
				Dbug.d("BleManager", "Trying to use an existing mBluetoothGatt for connection.==false==");
				return false;
			}
		}

		device = mBluetoothAdapter.getRemoteDevice(address);
		if (device == null) {
			Log.w(TAG, "Device not found.  Unable to connect.  "+address);
			Log.d(TAG,
					"Device not found.  Unable to connect..  "+address);
			Dbug.d("BleManager", "====device == null=="+address);
			return false;
		}
		// We want to directly connect to the device, so we are setting the
		// autoConnect
		// parameter to false.
		mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
		Log.d(TAG, "Trying to create a new connection.");
		mBluetoothDeviceAddress = address;
		System.out.println("device.getBondState==" + device.getBondState());
		return true;
	}

	public ConnectState getConnectState() {
		return connectState;
	}

	public void setConnectState(ConnectState connectState) {
		this.connectState = connectState;
	}

	//用于内部重连
	private boolean reConnect() {
		connectState = ConnectState.CONNECT_ING;
		if (mHandler != null) {
			mHandler.removeMessages(MSG_CONNECT_TIME_OUT);
			//启动连接超时检测
			mHandler.sendEmptyMessageDelayed(MSG_CONNECT_TIME_OUT, BleConfig.getInstance().getConnectTimeOut());
		}
		if (mBluetoothAdapter == null || device == null) {
			Log.w(TAG,
					"BluetoothAdapter not initialized or unspecified address.");
			Dbug.d("BleManager", "BluetoothAdapter not initialized or unspecified address.");

			return false;
		}

		// Previously connected device. Try to reconnect.
		if (mBluetoothDeviceAddress != null
				&& device.getAddress().equals(mBluetoothDeviceAddress)
				&& mBluetoothGatt != null) {
			if (mBluetoothGatt.connect()) {
				Dbug.d("BleManager", "Trying to use an existing mBluetoothGatt for connection.==true==");
				return true;
			} else {
				Dbug.d("BleManager", "Trying to use an existing mBluetoothGatt for connection.==false==");
				return false;
			}
		}

		device = mBluetoothAdapter
				.getRemoteDevice(device.getAddress());
		if (device == null) {
			Log.w(TAG, "Device not found.  Unable to connect.");
			Log.d(TAG,
					"Device not found.  Unable to connect.");
			Dbug.d("BleManager", "====device == null==");

			return false;
		}
		// We want to directly connect to the device, so we are setting the
		// autoConnect
		// parameter to false.
		mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
		Log.d(TAG, "Trying to create a new connection.");
		mBluetoothDeviceAddress = device.getAddress();
		System.out.println("device.getBondState==" + device.getBondState());

		return true;
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect() {
		connectState = ConnectState.CONNECT_INIT;
		if (mHandler != null) {
			mHandler.removeCallbacksAndMessages(null);
		}
		isHandDisconnect = true;
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.disconnect();
		resetStatus();
	}

	private void resetStatus() {
		Dbug.e("","===resetStatus 重置状态");
		//移除所有消息
		connectRetryCount = 0;
		mHandler.removeCallbacksAndMessages(null);
	}

	/**
	 * After using a given BLE device, the app must call this method to ensure
	 * resources are released properly.
	 */
	public void close() {
		if (mBluetoothGatt == null) {
			return;
		}
		mBluetoothGatt.close();

		mBluetoothGatt = null;
	}

	/**
	 * Request a read on a given {@code BluetoothGattCharacteristic}. The read
	 * result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
	 * callback.
	 *
	 * @param characteristic
	 *            The characteristic to read from.
	 */
	public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.readCharacteristic(characteristic);
	}

	/**
	 * Enables or disables notification on a give characteristic.
	 *
	 * @param characteristic
	 *            Characteristic to act on.
	 * @param enabled
	 *            If true, enable notification. False otherwise.
	 */
	public void setCharacteristicNotification(
			BluetoothGattCharacteristic characteristic, boolean enabled) {

		Dbug.e("BleManagerControlService", "==UUID_READ=="+UUID_READ.toString());
		Dbug.e("BleManagerControlService", "==UUID_WRITE=="+UUID_WRITE.toString());
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

		// This is specific to Heart Rate Measurement.
//		if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
//			BluetoothGattDescriptor descriptor = characteristic
//					.getDescriptor(UUID
//							.fromString(BleSampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
//			descriptor
//					.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//			mBluetoothGatt.writeDescriptor(descriptor);
//		} else
			if (UUID_READ.equals(characteristic.getUuid())) {
			BluetoothGattDescriptor descriptor = characteristic
					.getDescriptor(UUID
							.fromString(BleSampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
			descriptor
					.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
			mBluetoothGatt.writeDescriptor(descriptor);
		} else if (UUID_WRITE.equals(characteristic.getUuid())) {
			BluetoothGattDescriptor descriptor = characteristic
					.getDescriptor(UUID
							.fromString(BleSampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
			descriptor
					.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
			mBluetoothGatt.writeDescriptor(descriptor);
		}
	}

	/**
	 * Retrieves a list of supported GATT services on the connected device. This
	 * should be invoked only after {@code BluetoothGatt#discoverServices()}
	 * completes successfully.
	 *
	 * @return A {@code List} of supported services.
	 */
	public List<BluetoothGattService> getSupportedGattServices() {
		if (mBluetoothGatt == null){
			return null;
		}
		return mBluetoothGatt.getServices();
	}

	public void writeStringToGatt(byte[] data) {
		if (data !=null && data.length != 0 && data.length < 20) {
			try {
				Dbug.d("BleManagerControlService", "==writeStringToGatt==try==");
				if (myGattService != null) {
					Dbug.d("BleManagerControlService", "==writeStringToGatt==try=myGattService != null=执行写入");
					if (writeGattCharacteristic==null) {
						writeGattCharacteristic = myGattService
								.getCharacteristic(UUID_WRITE);
					}
					writeGattCharacteristic.setValue(data);
					mBluetoothGatt.writeCharacteristic(writeGattCharacteristic);
					//写值,有时候没写入成功,给个失败回调
				}else{
					Dbug.d("BleManagerControlService", "==writeStringToGatt==try=myGattService == null=执行写入失败");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				Dbug.d("BleManagerControlService", "==writeStringToGatt==执行写入失败 catch= ="+e);
			}
		} else {
			Dbug.d("BleManagerControlService", "==writeStringToGatt==else= =return==执行写入失败 "+data);
			return;
		}
	}




}
