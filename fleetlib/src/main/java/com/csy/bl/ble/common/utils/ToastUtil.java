//package com.csy.bl.ble.common.utils;
//
//import android.content.Context;
//import android.widget.Toast;
//
///**
// * Created by chenshouyin on 2017/11/16.
// * Email:shouyinchen@gmail.com
// */
//
//public class ToastUtil {
//    private static Toast toast;
//
//    public static void showToast(Context ctx, String msg, int duration){
//        if(toast==null){
//            toast= Toast.makeText(ctx, msg, duration);
//        }
//        toast.setDuration(duration);
//        toast.setText(msg);
//        toast.show();
//    }
//
//    public static void showToast(Context ctx, int resId, int duration){
//        if(toast==null){
//            toast= Toast.makeText(ctx, resId, duration);
//        }
//        toast.setDuration(duration);
//        toast.setText(resId);
//        toast.show();
//    }
//
//    public static void showToast(Context ctx, String msg){
//        showToast(ctx, msg,1000);
//    }
//
//    public static void showToast(Context ctx, int resId){
//        showToast(ctx, resId,1000);
//    }
//}
//
