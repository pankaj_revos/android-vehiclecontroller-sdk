package com.revos.android.fleetsdk

import android.app.Application
import com.revos.android.fleet.FleetSDK
import timber.log.Timber

class SdkApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        FleetSDK.initialize(this)
    }
}