package com.revos.android.fleetsdk

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.revos.android.fleet.FleetSDK
import com.revos.android.fleet.FleetUser
import com.revos.android.fleet.data.Booking
import com.revos.android.fleet.data.Device
import com.revos.android.fleet.data.Direction
import com.revos.android.fleet.data.TripDataFeed
import com.revos.android.fleet.managers.LeaseManager
import com.revos.android.fleet.managers.LoginManager
import org.joda.time.DateTime
import timber.log.Timber
import java.util.*
import com.revos.android.fleet.data.v2.RentalVehicle
import com.revos.android.fleet.managers.VehicleManager
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    val leaseManager = FleetSDK.getLeaseManager()
    val loginManager = FleetSDK.getLoginManager()
    val vehicleManager = FleetSDK.getVehicleManager()
    lateinit var statusText : AppCompatTextView
    private lateinit var fleetuser : FleetUser.Builder
    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val loginManager = FleetSDK.getLoginManager()
        fleetuser = FleetUser.Builder(this,"39efcnegzzzegc", "pankaj_test_open")
        fleetuser.setEmail("pankaj@revos.in")
        fleetuser.setPhoneNumber("+919019818118")

        loginManager.setCallback(loginCallback)
        //loginManager.login(fleetuser.build())
        val button = findViewById<AppCompatButton>(R.id.startEndBooking)

        button.setOnClickListener {
            vehicleManager.connectUsingVin("color4_batch3", this)
        }
        statusText = findViewById<AppCompatTextView>(R.id.statusTextView)

        val createBooking = findViewById<AppCompatButton>(R.id.createBooking)
        createBooking.setOnClickListener {
            //leaseManager.getRentalVehicle(fleetuser.build(), "TEST_TB1")
           // loginManager.login(fleetuser.build())
           vehicleManager.getMyVehicles(this)
        }
       // leaseManager.getBooking(fleetuser.build(), "61e545b59a0a0700149937ee")
    }

    override fun onStart() {
        super.onStart()
        leaseManager.addCallback(leaseCallback)
        vehicleManager.addCallback(vehicleManagerCallback)
    }

    fun createBooking(){
        val cal = Calendar.getInstance()
        cal.timeInMillis = System.currentTimeMillis()

        val currTime = DateTime(System.currentTimeMillis())
        val bookingTime = currTime.toDate()
        val startTime = currTime.plusMinutes(3)
        val endTime = startTime.plusHours(1)

        val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")
        Timber.e("start time " + sdf.format(startTime.toDate()))
        Timber.e("end time " + sdf.format(endTime.toDate()))

        val booking = Booking(bookingTime, startTime.toDate(), endTime.toDate(),
            currTime.toDate(), Booking.TYPE.SHORT_TERM, "Short Booking",
            "TEST_TB1", id = null, 2f)

        booking.priceInfo = "5fcf5d220274390009544380"
        leaseManager.createBooking(fleetuser.build(), booking,this@MainActivity)
    }

    private val vehicleManagerCallback = object : VehicleManager.Callback {

        override fun onConnectionStatusUpdate(status: VehicleManager.ConnectionStatus) {
            Timber.e("onConnectionStatusUpdate ${status.name}")
            if(status == VehicleManager.ConnectionStatus.Connected){
                val battery = vehicleManager.getBatteryStatus(this@MainActivity)
                Timber.e("Battery $battery")
                val vehicle = vehicleManager.getConnectedVehicle(this@MainActivity)
                Timber.e("Vehicle details ${vehicle?.model?.name}")
                val latLng = vehicleManager.getVehicleLastKnownLocation(this@MainActivity)
                Timber.e("Vehicle LatLng ${latLng[0]} ${latLng[1]}")
                val speed = vehicleManager.getSpeed(this@MainActivity)
                Timber.e("Vehicle Speed $speed")

                vehicleManager.getTripsForVehicle("color4_batch3")
            }
        }

        override fun onVehicleAvailable(devices: ArrayList<Device>) {
            Timber.e("onVehicleAvailable ${devices.size}")
            for(device in devices){
                Timber.e("Device ${device.vehicle?.vin}")
            }
        }

        override fun onTripDetailsAvailable(tripDetails: List<TripDataFeed>) {
            Timber.e("Total trips made ${tripDetails.size}")
        }

        override fun onNavigation(direction: Direction) {

            val status = StringBuilder().append("Directions : \n")
            status.append("currentDestination ${direction.currentDestination} \n")
            status.append("distanceFromTurn ${direction.distanceFromTurn} \n")
            status.append("expectedTimeToArrive ${direction.expectedTimeToArrive} \n")
            status.append("remainingTime ${direction.remainingTime} \n")
            status.append("remainingDistance ${direction.remainingDistance} \n")

            statusText.text = status.toString()

            val imageView = findViewById<AppCompatImageView>(R.id.directionsImg)
            if(direction.turnIconBitmap != null){
                imageView.setImageBitmap(direction.turnIconBitmap)
            }
        }
    }

    private val loginCallback = object : LoginManager.Callback {
        override fun onLoginSuccessful() {
            Timber.e("onLoginSuccessful")
        }

        override fun onRegistrationSuccessful() {

        }

        override fun onLoginFailed(message: String) {

        }

        override fun onRegistrationFailed(message: String) {

        }
    }

    var currentBookingObj : Booking? = null
    var pendingOrder : String? = null
    private val leaseCallback = object : LeaseManager.Callback {

        override fun onBookingCreated(booking: Booking) {
            Timber.e( "onBookingCreated ${booking.id}")
            val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS")
            Timber.e("onBookingCreated: start time " + sdf.format(booking.startTime))
            Timber.e("onBookingCreated: end time " + sdf.format(booking.endTime))
            currentBookingObj = booking
        }

        override fun onPaymentInitialised(orderId: String, paymentLink: String) {
            Timber.e( "onPaymentInitialised $orderId $paymentLink" )
            pendingOrder = orderId
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(paymentLink)
            startActivity(intent)
        }

        override fun onBookingSuccessful(booking: Booking) {
            currentBookingObj = booking
            Timber.e("onBookingSuccessful" )
        }

        override fun onBookingsAvailable(bookings: List<Booking>) {
            Timber.e(" ${bookings.size}")
        }


        override fun onError(message: String?) {
            Timber.e(" onError $message")
        }

        override fun onBookingAvailable(booking: Booking) {
            Timber.e(" onBookingAvailable ${booking.id}")
            currentBookingObj = booking
            //leaseManager.startBooking(fleetuser.build(), booking.id!!)
            leaseManager.endBooking(fleetuser.build(), booking.vin, booking.id!!)
        }

        override fun onVehicleAvailable(rentalVehicle: RentalVehicle) {
            Timber.e("${rentalVehicle.vin} ${rentalVehicle.pricingInfo?.size}")
        }
    }

    override fun onResume() {
        super.onResume()
        if(currentBookingObj != null && pendingOrder != null ){
            leaseManager.getPaymentStatus(fleetuser.build(), pendingOrder!!, currentBookingObj!!)
        }
    }

    override fun onStop() {
        super.onStop()
        leaseManager.removeCallback(leaseCallback)
        vehicleManager.removeCallback(vehicleManagerCallback)
    }
}